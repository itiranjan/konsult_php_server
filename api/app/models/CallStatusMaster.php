<?php

class CallStatusMaster extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'call_status_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'call_status_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    public function getCommunicationTypeId($params = array()) {

        if (empty($params['name'])) {
            return 0;
        }
        
        $params['name'] = strtolower($params['name']);
        
        if($params['name'] == 'not connected' || $params['name'] == 'notconnected') {
            return 7;
        }

        $call_status_id = CallStatusMaster::where('name', '=', $params['name'])->pluck('call_status_id');

        return $call_status_id;
    }

}
