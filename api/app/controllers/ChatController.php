<?php

class ChatController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool $checkAuthToken (To Check Auth token)
     * @return  array 
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 28 apr 2015
     * @description : This method is used for send friend request to doctor 
     * @access public
     * @return  array  
     */
    public function friendRequest() {
        try {
            $checkAuthToken = True;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $flag = 0;
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', 1);
            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);
            $data['patient'] = $user_id;
            $user = new User();
            if ($user->validateCallRequest($data)) {
                $rel_exist = DoctorPatientRelation::whereRaw('(doctor = ' . $data['doctor'] . ' and patient = ' . $user_id . ') or (patient = ' . $data['doctor'] . ' and doctor = ' . $user_id . ')')->first();

                if ($rel_exist) {
                    $result['status'] = 0;
                    if (!empty($rel_exist['blocked_by'])) {
                        if ($user_id == $rel_exist['blocked_by']) {
                            $result['msg'] = Config::get('config_msg.USER_BLOCK_BY_U');
                        } else {
                            $result['msg'] = Config::get('config_msg.USER_BLOCK_U');
                        }
                    } else {
                        if ($rel_exist['is_approved'] == 1) {
                            $result['msg'] = Config::get('config_msg.BOTH_FRI');
                        } else if ($rel_exist['is_approved'] == 2) {
                            $result['msg'] = Config::get('config_msg.U_ALREADY_SENT_REQ');
                        } else {
                            $flag = 1;
                        }
                    }
                } else {
                    $flag = 1;
                }

                if ($flag == 1) {
                    if (!empty($rel_exist['doctor_patient_id'])) {
                        DoctorPatientRelation::where('doctor_patient_id', '=', $rel_exist['doctor_patient_id'])->update(array('is_approved' => 2));
                    } else {
                        $doctorPatientRelation = new DoctorPatientRelation();
                        $doctorPatientRelation->doctor = $data['doctor'];
                        $doctorPatientRelation->patient = $user_id;
                        $doctorPatientRelation->is_approved = 2;
                        $doctorPatientRelation->save();
                    }
                    $communicationHelper = $this->helper->load('communication');
                    $user_app = DeviceInfo::where('user_id', '=', $data['doctor'])->first();
                    $user = User::where('user_id', '=', $data['patient'])->first();
                    $user_detail = UserProfile::where('user_id', '=', $data['patient'])->first();
                    $photo = User::getPhoto(array('photo' => $user_detail['photo'], 'is_doctor' => $user_detail['is_doctor']));

                    if ($user_detail)
                        $GCMData = array();
                    $GCMData['from_user_id'] = $user['user_id'];
                    $GCMData['from_user_name'] = trim($user['salutation'] . " " . $user['name']);
                    $GCMData['from_user_photo'] = $photo;
                    $GCMData['msg_id'] = '';
                    $GCMData['msg_time'] = time();
                    $GCMData['msg_text'] = Config::get('config_msg.U_RECEIVED_NEW_FRI_REQ') . trim($user['salutation'] . " " . $user['name']);
                    $GCMData['type'] = 'friend_request';

                    if ($user_app['platform_id'] == 1) {
                        $params['GCMDeviceIds'] = $user_app['registartion_id'];
                        $params['msg'] = json_encode($GCMData);
                        $result['response'] = $communicationHelper->gcmCommunication($params);
                    } else if ($user_app['platform_id'] == 2) {
                        $params['APNSDeviceIds'] = $user_app['registartion_id'];
                        $params['push_msg'] = $GCMData;
                        $result['response'] = $communicationHelper->pushApnsCommunication($params);
                    }

                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.FRI_REQ');
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FRI_REQ_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 28 apr 2015
     * @description : This method is used for received friend request response
     * @access public
     * @return  array  
     */
    public function friendRequestResponse() {
        try {
            $checkAuthToken = True;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', 1);
            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);
            $data['patient'] = $data['user_id'];
            $data['doctor'] = $user_id;
            $user = new User();
            if ($user->validateCallRequest($data)) {
                $data['response'] = ($data['response'] == 1) ? 1 : 3;
                DoctorPatientRelation::whereRaw('doctor = ' . $data['doctor'] . ' and patient = ' . $data['patient'])->update(array('is_approved' => $data['response']));

                if ($data['response'] == 1) {
                    $communicationHelper = $this->helper->load('communication');

                    $user_app = DeviceInfo::where('user_id', '=', $data['patient'])->first();
                    $user = User::where('user_id', '=', $data['doctor'])->first();

                    $GCMData = array();
                    $GCMData['from_user_id'] = $user['user_id'];
                    $GCMData['from_user_name'] = trim($user['salutation'] . " " . $user['name']);
                    $GCMData['msg_id'] = '';
                    $GCMData['msg_time'] = time();
                    $GCMData['msg_text'] = trim($user['salutation'] . " " . $user['name']) . Config::get('config_msg.FRI_REQ_ACCEPT');
                    $GCMData['type'] = 'request_accept';

                    $params['msg'] = json_encode($GCMData);

                    if ($user_app['platform_id'] == 1) {
                        $params['GCMDeviceIds'] = $user_app['registartion_id'];
                        $params['msg'] = json_encode($GCMData);
                        $result['response'] = $communicationHelper->gcmCommunication($params);
                    } else if ($user_app['platform_id'] == 2) {
                        $params['APNSDeviceIds'] = $user_app['registartion_id'];
                        $params['push_msg'] = $GCMData;
                        $result['response'] = $communicationHelper->pushApnsCommunication($params);
                    }

                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.FRI_REQ_RES_ACCEPT');
                } else {
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.FRI_REQ_RES_REJECT');
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FRI_REQ_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function pendingRequest() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $doctorPatient = new DoctorPatientRelation();
            $pendingApprovals = $doctorPatient->pendingApproval($user_id);
            
            foreach($pendingApprovals as $pendingApproval) {
                $pendingApproval->photo = User::getPhoto(array('photo' => $pendingApproval->photo, 'is_doctor' => 0));
            }
            
            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $result['status'] = 1;
            $result['msg'] = "Pending approval patient list.";
            $result['response']['patient_list'] = $pendingApprovals;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
