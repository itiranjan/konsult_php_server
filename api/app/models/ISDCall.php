<?php

class ISDCall extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'isd_calls';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'isd_call_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

}
