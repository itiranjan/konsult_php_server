<?php

abstract class ApiBaseController extends BaseController {

    protected $headers;
    protected $authToken;
    protected $verfiedAuth;
    protected $user;
    protected $helper;

    abstract protected function checkAuth($checkAuthToken = TRUE);

    public function __construct() {
        $this->helper = new Helper();
    }

    /**
     * @author : Parth
     * @created : 23 jan 2015
     * @description : This method used to check Auth token vaild or not
     * @access public
     * @param  string  type [ $tokenval ]
     * @return  string 
     */
    protected function checkAuthToken($tokenval = '') {

        if (empty($tokenval)) {
            return false;
        }

        $data = User::whereRaw('token = \'' . $tokenval . '\'')->first();

        if (count($data)) {

            $time_diff = $this->helper->load('date')->time_difference($data->updated_at, date("Y-m-d H:i:s"));
            $expiry_min = ApiConfig::whereRaw('status = ' . 1 . '')->pluck('auth_expirytime');
            if ($time_diff < $expiry_min) {
                $token = $this->saveAuthToken($data->user_id);
                return $token;
            }
        }

        return false;
    }

    /**
     * @author : Parth
     * @created : 23 jan 2015
     * @description : This method used to get Auth token value
     * @access public
     * @return  string 
     */
    protected function getAuthToken() {
        return isset($this->headers['AUTH_TOKEN']) ? $this->headers['AUTH_TOKEN'] : false;
    }

    /**
     * @author : Parth
     * @created : 23 jan 2015
     * @description : This method used to reset Auth token value
     * @access public
     * @return  string 
     */
    protected function resetAuthToken() {
        return md5(time());
    }

    /**
     * @author : Parth
     * @created : 23 jan 2015
     * @description : This method used to set Auth token in header field
     * @access public
     * @param  string  type [ $header ] [optional]
     * @param  string  type [ $value ] [optional]
     * @return  string 
     */
    protected function setHeader($header = '', $value = '') {

        if (empty($header)) {
            return false;
        }

        if ($header === 'AUTH_TOKEN') {
            if (is_null($value)) {
                $this->headers[$header] = NULL;
            } elseif ($value == '') {
                $this->headers[$header] = $this->resetAuthToken();
            } else {
                $this->headers[$header] = $value;
            }
        } else {
            $this->headers[$header] = $value;
        }

        return true;
    }

    /**
     * @author : Parth
     * @created : 23 jan 2015
     * @description : This method used to get Auth token value
     * @access public
     * @param  string  type [ $header ] [optional]
     * @return  string 
     */
    protected function getHeader($header = '') {

        if (!array_key_exists($header, $this->headers)) {
            return false;
        }

        return $this->headers[$header];
    }

    /**
     * @author : Shipra
     * @created : 23 jan 2015
     * @description : This method used to save and update Auth token 
     * @access public
     * @param  int  type [ $user_id ]
     * @return  string 
     */
    public function saveAuthToken($user_id) {

        $user = new User;
        $auth_token = User::where('user_id', '=', $user_id)->pluck('token');

        if ($auth_token) {
            User::where('user_id', '=', $user_id)->update(array('updated_at' => date("Y-m-d H:i:s")));
        } else {
            $this->setHeader('AUTH_TOKEN');
            $auth_token = $user->token = $this->getHeader('AUTH_TOKEN');
            User::where('user_id', '=', $user_id)->update(array('token' => $auth_token, 'updated_at' => date("Y-m-d H:i:s")));
        }

        return $auth_token;
    }

    /**
     * @author : Shipra
     * @created : 23 jan 2015
     * @description : This method used to  get user id by Auth token 
     * @access public
     * @param  string  type [ $tokenval ] [optional]
     * @return  string 
     */
    public function getUserIdbyToken($tokenval = '') {

        if (empty($tokenval)) {
            return false;
        }

        $user_id = User::where('token', '=', $tokenval)->pluck('user_id');

        if ($user_id) {
            return $user_id;
        } else {
            return false;
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created :  27 jan 2015
     * @description : This method used to verify Api key, App Version, config version and Auth Token
     * @access protected
     * @param array headerFiled
     * @param bool checkAuthToken (To Check Auth token) [optional]
     * @return array 
     */
    protected function appStatusVerify($headerFiled, $checkAuthToken = TRUE) {
        ob_start('ob_gzhandler');
        if ($headerFiled) {
            $arr = array();
            $api_id = ApiPlatform::where('api_key', '=', $headerFiled['API_KEY'])->pluck('id');

            if ($api_id) {
                $app_version_new = ApiVersion::where('platform_id', '=', $api_id)->orderBy('id', desc)->first();
                $app_version = ApiVersion::whereRaw('platform_id =' . $api_id . ' and app_version =\'' . $headerFiled['APP_VERSION'] . '\'')->orderBy('id', desc)->first();
                if ($headerFiled['APP_VERSION'] == $app_version_new['app_version']) {
                    $appversionflag = 1;
                } else {
                    $flag = 0;
                    if ($app_version) {
                        if (strtotime(date("Y-m-d", strtotime($app_version_new['force_update_date']))) > strtotime(date("Y-m-d"))) {

                            $arr['config_status'] = 1;
                            $arr['config_msg'] = Config::get('config_msg.UPDATE_APP_VER'); //"Update App version";
                            $arr['response']['app_version'] = $app_version_new['app_version'];
                            $arr['response']['release_date'] = $app_version_new['release_date'];
                            $arr['response']['force_update_date'] = $app_version_new['force_update_date'];
                            $appversionflag = 0;
                            $flag = 1;
                        }
                    }

                    if ($flag == 0) {
                        $arr['config_status'] = 0;
                        $arr['config_msg'] = Config::get('config_msg.APP_VER_MISMATCH'); //"App version Mismatch";
                        $arr['response']['app_version'] = $app_version_new['app_version'];
                        $arr['response']['release_date'] = $app_version_new['release_date'];
                        $arr['response']['force_update_date'] = $app_version_new['force_update_date'];
                        $appversionflag = -1;
                    }
                }

                if ($appversionflag == 1 || $appversionflag == 0) {
                    $config_version_new = ApiConfig::where('app_version_id', '=', $app_version['id'])->orderBy('id', desc)->first();

                    if ($config_version_new['api_config_version'] == $headerFiled['CONFIG_VERSION']) {
                        if ($appversionflag == 1) {
                            $arr['config_status'] = 1;
                            $arr['config_msg'] = Config::get('config_msg.APP_STATUS_SUCCESS');
                        }
                    } else {
                        $arr['config_status'] = 0;
                        $arr['config_msg'] = Config::get('config_msg.CONFIG_VER_MISMATCH');
                        $arr['response']['config_version'] = $config_version_new['api_config_version'];
                    }
                }
            } else {
                $arr['config_status'] = 0;
                $arr['config_msg'] = Config::get('config_msg.API_KEY_MISMATCH');
            }
            if ($arr['config_status'] == 1 && $checkAuthToken == TRUE) {
                $auth_token = $this->checkAuthToken($headerFiled['AUTH_TOKEN']);
                if (!$auth_token) {
                    $arr['config_status'] = 0;
                    $arr['config_msg'] = Config::get('config_msg.AUTH_TOKEN_EXPIRE');
                }
            }
            return $arr;
        } else {
            return false;
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created :  27 jan 2015
     * @description : This method used to get Error code with msg
     * @access public
     * @param array errors
     * @return array 
     */
    public function getError($errors) {

        if (!is_array($errors)) {
            $errors[] = 1000;
        }

        $errorDetail = APIError::whereIn('error_code', $errors)->get();

        return $errorDetail;
    }

}
