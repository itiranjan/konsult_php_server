<?php
/**
 * Template Name: What We Treat
 * Description: A Page Template that display Listing of what we treat.
 *
 * @author Sahil Ahlawat
 */
 get_header();
 ?>
 <?php
 	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
						global $wp;
						global $wpdb;
						global $my_wpdb ;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						//$my_wpdb = new WPDB('root', '', 'Konsult_Prod', 'localhost');
						global $specializationData ;
						$specializationData = $my_wpdb->get_results("SELECT DISTINCT specialization_master.name as name FROM specialization_master JOIN doctor_specialization_rel on doctor_specialization_rel.specialization_id = specialization_master.specialization_id ORDER BY specialization_master.priorities DESC LIMIT 21;", 0, 0);
						global $symptomData ;
     $symptomData = $my_wpdb->get_results("SELECT DISTINCT symptom_master.name as name FROM symptom_master JOIN symptom_specialization_rel ON symptom_master.symptom_id = symptom_specialization_rel.symptom_id JOIN specialization_master ON specialization_master.specialization_id = symptom_specialization_rel.specialization_id ORDER BY specialization_master.priorities DESC LIMIT 21;", 0, 0);
						
						?>
						<div class="wconta">
						<div class="partition"></div>
						<h1>Treatments We Provide Through Online Consultation</h1>
						
						<div class="searchcalldiv">
						<form id="searchformforspsy" method="GET" action="<?php echo bloginfo('url'); ?>/what-we-treat/" >
						<input class="stf" type="text" value="" placeholder="Search Doctors, Specialities or Symptoms" name="searchq" />
						
						<!--<input class="ssb" type="submit" name="submitsearch" value="&#128269;" />-->
						<button type="submit" form="searchformforspsy" value="Submit"><i class="fa fa-search 2x"></i></button>
						</form>
						<div class="searcharesult">
						<ul class="resultul">
						<!--<li><a href="#"><span class="left">acne</span><span class="right">symptom</span></a></li>
						<li><a href="#"><span class="left">acne</span><span class="right">Speciality</span></a></li>-->
						</ul>
						</div>
						</div>
						<div class="searchresult">
					
						<?php
						if(isset($_REQUEST["searchq"])){
							?>
								<h1>
						Search Results  
						</h1>
							<?php
							if($_REQUEST["searchq"]){$search = $_REQUEST["searchq"]; }
							else{
								$search = "";
							}
							global $specializationsearchData ;
						$specializationsearchData = $my_wpdb->get_results("select * FROM specialization_master WHERE name like '%$search%' limit 15;", 0, 0);
						if($specializationsearchData){foreach($specializationsearchData as $spsd){
							?>
   <div class="innerelement">
 
                              <a href="<?php echo bloginfo('url'); ?>/speciality/<?php echo str_replace(' ', '-', strtolower($spsd->name))?>" ><h3><?php echo "Speciality : ".$spsd->name; ?></h3>
								 <!--<img src="<?php// echo $sd->photo_url; ?>" />-->
								 </a>
   </div>
   <?php
						}}
						global $symptomsearchData ;
						$symptomsearchData = $my_wpdb->get_results("select * FROM symptom_master WHERE name like '%$search%' limit 15;", 0, 0);
							if($symptomsearchData){foreach($symptomsearchData as $stsd){
								?>
   <div class="innerelement">
   
   
   <a href="<?php echo bloginfo('url'); ?>/symptom/<?php echo str_replace(' ', '-', strtolower($stsd->name))."-treatment";?>">
                                 <h3><?php echo "Symptom : ".$stsd->name; ?></h3>
								 </a>
   </div>
   <?php
							}}
						}
						?>
						</div>
						<div class="colhalf cathalf">
						<h2>Most Searched Specialities</h2>
						<?php
						foreach ($specializationData as $sd) {
   ?>
   <div class="innerelement">
 
                              <a href="<?php echo bloginfo('url'); ?>/speciality/<?php echo str_replace(' ', '-', strtolower($sd->name))?>" ><h3><?php 
       if(strlen($sd->name) > 20)
       {
           echo substr($sd->name,0,20)."..";
       }
                            else{
                             echo $sd->name;   
                            }
        ?></h3>
								 <!--<img src="<?php// echo $sd->photo_url; ?>" />-->
								 </a>
   </div>
   <?php
                                                      }
 ?>
 <div class="vallbtn">
 <a href="<?php echo bloginfo('url').'/specialities' ?>"> View All Specialities</a>
 </div>
 </div>
 <div class="colhalf symphalf">
 	<h2>Most Searched Symptoms</h2>
 <?php
						foreach ($symptomData as $syd) {
   ?>
   <div class="innerelement">
   
   
   <a href="<?php echo bloginfo('url'); ?>/symptom/<?php echo str_replace(' ', '-', strtolower($syd->name))?>-treatment">
                                 <h3><?php 
       if(strlen($syd->name) > 20)
       {
           echo substr($syd->name,0,20)."..";
       }
                            else{
                             echo $syd->name;   
                            }
        ?></h3>
								 </a>
   </div>
   <?php
                                                      }
 ?>
 <div class="vallbtn">
 <a href="<?php echo bloginfo('url').'/symptoms' ?>"> View All Symptoms</a>
 </div>
 </div>
 <div class="partition"></div>
 </div>
 <style>
     @media only screen and (max-width: 768px) {
         
         .wconta .colhalf{
	 width: 98% !important;
             margin:0 auto !important;
             float:none !important;
         }
         .wconta .colhalf h2 {
    
    display: block !important;
    clear: both !important;
}
      .vallbtn a {
    margin-left: -15%;
}   
     }
    .colhalf .innerelement h3{
    word-wrap: break-word;
    text-overflow: ellipsis;
    max-height: 29px;
    overflow: hidden;
        font-size: 12px !important;
    font-weight: 400;
     color: #24cdd9 !important;
        display:initial;
         line-height: 35px;
            margin-left: 25%;
     }
 .resultul{
 color: white;
 }
 .searchcalldiv button:hover{
	 background: none;
	 color:#24cdd9;
	 
box-shadow: none;
 }
 .searchcalldiv button{
	 background: none;
color: rgba(0,0,0,.4);
box-shadow: none;
padding: 0px;
margin: 0px;
line-height: 0px;
font-size: 30px;
 }
 .searcharesult ul li span.right{
	 width: 48%;
float: left;
text-align: right;
text-transform: uppercase;
color:rgba(0,0,0,.2);
margin-right: 10px;

 }
 .searcharesult ul li span.left{
	 width: 48%;
float: left;
text-align: left;
color:rgba(0,0,0,.7);
margin-left: 10px;
text-transform: capitalize;
 }
 .searcharesult ul li:hover{
	 background:rgba(0,0,0,.1);
 }
 .searcharesult ul li{
	 border: 1px solid rgba(0,0,0,.1);
     padding: 10px 0px 10px 0px;
	 float: left;
width: 100%;
 }
 .searcharesult ul{
	 width: 100%;
float: left;
 }
 .searcharesult{
	 float: left;
box-shadow: 0px 0px 4px 0px;
width: 96%;
margin-left: 20px;
margin-top: 10px;
 }
 .partition{
	 
	 height:20px;
	 width:100%;
	 float:left;
 }
 .searchresult{
width: 90%;
clear: both;
margin-top: 20px;
margin-bottom: 20px;
float: left;
padding: 12px;
margin-left: 5%;
 }
 .searchcalldiv{
	width: 40%;
padding: 0px;
margin: 0 auto;
 }
 .searchcalldiv form{
	width: 100%;
padding: 5px 0px 5px 15px;
margin: 0px;
border: 1px solid rgba(0,0,0,.1);
float: left;
border-radius: 0px;
 }
 .searchcalldiv .stf{
	 width: 80%;
float: left;
box-shadow: none;
border: none;
margin:0px;
 }
 .searchcalldiv .ssb{
width: 20%;
float: left;
/*
margin-left: 20px;
margin-top: 0px;
*/
font-size: 33px;
line-height: 0px;
background: none;
border: none;
box-shadow: none;
margin:0px;
 }
 .searchcalldiv input{}
 .vallbtn a:hover {
	 background: #e872a6;
    color: white;
 }
 .vallbtn a {
    background: #24cdd9;
       padding: 7px 30px;
    color: white;
}
 .vallbtn {
	    margin-left: -27px;
    width: 100%;
    clear: both;
    padding: 25px;
 }
 a:hover   {
  /* Applies to links under the pointer */
  text-decoration:  none;
 }
 #Footer{
	 
	 clear: both;
     
 }
 h1{
     font-size:26px;
     }
 .wconta .colhalf h2{
	 font-size: 20px;
         font-weight: 500;
 }
 .wconta .colhalf .innerelement h3, .searchresult .innerelement h3{
	 font-size: 12px;
	 
	 color:black;
 }
 .wconta .colhalf .innerelement:hover h3, .searchresult .innerelement:hover h3{
/*color:#24cdd9; 
color:white;*/
	 }
 .wconta .colhalf .innerelement:hover, .searchresult .innerelement:hover{
/*	 
border:1px solid #24cdd9;
background: #24cdd9;*/
	 
 }
 .wconta .colhalf .innerelement, .searchresult .innerelement{
width: 32%;
float: left;
background: white;
margin: 0px 0px -10px 0px;
/*border:1px solid rgba(0,0,0,.7);*/
     text-align: left;
 }
 .wconta .colhalf{
	 width: 48%;
float: left;
padding: 0px 10px;
 }
 .wconta h1{}
 .wconta{
 text-align: center;
width: 95%;
     min-height: 800px;
         margin: 0 auto;
/*
padding: 9px;
*/
clear: both;
 }
     .cathalf {
    border-right: 1px solid;
}
     
 </style>
 <script>
 // jQuery(function(){
  // jQuery('.stf').keyup(  );
  // alert("typing!!");
// });.
jQuery(document).ready(function(){
jQuery(".stf").on("change keyup paste", function(){
	
	if(jQuery(".stf").val() != ""){
		load_contents(jQuery(".stf").val());
	}
	else{
		jQuery(".resultul").html("");
	}
	
    
})
 function load_contents(search_keyword){
	var data = {
		'action': 'what_we_do_ajax_request',
		'page': search_keyword,
		
	};
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	jQuery.post(ajaxurl, data, function(response) {
		
		
		if(response.trim().length == 0){
			jQuery(".resultul").html("<li><a href='#'><span class='left'>Sorry No Result Found!</span><span class='right'></span></a></li>");
		}
		jQuery(".resultul").html(response);
		
//alert(response);		
	});

			}
			});
			
 </script>
 <?php
 get_footer();
 ?>
