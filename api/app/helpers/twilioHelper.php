<?php

$basePath = base_path();
$basePath = substr($basePath, 0, -3);
include_once $basePath . 'api/vendor/twilio/Twilio/autoload.php';

class twilioHelper {

    protected $helper;

    public function __construct() {
        $this->helper = new Helper();
        $this->twilio_account_sid = Config::get('constants.twilio_account_sid');
        $this->twilio_auth_token = Config::get('constants.twilio_auth_token');
        $this->twilio_api_key = Config::get('constants.twilio_api_key');
        $this->twilio_secret_key = Config::get('constants.twilio_secret_key');
        $this->twilio_outgoing_sid = Config::get('constants.twilio_outgoing_sid');
        $this->twilio_number = Config::get('constants.twilio_number');
        $this->twilio_account_sid_twmil_kit = Config::get('constants.twilio_account_sid_twmil_kit');
        $this->host = parse_url(Request::url(), PHP_URL_HOST);
        $this->doctor_ringing_timeout = 60;
        $this->operator_ringing_timeout = 60;

        $opNumber = Config::get('constants.OPERATOR_NUMBER');
        $opNumberArray = explode(",", $opNumber);
        $this->operator_number = $opNumberArray[0];
    }

    public function errorMsgInVoip($params = array()) {
        // A message for Twilio's TTS engine to repeat
        $sayMessage = $params['errorMsg'];

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));

        return $twiml;
    }

    public function voipCall($params = array()) {

        // A message for Twilio's TTS engine to repeat
        $sayMessage = 'Thank you for calling Konsult App, we are connecting your doctor shortly!';

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $dial = $twiml->dial(array('callerId' => $this->twilio_number));
        $dial->number($params['docMobile'], ["statusCallbackEvent" => 'completed', "statusCallbackMethod" => "GET", "statusCallback" => "http://$this->host/callinfo?twilio_call_id=$encodedCallId"]);

        return $twiml;
    }

    public function initiateCall($params = array()) {

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $client = new Twilio\Rest\Client($this->twilio_account_sid, $this->twilio_auth_token);

        if(!empty($client)) {
            try {
                
                if(!empty($params['clinic_type'])) {
                    $otherParams = array("Url" => "http://$this->host/call-connect/$encodedCallId",
                        "Record" => true,
                        );
                } else {
                    $otherParams = array("Url" => "http://$this->host/call-connect/$encodedCallId");                    
                }

                $client->calls->create(
                        $params['patMobile'], // The visitor's phone number
                        $this->twilio_number, // A Twilio number in your account
                        $otherParams
                );
            } catch (Exception $e) {
                // Failed calls will throw
                return array('status' => 0,  'message' => $e->getMessage());
            }
        } else {
            return array('status' => 0,  'message' => 'Issue with creating a client object!');
        }

        // return a JSON response
        return array('status' => 1,  'message' => 'Call incoming!');
    }

    public function callConnect($params = array()) {
        // A message for Twilio's TTS engine to repeat
        $sayMessage = 'Thank you for calling Konsult App, we are connecting your doctor shortly!';

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));

        $dialOptions = array("timeout" => $this->doctor_ringing_timeout, "timeLimit" => $params['max_call_duration'], /* "record" => 'record-from-answer-dual', */ 'ringTone' => 'in');

        if ($params['connectOperator']) {
            $dialOptions = array_merge($dialOptions, array('action' => "http://$this->host/operator-option/$encodedCallId"));
        }

        $dial = $twiml->dial($dialOptions);

        $dial->number($params['docMobile'], ["statusCallbackEvent" => 'completed', "statusCallbackMethod" => "GET", "statusCallback" => "http://$this->host/callinfo?twilio_call_id=$encodedCallId"]);

        return $twiml;
    }

    public function operatorOption($params = array()) {

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $twiml = new Twilio\Twiml();
        $gather = $twiml->gather(['input' => 'speech dtmf', 'timeout' => $this->operator_ringing_timeout, 'numDigits' => 1, 'action' => "http://$this->host/operator-connect/$encodedCallId"]);
        $twiml->pause(['length' => 3]);
        $gather->say('If you could not connect with doctor or you have feedback or query for us, please press 1 to talk to our operator else you can disconnect.', array('voice' => 'alice'));
        $twiml->pause(['length' => 5]);
        $twiml->say("Sorry! We are not getting any input. Thank you for calling Konsult App!", array('voice' => 'alice'));

        return $twiml;
    }

    public function operatorConnect($params = array()) {

        if ($params['Digits'] == 1) {
            $sayMessage = 'Please wait while we connect you to with our available operator!';
        } else {
            $sayMessage = 'Please enter the correct option. If you have feedback or query for us, please press 1 to talk to our operator else you can disconnect.';
        }

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));
        $dial = $twiml->dial(["timeout" => $this->operator_ringing_timeout, /* "record" => 'record-from-answer-dual', */ 'ringTone' => 'in']);
        $dial->number($this->operator_number, ["statusCallbackEvent" => 'completed', "statusCallbackMethod" => "GET", "statusCallback" => "http://$this->host/operatorCallInfo/$encodedCallId"]);
        //$twiml->say("Sorry!, our all operators are busy to attending other users. Please try again later. Thank you for calling konsult app!", array('voice' => 'alice'));

        return $twiml;
    }

    public function generateCapabilityToken($params = array()) {

        $patientId = $params['user_id'];

        $capability = new Twilio\Jwt\ClientToken($this->twilio_account_sid, $this->twilio_auth_token);
        $capability->allowClientIncoming($patientId);

        $capability->allowClientOutgoing($this->twilio_outgoing_sid);

        return $capability->generateToken();
    }

    public function sendSMS($params = array()) {

        $client = new Twilio\Rest\Client($this->twilio_account_sid, $this->twilio_auth_token);

        try {

            $client->messages->create(
                    $params['To'], array('from' => $this->twilio_number,
                'body' => $params['Text'])
            );
        } catch (Exception $e) {
            // Failed to send sms will throw
            return $e;
        }

        // return a JSON response
        return array('status' => 1, 'message' => 'SMS sent!');
    }
    
    public function initiateISDCall($params = array()) {

        $encodedISDCallId = urlencode(str_replace(' ', '', $params['isd_call_id']));

        $client = new Twilio\Rest\Client($this->twilio_account_sid, $this->twilio_auth_token);

        if(!empty($client)) {
            try {

                $client->calls->create(
                        $params['caller_phone'], // The visitor's phone number
                        $this->twilio_number, // A Twilio number in your account
                        array("Url" => "http://$this->host/call-isd-connect/$encodedISDCallId",
                        //"Record" => true,
                        )
                );
            } catch (Exception $e) {
                // Failed calls will throw
                return array('status' => 0,  'message' => $e->getMessage());
            }
        } else {
            return array('status' => 0,  'message' => 'Issue with creating a client object!');
        }

        // return a JSON response
        return array('status' => 1,  'message' => 'Call incoming!');
    }

    public function ISDCallConnect($params = array()) {
        // A message for Twilio's TTS engine to repeat
        $sayMessage = 'Thank you for calling Konsult App, we are connecting other party shortly!';

        $encodedCallId = urlencode(str_replace(' ', '', $params['isd_call_id']));

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));

        $dialOptions = array("timeout" => $this->doctor_ringing_timeout, "timeLimit" => $params['max_call_duration'], /* "record" => 'record-from-answer-dual', */ 'ringTone' => 'in');

        $dial = $twiml->dial($dialOptions);

        $dial->number($params['receiver_phone'], ["statusCallbackEvent" => 'completed', "statusCallbackMethod" => "GET", "statusCallback" => "http://$this->host/isdCallInfo?isd_call_id=$encodedCallId"]);

        return $twiml;
    }  
    
    public function medicalTourismInitiateCall($params = array()) {

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $client = new Twilio\Rest\Client($this->twilio_account_sid, $this->twilio_auth_token);

        if(!empty($client)) {
            try {

                $client->calls->create(
                        $params['caller_phone'], // The visitor's phone number
                        $this->twilio_number, // A Twilio number in your account
                        array("Url" => "http://$this->host/medicalTourismcallConnect/$encodedCallId",
                        "Record" => true,
                        )
                );
            } catch (Exception $e) {
                // Failed calls will throw
                return array('status' => 0,  'message' => $e->getMessage());
            }
        } else {
            return array('status' => 0,  'message' => 'Issue with creating a client object!');
        }

        // return a JSON response
        return array('status' => 1,  'message' => 'Call incoming!');
    }

    public function medicalTourismCallConnect($params = array()) {
        // A message for Twilio's TTS engine to repeat
        $sayMessage = 'Thank you for calling Konsult International, we are connecting other party shortly. This call may be recorded for trainning and quality purpose.';

        $encodedCallId = urlencode(str_replace(' ', '', $params['call_id']));

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));

        $dialOptions = array("timeout" => $this->doctor_ringing_timeout, "timeLimit" => $params['max_call_duration'], /* "record" => 'record-from-answer-dual', */ 'ringTone' => 'in');

        $dial = $twiml->dial($dialOptions);

        $dial->number($params['receiver_phone'], ["statusCallbackEvent" => 'completed', "statusCallbackMethod" => "GET", "statusCallback" => "http://$this->host/medicalTourismCallInfo?call_id=$encodedCallId"]);

        return $twiml;
    }     
    
    public function medicalTourism($params = array()) {

        $sayMessage = 'Thank you for reaching Konsult. One of our executive will get back to you within 24 hrs.';

        $twiml = new Twilio\Twiml();
        $twiml->say($sayMessage, array('voice' => 'alice'));      
        $twiml->reject();

        return $twiml;
    }    

}
