<?php

class AutoCompleteController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool checkAuthToken (To Check Auth token) [optional]
     * @return  array 
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                } else if ($result['config_status'] == 1) {
                    $this->verfiedAuth = true;
                    $this->authToken = $requestHeaders['AUTH_TOKEN'];
                }
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of universities.
     * @access public
     * 
     * @return array 
     */
    public function universityList() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.UNIVERSITY_LIST');
            $result['response'] = University::select('university_id as id', 'name')->orderBy('name')->get();
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of designations.
     * @access public
     * 
     * @return array 
     */
    public function designationList() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.DESIGNATION_LIST');
            $result['response'] = Designation::select('designation_id as id', 'name')->orderBy('name')->get();
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of degree.
     * @access public
     * 
     * @return array 
     */
    public function degreeList() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.DEGREE_LIST');
            $result['response'] = Degree::select('degree_id as id', 'name')->orderBy('name')->get();
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of hospitals.
     * @access public
     * 
     * @return array 
     */
    public function hospitalList() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.HOSPITAL_LIST');

            $data = Input::all();

            $queryString = '';
            $requestHeaders = apache_request_headers();
            if (!empty($requestHeaders['AUTH_TOKEN'])) {
                $user_id = $this->getUserIdbyToken($requestHeaders['AUTH_TOKEN']);
                if (!empty($user_id)) {
                    $isClinic = UserProfile::whereRaw('user_id = ' . $user_id)->pluck('clinic_type');
                    if ($isClinic) {
                        $queryString = "(doctor_profile.list_category = $isClinic OR doctor_profile.list_category = 3) ";
                        $listCategories = array($isClinic, 3);
                    }
                }
            }


            if (!empty($data['hospitalCount'])) {
                $query = DB::table('hospital_master')->join('doctor_workplace', 'doctor_workplace.hospital_id', '=', 'hospital_master.hospital_id')->join('doctor_profile', 'doctor_profile.user_id', '=', 'doctor_workplace.user_id')->select('hospital_master.hospital_id as id', 'hospital_master.name')->where('doctor_profile.is_approved', '=', 1);

                if (!empty($queryString)) {

                    $query->where(function ($query) use ($listCategories) {
                        foreach ($listCategories as $listCategory) {
                            $query->orWhere('doctor_profile.list_category', '=', $listCategory);
                        }
                    });
                }

                $query = $query->groupBy('hospital_master.hospital_id')->orderBy('priorities', 'DESC');
                if (is_numeric($data['hospitalCount']) && $data['hospitalCount'] != '*') {
                    $query = $query->limit($data['hospitalCount']);
                }

                $result['response'] = $query->get();
            } else {
                $result['response'] = Hospital::select('hospital_id as id', 'name')->orderBy('name')->get();
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of specializations.
     * @access public
     * 
     * @return array 
     */
    public function specializationList() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $specializationList = Specialization::select('specialization_id as id', 'name', 'photo_url', 'priorities')->orderby('priorities', 'DESC')->orderBy('name');

            if (!empty($data['count'])) {
                $specializationList->take($data['count']);
            }

            $specializationList = $specializationList->get();

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.SPECIALIZATION_LIST');
            $result['response'] = $specializationList;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  This method used to get list of cities.
     * @access public
     * 
     * @return array 
     */
    public function cityList() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.CITY_LIST');
            $result['response'] = City::select('city_id as id', 'name')->orderBy('name')->get();
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 23 mar 2015
     * @description :  This method used to generate insert query.
     * @access public
     * 
     * @return array 
     */
    public function getQuery() {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $city = City::get(array('city_id AS id', 'name'));
            $specialization = Specialization::get(array('specialization_id AS id', 'name'));
            $degree = Degree::get(array('degree_id AS id', 'name'));

            $query = '';
            $count = count($city);
            $pool = 0;
            if ($count > 0) {
                $query .= "INSERT INTO city_master ('city_id','name') VALUES ";
                for ($i = 0; $i < $count; $i++) {
                    if ($i != 0 && $pool != 200) {
                        $query.=" , ";
                    }
                    $pool++;
                    if ($pool == 201) {
                        $query .=";\nINSERT INTO city_master ('city_id','name') VALUES ";
                    }
                    $query.="(" . $city[$i]['id'] . ",'" . $city[$i]['name'] . "')";
                }
                $query.=";\n";
            }

            $count = count($specialization);
            $pool = 0;
            if ($count > 0) {
                $query .= "INSERT INTO specialization_master ('specialization_id','name') VALUES ";
                for ($i = 0; $i < $count; $i++) {
                    if ($i != 0 && $pool != 200) {
                        $query.=" , ";
                    }
                    $pool++;
                    if ($pool == 201) {
                        $query .=";\nINSERT INTO specialization_master ('specialization_id','name') VALUES ";
                    }
                    $query.="(" . $specialization[$i]['id'] . ",'" . $specialization[$i]['name'] . "')";
                }
                $query.=";\n";
            }

            $count = count($degree);
            $pool = 0;
            if ($count > 0) {
                $query .="INSERT INTO degree_master ('degree_id','name') VALUES ";
                for ($i = 0; $i < $count; $i++) {
                    if ($i != 0 && $pool != 200) {
                        $query.=" , ";
                    }
                    $pool++;
                    if ($pool == 201) {
                        $query .=";\nINSERT INTO degree_master ('degree_id','name') VALUES ";
                    }
                    $query.="(" . $degree[$i]['id'] . ",'" . $degree[$i]['name'] . "')";
                }
                $query.=";\n";
            }

            return $query;
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 23 mar 2015
     * @description :  This method used to get query version by config table.
     * @access public
     * 
     * @return array 
     */
    public function getQueryVersion($version) {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $db_version = ApiConfig::where('id', '=', 1)->pluck('db_version');

            if ($version == $db_version) {
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['version'] = $db_version;
                $result['msg'] = Config::get('config_msg.DB_UPDATE');
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['status'] = 0;
                $result['version'] = $db_version;
                $result['msg'] = Config::get('config_msg.DB_UPDATE_FAIL');
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
