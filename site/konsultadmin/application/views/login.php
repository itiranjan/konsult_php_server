<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <title><?= (isset($this->title)) ? SITE . ' [' . CLIENT . '] ' . $this->title : SITE . '[' . CLIENT . ']'; ?></title>
        <!-- href="<?= URL; ?>" /-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link rel="stylesheet" href="<?php echo(URL . '/assets/css/stylesheet.css'); ?>" />
            <script type="text/javascript" src="<?php echo(JS . "/jquery.min.js"); ?>"></script>
            <script>
                $(document).keypress(function (e) {
                    if (e.which == 13) {
                        //alert('You pressed enter!');
                        loginUser();
                    }
                });
                var error = 'Your request cannot be processed due to some technical problems. Please try after some time!';
                function loginUser() {

                    if (!document.getElementById('p_username').value || !document.getElementById('p_password').value) {
                        alert("All fields are required!!");
                        return;
                    }

                    // Launch ajax request
                    $.ajax({
                        // The link we are accessing.
                        url: '<?php echo URL; ?>/index.php/verifylogin',
                        // The type of request.
                        type: 'POST',
                        // The data we are sending.
                        data: $("form").serialize(),
                        // The type of data that is getting returned.
                        dataType: 'json',
                        beforeSend: function () {
                            $('#loading').show();
                        },
                        complete: function () {
                            $('#loading').hide();
                        },
                        success: function (data) {
                            if (data.status == 1) {
                                var res = data.action.split("_");
                                var redirect_url = "<?php echo URL; ?>/index.php/" + res[0];

                                if (res[1] != 'index') {
                                    redirect_url = redirect_url + '/' + res[1];
                                }

                                window.location = redirect_url;
                            }
                            else {
                                alert(data.message);
                                location.reload(true);
                            }
                        },
                        error: function () {
                            alert(error);
                        }
                    });
                    // Prevent default click
                    return false;
                }
            </script>
    </head>
    <body class="login_bg">
        <?php echo LOADER; ?>
        <div id="login-box">
            <h1>Website Admin<span><?= SITE ?></span></h1>
            <form name="form_login" id="form_login" action="verifylogin" method="post" class="login">
                <input type="hidden" name="identifier" value="<?php session_id(); ?>"/>
                <fieldset>  
                    <legend>Enter Username/Password</legend>
                    <span class="loginLbl">Username</span>
                    <input type="text" id="p_username" name="username" class="mediuminput" value=""/>
                    <span class="loginLbl">Password</span>
                    <input type="password" id="p_password" name="password" class="mediuminput"  value=""/>
                </fieldset>
                <input type="hidden" name="p_loginuser" id="p_loginuser" value="1"/>
                <input type="button" name="btn_login" id="btn_login" value="Log In" onclick="loginUser()" />
            </form>

        </div>
    </body>
</html>