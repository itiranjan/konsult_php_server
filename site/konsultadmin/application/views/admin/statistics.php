&nbsp;&nbsp;&nbsp;<span style="font-size:20px">Site Statistics<br/><br/></span>

&nbsp;&nbsp;&nbsp;<?php echo "Total Doctors: ".$total_doctors;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Approved Doctors: ".$total_approved_doctors;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Un-approved Doctors: ".$total_unapproved_doctors;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Patients: ".$total_patients;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Active Patients: ".$total_active_patients;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Calls: ".$total_calls;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Connected Calls: ".$total_connected_calls;?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Call Duration (In Mins): ".$total_call_duration;?>

<?php if($total_connected_calls > 0): ?>
    <br/><br/>
    &nbsp;&nbsp;&nbsp;<?php echo "Average Call Duration (In Mins): ".round(($total_call_duration/$total_connected_calls), 2);?>
<?php endif; ?>

<br/><br/>
&nbsp;&nbsp;&nbsp;<?php echo "Total Revenue (In INR): ".$total_revenue;?>