<?php

class user_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $ci = get_instance();
        $ci->load->helper('miscellaneous_helper');
    }

    function userList($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT SUM(spends.total_charges) as amount_paid, users.user_id, users.name, users.email,mobile, user_profile.last_accessed,users.created_at, user_wallet.email as wallet_email, (CASE WHEN (uwi.user_wallet_id IS NULL) THEN 'No' ELSE 'Yes' END) as paytm,user_profile.followed_by, user_profile.comment, user_profile.clinic_type FROM users INNER JOIN user_profile ON users.user_id = user_profile.user_id LEFT JOIN user_wallet ON user_wallet.user_id = users.user_id AND wallet_id = 3  LEFT JOIN user_wallet as uwi ON uwi.user_id = users.user_id AND uwi.wallet_id = 1 LEFT JOIN calls ON calls.caller_id = users.user_id AND calls.call_duration > 0 LEFT JOIN spends ON spends.call_id = calls.call_id ";
        $mainQueryParams['groupBy'] = " users.user_id ";
        $mainQueryParams['defaultSorting'] = " users.created_at DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM users INNER JOIN user_profile ON users.user_id = user_profile.user_id LEFT JOIN user_wallet ON user_wallet.user_id = users.user_id AND user_wallet.wallet_id = 3 LEFT JOIN user_wallet as uwi ON uwi.user_id = users.user_id AND uwi.wallet_id = 1 LEFT JOIN calls ON calls.caller_id = users.user_id AND calls.call_duration > 0 LEFT JOIN spends ON spends.call_id = calls.call_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function totalPatients($params = array()) {

        $sql = "SELECT COUNT(*) AS total_patients FROM users";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_patients;
        }

        return 0;
    }

    function updateUserDetails($rowData = array()) {

        extract($rowData);

        $sql = "UPDATE user_profile SET `$column_name` = '$column_value', `followed_by` = '$followed_by' WHERE user_id = $user_id";

        return $this->db->query($sql);
    }

    function userDetails($params = array()) {

        $mobile = $params['mobile'];
        $sql = "SELECT user_id, name, email, mobile, created_at FROM users WHERE users.mobile='$mobile'";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row();
        }

        return 0;
    }

    function getUserCommunications($params = array()) {

        $userId = $params['user_id'];
        $sql = "SELECT kcc_call_id, doctor_user_id, per_min_charges, call_duration, amount, call_starttime, caller_status, receiver_status FROM kcc_calls AS kc JOIN kcc_transactions AS kt ON kc.kcc_call_id = kt.transaction_details_id WHERE kc.user_id='$userId' AND kt.transaction_type_id = 1 ORDER BY kcc_call_id DESC";

        $query = $this->db->query($sql);

        if ($query->num_rows >= 1) {
            return $query->result();
        }

        return array();
    }

//    function getAppUserCommunications($params = array()) {
//
//        $userId = $params['user_id'];
//        $sql = "SELECT kcc_call_id, doctor_user_id, per_min_charges, call_duration, amount, call_starttime, caller_status, receiver_status FROM kcc_calls AS kc JOIN kcc_transactions AS kt ON kc.call_id = kt.transaction_details_id WHERE kc.user_id='$userId' AND kt.transaction_type_id = 1";
//
//        $query = $this->db->query($sql);
//
//        if ($query->num_rows >= 1) {
//            return $query->result();
//        }
//
//        return array();
//    }

    function getUserRecharge($params = array()) {

        $userId = $params['user_id'];
        $sql = "SELECT kr.created_at as recharged_on, kt.amount, kci.code FROM kcc_recharge_info AS kr JOIN kcc_transactions AS kt ON kr.recharge_info_id = kt.transaction_details_id JOIN kcc_code_info AS kci ON kci.code_info_id = kr.code_info_id WHERE kr.user_id='$userId' AND kt.transaction_type_id = 2";

        $query = $this->db->query($sql);

        if ($query->num_rows >= 1) {
            return $query->result();
        }

        return 0;
    }

    function getUserStatement($params = array()) {

        $userId = $params['user_id'];

        $sql = "SELECT statement_id, closing_balance, amount_added,	amount_spend, statement_date FROM kcc_account_statements WHERE user_id='$userId'";

        $query = $this->db->query($sql);

        if ($query->num_rows >= 1) {
            return $query->result();
        }

        return 0;
    }

    function getCitrusToken($params = array()) {

        $userId = $params['user_id'];

        $sql = "SELECT token FROM user_wallet WHERE user_id = $userId";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->token;
        }

        return '';
    }

    function getUserAuthToken($params = array()) {

        $userId = $params['user_id'];

        $sql = "SELECT token FROM users WHERE user_id = $userId";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->token;
        }

        return '';
    }

}
