<?php

class dateHelper {

    /**
     * @author : Parth
     * @created : 27 jan 2015
     * @description : This function is used to get date in user define  format
     * @param string [optional] $format
     * @param string [optional] $date
     * @access public
     * @return  array 
     */
    function getDate($format = '', $date = '') {
        if ($date == '') {
            $tm = time();
        } else {
            $tm = strtotime($date);
        }
        $dt = date("Y-m-d H:i:s", $tm);

        return $dt;
    }

    /**
     * @author : Parth
     * @created : 27 jan 2015
     * @description : This function is used to get time difference between two dates
     * @param string $date1
     * @param string $date2
     * @access public
     * @return  array 
     */
    public function time_difference($date1, $date2) {
        if (!$this->is_date($date1) or ! $this->is_date($date2)) {
            return FALSE;
        }
        $to_time = strtotime($date1);
        $from_time = strtotime($date2);

        return round(($from_time - $to_time) / 60);
    }

    /**
     * @author : Parth
     * @created : 27 jan 2015
     * @description : This function is used to check date is valid format or not
     * @param string $date
     * @param string [Optional] $format
     * @access public
     * @return  array 
     */
    public function validateDate($date, $format = 'Y-m-d H:i:s') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }

    /**
     * @author : Parth
     * @created : 27 jan 2015
     * @description : This function is used to date is valid or not
     * @param string $str
     * @access public
     * @return  array 
     */
    function is_date($str) {

        $stamp = strtotime($str);
        if (!is_numeric($stamp))
            return FALSE;

        $month = date('m', $stamp);
        $day = date('d', $stamp);
        $year = date('Y', $stamp);

        if (checkdate($month, $day, $year))
            return TRUE;

        return FALSE;
    }

}
