<?php
/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */
global $sssssextra;
if($_SERVER['REQUEST_URI'] == '/category/events/' || $_SERVER['REQUEST_URI'] == '/category/'){
header('Location:http://www.konsult.com/health-tips/');
exit;
}

?>
<!DOCTYPE html>
<?php 
	if( $_GET && key_exists('mfn-rtl', $_GET) ):
		echo '<html class="no-js" lang="ar" dir="rtl">';
	else:
?>
<html class="no-js" <?php language_attributes(); ?> <?php mfn_tag_schema(); ?>>
<?php endif; ?>

<!-- head -->
<head>

<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="msvalidate.01" content="AA5344229C960043B92BF47270B21EC8" />
    <?php 
    $alphabetis = get_query_var( 'alphabet', 'a' ); 
    if($alphabetis == "a"){
    if ( is_singular() ) echo '<link rel="canonical" href="' . get_permalink() . '" />'; 
    }
    else{
        if ( is_singular() ) echo '<link rel="canonical" href="' . get_permalink() . '/'.$alphabetis.'" />';
    }
    ?>
    
<?php if( mfn_opts_get('responsive') ) echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
 ?>
<?php
    if(is_page(4523)){
        ?>
    <title itemprop="name">List of top Indian doctors available on call | <?php echo $alphabetis; ?></title>
    <meta name="description" content="Get the list of top doctors available for tele-consultation through Konsult App. These doctors whose name starts with <?php echo $alphabetis; ?>, are available on call." />
    <meta name="title" content="" />
    <meta name="keyword" content="" />
    
    <?php
    }
    if(is_page(4132)){
        ?>
    
    <title itemprop="name">Top medical symptoms which can be treated through tele-consultation | <?php echo $alphabetis; ?></title>
    <meta name="description" content="Get the list of medical symptoms listed with alphabet <?php echo $alphabetis; ?>, which can be treated through Konsult App" />
    <meta name="title" content="" />
    <meta name="keyword" content="" />
    <?php
    }
    if(is_page(4480)){
        ?>
    <title itemprop="name"> Top specialities available for tele-consultation on Konsult App | <?php echo $alphabetis; ?></title>
    <meta name="description" content=" Get the list of specialities listed with alphabet <?php echo $alphabetis; ?>, which can be availed through Konsult App." />
    <meta name="title" content="" />
    <meta name="keyword" content="" />
    <?php
    }
    ?>

<?php if(is_attachment()){
  echo '<meta name="robots" content="noindex,follow" />';
} ?>
<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" class="shotcut" href="<?php mfn_opts_show( 'favicon-img', THEME_URI .'/images/favicon.ico' ); ?>" />	
<?php if( mfn_opts_get('apple-touch-icon') ): ?>
<link rel="apple-touch-icon" href="<?php mfn_opts_show( 'apple-touch-icon' ); ?>" />
<?php endif; ?>	

<!-- wp_head() -->
<?php wp_head(); ?>
<?php
if(is_front_page()){
	?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
}
elseif(is_page("doctor-registration")){
	?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
}elseif(is_page("careers")){
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<?php
}
else{}
    
    if(is_tag()){
        $taggg = single_tag_title( "", false );
        ?>
    <meta name="description" content="Read the health tips associated with <?php echo $taggg; ?> and get everything that you need to know about <?php echo $taggg; ?>.">
    <?php
    }
    
?>
    
</head>

<!-- body -->
<body <?php body_class(); ?>>


<ul class='offersul' style="display:none">
<?php

global $post;
$args = array( 'posts_per_page' => 5,'post_type' => 'offerelement' );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<li>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</li>
<?php endforeach; 
wp_reset_postdata();?>

</ul>
	<div class="headersearchresults">
	<ul class="resul">
	</ul>
	</div>
	<?php do_action( 'mfn_hook_top' ); ?>
	
	<?php //get_template_part( 'includes/header', 'sliding-area' ); ?>
	
	<?php if( mfn_header_style( true ) == 'header-creative' ) get_template_part( 'includes/header', 'creative' ); ?>
	
	<!-- #Wrapper -->
	<div id="Wrapper">
	
		<?php 
			// Header Featured Image ----------
			$header_style = '';
			
			// Image -----
			// if( mfn_ID() && ! is_search() ){
				// $sssssextra = "Setted";
				// if( ( ( mfn_ID() == get_option( 'page_for_posts' ) ) || ( get_post_type() == 'page' ) ) && has_post_thumbnail( mfn_ID() ) ){
					
					// // Pages & Blog Page ---
					// $subheader_image = wp_get_attachment_image_src( get_post_thumbnail_id( mfn_ID() ), 'full' );
					// $header_style .= ' style="background-image:url('. $subheader_image[0] .');"';

				// } elseif( get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) ){

					// // Single Post ---
					// $header_style .= ' style="background-image:url('. get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) .');"';

				// }
			// }
			?>
			
			<?php
			// Attachment -----
			// if( mfn_opts_get('img-subheader-attachment') == 'fixed' ){
				// $header_style .= ' class="bg-fixed"';
			// } elseif( mfn_opts_get('img-subheader-attachment') == 'parallax' ){
				// $header_style .= ' class="bg-parallax" data-stellar-background-ratio="0.5"';
			// }
		?>
		
		<?php //if( mfn_header_style( true ) == 'header-below' ) echo mfn_slider(); ?>
	
		<!-- #Header_bg -->
		<div id="Header_wrapper" <?php echo $header_style; ?>>
	
			<!-- #Header -->
			<header id="Header">
				<?php if( mfn_header_style( true ) != 'header-creative' ) get_template_part( 'includes/header', 'top-area' ); ?>	
				<?php //if( mfn_header_style( true ) != 'header-below' ) echo mfn_slider(); ?>
			</header>
				<?php
if(!is_front_page()){
	
				if( ( mfn_opts_get('subheader') != 'all' ) && ! get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ){
					
					if( is_search() ){
						// Page title -------------------------
						
						echo '<div id="Subheader">';
							echo '<div class="container">';
								echo '<div class="column one">';
								
									global $wp_query;
									$total_results = $wp_query->found_posts;
									
									$translate['search-results'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-results','results found for:') : __('results found for:','betheme');								
									echo '<h1 class="title">'. $total_results .' '. $translate['search-results'] .' '. get_search_query() .'</h1>';
									
								echo '</div>';
							echo '</div>';
						echo '</div>';
						
					} elseif( ! mfn_slider() || mfn_opts_get( 'subheader-slider-show' ) ){
						// Page title -------------------------
						
						// Subheader | Options
						$subheader_options = mfn_opts_get( 'subheader' );

						if( is_array( $subheader_options ) && isset( $subheader_options['hide-subheader'] ) ){
							$subheader_show = false;
						} elseif( get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ){
							$subheader_show = false;
						} else {
							$subheader_show = true;
						}
						
						if( is_array( $subheader_options ) && isset( $subheader_options['hide-breadcrumbs'] ) ){
							$breadcrumbs_show = false;
						} else {
							$breadcrumbs_show = true;
						}
						
						if( is_array( $subheader_options ) && isset( $subheader_options['breadcrumbs-link'] ) ){
							$breadcrumbs_link = 'has-link';
						} else {
							$breadcrumbs_link = 'no-link';
						}
						
						
						
						
						
						

						
						
						
						// Subheader | Print
						if( $subheader_show ){
							echo '<div id="Subheader">';
								echo '<div class="container">';
									echo '<div class="column one">';
										
										// Title
										echo '<h1 class="title">'. $specializationData->name .'</h1>';
										//echo "arsh";
										// Breadcrumbs
										if( $breadcrumbs_show ) mfn_breadcrumbs( $breadcrumbs_link );
										
									echo '</div>';
								echo '</div>';
							echo '</div>';
						}
						
					}
					
				}
				}
			?>
		
		</div>
		
		<?php //do_action( 'mfn_hook_content_before' ); ?>
