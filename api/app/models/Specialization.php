<?php

class Specialization extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'specialization_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'specialization_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    public function getSubSpecializations($params = array()) {

        $specializationIds = $params['specializationIds'];
        if (is_int($specializationIds) || is_string($params['specializationIds'])) {
            $specializationIds = explode(',', $params['specializationIds']);
        }

        $subSpecializationIds = $specializationIds;
        foreach ($specializationIds as $specializationId) {

            $subSpecializationId = Specialization::where('specialization_id', '=', $specializationId)->pluck('sub_specialization_ids');

            if (!empty($subSpecializationId)) {

                $subSpecializationId = trim($subSpecializationId);

                //STRING SHOULD ONLY CONTAINS COMMA AND NUMBER
                $stringToArray = str_split($subSpecializationId);
                $isStringCorrupt = 0;

                foreach ($stringToArray as $stringElement) {

                    if (!is_numeric($stringElement) && $stringElement != ',') {
                        $isStringCorrupt = 1;
                        break;
                    }
                }

                if ($isStringCorrupt) {
                    continue;
                }

                $subSpecializationId = explode(',', $subSpecializationId);
                $subSpecializationIds = array_merge($subSpecializationIds, $subSpecializationId);
            }
        }

        $subSpecializationIds = array_unique($subSpecializationIds);
        $subSpecializationIds = implode(',', $subSpecializationIds);

        return $subSpecializationIds;
    }

    public function prioritySpecialization() {

        $details = DB::table('specialization_master')
                ->orderBy('priorities', 'DESC')->take(15)
                ->select('specialization_id', 'name', 'photo_url', 'priorities')
                ->get();

        for ($i = 0; $i < count($details); $i++) {
            if (!empty($details[$i]->photo_url)) {
                $details[$i]->photo_url = $details[$i]->photo_url;
            }
        }

        return $details;
    }

}
