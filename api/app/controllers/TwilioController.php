<?php

class TwilioController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function CallConnect($callId) {

        $call = Call::where('call_id', '=', $callId)->first();

        $telecom_request = json_decode($call->telecom_request);

        if (empty($call) || empty($call->call_id) || $call->telecom_id != 2) {
            return;
        }

        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->callConnect(array('call_id' => $telecom_request->call_id, 'docMobile' => $telecom_request->docMobile, 'max_call_duration' => $telecom_request->max_call_duration, 'patMobile' => $telecom_request->patMobile));

        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }

    public function operatorOption($callId) {

        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->twilioOperatorOption(array('call_id' => $callId));

        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }

    public function operatorConnect() {

        $input = Input::all();

        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->twilioOperatorConnect($input);

        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }

    public function getCapabilityToken() {

        $checkAuthToken = True;
        $result = $this->checkAuth($checkAuthToken);
        if ($result['config_status'] == -1) {
            return ($result);
        } elseif ($result['config_status'] == 0) {
            $this->setHeader('RESPONSE_CODE', 0);
            return Response::make($result, 200, $this->headers);
        }

        $this->setHeader('AUTH_TOKEN', $this->authToken);

        $user_id = $this->getUserIdbyToken($this->authToken);

        if (empty($user_id)) {
            $result['status'] = 0;
            $result['msg'] = "Invalid user id";
            return Response::make($result, 200);
        }

        $this->setHeader('RESPONSE_CODE', '1');

        $twilioHelper = $this->helper->load('twilio');
        $token = $twilioHelper->generateCapabilityToken(array('user_id' => $user_id));

        $result['msg'] = 'Capability token';
        $result['status'] = 1;
        $result['token'] = $token;
        return Response::make($result, 200, $this->headers);
    }

    public function operatorCallInfo($callId) {

        $input = Input::all();
        if ($callId) {

            $callFrom = DB::table('calls')->join('users', 'users.user_id', '=', 'calls.caller_id')->where('call_id', '=', $callId)->pluck('mobile');

            $input['Transactionid'] = $callId;
            $input['CallSid'] = $input['CallSid'];
            $input['CallTo'] = $input['To'];
            $input['CallFrom'] = $callFrom;
            $input['Created'] = $input['Timestamp'];

            if ($input['CallStatus'] == 'completed') {
                $input['CallStatus'] = $input['customerCallStatus'] = 'Connected';
                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['total_call_duration'];
                if (isset($input['RecordingDuration'])) {
                    $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['RecordingDuration'];
                }

                Call::whereRaw('call_id = ' . $callId)->update(array('operator_call_duration' => $input['DialCallDuration']));
            } else {

                if ($input['CallStatus'] == 'no-answer' || $input['CallStatus'] == 'busy') {
                    $input['CallStatus'] = 'Missed';
                } else {
                    $input['CallStatus'] = 'NotConnected';
                }

                $input['customerCallStatus'] = 'Connected';

                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = 0;
            }
        }

        $result['status'] = 1;
        $result['msg'] = "Data Successfully inserted";
        $this->setHeader('RESPONSE_CODE', '1');
        return Response::make($result, 200, $this->headers);
    }

    public function connectISDCall() {

        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $input = Input::all();
            $caller_phone = $input['caller_phone'];
            $receiver_phone = $input['receiver_phone'];
            $max_call_duration = $input['max_call_duration'];
            $caller_id = $input['caller_id'];

            $twilioHelper = $this->helper->load('twilio');

            $twilioParams = array('caller_phone' => $caller_phone, 'receiver_phone' => $receiver_phone, 'max_call_duration' => $max_call_duration);

            $isdCall = new ISDCall();
            $isdCall->caller_id = $caller_id;
            $isdCall->receiver_phone = $receiver_phone;
            $isdCall->telecom_request = json_encode($twilioParams);
            $isdCall->save();
            
            $twilioParams = array_merge($twilioParams, array('isd_call_id' => $isdCall->isd_call_id));

            $result['twilio'] = $twilioHelper->initiateISDCall($twilioParams);
            if (isset($result['twilio']['status']) && !empty($result['twilio']['status'])) {
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.CALL_PLACED');
            } else {
                $result['status'] = 0;
                $result['error_msg'] = $result['twilio']['message'];
                $result['msg'] = 'Service provider failure!';
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function callISDConnect($isdCallId) {

        $call = ISDCall::where('isd_call_id', '=', $isdCallId)->first();

        $telecom_request = json_decode($call->telecom_request);

        if (empty($call) || empty($call->isd_call_id)) {
            return;
        }

        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->ISDCallConnect(array('isd_call_id' => $call->isd_call_id, 'receiver_phone' => $telecom_request->receiver_phone, 'max_call_duration' => $telecom_request->max_call_duration, 'caller_phone' => $telecom_request->caller_phone));

        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }

    public function isdCallInfo() {

        $input = Input::all();
        $isd_call_id = !empty($input['isd_call_id']) ? $input['isd_call_id'] : 0;

        if ($isd_call_id) {

            $isd_call = ISDCall::where('isd_call_id', '=', $isd_call_id)->first();

            if ($isd_call) {
                if ($isd_call->caller_call_status_id != 4 || $isd_call->receiver_call_status_id != 4) {
                    $result['status'] = 0;
                    $result['msg'] = "Communication data already inserted!";
                    $this->setHeader('RESPONSE_CODE', '0');
                    return Response::make($result, 200, $this->headers);
                }
            } else {
                $result['status'] = 0;
                $result['msg'] = "Communication doesn't exist!";
                $this->setHeader('RESPONSE_CODE', '0');
                return Response::make($result, 200, $this->headers);
            }

            if ($input['CallStatus'] == 'completed') {
                $input['CallStatus'] = $input['customerCallStatus'] = 'Connected';
                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['total_call_duration'];
                if (isset($input['RecordingDuration'])) {
                    $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['RecordingDuration'];
                }
            } else {

                if ($input['CallStatus'] == 'no-answer' || $input['CallStatus'] == 'busy') {
                    $input['CallStatus'] = 'Missed';
                } else {
                    $input['CallStatus'] = 'NotConnected';
                }

                $input['customerCallStatus'] = 'Connected';

                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = 0;
            }

            $data = Array();
            if (!empty($input['CallSid'])) {
                $data['CallSid'] = $input['CallSid'];
            } if (!empty($input['CallFrom'])) {
                $data['CallFrom'] = $input['CallFrom'];
            } if (!empty($input['CallTo'])) {
                $data['CallTo'] = $input['CallTo'];
            } if (!empty($input['CallStatus'])) {
                $data['CallStatus'] = $input['CallStatus'];
            } if (!empty($input['Created'])) {
                $data['Created'] = $input['Created'];
            } if (!empty($input['DialCallDuration'])) {
                $data['DialCallDuration'] = $input['doctor_call_duration'];
            } if (!empty($input['StartTime'])) {
                $data['StartTime'] = $input['StartTime'];
            } if (!empty($input['EndTime'])) {
                $data['EndTime'] = $input['EndTime'];
            } if (!empty($input['CallType'])) {
                $data['CallType'] = $input['CallType'];
            } if (!empty($input['Transactionid'])) {
                $data['Transactionid'] = $input['Transactionid'];
            } if (!empty($input['customerCallStatus'])) {
                $data['customerCallStatus'] = $input['customerCallStatus'];
            } if (!empty($input['total_call_duration'])) {
                $data['total_call_duration'] = $input['total_call_duration'];
            }

            if (!empty($input['dtmf'])) {
                $data['dtmf'] = 0;
                if ($input['dtmf'] == '1' || $input['dtmf'] == 1) {
                    $data['dtmf'] = 1;
                }
            }

            $data['CallStatus'] = strtolower($data['CallStatus']);
            $data['CallStatus'] = ucfirst($data['CallStatus']);
            $data['customerCallStatus'] = strtolower($data['customerCallStatus']);
            $data['customerCallStatus'] = ucfirst($data['customerCallStatus']);

            if (!empty($data['customerCallStatus'])) {
                $CallStatusMaster = new CallStatusMaster();
                $isd_call->receiver_call_status_id = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['customerCallStatus']));
            }

            if (!empty($data['CallStatus'])) {
                $isd_call->caller_call_status_id = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['CallStatus']));
            }

            $isd_call->call_duration = $data['DialCallDuration'] ? $data['DialCallDuration'] : 0;
            $isd_call->telecom_response = json_encode($input);
            $isd_call->save();
            
            $result['status'] = 1;
            $result['response'] = $input;
            
            return Response::make($result, 200, $this->headers);
        }
    }
    
    public function medicalTourism() {       
        
        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->medicalTourism();
        
        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');
        
        if(!empty($_GET['From'])){
            
            $patientRaw = DB::connection('mysql2')->table('mt_patients')->select('patient_id')->where('mobile', '=', $_GET['From'])->first();
            if(empty($patientRaw->patient_id)) {
                $countryRaw = DB::connection('mysql2')->table('kc_patient_country')->select('country_id', 'name')->where('iso', '=', $_GET['FromCountry'])->first();
                
                $countryRaw->country_id = ($countryRaw->country_id ? $countryRaw->country_id : 91);
                
                $assignedToUser = DB::connection('mysql2')->table('mt_users')->select('user_id','name', 'mobile', 'phonecode')->where('auto_assign', '=', 1)->first();
                
                $autoCreation = DB::connection('mysql2')->table('mt_users')->select('user_id')->where('auto_creation', '=', 1)->first();
                
                $autoAssignPartner = DB::connection('mysql2')->table('mt_partners')->select('partner_id')->where('auto_assign', '=', 1)->first();
                
                $currentHour = date('H');
                if($currentHour > 17 && $currentHour <= 23) {
                    $nextActionIn = date('Y-m-d 10:00:00', strtotime('+1 days'));
                } elseif($currentHour >= 10 && $currentHour <= 17) {
                    $nextActionIn = date('Y-m-d H:i:s');
                } else {
                    $nextActionIn = date('Y-m-d 10:00:00');
                }
                
                $patientId = DB::connection('mysql2')->table('mt_patients')->insertGetId(['mobile' => $_GET['From'], 'name' => '', 'country_id' => $countryRaw->country_id, 'partner_id' => $autoAssignPartner->partner_id, 'created_by' => $autoCreation->user_id, 'assigned_to' => $assignedToUser->user_id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'next_action_in' => $nextActionIn]);
                
                if(!empty($patientId)) {
                    
                    //SEND SMS
                    $smsText = Config::get('constants.SMS_FIRST_COMMUNICATION');
                    if ($smsText) {

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $smsParams['Text'] = str_replace(array('<NAME>', '<PATIENT_NUMBER_NAME>', '<TIME>'), array($assignedToUser->name, "(#$patientId)", '12 hrs'), $smsText);
                        $smsParams['To'] = $assignedToUser->phonecode.$assignedToUser->mobile;
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }                    
                    
                    $toCountryRaw = DB::connection('mysql2')->table('kc_patient_country')->select('name')->where('iso', '=', $_GET['ToCountry'])->first();                    
                    
                    $comment = "Received a missed call on ".$_GET['Called']." ($toCountryRaw->name) from ".$_GET['From'];
                    DB::connection('mysql2')->table('mt_comments')->insert(['subject_type' => 'patient', 'subject_id' => $patientId, 'poster_id' => $autoCreation->user_id, 'comment' => $comment, 'created_at' => date('Y-m-d H:i:s'), 'type' => 'system']);
                }
            }
        }

        return $response;        
    }
    
    public function medicalTourismInitiateCall() {

        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $input = Input::all();
            $caller_phone = $input['caller_phone'];
            $receiver_phone = $input['receiver_phone'];
            $max_call_duration = $input['max_call_duration'];
            $user_id = $input['user_id'];
            $patient_id = $input['patient_id'];

            $twilioHelper = $this->helper->load('twilio');

            $twilioParams = array('caller_phone' => $caller_phone, 'receiver_phone' => $receiver_phone, 'max_call_duration' => $max_call_duration);
            
            $callId = DB::connection('mysql2')->table('mt_calls')->insertGetId(['user_id' => $user_id, 'patient_id' => $patient_id, 'caller_phone' => $caller_phone, 'receiver_phone' => $receiver_phone, 'telecom_request' => json_encode($twilioParams), 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);            
            
            $twilioParams = array_merge($twilioParams, array('call_id' => $callId));

            $result['twilio'] = $twilioHelper->medicalTourismInitiateCall($twilioParams);
            if (isset($result['twilio']['status']) && !empty($result['twilio']['status'])) {
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.CALL_PLACED');
            } else {
                $result['status'] = 0;
                $result['error_msg'] = $result['twilio']['message'];
                $result['msg'] = 'Service provider failure!';
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function medicalTourismcallConnect($callId) {

        $call = DB::connection('mysql2')->table('mt_calls')->select('*')->where('call_id', '=', $callId)->first();

        $telecom_request = json_decode($call->telecom_request);

        if (empty($call) || empty($call->call_id)) {
            return;
        }

        $twilioHelper = $this->helper->load('twilio');
        $twiml = $twilioHelper->medicalTourismCallConnect(array('call_id' => $call->call_id, 'receiver_phone' => $telecom_request->receiver_phone, 'max_call_duration' => $telecom_request->max_call_duration, 'caller_phone' => $telecom_request->caller_phone));

        $response = Response::make($twiml, 200);
        $response->header('Content-Type', 'text/xml');

        return $response;
    }

    public function medicalTourismCallInfo() {

        $input = Input::all();
        $call_id = !empty($input['call_id']) ? $input['call_id'] : 0;

        if ($call_id) {

            $call = DB::connection('mysql2')->table('mt_calls')->select('*')->where('call_id', '=', $call_id)->first();

            if ($call) {
                if ($call->caller_call_status_id != 4 || $call->receiver_call_status_id != 4) {
                    $result['status'] = 0;
                    $result['msg'] = "Communication data already inserted!";
                    $this->setHeader('RESPONSE_CODE', '0');
                    return Response::make($result, 200, $this->headers);
                }
            } else {
                $result['status'] = 0;
                $result['msg'] = "Communication doesn't exist!";
                $this->setHeader('RESPONSE_CODE', '0');
                return Response::make($result, 200, $this->headers);
            }

            if ($input['CallStatus'] == 'completed') {
                $input['CallStatus'] = $input['customerCallStatus'] = 'Connected';
                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['total_call_duration'];
                if (isset($input['RecordingDuration'])) {
                    $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['RecordingDuration'];
                }
            } else {

                if ($input['CallStatus'] == 'no-answer' || $input['CallStatus'] == 'busy') {
                    $input['CallStatus'] = 'Missed';
                } else {
                    $input['CallStatus'] = 'NotConnected';
                }

                $input['customerCallStatus'] = 'Connected';

                $input['total_call_duration'] = $input['CallDuration'];
                $input['DialCallDuration'] = $input['doctor_call_duration'] = 0;
            }

            $data = Array();
            if (!empty($input['CallSid'])) {
                $data['CallSid'] = $input['CallSid'];
            } if (!empty($input['CallFrom'])) {
                $data['CallFrom'] = $input['CallFrom'];
            } if (!empty($input['CallTo'])) {
                $data['CallTo'] = $input['CallTo'];
            } if (!empty($input['CallStatus'])) {
                $data['CallStatus'] = $input['CallStatus'];
            } if (!empty($input['Created'])) {
                $data['Created'] = $input['Created'];
            } if (!empty($input['DialCallDuration'])) {
                $data['DialCallDuration'] = $input['doctor_call_duration'];
            } if (!empty($input['StartTime'])) {
                $data['StartTime'] = $input['StartTime'];
            } if (!empty($input['EndTime'])) {
                $data['EndTime'] = $input['EndTime'];
            } if (!empty($input['CallType'])) {
                $data['CallType'] = $input['CallType'];
            } if (!empty($input['Transactionid'])) {
                $data['Transactionid'] = $input['Transactionid'];
            } if (!empty($input['customerCallStatus'])) {
                $data['customerCallStatus'] = $input['customerCallStatus'];
            } if (!empty($input['total_call_duration'])) {
                $data['total_call_duration'] = $input['total_call_duration'];
            }

            if (!empty($input['dtmf'])) {
                $data['dtmf'] = 0;
                if ($input['dtmf'] == '1' || $input['dtmf'] == 1) {
                    $data['dtmf'] = 1;
                }
            }

            $data['CallStatus'] = strtolower($data['CallStatus']);
            $data['CallStatus'] = ucfirst($data['CallStatus']);
            $data['customerCallStatus'] = strtolower($data['customerCallStatus']);
            $data['customerCallStatus'] = ucfirst($data['customerCallStatus']);

            $updateCallInfo = array();
            if (!empty($data['customerCallStatus'])) {
                $CallStatusMaster = new CallStatusMaster();
                $updateCallInfo['receiver_call_status_id'] = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['customerCallStatus']));
            }

            if (!empty($data['CallStatus'])) {
                $updateCallInfo['caller_call_status_id'] = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['CallStatus']));
            }

            $updateCallInfo['call_duration'] = $data['DialCallDuration'] ? $data['DialCallDuration'] : 0;
            $updateCallInfo['telecom_response'] = json_encode($input);
            $updateCallInfo['updated_at'] = date('Y-m-d H:i:s');
            
            DB::connection('mysql2')->table('mt_calls')->where('call_id', $call_id)->update($updateCallInfo);
            
            $comment = "Called to '$call->receiver_phone' from '$call->caller_phone'".". Call status: ".$data['CallStatus'];
            
            if(!empty($updateCallInfo['call_duration'])) {
                $comment .= " & call duration: ".$updateCallInfo['call_duration'];
            }
            
            DB::connection('mysql2')->table('mt_comments')->insert(['subject_type' => 'patient', 'subject_id' => $call->patient_id, 'poster_id' => $call->user_id, 'comment' => $comment, 'created_at' => date('Y-m-d H:i:s'), 'type' => 'system']);
            
            $result['status'] = 1;
            $result['response'] = $input;
            
            return Response::make($result, 200, $this->headers);
        }
    }    

}
