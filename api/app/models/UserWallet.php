<?php

class UserWallet extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'user_wallet';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'user_wallet_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

    public function getWalletInfoColumn($params = array()) {

        $query = DB::table('user_wallet AS uci')->where('uci.user_id', '=', $params['user_id']);

        $wallet_id = ($params['wallet_id']) ? $params['wallet_id'] : 3;

        $query->where('uci.wallet_id', '=', $wallet_id);

        $columnName = $params['columnName'] ? $params['columnName'] : 'id';

        $columnName = 'uci.' . $columnName;

        if ($params['checkExpiry']) {
            $row = $query->select('token', 'expires')->first();
            $expired = 1;
            if (!empty($row) && !empty($row->expires)) {
                $expired = (($row->expires - time()) > 0) ? 0 : 1;
            }

            $token = !empty($row->token) ? $row->token : NULL;

            if (!empty($expired) && !empty($token)) {
                $status = 2; //TOKEN EXPIRED
            } elseif (empty($expired) && !empty($token)) {
                $status = 1; //OK
            } else {
                $status = 0; //SOMETHING WRONG
            }

            return array('status' => $status, 'token' => $token);
        }

        return $query->pluck($columnName);
    }

}
