<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('fetchMVCBalance')) {

    function fetchMVCBalance($params = array()) {

        $emailId = $params['emailId'];
        $mobile = '';

        $MerchantID = Merchant_Id;
        $PartnerID = Parterner_Id;
        $Password = PassWord;
        $mvcurl = CITRUS_VIRTUAL_URL . Get_Balance;
        $headerValue = Content_Type;
        $data = '{"MerchantID": "' . $MerchantID . '", "CampaignCode": "KONSULTCASH", "Email": "' . $emailId . '", "Mobile": "' . $mobile . '", "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';
        $user_citrus_mvc = httpPostWithHeader($mvcurl, $headerValue, $post = 1, $data);
        $user_citrus_mvc = json_decode($user_citrus_mvc, true);

        if (empty($user_citrus_mvc['d'])) {
            $user_citrus_mvc['errorMsg'] = 'Unable to fetch MVC balance.';
        }

        $user_citrus_mvc['mvcAmount'] = $user_citrus_mvc['d']['Campaigns'][0]['Amount'];

        return $user_citrus_mvc;
    }

}

if (!function_exists('fetchRealBalance')) {

    function fetchRealBalance($params = array()) {

        $citrusToken = $params['citrusToken'];

        $user_token = CITRUS_AUTHTOKEN_PREFIX . $citrusToken;
        $url = CITRUS_URL . CITRUS_BALANCE;
        $user_citrus = httpGetWithHeader($url, $user_token);
        $user_citrus = json_decode($user_citrus, true);

        if (!empty($user_citrus['error'])) {
            $user_citrus['errorMsg'] = 'Unable to fetch main balance.';
        }

        return $user_citrus;
    }

}

if (!function_exists('httpGetWithHeader')) {

    function httpGetWithHeader($url, $headerValue, $post = Null, $data = Null) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization:' . $headerValue));
        if ($post == 1) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}

if (!function_exists('httpPostWithHeader')) {

    function httpPostWithHeader($url, $headerValue, $post = Null, $data = Null) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:' . $headerValue));
        if ($post == 1) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}

