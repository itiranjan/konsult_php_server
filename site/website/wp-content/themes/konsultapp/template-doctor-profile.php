<?php
/**
 * Template Name: Doctor Profile
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */
get_header('profile');



ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

//$docRow = $my_wpdb->get_row("select u.user_id,CONCAT(u.salutation, ' ', u.name) AS name,up.address,up.is_doctor,cm.name AS city,up.photo,up.photo_url,dp.total_experience,ds.specialization,u.email,dp.area,dp.other_specializations from users u LEFT JOIN user_profile up ON (u.user_id=up.user) LEFT JOIN doctor_profile dp ON (up.user=dp.user) LEFT JOIN doctor_specialization_rel ds ON(dp.user=ds.doctor) LEFT JOIN city_master cm ON up.city=cm.city_id where user_id='$user_id'");
//print_r($docRow) ;
//echo $user_id ;





//print_r($qualifications);
/*Value Collected
name
address
city
photo
total experince 
specializationId
specializationName
email
degreeName
universityName
collegeName
hospitalName
designationName
area
*/

$docRoww = $my_wpdb->get_results("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, dp.address, up.is_doctor, cm.name AS city, up.photo, up.photo,
dp.total_experience,dsr.specialization_id as specializationID, dsm.name as specializationName , u.email, dp.area, dp.about as about, dp.other_specializations, dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user_id )
LEFT JOIN doctor_profile dp ON ( up.user_id = dp.user_id )
LEFT JOIN doctor_qualifications dq ON ( up.user_id = dq.user_id )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user_id = dsr.user_id )
LEFT JOIN specialization_master dsm ON ( dsr.specialization_id = dsm.specialization_id )

LEFT JOIN doctor_workplace dex ON ( dp.user_id = dex.user_id )


LEFT JOIN city_master cm ON dp.city_id = cm.city_id
WHERE dp.user_id ='$user_id'");
?>
				<div class="Sahil" style="display:none">
				<?php
				$eddd =array();
				$qddd =array();
			foreach($docRoww as $docR){
				 if (!in_array($docR->degreeName, $eddd)) {
    array_push($eddd,"<b>".$docR->degreeName."</b>"." - ".$docR->collegeName);
    }
				 if (!in_array($docR->hospitalName, $qddd)) {
    array_push($qddd,"<b>".$docR->designationName."</b>"." - ".$docR->hospitalName);
    }
				//echo $docR->degreeName."<br>";
				//echo $docR->collegeName."<br>";
				//echo $docR->hospitalName."<br>";
				//echo $docR->designationName;
				
			}
			?>
				</div>
				



<script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

 	<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jssocials.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jssocials-theme-flat.css" />


<script>
//alert("hello");
$(function() {
var showTotalChar = 190, showChar = "More (+)", hideChar = "Less (-)";
$('.show').each(function() {
var content = $(this).text();
if (content.length > showTotalChar) {
var con = content.substr(0, showTotalChar);
var hcon = content.substr(showTotalChar, content.length - showTotalChar);
var txt= con +  '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
$(this).html(txt);
}
});
$(".showmoretxt").click(function() {
if ($(this).hasClass("sample")) {
$(this).removeClass("sample");
$(this).text(showChar);
} else {
$(this).addClass("sample");
$(this).text(hideChar);
}
$(this).parent().prev().toggle();
$(this).prev().toggle();
return false;
});
});
</script>
<!-- #Content -->
<div id="Content">
    <div class="content_wrapper profile_width clearfix">

        <!-- .sections_group -->
       <div class="section_wrapper clearfix"><div class="items_group clearfix"><div class="column one-fourth column_photo_box "><div class="photo_box "><div class="image_frame"><div class="image_wrapper"><div class="mask"></div><img class="scale-with-grid" src="http://api.konsultapp.com/photo/<?php if($docRow->photo != null){ echo $docRow->photo; }else{ echo "user.png"; }  ?>" alt="<?php echo $name.", ".$specialization.", ".$hospitalName ;  ?>" /></div></div></div>
</div><div class="column three-fourth column_column "><div class="column_attr align_left animate fadeInUp" data-anim-type="fadeInUp" style=" padding:0px;"><div class="column_attr animate fadeInLeft" data-anim-type="fadeInLeft" style="padding:10px 10px 0 0;"><div><h1><?php echo $name ;  ?></h1>
<?php  echo $degreeName ;  ?>, <?php  echo $specialization ;   ?>, <?php  echo $designationName ;  ?> - <?php  echo $hospitalName ;  ?></div>

<div class="learn-more-btn rgt align_right ">
		<a href="#" id="onclick">  <span class="learn-more-txt feed">Give Feedback</span> </a>
	</div></div>

<div class="column_attr align_left animate fadeInUp" data-anim-type="fadeInUp" style=" background-color:rgba(238, 238, 238, 0.37); padding:7px 5px 0px; margin:10px 0 5px 0;float: left; width: 100%;">

<div class="column one-third column_column " style="margin-bottom: 5px;"><div class="column_attr" style=""><img class="scale-with-grid tst" src="http://test.konsultapp.com/wp-content/uploads/2017/02/icon1.png" alt="" style="width: 20px;float: left;margin-right: 5px;"><div style="padding-top:0px;"><b>Expierience: </b> <?php  echo $docRow->total_experience ;  ?> Years</div></div></div>

<div class="column two-third column_column " style="margin-bottom: 5px;"><div class="column_attr" style="">

<img class="scale-with-grid tst" src="http://test.konsultapp.com/wp-content/uploads/2017/02/icon2.png" alt="" style="width: 20px;float: left;margin-right: 5px;"><div style="padding-top: 0px;"><b>Fee :</b> <i class="fa fa-inr" aria-hidden="true"></i><?php echo $per_min_charges."/min"; ?></div></div></div></div>


<p>
<div class="show"><?php  echo $docRow->about ;  ?></div>
	
	</p>

	
	<div class="column one-second column_column "><div id="shareRoundIcons"></div></div>
		   
		  <script>
        $("#shareRoundIcons").jsSocials({
    showLabel: false,
    showCount: false,
    shares: [ "twitter", "facebook",  "linkedin", "pinterest"]
});

    </script> 
		   
	<div class="column one-second column_column "><div class="learn-more-btn rgt align_right1">
		<a href="<?php echo get_home_url()."/download-app/" ?>">  <span class="learn-more-txt">Consult Now</span> </a>
	</div></div></div></div>

<h1 class="tit">Profile of <?php  echo $docRow->name ;  ?></h1><br>



<!-- practice starts here -->
<div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp">
                                   <div class="round1">
                                     <i class="fa fa-plus-square" aria-hidden="true"></i>
                                </div><b>Practice At:</b> <br><?php echo "<div class='bbbtb'>".implode("<br />",array_unique($qddd)).'</div>';//echo $hospitalName ; ?></div></div>	
<!-- practice ends here -->	
	<div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp">
                                   <div class="round1">
                                     <i class="fa fa-user-md" aria-hidden="true"></i>
									
                                </div><b>Speciality:</b><br><?php  echo "<a href='/speciality/".str_replace(' ', '-', strtolower($specialization))."' >".$specialization."</a>" ; ?></div></div>

<div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp"><div class="round1">
                                    <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                </div><b>Education:</b><br><?php  //echo $degreeName ;
///if($collegeName != ""){
								//echo " - $collegeName" ; 
//}
echo "<div class='bbbtb'>".implode("<br />", array_unique($eddd)).'</div>';
								?></div></div>
<?php

?>
<div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp"><div class="round1">
                                     <i class="fa fa-globe" aria-hidden="true"></i>
                                </div><b>Language Spoken:</b><br><?php  echo "English, Hindi" ; ?></div></div>
                    
		   <?php
		   
	//echo "SELECT doctor FROM doctor_specialization_rel where doctor_specialization_id = '$specialId'  ORDER BY RAND() LIMIT 10" ;
$award = $my_wpdb->get_results("SELECT award FROM doctor_awards where user_id = '$user_id' ");
		   
		  if(count($award) != 0)  {
		   ?>
		   
                               <div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp"><div class="round1">
                                     <i class="fa fa-trophy" aria-hidden="true"></i>
                                </div>
								<div class="serv1">
								<b>Award/ Recognition:</b><br>
								<ul class="awardandrec">
						<?php	

foreach ($award as $aw) {
	
	?>
									
									
								<li><?php  echo $aw->award ; ?></li>
								<?php } ?>
								</ul>
								</div>
								</div></div>
                 <?php } ?> 
		   
		   
		   <?php
		  // echo $user_id ;
	$service = $my_wpdb->get_results("SELECT name FROM  services_master ser LEFT JOIN doctor_service_rel docser ON (ser.service_id=docser.service_id) where docser.user_id = '$user_id' ");
		 
		 //  print_r($service);
		   if(count($service) != 0)  {
		   ?>
		   
 <div class="column one column_column "><div class="column_attr align_left animate fadeInUp exp-box" data-anim-type="fadeInUp"><div class="round1">
                                     <i class="fa fa-plus" aria-hidden="true"></i>
                                </div>
								<div class="serv1"><div><b>Services:</b></div>	
									<?php	

foreach ($service as $ser) {
	
	?>	
								<div class="serv"><i class="fa fa-check-circle-o" aria-hidden="true"></i> <?php  echo $ser->name ; ?></div>
								<?php } ?>	
								</div>
							<?php } ?>	
								</div></div>
	


<h1 class="tit">Other <?php   echo $specialization ;   ?> for Telephonic Consultation</h1>
<div id="prof" class="column one column_portfolio_slider"><div class="portfolio_slider  arrows arrows_always">
<a class="slider_nav slider_prev themebg" href="#" style="display: block;"><i class="icon-left-open-big"></i></a>
<a class="slider_nav slider_next themebg" href="#" style="display: block;"><i class="icon-right-open-big"></i></a>
<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 100%; height: 250px; margin: 0px; overflow: hidden; cursor: move;">

<ul class="portfolio_slider_ul" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 4784px; height: 4px; list-style-type: none; z-index: auto;">

<?php
	//echo "SELECT doctor FROM doctor_specialization_rel where doctor_specialization_id = '$specialId'  ORDER BY RAND() LIMIT 10" ;
$specialIdRand = $my_wpdb->get_results("SELECT doctor_specialization_rel.user_id FROM doctor_specialization_rel JOIN doctor_profile on doctor_profile.user_id = doctor_specialization_rel.user_id where doctor_specialization_rel.specialization_id = '$specialId' AND doctor_profile.is_approved = 1 ORDER BY RAND() LIMIT 10");	

foreach ($specialIdRand as $doc) {	
$docId = 	$doc->user_id ;
	
$docRow = $my_wpdb->get_row("select u.user_id,CONCAT(u.salutation, ' ', u.name) AS name,dp.address,up.is_doctor,cm.name AS city,up.photo,up.photo,dp.total_experience, dp.per_min_charges as charges, dp.page_url as pageUrl, ds.specialization_id,u.email,dp.area,dp.other_specializations from users u LEFT JOIN user_profile up ON (u.user_id=up.user_id) LEFT JOIN doctor_profile dp ON (up.user_id =dp.user_id) LEFT JOIN doctor_specialization_rel ds ON(dp.user_id=ds.user_id) LEFT JOIN city_master cm ON dp.city_id=cm.city_id where dp.user_id='$docId'");
	
?>	
	
	
<li class="sdoctorsprofiegrid">
<div class="icon_box icon_position_top no_border">
		<a href="<?php echo get_home_url().'/doctor-profile/'.$docRow->pageUrl ; ?>/"> 
		<!--<a href="http://test.konsultapp.com/doctor-profile/<?php // echo $docRow->pageUrl ; ?>/"> -->
			<div class="image_wrapper why-use-img prof-pic">
                
    <?php
    $imageavailabl = 0;
    if(file_exists(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/konnect/api/public/photo/'.$docRow->photo)){
        $imageavailabl = 1;
    }
    else{}
   
    if($docRow->photo != null AND $imageavailabl == 1){
        ?>
                <img src='http://api.konsultapp.com/photo/<?php  echo $docRow->photo ;  ?>' alt="<?php  echo $docRow->name ;  ?>" class="prof-pc" align="middle">
              
                <?php
    }
    else{
        ?>
                  <img src='http://api.konsultapp.com/photo/user.png' alt="<?php  echo $docRow->name ;  ?>" class="prof-pc" align="middle" alt="<?php  echo $docRow->name.", ".$specialization ;  ?>">
                <?php
    }
                ?>
				
			</div>
			<div class="desc_wrapper">
				<div class="member-name"><b><?php  echo $docRow->name ;  ?></b></div>
			<div class="desc service-desc"><?php   echo $specialization ; ?><br>
			Expierience: <?php  echo $docRow->total_experience ; ?> Years<br>
			Fee:  <i class="fa fa-inr" aria-hidden="true"></i> <?php  echo $docRow->charges ; ?>/min

		</div>
			
		
			</div>
		</a>
	</div></li>
	<!-- css for live website -->
	<style>
	
	h1.title{
		font: inherit !important;
    font-size: 15px !important;
	padding: 4px 0px 0px 0px;
color: white !important;
	}
	h1{
    font: inherit;
    font-size: 25px;
}
	.portfolio_slider .slider_nav:hover{
		height: 60px ;
line-height: 60px ;
width: 30px ;
	opacity:1 ;
	}
	.portfolio_slider .slider_nav{
		height: 40px ;
line-height: 40px ;
width: 24px ;
opacity:.3 ;
transition-delay: .5s; /* delays for 1 second */
-webkit-transition-delay: .5s; /* for Safari & Chrome */
	}
	.sdoctorsprofiegrid .member-name{
		color:black;
	}
	.sdoctorsprofiegrid{width: 216px !important;}
	@media only screen and (max-width: 500px) {
   .sdoctorsprofiegrid{width: 204px !important;}
}
.awardandrec{
	display: grid;
}
	</style>
<?php   }   ?>


	
	

	
	

	

	
	

	

</ul></div></div>
</div>


</div></div>



</div>

	</div>
</div>

<!-- #Content -->
<div id="Content">
    <div class="content_wrapper clearfix">

        <!-- .sections_group -->
        <div class="sections_group">

            <div class="entry-content" itemprop="mainContentOfPage">
<?php
while (have_posts()) {
    the_post();       // Post Loop
    mfn_builder_print(get_the_ID()); // Content Builder & WordPress Editor Content
}
?>
            </div>

                <?php if (mfn_opts_get('page-comments')): ?>
                <div class="section section-page-comments">
                    <div class="section_wrapper clearfix">

                        <div class="column one comments">
                <?php comments_template('', true); ?>
                        </div>

                    </div>
                </div>
                        <?php endif; ?>


        </div>

        <!-- .four-columns - sidebar -->


    </div>
</div>

<!--Contact Form -->
<div id="contactdiv">
<form class="conform" action="#" id="contact">
<a  class="img1" id="cancel"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
<h3>Contact Form</h3>
<hr/>
<label>Name: <span>*</span></label>
<input type="text" id="name" placeholder="Name"/>
<label>Email: <span>*</span></label>
<input type="text" id="email" placeholder="Email"/>
<label>Contact No: <span>*</span></label>
<input type="text" id="contactno" placeholder="10 digit Mobile no."/>
<label>Message:</label>
<textarea id="message" placeholder="Message......."></textarea>
<input class="inp" type="button" id="send" value="Send"/>
<input class="inp" type="button" id="cancel" value="Cancel"/>
</form>
</div>



<div id="thanksdiv">
<div id="thanksbox">
<a  class="img1" id="closebox"><i class="fa fa-times-circle" aria-hidden="true"></i></a><br />

<div class="thanks-txt">Thanks To Give FeedBack, Our Team Will Contact You Soon<br /><br />



<input class="inp" type="button" id="close" value="Close"/>	</div><br />
</div>	
	
	
	

</div>


<script>
$(document).ready(function() {

	
	
	
	
	

$("#onclick").click(function() {
$("#contactdiv").css("display", "block");
});
$("#contact #cancel").click(function() {
$(this).parent().parent().hide();
});
$("#thanksdiv #close").click(function() {
$(this).parent().parent().hide();
});
$("#thanksdiv #closebox").click(function() {
$(this).parent().parent().hide();
});
// Contact form popup send-button click event.
$("#send").click(function() {
var name = $("#name").val();
var email = $("#email").val();
var contact = $("#contactno").val();
var message = $("#message").val();
if (name == "" || email == "" || contactno == "" || message == ""){
alert("Please Fill All Fields");
}else{
if (validateEmail(email)) {
	
	
	var data = {
		'action': 'feedback_ajax_request',
		'doctor': '<?php echo $user_id ; ?>',
		'name': name,
		'email': email,
		'contact': contact,
		'message': message,
		
	};
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	$.post(ajaxurl, data, function(response) {
		
	$("#contactdiv").css("display", "none");
	$("#thanksdiv").css("display", "block");	
			
	});
	
	
	
	
	
	
	
}else {
alert('Invalid Email Address');
}
function validateEmail(email) {
var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
if (filter.test(email)) {
return true;
}else {
return false;
}
}
}
});
// Login form popup login-button click event.

});	

// $( document ).ready(function() {
	// //alert( $('#mek').attr('content'));
    // $('#mek').attr('content', '<?php echo "$name $specialization $city"; ?>');	
    // $('#medd').attr('content', '<?php echo "This is going to be the doctors description."; ?>');	
// });

</script>


<style>
.bbbtb{
margin-left: 50px;
}
    @media only screen and (min-width: 768px)
    {
body:not(.template-slider) #Content {
    min-height: 0px !important;
}
    }
</style>


        <?php get_footer(); ?>