<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class appLink extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('applink_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/appLink");
    }

    public function appLinkList($way) {

        $data = array();
        $resultsData = $this->applink_model->appLinkList($_POST);
        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'applinklog_id' => $row->applinklog_id,
                'mobile' => $row->mobile,
                'sent_count' => $row->sent_count,
                'created_at' => $row->created_at,
                'updated_at' => $row->updated_at,
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

}
