﻿CKEDITOR.editorConfig = function(config) {
    config.skin = 'v2';
    config.toolbar = 'MyToolbar';
    config.toolbar_MyToolbar =
        [
	       // { name: 'styles', items: ['Font', 'FontSize'] },
	        { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline'] },
	       // { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote'] },
			  { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent'] },
	        //{ name: 'links', items: ['Link', 'Unlink'] },
	       // { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', '-', 'Undo', 'Redo'] },
	      //  { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
	      //  { name: 'insert', items: ['Image', 'Video', 'Table', 'HorizontalRule', 'SpecialChar'] }
        ];
    config.width = '75%';
};