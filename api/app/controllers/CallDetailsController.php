<?php

class CallDetailsController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool $checkAuthToken (To Check Auth token)
     * @return  array
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    /**
     * @author : Anil Kumar
     * @created : 31 dec 2015
     * @description : This method is used for  finding latest Os Version and app Vresion
     * @access public
     * @return  array
     */
    public function latestVersion() {

        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $param = Input::all();

            $osVersion = new OsVersion();
            $detailArr = $osVersion->findosVersion($param);

            $msg = ($detailArr) ? Config::get('config_msg.OS_VersionMessage') : Config::get('config_msg.OS_VersionMessageInvalid');

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = $msg;
            $result['response']['list'] = $detailArr;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Anil Kumar
     * @created : 23 sep 2015
     * @description : This method is used for find the mvc bal with code and userid and issued to user account
     * @access public
     * @return  array
     */
    public function promodetails() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $param = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);
            $citrusHelper = $this->helper->load('citrus');

            $citrusModel = new UserWallet();
            $citrusEmail = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'columnName' => 'email'));

            if ($citrusEmail) {
                $data['Email'] = $citrusEmail;
                $data['user_id'] = $user_id;
                $data['promoCode'] = $param['promo_code'];
                $data['connect'] = $citrusHelper;

                $detailArr = array();
                $promoValidate = new CouponUsage();
                $filresult = $promoValidate->promo_code_model($data);
                $mkarr = json_decode($filresult, true);
                array_push($detailArr, $mkarr);

                $this->setHeader('AUTH_TOKEN', $this->authToken);
                $this->setHeader('RESPONSE_CODE', '1');

                $result['status'] = ($mkarr['success'] == "1") ? 1 : 0;
                $result['response']['list'] = $detailArr;

                //SEND SMS ALERT
                $smsText = Config::get('constants.NETCORE_SMS_TEXT_RECHARGE_WALLET');
                if ($smsText && !empty($result['status'])) {

                    $user = User::where('user_id', '=', $user_id)->select('name', 'mobile', 'email')->get();
                    if (!empty($user[0]['name']) && !empty($user[0]['mobile'])) {

                        //AVAILABLE MVC BALANCE IN WALLET         
                        $citrusHelper = $this->helper->load('citrus');
                        $mvcBalance = $citrusHelper->fetchMVCBalance(array('emailId' => $citrusEmail));

                        //AVAILABLE REAL BALANCE IN WALLET
                        $user_citrus_acc = UserWallet::where('user_id', '=', $user_id)->first();
                        $user_token = $user_citrus_acc['token'];
                        $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $user_token));

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $totalAvailableBalance = $mvcBalance['mvcAmount'] + $mainBalance['value'];
                        $smsParams['Text'] = str_replace(array('<User>', '<Value1>', '<Value2>'), array($user[0]['name'], "INR " . $mkarr['amount'], "INR " . $totalAvailableBalance), $smsText);
                        $smsParams['To'] = $user[0]['mobile'];
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                }
            } else {
                $this->setHeader('AUTH_TOKEN', $this->authToken);
                $this->setHeader('RESPONSE_CODE', '0');

                $result['status'] = 0;
                $result['msg'] = "Missing citrus mapping!";
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Anil Kumar
     * @created : 08 sep 2015
     * @description : This method is used for find the call duration of patiend/doctor 
     * @access public
     * @return  array
     */
    public function totalcalldetails() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $callDetailsList = $params = array();
            $communication = new Call();
            $params['receiver_id'] = $this->getUserIdbyToken($this->authToken);

            $data = Input::all();
            $locale = isset($data['locale']) ? $data['locale'] : '';
            $callTypeData = User::getCallTypeData(array('user_id' => $params['receiver_id'], 'locale' => $locale));

            $callDetailsData = $communication->callDetailsList($params);
            $doctorPatientMapping = new DoctorPatientMapping();
            foreach ($callDetailsData as $callDetail) {
                $response1['PhotoPath'] = User::getPhoto(array('photo' => $callDetail->photo, 'is_doctor' => 1));
                $response1['OtherPartyName'] = trim($callDetail->OtherParty);
                $response1['user_id'] = $callDetail->id;
                $response1['call_duration'] = empty($callDetail->call_duration) ? "null" : $callDetail->call_duration;
                $response1['comm_datetime'] = gmdate('Y-m-d H:i:s', strtotime($callDetail->comm_datetime));
                $response1['Bound'] = $callDetail->bound;
                $response1['call_charges'] = ($callDetail->bound == 'O') ? $callDetail->total_charges : $callDetail->doctor_charges;
                $response1['call_charges'] = $callTypeData['internationalCall'] ? round($response1['call_charges'] / $callTypeData['currencyFactor'], 2) : intval($response1['call_charges']);
                $callCharges = $response1['call_charges'];
                $response1['call_charges'] = empty($callCharges) ? "0" : "$callCharges";
                $response1['enableChat'] = $doctorPatientMapping->checkMapping(array('doctor_id' => $callDetail->id, 'patient_id' => $params['receiver_id'], 'type' => 'calling'));
                $json_response1 = json_encode($response1);
                $mkarr = json_decode($json_response1);
                array_push($callDetailsList, $mkarr);
            }

            if ($callDetailsList) {
                $msg = Config::get('config_msg.DT_LIST');
            } else {
                $msg = Config::get('config_msg.DT_LIST_NOT_FOUND');
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = $msg;

            $userObject = new User();
            $result['response']['user'] = $userObject->getUserColumn(array('user_id' => $params['receiver_id'], 'columnName' => 'name'));
            $result['response']['list'] = $callDetailsList;
            $result['response']['currency'] = $callTypeData['currency'];
            $result['response']['currencySymbol'] = $callTypeData['currencySymbol'];
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function callingFlags() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            $data = Input::all();
            $doctor_id = $data['doctor_id'];

            if (!empty($doctor_id)) {
                $result['is_allow_new_user'] = Doctor::where('user_id', '=', $doctor_id)->pluck('allow_new_user');

                $relationExist = DoctorPatientRelation::whereRaw('(doctor = ' . $doctor_id . ' and patient = ' . $user_id . ') or (patient = ' . $doctor_id . ' and doctor = ' . $user_id . ')')->first();

                if ($relationExist) {
                    $result['is_friend'] = $relationExist['is_approved'];
                } else {
                    $result['is_friend'] = 0;
                }

                $result['doctor_id'] = $doctor_id;
                $result['status'] = 1;
                $this->setHeader('RESPONSE_CODE', '1');
                return Response::make($result, 200, $this->headers);
            } else {
                $result['status'] = 0;
                $result['msg'] = 'Doctor id can not be blank.';
                return Response::make($result, 200, $this->headers);
            }
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function callBackRequest() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $data['request_from'] = $this->getUserIdbyToken($this->authToken);
            $data['request_to'] = $data['receiver_id'];
            $data['wallet_id'] = ($data['wallet_id']) ? $data['wallet_id'] : 3;

            $patientIsClinic = UserProfile::where('user_id', '=', $data['request_from'])->pluck('clinic_type');
            if ($patientIsClinic) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['status'] = 0;
                $result['errors'] = 'Konsult clinics are not allowed to send call back request.';
                return Response::make($result, 200, $this->headers);
            }

            $doctorExist = User::where('user_id', '=', $data['request_to'])->exists();

            $callbackReqestId = CallBackRequest::where('request_from', '=', $data['request_from'])->where('request_to', '=', $data['request_to'])->where('status', '=', 'open')->pluck('callback_request_id');
            if (!empty($callbackReqestId)) {

                CallBackRequest::where('callback_request_id', '=', $callbackReqestId)->update(array('updated_at' => date("Y-m-d H:i:s")));

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = 'Call back request updated successfully as one callback request is already open.';
            } else {

                $callBackRequest = new CallBackRequest();
                if (!$callBackRequest->validate($data)) {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $result['status'] = 0;
                    $result['errors'] = $callBackRequest->errors();
                } elseif (!$doctorExist) {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $result['status'] = 0;
                    $result['errors'] = 'Invalid receiver id.';
                } else {

                    $callBackRequest->request_from = $data['request_from'];
                    $callBackRequest->request_to = $data['request_to'];
                    $callBackRequest->wallet_id = $data['wallet_id'];
                    if (isset($data['communication_id'])) {
                        $callBackRequest->request_call_id = $data['communication_id'];

                        if (!empty($callBackRequest->wallet_id)) {
                            $wallet_id = Call::where('call_id', '=', $data['communication_id'])->pluck('wallet_id');
                            if (!empty($wallet_id)) {
                                $callBackRequest->wallet_id = $data['wallet_id'];
                            }
                        }
                    }
                    if (isset($data['per_min_charges'])) {
                        $callBackRequest->per_min_charges = $data['per_min_charges'];
                    }
                    if (isset($data['status'])) {
                        $callBackRequest->status = $data['status'];
                    }

                    $callBackRequest->save();

                    //SEND SMS TO DOCTOR
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_DOCTOR_CALLBACK_REQUEST');

                    $doctorMobile = User::where('user_id', '=', $callBackRequest->request_to)->pluck('mobile');
                    if ($smsText && $doctorMobile) {

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $smsParams['Text'] = str_replace(array('<DOCTOR_NAME>', '<PATIENT_NAME>'), array('Doctor', 'Patient'), $smsText);
                        $smsParams['To'] = $doctorMobile;
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }

                    $this->setHeader('RESPONSE_CODE', '1');
                    $result['status'] = 1;
                    $result['msg'] = 'Call back request sent successfully.';
                }
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function userCallBackRequests() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $params = array();
            $params['caller_id'] = $this->getUserIdbyToken($this->authToken);

            $callBackRequest = new CallBackRequest();
            $callBackRequests = $callBackRequest->callBackUserList($params);

            $requestCount = Count($callBackRequests);

            if ($requestCount) {
                $requestList = Array();

                foreach ($callBackRequests as $key => $value) {
                    $requestList[$key] = $value;
                    $value->created_at = gmdate('Y-m-d H:i:s', strtotime($value->created_at));
                    $value->request_date = gmdate('Y-m-d H:i:s', strtotime($value->updated_at));
                    $value->photo = User::getPhoto(array('photo' => $value->photo, 'is_doctor' => 1));
                    $doctor_id = $requestList[$key]->user_id;
                    $requestList[$key]->enableChat = 1;
                    $experience = DB::select("select address, hospital_name from doctor_workplace where user_id='$doctor_id'");
                    foreach ($experience as $doctexp) {
                        $requestList[$key]->address = empty($doctexp->address) ? '' : $doctexp->address;
                        $requestList[$key]->hospital_name = empty($doctexp->hospital_name) ? '' : $doctexp->hospital_name;
                    }
                }

                $result['requestCount'] = $requestCount;
                $result['userCallBackRequests'] = $requestList;
                $result['msg'] = 'List of call back requests.';
            } else {
                $result['requestCount'] = $requestCount;
                $result['msg'] = 'No call back request found!';
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function doctorCallBackRequests() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $params = array();
            $params['request_to'] = $this->getUserIdbyToken($this->authToken);

            $callBackRequest = new CallBackRequest();
            $callBackRequests = $callBackRequest->callBackDoctorRequest($params);

            $requestCount = Count($callBackRequests);

            if ($requestCount) {
                $requestList = Array();
                foreach ($callBackRequests as $key => $value) {
                    $requestList[$key] = $value;
                    $value->created_at = gmdate('Y-m-d H:i:s', strtotime($value->created_at));
                    $value->request_date = gmdate('Y-m-d H:i:s', strtotime($value->updated_at));
                    $value->photo = User::getPhoto(array('photo' => $value->photo, 'is_doctor' => 0));
                }

                $result['requestCount'] = $requestCount;
                $result['doctorCallBackRequests'] = $requestList;
                $result['msg'] = 'List of call back requests.';
            } else {
                $result['requestCount'] = $requestCount;
                $result['msg'] = 'No call back request found!';
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function callBack() {

        $data = Input::all();
        $twilioHelper = $this->helper->load('twilio');        
        
        //MAINTENANCE CHECK
        $maintainance = array();
        $maintainance['knowlarity_down'] = Config::get('constants.KNOWLARITY_DOWN');
        $maintainance['citrus_down'] = Config::get('constants.CITRUS_DOWN');
        $maintainance['database_server_down'] = Config::get('constants.DATABASE_SERVER_DOWN');
        foreach ($maintainance as $key => $value) {
            if (!empty($value)) {
                $errorMsg = "Apologies! Server is down for maintenance. Please try again later.";
                if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                    $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => $errorMsg));
                    return Response::make($twiml, 200, $this->headers);
                } else {
                    $result['status'] = $value;
                    $result['error_type'] = "$key";
                    $result['error_msg'] = $errorMsg;
                    return Response::make($result, 200, $this->headers);
                }
            }
        }

        try {
            $checkAuthToken = True;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $callBackRequestId = $data['callback_request_id'];

            $callBackRequest = CallBackRequest::where('callback_request_id', '=', $callBackRequestId)->select('request_from', 'request_to', 'callback_request_id', 'status', 'wallet_id')->first();

            $userIdByToken = $this->getUserIdbyToken($this->authToken);

            if (empty($callBackRequest['callback_request_id']) || ($userIdByToken != $callBackRequest['request_to']) || $callBackRequest['status'] != 'open') {
                
                $errorMsg = 'Invalid callback request id or request is closed!';
                if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                    $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => $errorMsg));
                    return Response::make($twiml, 200, $this->headers);
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1040;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.CALL_FAIL');
                    $result['errors'] = $errorMsg;
                    return Response::make($result, 200, $this->headers);
                }                
            }

            $data['patient'] = $user_id = $callBackRequest['request_from'];
            $data['doctor'] = $callBackRequest['request_to'];
            $user = new User();

            if ($user->validateCallRequest($data)) {
                
                //VARIABLE INTIALIZATION FOR INTERNATIONAL CALL OF FIXED MINS CONCEPT
                $fixedMinsCall = 0;                
                
                $this->setHeader('AUTH_TOKEN', $this->authToken);

                $user_detail = User::where('user_id', '=', $user_id)->select('mobile', 'user_id', 'name')->first();

                $patientMob = $user_detail['mobile'];
                $locale = isset($data['locale']) ? $data['locale'] : '';
                $callTypeData = User::getCallTypeData(array('mobile' => $patientMob, 'locale' => $locale));
                $domesticCall = !$callTypeData['internationalCall'];

                $per_min_charges = Doctor::where('user_id', '=', $data['doctor'])->pluck('per_min_charges');
                if (!$domesticCall) {
                    $per_min_charges = $per_min_charges * (floatval($callTypeData['international_per_min_rules']));
                    $per_min_charges = round($per_min_charges, 2);
                }

                $communicationHelper = $this->helper->load('communication');


                $doctorPerMinCharges = $per_min_charges;

                $user = UserProfile::where('user_id', '=', $user_id)->select('photo')->first();
                $user['photo'] = User::getPhoto(array('photo' => $user['photo'], 'is_doctor' => 0));

                $httpHelper = $this->helper->load('http');
                if ($domesticCall) {

                    $citrusModel = new UserWallet();

                    $walletTokenParams = array('user_id' => $user_id, 'wallet_id' => $callBackRequest->wallet_id, 'columnName' => 'token');

                    if ($callBackRequest->wallet_id == 1) {
                        $walletTokenParams = array_merge($walletTokenParams, array('checkExpiry' => 1));
                    }

                    $user_token = $citrusModel->getWalletInfoColumn($walletTokenParams);

                    if ($callBackRequest->wallet_id == 1 && (empty($user_token['status']) || $user_token['status'] == 2)) {
                        $result = array();
                        $result['status'] = $user_token['status'];
                        $result['msg'] = ($user_token['status'] == 2) ? 'Token expired!' : 'Missing token';
                        return Response::make($result, 200, $this->headers);
                    } elseif (empty($user_token)) {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1040;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.INVAILD_CITRUS_AUTHTOKEN');
                        $result['errors'] = $error;
                        return Response::make($result, 200, $this->headers);
                    }

                    if ($callBackRequest->wallet_id == 1) {
                        $paytmHelper = $this->helper->load('paytm');
                        $userTotalBalance = $paytmHelper->walletWebCheckBalance(array('ssotoken' => $user_token['token'], 'getBalanceOnly' => 1));

                        if (is_array($userTotalBalance) && isset($userTotalBalance['status'])) {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['status'] = $userTotalBalance['status'];
                            $result['msg'] = "Unable to fetch balance!";
                            $result['errors'] = $userTotalBalance['response'];
                            return Response::make($result, 200, $this->headers);
                        }
                    } elseif ($callBackRequest->wallet_id == 3) {
                        $citrusHelper = $this->helper->load('citrus');
                        $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $user_token));

                        if (!empty($mainBalance['error'])) {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['status'] = 0;
                            $result['msg'] = $mainBalance['errorMsg'];
                            $result['errors'] = $mainBalance;
                            return Response::make($result, 200, $this->headers);
                        }

                        $citrusModel = new UserWallet();
                        $emailID = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'columnName' => 'email'));
                        $mvcBalance = $citrusHelper->fetchMVCBalance(array('emailId' => $emailID));
                        if (empty($mvcBalance['d'])) {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['status'] = 0;
                            $result['msg'] = $mvcBalance['errorMsg'];
                            $result['errors'] = $mvcBalance;
                            return Response::make($result, 200, $this->headers);
                        }

                        $userTotalBalance = $totbal = $mvcBalance['mvcAmount'] + $mainBalance['value'];
                    }
                } else {
                    
                    //CHECK IF MINS ARE AVAILABLE
                    $patDocMapping = PatDocMinInfo::where('patient_user_id', '=', $user_id)->where('doctor_user_id', '=', $data['doctor'])->first();
                    if (!empty($patDocMapping) && !empty($patDocMapping->min_info_id) && $patDocMapping->available_seconds >= 60) {
                        $fixedMinsCall = 1;
                    }                    
                    
                    $kccTransaction = new KccTransaction();
                    $userTotalBalance = $kccBalance = $kccTransaction->getBalance(array('user_id' => $user_id));
                }
                
                $enabled_fix_charges_per_call_for_international = Config::get('constants.ENABLED_FIX_CHARGES_PER_CALL_FOR_INTERNATIONAL');
                if($enabled_fix_charges_per_call_for_international && !$domesticCall) {
                    $minAmountForCall = $minAmountForCall * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                }                

                if ($doctorPerMinCharges <= $userTotalBalance  || $fixedMinsCall) {

                    $user_app = DeviceInfo::where('user_id', '=', $data['doctor'])->select('platform_id', 'registartion_id')->first();

                    if (1) {

                        //INSERT DATA IN KCC COMMUNICATION TABLE IF INTERNATIONAL CALL
                        $kccCallId = 0;
                        if (!$domesticCall) {
                            $kccCall = new KccCall();
                            $kccCall->user_id = $user_id;
                            $kccCall->doctor_user_id = $data['doctor'];
                            $kccCall->per_min_charges = $doctorPerMinCharges;
                            $kccCall->save();
                            $kccCallId = $kccCall->kcc_call_id;
                        }

                        $commData = new Call();
                        $commData->caller_id = $user_id;
                        $commData->receiver_id = $data['doctor'];
                        $commData->caller_call_status_id = 4;
                        $commData->per_min_charges = $doctorPerMinCharges;
                        $commData->fixed_min_call = $fixedMinsCall;
                        $commData->kcc_call_id = $kccCallId;
                        $commData->save();

                        CallBackRequest::where('callback_request_id', '=', $callBackRequestId)->update(array('callback_call_id' => $commData->call_id));

                        $GCMData = array();
                        $GCMData['from_user_id'] = $user_detail['user_id'];
                        $GCMData['from_user_name'] = $user_detail['name'];
                        $GCMData['from_user_photo'] = $user['photo'];
                        $GCMData['msg_id'] = $commData->call_id;
                        $GCMData['msg_time'] = time();
                        $GCMData['type'] = 'call_initiate';

                        if ($user_app['platform_id'] == 1) {
                            $params['GCMDeviceIds'] = $user_app['registartion_id'];
                            $params['msg'] = json_encode($GCMData);
                            $result['response'] = $communicationHelper->gcmCommunication($params);
                        } else if ($user_app['platform_id'] == 2) {
                            $params['APNSDeviceIds'] = $user_app['registartion_id'];
                            $GCMData['msg_text'] = $user_detail['name'] . " " . Config::get('config_msg.CALL_INITIATE');
                            $params['push_msg'] = $GCMData;
                            $result['response'] = $communicationHelper->pushApnsCommunication($params);
                        }

                        $bal = floatval($userTotalBalance);
                        if ($bal >= $doctorPerMinCharges) {
                            if ($doctorPerMinCharges > 0) {
                                $tot_mins = floatval($bal) / floatval($doctorPerMinCharges);
                                $max_call_duration = intval($tot_mins) * 60;
                            } else {
                                $max_call_duration = 15 * 60;
                            }
                        } else {
                            $max_call_duration = ($doctorPerMinCharges > 0) ? 0 : 15 * 60;
                        }

                        $doctor_detail = User::where('user_id', '=', $data['doctor'])->first();
                        $patMobile = '+' . $user_detail['mobile'];
                        $docMobile = '+' . $doctor_detail['mobile'];

                        //DISCONNECT THE CALL AFTER 15 MINS 
                        $MAX_CALL_DURATION_CAPPING = Config::get('constants.MAX_CALL_DURATION_CAPPING');
                        if ($max_call_duration > $MAX_CALL_DURATION_CAPPING) {
                            $max_call_duration = $MAX_CALL_DURATION_CAPPING;
                        }
                        
                        if($fixedMinsCall) {
                            $max_call_duration = $patDocMapping->available_seconds;
                        }                        
                        
                        if (Config::get('constants.twilio_enabled')) {

                            $twilioParams = array('call_id' => $commData->call_id, 'patMobile' => $docMobile, 'docMobile' => $patMobile, 'max_call_duration' => $max_call_duration);
                            
                            $telecom_id = 2;
                            if(!empty($data['voipCall'])) {
                                $telecom_id = 3;
                            }

                            Call::where('call_id', '=', $commData->call_id)->update(array('telecom_id' => $telecom_id, 'telecom_request' => json_encode($twilioParams)));

                            if(empty($data['voipCall'])) {
                                $result['twilio'] = $twilioHelper->initiateCall($twilioParams);
                                if (isset($result['twilio']['status']) && !empty($result['twilio']['status'])) {
                                    $result['status'] = 1;
                                    $result['msg'] = Config::get('config_msg.CALL_PLACED');
                                } else {
                                    $result['status'] = 0;
                                    $result['error_msg'] = $result['twilio']['message'];
                                    $result['msg'] = 'Service provider failure!';
                                }                         
                                Call::where('call_id', '=', $commData->call_id)->update(array('telecom_response' => json_encode($result)));
                            }
                            else {
                                $twiml = $twilioHelper->voipCall($twilioParams);
                                return Response::make($twiml, 200, $this->headers);
                            }

                        } else {

                            $url = Config::get('constants.KNOWLARITYINT') . "customer_number=" . $docMobile . "&agent_number=" . $patMobile . "&call_id=" . $commData->call_id . "&max_call_duration=" . $max_call_duration . "&auth_key=" . Config::get('constants.KNOWLARITY_AUTH_KEY');

                            Call::where('call_id', '=', $commData->call_id)->update(array('telecom_request' => $url));

                            if (!$domesticCall) {
                                KccCall::where('kcc_call_id', '=', $kccCallId)->update(array('request_details' => $url));
                            }

                            $httpHelper = $this->helper->load('http');
                            $doc = $httpHelper->httpGet($url);
                            $xml = simplexml_load_string($doc);
                            $json = json_encode($xml);
                            $responseArray = json_decode($json, TRUE);
                            $result['url'] = $url;
                            if ($responseArray['status'] == 'Success') {
                                $this->setHeader('RESPONSE_CODE', '1');
                                $result['status'] = 1;
                                $result['msg'] = Config::get('config_msg.CALL_PLACED');
                                $result['response'] = $responseArray;
                            } else {
                                $this->setHeader('RESPONSE_CODE', '0');
                                $result['status'] = 0;
                                $result['url'] = $url;
                                $result['msg'] = "Service Provider Failure!";
                                $result['response'] = $responseArray;
                                Call::where('call_id', '=', $commData->call_id)->update(array('telecom_response' => $json));

                                //ERROR LOGGING                                
                                $errorLog = new ErrorLogs();
                                $errorLog->type = 'knowlarity_error_callapihit';
                                $errorLog->request = $url;
                                $errorLog->response = json_encode($responseArray);
                                $errorLog->save();                            
                            }
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1028;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = "Patient do not have sufficient balance.";
                    $result['errors'] = $error;

                    //SEND SMS TO PATIENT
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALLBACK_PATIENT_INSUFFICIENT_BALANCE');
                    if ($smsText) {
                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        
                        $doctorDetails = User::where('user_id', '=', $data['doctor'])->select('salutation', 'name')->first();
                        
                        $drName = $doctorDetails['salutation'] . " " . $doctorDetails['name'];
                        $smsParams['Text'] = str_replace(array('<DOCTOR_NAME>'), array($drName), $smsText);
                        $smsParams['To'] = $patientMob;
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.CALL_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
