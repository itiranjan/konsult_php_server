<?php

class FavoriteDoctor extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'user_favorite_doctors';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'favorite_doctor_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'patient' => 'required|numeric',
        'doctor' => 'required|numeric'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 30 jan 2015
     * @description : This method used to get favorite doctor list
     * @access public
     * @return  array 
     */
    public function favDoctorList() {
        return $this->belongsTo('User', 'doctor', 'user_id');
    }

}
