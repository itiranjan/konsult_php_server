<?php

class ApiConfig extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'api_configs';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

}
