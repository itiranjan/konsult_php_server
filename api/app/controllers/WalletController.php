<?php

class WalletController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool checkAuthToken (To Check Auth token)
     * @return  array 
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 6 Feb 2015
     * @description : This method used to get list of user transaction
     * @param  int $page
     * @access public
     * @return  array 
     */
    public function transactionList($page = 1) {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);
            $trans = new Transactions();
            $transList = $trans->transactionList($user_id, $page);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.TRANSACTION_LIST');
            $result['response']['transaction_list'] = $transList;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function redirectUrlLoadCash() {
        return View::make('redirectUrlLoadCash');
    }

    public function signinOtp() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $mobile = $data['mobile'];

            $user_id = $this->getUserIdbyToken($this->authToken);

            $email = User::where('user_id', '=', $user_id)->pluck('email');

            if (empty($mobile)) {
                $result['status'] = 0;
                $result['msg'] = "Mobile number is required!";
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');
            
            $walletParams = array();
            $walletParams['mobile'] = $mobile;
            if(!empty($email)) {
                $walletParams['email'] = $email;
            }

            $signinOtp = $paytmHelper->signinOtp($walletParams);

            return Response::make($signinOtp, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function signinValidateOtp() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $otp = $data['otp'];

            $state = $data['state'];

            $user_id = $this->getUserIdbyToken($this->authToken);

            if (empty($otp) || empty($state)) {
                $result['status'] = 0;
                $result['msg'] = "OTP and state are required!";
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');

            $signinValidateOtp = $paytmHelper->signinValidateOtp(array('otp' => $otp, 'state' => $state));

            if (!empty($signinValidateOtp)) {
                $signinValidateOtpDecode = json_decode($signinValidateOtp, true);

                if (!empty($signinValidateOtpDecode['access_token'])) {

                    $tokenId = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->pluck('user_wallet_id');
                    if ($tokenId) {
                        UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->update(array('token' => $signinValidateOtpDecode['access_token'], 'expires' => $signinValidateOtpDecode['expires'], 'login' => 1));
                    } else {
                        $walletInfoModel = new UserWallet();
                        $walletInfoModel->user_id = $user_id;
                        $walletInfoModel->token = $signinValidateOtpDecode['access_token'];
                        $walletInfoModel->expires = $signinValidateOtpDecode['expires'];
                        $walletInfoModel->wallet_id = 1;
                        $walletInfoModel->save();
                    }
                }
            }

            return Response::make($signinValidateOtp, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function userDetails() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $token = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->pluck('token');

            if (empty($token)) {
                $result = array();
                $result['status'] = 0;
                $result['msg'] = 'Missing paytm token!';
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');

            $userDetails = $paytmHelper->userDetails(array('session_token' => $token));
            return Response::make($userDetails, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function walletWebCheckBalance() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $token = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->pluck('token');

            if (empty($token)) {
                $result = array();
                $result['status'] = 0;
                $result['msg'] = 'Missing paytm token!';
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');

            $checkBalance = $paytmHelper->walletWebCheckBalance(array('ssotoken' => $token));

            $checkBalance = json_decode($checkBalance);

            if ($checkBalance->response) {
                $result['msg'] = 'Balance fetched!';
                $result['status'] = 1;
                $result['balance'] = $checkBalance->response->amount;
            } else {
                $status = 0;
                if($checkBalance->statusCode == 403 || $checkBalance->statusCode == 404) {
                    $status = 2;
                }                
                
                $result['msg'] = 'Unble to fetch balance!';
                $result['status'] = $status;
                $result['error'] = $checkBalance;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function initiateAddMoney($params = array()) {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $citrusModel = new UserWallet();
            $token = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'wallet_id' => 1, 'columnName' => 'token', 'checkExpiry' => 1));

            if (empty($token['status']) || $token['status'] == 2) {
                $result = array();
                $result['status'] = $token['status'];
                $result['msg'] = ($token['status'] == 2) ? 'Token expired!' : 'Missing token';
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $paytmHelper = $this->helper->load('paytm');
            $initiateAddMoney = $paytmHelper->initiateAddMoney(array('sso_token' => $token['token'], 'amount' => $data['amount'], 'user_id' => $user_id));

            return Response::make($initiateAddMoney, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function oltpHandlerffWithdrawScw($params = array()) {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $token = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->pluck('token');

            if (empty($token)) {
                $result = array();
                $result['status'] = 0;
                $result['msg'] = 'Missing paytm token!';
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');

            $data = Input::all();

            $data['amount'] = 10.00;

            $order_id = 'AM_' . $user_id . "_" . time(); //ADD THE COMMUNICATION ID
            $mobile = User::where('user_id', '=', $user_id)->pluck('mobile');
            $mobile = substr($mobile, 2);

            $initiateAddMoney = $paytmHelper->oltpHandlerffWithdrawScw(array('sso_token' => $token, 'amount' => $data['amount'], 'user_id' => $user_id, 'order_id' => $order_id, 'mobile' => $mobile));
            return Response::make($initiateAddMoney, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function walletLogout() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $data = Input::all();
            $wallet_id = $data['wallet_id'];

            if ($user_id && $wallet_id) {

                UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', $wallet_id)->update(array('login' => 0));

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = "User has loggout from wallet successful!";
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1040;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.LOGOUT_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function walletLoginStatus() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $data = Input::all();
            $wallet_id = $data['wallet_id'];

            if ($user_id && $wallet_id) {

                $result['login'] = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', $wallet_id)->pluck('login');

                //WILL HANDLE IT LATER
//                if($userWallet->expires != NULL) {
//                    
//                }

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = "Wallet login status fetched successfully!";
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1040;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = "Unable to fetch wallet login status!";
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function salesToUserCredit($params = array()) {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $token = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 1)->pluck('token');

            if (empty($token)) {
                $result = array();
                $result['status'] = 0;
                $result['msg'] = 'Missing paytm token!';
                return Response::make($result, 200, $this->headers);
            }

            $paytmHelper = $this->helper->load('paytm');

            $data = Input::all();

            $data['amount'] = $data['amount'];

            $order_id = 'DUMMY_CALL_' . $user_id . "_" . time(); //ADD THE COMMUNICATION ID
            $mobile = User::where('user_id', '=', $user_id)->pluck('mobile');
            $mobile = substr($mobile, 2);

            $creditMoney = $paytmHelper->salesToUserCredit(array('sso_token' => $token, 'amount' => $data['amount'], 'user_id' => $user_id, 'order_id' => $order_id, 'mobile' => $mobile));
            return Response::make($creditMoney, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
