<style>
    tr { font-family:tahoma;font-size:11px;height:21px; }
    .red{
        color:red;
    }
    .blue{
        color:blue;
    }
    .green{
        color:green;
    }
</style>


    <script>
        var base_url = "<?php echo URL; ?>";
        var mainGrid;
    </script>

    <div id="window3">
        <form id="DetailP">

            <table width="100%">
                <tr>
                    <td width="28%">Name</td>
                    <td width="2%">:</td>
                    <td width="70%"><div  style="width:250px" id="name"/>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td><div  style="width:250px" id="email"/>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>Mobile</td>
                    <td>:</td>
                    <td><div style="width:250px" id="mobile"/>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>City</td>
                    <td>:</td>
                    <td><div style="width:250px" id="city"/>&nbsp;&nbsp;
                    </td>
                </tr>
                 <tr>
                    <td>Is Approved</td>
                    <td>:</td>
                    <td><div  style="width:250px" id="is_approved"/>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>Medical Registration No</td>
                    <td>:</td>
                    <td><div  style="width:250px" id="medical_registration_no"/>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>Total Experience (In Years)</td>
                    <td>:</td>
                    <td><div style="width:250px" id="total_experience"/>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>Per Min Charges</td>
                    <td>:</td>
                    <td><div style="width:250px" id="per_min_charges"/>&nbsp;&nbsp;
                    </td>
                </tr>

                <tr>
                    <td>Intro</td>
                    <td>:</td>
                    <td><div  style="width:250px" id="info"/>&nbsp;&nbsp;
                    </td>
                </tr>
                  <tr>
                     <td><br/></td>
                    <td></td>
                    <td></td>
                </tr>

                <tr>
                    <td>Specialization</td>
                    <td>:</td>
                    <td><div style="width:250px" id="specialization"/>&nbsp;&nbsp;
                    </td>
                </tr>
                  <tr>
                     <td><br/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Qualification</td>
                    <td>:</td>
                    <td><div style="width:500px" id="qualification"/>&nbsp;&nbsp;
                    </td>
                </tr>
                  <tr>
                     <td><br/></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td >Experience</td>
                    <td>:</td>
                    <td><div style="width:500px" id="experience"/>&nbsp;&nbsp;
                    </td>
                </tr>
                 <tr>
                     <td><br/></td>
                    <td></td>
                    <td></td>
                </tr>
               
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
        </form>
    </div>
    <div style="width:100%">
        <div style="width:100%;display:block;">
            
        <div id="clientsDb"> 
            <span style="font-size:20px">Doctors List<br/><br/></span>
            <div id="grid" ></div>
        </div>    

        <script>

            var window3 = $("#window3");
            $(document).ready(function() {

                var onClose = function()
                {
                    mainGrid.data("kendoGrid").refresh();
                }

                window3.kendoWindow({
                    width: "700px",
                    visible: false,
                    title: "Doctor Detail"
                });

                var selectedRows = [];
                mainGrid = $("#grid").kendoGrid({
                    toolbar:["excel"],
                    selectable: "multiple cell",
                    allowCopy: true,
                    excel: {
                        allPages: true,
                        fileName: "doctor_list_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                        filterable: true
                    },
                    dataSource: {                        
                        type: "json",
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        transport: {
                            read: {
                                type: "POST",
                                url: base_url + "/index.php/doctor/doctorList",
                                dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                            },
                            parameterMap: function (options) {
                                if (options.filter) {
                                    KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                                }
                                return options;
                            },
                        },                        
                        
                        schema: {
                            data: "list",
                            total: "total",
                            model: {
                                fields: {
                                    users$user_id: { type: "number", editable: false},
                                    users$name: {type:"string", editable: false},
                                    users$email: {type:"string", editable: false},
                                    user_wallet$email: {type:"string", editable: false},
                                    specialization: {type:"string", editable: false},
                                    city_master$name: {type:"string", editable: false},
                                    mobile: {type: "string", editable: false},
//                                    patient_count: {type: "number", editable: false},
                                    created_at: {type: "date", editable: false},
                                    online_status: {type: "number", editable: false},
                                    is_approved: {type: "number", editable: false},
                                }
                            }
                        },
                        pageSize: 100
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            }
                        }
                    },                     
                    sortable: true,
                    dataBound: function() {
                        this.expandRow(this.tbody.find("tr.k-master-row").first());
                    },
                    pageable: {
                        refresh: true,
                        pageSize: 100,
                        numeric: true,
                        buttonCount: 20,
                        info: true
                    },
                    change: function(e) {
                        var selected = this.select();
                        for (var i = 0; i < selected.length; i++) {
                            var dataItem = this.dataItem(selected[i]);
                            selectedRows.push(dataItem);
                        }
                    },
                    columns: [
                        {field: "users$user_id", title: "Id", width: '8px'},
                        {field: "users$name", title: "Name", width: '12px'},
                        {field: "users$email", title: "Email", width: '15px'},
                        {field: "user_wallet$email", title: "Citrus Email", width: '15px'},
                        {field: "mobile", title: "Mobile", width: '10px'},
                        {field: "specialization", title: "Specialization", width: '15px', filterable: false, sortable: false},
                        {field: "city_master$name", title: "City", width: '8px'},
//                        {field: "patient_count", title: "# Patients", width: '8px'},
                        {field: "created_at", title: "Added On", width: '13px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "online_status", title: "Online", width: '5px', template: "<a id='toggle' onclick='<?php echo (!empty($this->session->userdata('permissions')['doctor_toggleStatus_edit']) ? 'toggleStatus(#= users$user_id#,#= online_status#);' : "javascript:void(0);"); ?>' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/#= online_status == 0 ? 'offline.png' : 'online.png' #' title='Click to change availability status'></a>", filterable: { ui: statusFilter, "messages": { "info": "Select Status:" }, operators: {number: {eq: "Equal To"}}}},
                        {field: "is_approved", title: "Approve", width: '5px', template: "<a id='toggle' onclick='<?php echo (!empty($this->session->userdata('permissions')['doctor_toggleApproved_edit']) ? 'toggleApproved(#= users$user_id#,#= is_approved#);' : "javascript:void(0);"); ?>' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/#= is_approved == 0 ? 'msg_warning.png' : 'msg_success.png' #' title='Click to change aprroval status'></a>", filterable: { ui: approvalFilter, "messages": { "info": "Select Status:" }, operators: {number: {eq: "Equal To"}}}},
                        {field: "user_id", title: "-", width: '3px', template: "<a id='edit' onclick='viewDetail(#= users$user_id#,this);' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/view.jpg' title='View Detail'></a>", filterable: false, sortable: false},
                        {field: "user_id", title: "-", width: '5px', template: "<a id='edit1' onclick='viewdoctorRegistration(#= users$user_id#,this);' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/edit.jpg' title='Edit Details'></a>", filterable: false, sortable: false}
                    ],
                    editable: true,
                }).data("kendoGrid");              
                
                mainGrid.thead.kendoTooltip({
                    filter: "th",
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown
                        return $(target).text();
                    }                    
                });
            });
                                    
//            function specialityFilter(element) {
//                element.kendoAutoComplete({
//                    filter: "contains",
//                    placeholder: "Type...",
//                    dataTextField: "abc",
//                    //filter: "contains",
//                    dataSource: {
//                        type: "json",
//                        serverFiltering: true,
//                        transport: {
//                                read: base_url + "/index.php/doctor/specializationList",
//                                type: "POST"
//                            },
//                    },
//                    //optionLabel: "--Select Value--"  
//                });
//            }         
            
            function statusFilter(element) {

                element.kendoDropDownList({
                    //filter: "contains",
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: [
                        { id: 1, name: "Online" },
                        { id: 0, name: "Offline" }
                    ],
                    optionLabel: "--Select Status--"  
                });
            }            
            
            function approvalFilter(element) {

                element.kendoDropDownList({
                    //filter: "contains",
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: [
                        { id: 1, name: "Approved" },
                        { id: 0, name: "Un-approved" }
                    ],
                    optionLabel: "--Select Status--"  
                });
            }

            function toggleStatus(id, value) {
                
                if(value == 1 && !confirm("Are you sure that you want to offline this doctor?")) {
                    return;
                }
                else if(value == 0 && !confirm("Are you sure that you want to online this doctor?")) {
                    return;
                }
                
                callFunction = function(d) {
                    $("#grid").data("kendoGrid").dataSource.read();
                };
                ajaxCall('doctor/toggleStatus', 'id=' + id + "&val=" + value, callFunction);
            }
            
            function toggleApproved(id, value) {
                
                if(value == 1 && !confirm("Are you sure that you want to dis-approve this doctor?")) {
                    return;
                }
                else if(value == 0 && !confirm("Are you sure that you want to approve this doctor? Note that we will not approve this doctor if doctor profile is not mapped with citrus.")) {
                    return;
                }
                
                callFunction = function(d) {
                    $("#grid").data("kendoGrid").dataSource.read();
                };
                ajaxCall('doctor/toggleApproved', 'id=' + id + "&val=" + value, callFunction);
            }


            function htmlEntities(str) {
                return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos');
            }

            function viewDetail(id, obj) {
                $.ajax({
                    type: 'POST',
                    url: "<?php echo URL; ?>/index.php/doctor/DoctorDetailById/" + id,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result);
                        window3.data("kendoWindow").center();
                        window3.data("kendoWindow").open();
                        $('#name').text(result[0].name);
                        $('#email').text(result[0].email);
                        $('#mobile').text(result[0].mobile);
                        $('#city').text(result[0].city);
                        $('#is_approved').text(result[0].is_approved);
                        $('#medical_registration_no').text(result[0].medical_registration_no);
                        $('#total_experience').text(result[0].total_experience);
                        $('#info').text(result[0].info);
                        $('#per_min_charges').text(result[0].per_min_charges);
                        $('#specialization').text(result[0].specialization);
                        $('#otherinfo').text(result[0].other_info);
                        $('#area').text(result[0].area);
                        str = '<table width="70%" border="1" ><tr><th>University</th><th>Degree</th><!--<th>Start Date</th><th>End Date</th>--></tr>';
                        for (i = 0; i < result[0].qualification.length; i++) {
                            str += "<tr><td>" + result[0].qualification[i].university + "</td><td>" + result[0].qualification[i].degree + "</td><!--<td>" + result[0].qualification[i].start_date + "</td><td>" + result[0].qualification[i].end_date + "</td>--></tr>";
                        }
                        str += '</table>';
                        $('#qualification').html(str);
                        
                        str = '<table width="70%" border="1" ><tr><th>Hospital</th><th>Designation</th><!--<th>Joining Date</th><th>End Date</th><th>Is Current</th>--></tr>';
                        for (i = 0; i < result[0].experience.length; i++) {
                            str += "<tr><td>" + result[0].experience[i].hospital + "</td><td>" + result[0].experience[i].designation + "</td><!--<td>" + result[0].experience[i].join_date + "</td><td>" + result[0].experience[i].end_date + "</td><td>" + result[0].experience[i].is_current + "</td>--></tr>";
                        }
                        str += '</table>';
                        $('#experience').html(str);
                    }
                });
            }

            function viewdoctorRegistration(id, obj) {
            	var email ="";
            	var mob = "";
               $.get(
            	"http://admin.konsultapp.com/index.php/verifylogin/doctorreg", 
                 // "http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/doctorreg",
                  {email: email,mobile: mob},
                  function(data) {
                   //alert(data);
                   //window.open("doctorRegistration.php");
                   //window.location.assign("doctorRegistration.php");
                   
        	 			$.get(
                    	  "http://admin.konsultapp.com/index.php/verifylogin/getcity", 
                         //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getcity",
                          {email: email,mobile: mob},
                          function(data1) {
                          	$('#city2').html(data1);
                          }
                       );

        	 			 $.get(
        	 	            	 "http://admin.konsultapp.com/index.php/verifylogin/getspecialization", 
        	 	                  //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getspecialization",
        	 	                  {email: email,mobile: mob},
        	 	                  function(data1) {
        	 	                  	$('#country_list_id').html(data1);
        	 	                  }
        	 	               );
        	 			 $.get(
        	 	            	 "http://admin.konsultapp.com/index.php/verifylogin/getdegree", 
        	 	                  //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getdegree",
        	 	                  {email: email,mobile: mob},
        	 	                  function(data2) {
        	 	                  	$('#degree2').html(data2);
        	 	                  }
        	 	               );
        	 			var email =id;
        	         	var mob = "test";
        	         	if(email !="" && mob !=""){
        					 //$.get( "http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getDoctorEditDataforweb", {email: email,mob:mob})
        					 $.get( "http://admin.konsultapp.com/index.php/verifylogin/getDoctorEditDataforweb", { email: email,mob:mob } )
        					  .done(function(data) {
        					    console.log(data);
        					    var obj = jQuery.parseJSON(data);
        					   if(obj.email !="not" && obj.mobile !="diff"){
                                    $('#salutation').val(obj.salutation);
                                    $('#name').val(obj.name);
        					    	$('#email').val(obj.email);
        					    	$('#mobile').val(obj.mobile);
        					    	$('#city').val(obj.city_name);
        					   		$('#address').val(obj.info);
        					    	$('#experience').val(obj.total_experience);
        					    	$('#specialization').val(obj.specialization_name);
        					    	$('#mregistration').val(obj.medical_registration_no);
        					    	$('#otherinfo').val(obj.other_info);
        					    	$('#pminute').val(obj.per_min_charges);
        					    	//$('#allow').val(obj.allow_new_user);
        					    	$(".allow").prop("checked", true)
        					    	$('#area').val(obj.area);
        					    	$('#editid').val("modify");
        					    	$("#email").prop("readonly", true);
        					    	$("#mobile").prop("readonly", true);
        					    for (var i in obj.qualification) {
        							if(i==0){
        								$('#qualid').val(obj.qualification[0].qualification_id);
        						    	$('#college').val(obj.qualification[0].university);
        						    	$('#degree').val(obj.qualification[0].degree);
        							}
        							if(i==1){
        								var qualification="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid1' id='qualid1'/><span class='loginLbl'>College</span><input list='college1' name='college1' id='college1'><datalist id='college1'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree1' id='degree1'><datalist id='degree2'><option value='Others'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear1' name='syear1' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear1' name='eyear1' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing1' name='curentpersuing1'><datalist id='curentpersuing1'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
        								$('#quali0').html(qualification);
        								$('#qualid1').val(obj.qualification[1].qualification_id);
        								$('#college1').val(obj.qualification[1].university);
        								$('#degree1').val(obj.qualification[1].degree);
        								q=1;
        							}
        							if(i==2){
        								var qualification1="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid2' id='qualid2'/><span class='loginLbl'>College</span><input list='college2' name='college2' id='college2'><datalist id='college2'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree2' id='degree2' class='deg2'><datalist id='degree2'><option value='Others'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear2' name='syear2' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear2' name='eyear2' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing2' name='curentpersuing2'><datalist id='curentpersuing2'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
        								$('#quali1').html(qualification1);
        								$('#qualid2').val(obj.qualification[2].qualification_id);
        								$('#college2').val(obj.qualification[2].university);
        								$('.deg2').val(obj.qualification[2].degree);
        								q=2;
        							}
        					    }
        					    	$('#hospri').val(obj.experience[0].hospital);
        					    	$('#designation').val(obj.experience[0].designation);
        					    	$('#hospitalid').val(obj.experience[0].workplace_id);
        					    	$('#sunstarttime').val(obj.availabilty[0].start_time);
        					    	$('#sunendtime').val(obj.availabilty[0].end_time);
        					    	$('#monstarttime').val(obj.availabilty[1].start_time);
        					    	$('#monendtime').val(obj.availabilty[1].end_time);
        					    	$('#tuestarttime').val(obj.availabilty[2].start_time);
        					    	$('#tueendtime').val(obj.availabilty[2].end_time);
        					    	$('#wedstarttime').val(obj.availabilty[3].start_time);
        					    	$('#wedendtime').val(obj.availabilty[3].end_time);
        					    	$('#thrustarttime').val(obj.availabilty[4].start_time);
        					    	$('#thruendtime').val(obj.availabilty[4].end_time);
        					    	$('#fristarttime').val(obj.availabilty[5].start_time);
        					    	$('#friendtime').val(obj.availabilty[5].end_time);
        					    	$('#satstarttime').val(obj.availabilty[6].start_time);
        					    	$('#satendtime').val(obj.availabilty[6].end_time);
        					   }else if(obj.mobile=="diff"){
        						   alert("This user is not registered as doctor.Please apply for registration");
        					   }else{
        						   alert("You are not entering valid Email and Mobile Number");
        					   }
        					  });
        	         	}else{
        	             	alert("Please enter Valid Email and Mobile No:-");
        	         	}
                  $('#breadcrumb').html(data);
                 // $('#city').html(data);
                  $('#clientsDb').hide();
                  }
               );
            }
            
            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function() {
                        $('#loading').show();
                    },
                    complete: function() {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function() {
                        alert(error);
                    }
                });
            }
        </script>
    </div>
</div>