<?php

class Transactions extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'transactions';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'transaction_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }

    /**
     * @author : Shipra Agrawal
     * @created : 02 Feb 2015
     * @description : This function get user transaction list
     * @param : int $userid
     * @param : int $page
     * @param : int $limit
     * @access public
     * @return  array 
     */
    public function transactionList($userid, $page = 1, $limit = 10) {
        $transList = DB::table('transactions AS t')->where('t.user_id', '=', $userid)
                ->join('wallet_master AS pg', 'pg.wallet_id', '=', 't.wallet_id')
                ->join('transaction_type_master AS tt', 'tt.id', '=', 't.type')
                ->join('transaction_status_master AS sm', 'sm.trans_status_id', '=', 't.trans_status')
                ->leftJoin('withdrawals AS wd', 't.transaction_id', '=', 'wd.transaction_id')
                ->leftJoin('spends AS sd', 't.spend_id', '=', 'sd.spend_id')
                ->leftJoin('communication AS c', 'sd.communication', '=', 'c.call_id')
                ->select('t.transaction_id', 't.call_id', 't.call_uuid', 'pg.name AS payment_gateway', 'tt.name AS transaction_type', 't.amount', 't.balance', 'sm.name AS transaction_status', 't.created_at', 'sd.spend_id', DB::raw(" case when c.receiver_id=t.user_id then concat(sd.doctor_charges,'##',c.caller_id,'##',(select name from users where user_id=c.caller_id),'##',c.call_duration,'##Incoming Call')when c.caller_id=t.user_id then concat(sd.total_charges,'##',c.receiver_id,'##',(select name from users where user_id=c.receiver_id),'##',c.call_duration,'##Outgoing Call')else null end AS comm_detail"), DB::raw('RIGHT(wd.bank_account_number,4) AS bank_account_number'))
                ->skip($limit * ($page - 1))->take($limit)
                ->orderBy('t.transaction_id', 'DESC')
                ->get();

        for ($i = 0, $c = count($transList); $i < $c; ++$i) {
            $transList[$i] = (array) $transList[$i];
            if (!empty($transList[$i]['comm_detail'])) {
                $arr = explode("##", $transList[$i]['comm_detail']);
                $transList[$i]['user_id'] = $arr[1];
                $transList[$i]['name'] = $arr[2];
                $transList[$i]['comm_type'] = $arr[4];
                $transList[$i]['call_duration'] = $arr[3];
                $transList[$i]['trans_datetime'] = gmdate('Y-m-d H:i:s', strtotime($transList[$i]['created_at']));
            }
            unset($transList[$i]['comm_detail']);
        }

        return $transList;
    }

}
