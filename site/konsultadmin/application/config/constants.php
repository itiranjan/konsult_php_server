<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */

//constants related to the IDS Specific
define('URL',			'http://admin.konsultapp.com');
define('APIURL',			'http://api.konsultapp.com/');
define('SITE',					'Konsult');


define('CLIENT',                                '');
define('LOADER',                                '<div id="loading"></div>');
define('IMG',                                	URL.'/assets/images/');
define('CSS',                                	URL.'/assets/css/');
define('JS',                                	URL.'/assets/js/');
define('Editor',                                URL.'assets/ckeditor/');
define('GET_DATE',                              date('Y-m-d H:i:s'));


define('CITRUS_URL', 'https://admin.citruspay.com');
define('CITRUS_BALANCE', '/service/v2/mycard');
define('CITRUS_PAY', '/service/v2/prepayment/pay');
define('CITRUS_REFUND', '/service/v2/prerefund/refund');
define('CITRUS_TRANSFER', '/service/v2/prepayment/transfer');
define('CITRUS_AUTHTOKEN_PREFIX', 'Bearer ');
define('CITRUS_ACCESS_KEY', 'MQ526OM21YGFL8OW0Z10');
define('CITRUS_SECRET_KEY', 'd098eb1e880a01e419fac438061c45c1adee01c8');
define('KONSULT_AUTHTOKEN', 'beb51de1-1d96-45fe-8f1c-6592e544395e');
define('CITRUS_VIRTUAL_URL', 'https://coupons.citruspay.com/cms_api.asmx');
define('CITRUS_VIRTUAL_SANDBOX_URL', 'https://sandboxcoupons.citruspay.com/cms_api.asmx');
define('Issue_Virtual_Currency', '/IssueCoupons');
define('Get_Balance', '/FetchCampaignBalance');
define('Redeem_Virtual_Currency', '/RedeemCampaign');
define('Rollback_Redemption', '/RollbackCampaign');
define('Content_Type', 'application/json');
define('Merchant_Id', 'haovrnrhhn');
define('Compaign_Code', 'KONSULTCASH');
define('Parterner_Id', 'haovrnrhhn');
define('PassWord', 'MQ526OM21YGFL8OW0Z10');