<?php

class CallRating extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'call_ratings';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'rating_id';

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'rating' => 'required|integer',
        'call_id' => 'required|integer'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

}
