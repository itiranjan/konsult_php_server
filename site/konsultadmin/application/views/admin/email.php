<?php
error_reporting(E_ALL ^ E_NOTICE);
//require_once URL.'mailer/excel_reader2.php';
$data = new Spreadsheet_Excel_Reader();
$data->read(FCPATH.'uploads/emails.xls'); 
?>
    
	<div style="width:1004px; margin: 0 auto; border:1px solid #F2F2F2;padding:10px;">
    <form action="<?php echo URL;?>index.php/email/send_mail" method="post">
    <table>
		<tr>
			<td></td>
			<td></td>
			<td>Upload Emails List (supported format : xls) : <input type="file" id="upload_xls" name="upload_xls"/></td>
		</tr>
		<tr>
			<td>From</td>
			<td>:</td>
			<td>
			<textarea rows="2" cols="30" style='width:100%' id="from" readonly  name="from">enquiry@21gfox.ca</textarea></td>
			<td></td>
		</tr>
		<tr>
			<td>To</td>
			<td>:</td>
			<td>
			<textarea rows="2" cols="30" style='width:100%' id="to" name="to"></textarea></td>
			<td><input type="button" class="k-button get_email" value="Emails" onclick="open_emails();"/></td>
		</tr>
		<tr>
			<td>Cc</td>
			<td>:</td>
			<td><textarea rows="2" cols="30" style='width:100%' id="cc" name="cc"></textarea></td>
			<td><input type="button" class="k-button get_email" value="Emails"/></td>
		</tr>
		<tr>
			<td>Bcc</td>
			<td>:</td>
			<td><textarea rows="2" cols="30" style='width:100%' id="bcc" name="bcc"></textarea></td>
			<td><input type="button" class="k-button get_email" value="Emails"/></td>
		</tr>
		<tr>
			<td>Subject</td>
			<td>:</td>
			<td>
			<textarea rows="2" cols="30" style='width:100%' id="subject" name="subject"></textarea></td>
			<td></td>
		</tr>
		<tr>
			<td valign="top">Content</td>
			<td valign="top">:</td>
			<td><textarea id="editor" rows="10" cols="30" style="height:440px" name="body">
						
			</textarea></td>
			<td valign="top"><input type="button" class='k-button' id="preview" value="Preview"/></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td><input type="submit" class="k-button" value="Submit"/></td>
		</tr>
	</table>
	</form>
    <div id="window1">
		<?php 
		//echo $data->dump(true,true); 

		//print_r($data->colcount($sheet_index=0));
		
		if($data->colcount($sheet_index=0) > 2)
			echo "Not Valid Import stylesheet, columns more than 2";
		$ul = "<ul style='list-style:none;margin-left:-40px;margin-top:-7px;'>
		<li><b><input type='checkbox' id='all'/> Emails List</b></li>
		<li><hr/></li>";
		for($i =1;$i<($data->rowcount($sheet_index=0)+1);$i++){
			
			$li = '<li><input type="checkbox" class="emails"/><k>'.$data->val($i,1,$sheet=0).' &lt;'.$data->val($i,2,$sheet=0).'&gt;</k></li>';
			$ul .= $li;
		}
		echo $ul;
		?>
		<input type="submit" class="k-button" value="Ok" id="select_list"/>
	</div>
</div>
<?php 

$html = file_get_contents(URL.'uploads/newsletter.html');

$ex_html = str_replace(PHP_EOL, '', $html);
//$ex_html = str_replace(' ','',$ex_html);
$ex_html = str_replace('"','\"',$ex_html);
?>
    <script>
	var to = "Stanely Pareira <stanley@inventiosolutions.com>";
	var cc ="Pranay Katiyar <pranay@inventiosolutions.com>";
	var bcc = "Shipra Agrawal <shipraagrawal7@gmail.com>";
	var subject="Transglobal Newletter";
	$('document').ready(function(){
		var clickEmail;
		var window = $("#window1");
		
		$(".k-addfolder").parent().css('display','none');
		
		
		
		$('#upload_xls').kendoUpload({
			async : {
				saveUrl : "<?php echo URL;?>index.php/email/xls_upload",
				autoUpload : false
			},
			multiple : false,
			success : function(){
				//alert('Uploaded');
				//window.location.reload(true);
				history.go(0);
			}
		});
		
		window.kendoWindow({
			width: "600px",
			title: "Emails List",
			actions: [
				"Pin",
				"Minimize",
				"Maximize",
				"Close"
			]
		});
		
		
		$(".get_email").bind("click", function() {
			window.data("kendoWindow").open();
		});
		window.data('kendoWindow').close();
		
		$('.get_email').click(function(){
			window.data('kendoWindow').center();
			window.data('kendoWindow').open();
			clickEmail = $(this);
		});
		
		$('#all').click(function(){
			if($(this).is(':checked') == true)
				$('.emails').prop('checked',true);
			else
				$('.emails').prop('checked',false);
		});
		
		$('#select_list').click(function(){
			var list = "";
			var i = 0;
			$('.emails').each(function(){
				if($(this).is(':checked') == true){
					if(i==0)
						list += $(this).next().text();
					else
						list += ','+$(this).next().text();
					i++;
				}
				
			});
			clickEmail.parent().prev().children().val(list);
		});
		$("#editor").kendoEditor({
            tools: [
                "bold",
                "italic",
                "underline",
                "strikethrough",
                "justifyLeft",
                "justifyCenter",
                "justifyRight",
                "justifyFull",
                "insertUnorderedList",
                "insertOrderedList",
                "indent",
                "outdent",
                "createLink",
                "unlink",
                "insertImage",
                "subscript",
                "superscript",
                "createTable",
                "addRowAbove",
                "addRowBelow",
                "addColumnLeft",
                "addColumnRight",
                "deleteRow",
                "deleteColumn",
                "viewHtml",
                "formatting",
                "fontName",
                "fontSize",
                "foreColor",
                "backColor"
            ],
			imageBrowser: {
						fileTypes: "*.jpg",
                           messages: {
                            dropFilesHere: "Drop files here"
                           },
                           transport: {
                                read: "<?php echo URL;?>index.php/email/readUploads",
                                destroy: {
                                    url: "<?php echo URL;?>index.php/email/destroy",
                                    type: "POST"
                                },
                                thumbnailUrl: "<?php echo URL;?>index.php/email/getImage",//"<?php echo URL;?>index.php/email/getImage",
                                uploadUrl: "<?php echo URL;?>index.php/email/saveFile",
                                imageUrl: "<?php echo URL;?>/uploads/{0}"///kendo-ui/service/ImageBrowser/Image?path={0}
                           }
                        }
        });
		
		
		
		$('#preview').click(function(){
			$('#to').val(to);
			$('#cc').val(cc);
			$('#bcc').val(bcc);
			$('#subject').val(subject);
			$('#editor').data('kendoEditor').value("<?php echo $ex_html;?>");
		});
	});
	
	
	function open_emails(){
		window.data('kendoWindow').open();
	}
	
	
        
	
    </script>


    
    
</body>
</html>
