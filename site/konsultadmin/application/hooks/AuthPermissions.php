<?php

class AuthPermissions extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    public function AuthPermissions() {

        $controller = $this->router->class;
        $action = $this->router->method;

        if ($controller != 'verifylogin') {

            $permissions = $this->session->userdata('permissions');
            
            if(empty($permissions)) {
                $permissions = array();
            }

            $permissionString = "$controller"."_"."$action";
            $editPermission = $permissionString."_".'edit';
            $viewPermission = $permissionString."_".'view';

            if((array_key_exists($editPermission, $permissions) || array_key_exists($viewPermission, $permissions)) && empty($permissions[$editPermission]) && empty($permissions[$viewPermission])) {
                echo "Unauthorized Access!!";die;
            }
        }
    }

}
