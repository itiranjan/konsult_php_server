<?php

class CouponUsage extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'coupon_usage';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = false;

    public function promo_code_model($data) {

        $userid = $data['user_id'];
        $emailID = $data['Email'];
        $promocode = $data['promoCode'];
        $curdate = date("Y-m-d");

        $mvcamt = "";
        $sdate = "";
        $edate = "";
        $result = DB::select("select * from coupons where PromoCode='$promocode'");
        foreach ($result as $record) {
            $promocode = $record->PromoCode;
            $mvcamt = $record->amount;
            $sdate = $record->valid_from;
            $edate = $record->valid_till;
            $Scrachcode = $record->scrach_code;
            $newUserOnly = $record->new_user_only;
            if ($Scrachcode == 1) {
                $newUserOnly = 0;
            }
        }

        $alreadyUsedPromoCode = DB::table('coupon_usage')->where('user_id', $userid)->pluck('Promo_code');
        $validCode = 1;
        if (!empty($newUserOnly) && !empty($alreadyUsedPromoCode)) {
            $validCode = 0;
        }

        if (!empty($validCode) && !empty($mvcamt) && (strtotime($curdate) >= strtotime($sdate) && strtotime($curdate) <= strtotime($edate))) {

            $dbpromocode = "";
            $dbuserid = "";
            $issuedate = "";
            $allow = "Yes";
            $dbuseridarr = array();

            $validrecord = DB::select("select * from coupon_usage where Promo_Code='$promocode'");
            foreach ($validrecord as $validrecord) {
                $dbpromocode = $validrecord->Promo_Code;
                $dbuserid = $validrecord->user_id;
                $issuedate = $validrecord->created_at;
                array_push($dbuseridarr, $dbuserid);
            }

            for ($i = 0; $i <= count($dbuseridarr) - 1; $i++) {
                if ($userid == $dbuseridarr[$i]) {
                    $Scrachcode = 0;
                }
            }

            if ($Scrachcode == 0) {
                $allow = "No";
            } else if ($Scrachcode == 2) {
                $allow = "Yes";
            }

            if (($promocode == $dbpromocode && $allow == "No")) {
                $response1['code'] = $promocode;
                $response1['success'] = "0";
                $response1['msg'] = ($userid == $dbuserid) ? Config::get('config_msg.PROMO_CODE_EXITS') : Config::get('config_msg.PROMO_CODE_INVALID');
            } else {
                $citrusHelper = $data['connect'];
                $user_issue_mvc = $citrusHelper->issueVirtualCurrency(array('emailId' => $emailID, 'amount' => $mvcamt));

                $mvcissuecoupan = $user_issue_mvc['d'];
                $mvc_issuecoupan = $mvcissuecoupan['status'];
                $StatusId = $mvc_issuecoupan['StatusID'];
                if (!empty($StatusId) && $StatusId == "1") {
                    if ($Scrachcode == 1) {
                        $sqlscrch = DB::update('update coupons SET scrach_code=0 Where code = ?', array("$promocode"));
                        $message = ($sqlscrch > 0) ? "Scratch code status has changed" : "Something went wrong.";
                    }

                    $insertPromoDetail = DB::insert('INSERT INTO coupon_usage (Promo_Code, user_id, created_at) VALUES (?, ?, ?)', array($promocode, $userid, $curdate));
                    $message = $insertPromoDetail ? Config::get('config_msg.PROMO_AMOUNT_ISSUED') : "Something went wrong.";

                    $response1['success'] = "1";
                    $response1['code'] = $promocode;
                    $response1['amount'] = $mvcamt;
                    $response1['msg'] = $message;
                } else {
                    $response1['success'] = "0";
                    $response1['code'] = $promocode;
                    $response1['amount'] = $mvcamt;
                    $response1['msg'] = 'Something went wrong. Please try again.';
                }
            }
        } else if (empty($mvcamt)) {
            $response1['success'] = "0";
            $response1['code'] = $promocode;
            $response1['msg'] = $validCode ? 'This promo code is not valid.' : 'This promo code is valid only for new users.';
        } else {
            $response1['success'] = "0";
            $response1['code'] = $promocode;
            $response1['msg'] = $validCode ? 'This promo code has been expired or not valid.' : 'This promo code is valid only for new users.';
        }

        $json_response1 = json_encode($response1);

        return $json_response1;
    }

}
