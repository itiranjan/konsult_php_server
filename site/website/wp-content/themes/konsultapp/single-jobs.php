<?php
 get_header();
 ?>
 <style>
 .careerbackimg{
	 min-height: 600px;
     background-size: cover !important;

 }
 #Subheader{
	 display:none;
 }
 h1{font-size: 35px;
 font-weight: bold;}
 hr{
	 background:#e872a6 !important;
	 color:#e872a6 !important;
	 height: 5px;
width: 10%;
 }
 .container{
	 text-align:center;
 }
 p{
	 font-size: 25px;
 }
 .secseperator{
	 height:50px;
	 width:100%;
 }
 .footimgh strong{
	 display:block;
	 text-align:right;
	 }
 .footimgh div{
	 width:50%;
	 margin:0 auto;
 }
 .footimgh{
	 background: #50C1CF;
	 padding:10px;
	 color:white;
	 text-align:center;
 }
 .headimgh{
	 min-height:550px;
 }
 .col-sm-6{
	 color:white;
	 min-height:400px;
	 background:#e872a6;
	 background-size:cover;
	 text-align:left;
	 padding: 0px 20px;
 }
 .col-sm-4{
	 min-height:300px;
	 background-size:cover;
 }
 .ihr{
	 background:white !important;
	 color:white !important;
	 height: 5px;
width: 10%;
margin-left:0px;
 }
 #Header{
	 min-height: 60px !important;
 }
 .lastcre select{
	/* height: 30px;*/
font-size: 15px;
padding: 12px;
 }
 .lastcre .wpcf7-file{
	 font-size:10px;
	 padding:0px;
 }
 .lastcre{
	 text-align:left;
 }
 .lastcre .resume{
	 padding:20px;
 }
 .lastcre .resumediv{
	 padding:20px;
 }
 .lastcre .one-second {
	 border:1px solid darkgrey;
	 border: 1px solid black;
     margin: 0px 0px -1px -1px;
     color: black;
 }
 .lastcre input,.lastcre select {
	 border:none;
	 color:black;
 }
 .lastcre li{
	 /*border:1px solid #e872a6;*/
	 color:#e872a6;
	 margin-top:10px;
 }
 .patient-info {
    border: 1px solid white;
    float: left;
    padding: 25px 15px;
    background-color: white;
}
em{
	color:#e872a6;
}
.piii-btn:hover {
	
    background: #e872a6 !important;
}
.piii-btn {
    width: 150px;
    border: none;
    border-radius: 0px !important;
    background: black !important;
    color: white !important;
}
 </style>

<div class="secseperator">
</div>
<section>
<div class="container">
    
    <div class="row">
    
    
    <?php 
				while ( have_posts() ){
					the_post();	
					get_template_part( 'includes/content', 'single' );
				}
			?>
    
    
    </div>
    
    
    </div>


</section>
<div class="secseperator">
</div>

<div class="secseperator">
</div>
<section>
<div class="container">
<div class="row">
<h1>All Current Opening</h1>
<hr />
<div class="col-sm-12 lastcre">
<?php echo get_the_content(); ?>

<div class="secseperator">
</div>
<?php
echo do_shortcode('[contact-form-7 id="3340" title="Career"]');
?>
</div>
</div>
</section>
<?php
get_footer();

?>
