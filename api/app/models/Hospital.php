<?php

class Hospital extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'hospital_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'hospital_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

}
