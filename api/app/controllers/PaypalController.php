<?php

class PaypalController extends ApiBaseController {

    const KonsultUserId = 160;

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function generateAccessToken() {

        $checkAuthToken = True;
        $result = $this->checkAuth($checkAuthToken);
        if ($result['config_status'] == -1) {
            return ($result);
        } elseif ($result['config_status'] == 0) {
            $this->setHeader('RESPONSE_CODE', 0);
            return Response::make($result, 200, $this->headers);
        }

        try {
            $twilioHelper = $this->helper->load('paypal');
            $accessToken = $twilioHelper->generateAccessToken();
            if (!empty($accessToken)) {
                $response['status'] = 1;
                $response['msg'] = 'Token generated successfully.';
                $response['clientToken'] = $accessToken;
            } else {
                $response['status'] = 0;
                $response['msg'] = 'Token generation failed.';
            }

            return Response::make($response, 200);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function transaction() {

        $checkAuthToken = True;
        $result = $this->checkAuth($checkAuthToken);
        if ($result['config_status'] == -1) {
            return ($result);
        } elseif ($result['config_status'] == 0) {
            $this->setHeader('RESPONSE_CODE', 0);
            return Response::make($result, 200, $this->headers);
        }

        $input = Input::all();
        $input['auth'] = trim($input['auth']);
        $input['amount'] = $this->decodeAuth(array('auth' => $input['auth']));
        
        if ($input['amount'] <= 0 || empty($input['payment_method_nonce'])) {
            $result['status'] = 0;
            $result['msg'] = "Invalid input's!";
            return Response::make($result, 200);
        }

        try {

            //LOG TRANSACTION
            $user_id = $this->getUserIdbyToken($this->authToken);
            $paypalTransaction = new PaypalTransaction();
            $paypalTransaction->user_id = $user_id;
            $paypalTransaction->save();

            $input['paypal_transaction_id'] = $paypalTransaction->transaction_id;

            $paypalHelper = $this->helper->load('paypal');
            $transaction = $paypalHelper->createTransaction($input);
            if (!empty($transaction['status'])) {
                $response['status'] = 1;
                $response['msg'] = 'Transaction has been completed successfully.';
                $response['transaction_id'] = $transaction['transaction_id'];
                $response['paypalResponse'] = $transaction['paypalResponse'];

                //INSERT ENTRY IN kcc_recharge_info && kcc_transactions
                $rechargeInfo = new KccRechargeInfo();
                $rechargeInfo->user_id = $user_id;
                $rechargeInfo->code_info_id = $input['paypal_transaction_id'];
                $rechargeInfo->recharge_type = 'paypal';
                $rechargeInfo->save();

                $currencyFactor = Config::get('constants.INTERNATIONAL_CALL_CHARGE_CURRENCY_FACTOR');

                $kccTransactionType = new KccTransactionType();
                $transaction = new KccTransaction();
                $transaction->transferred_to = $user_id;
                $transaction->transferred_from = PaypalController::KonsultUserId;
                $transaction->amount = $input['amount'] * $currencyFactor;
                $transaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));
                $transaction->transaction_details_id = $rechargeInfo->recharge_info_id;
                $transaction->save();
            } else {
                $response['status'] = 0;
                $response['msg'] = 'Transaction has been failed!';
                $response['error_msg'] = $transaction['error_msg'];
            }

            $paypalTransaction->response = json_encode($response);
            $paypalTransaction->save();

            return Response::make($response, 200);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function decodeAuth($params = array()) {

        $params['auth'] = base64_decode($params['auth']);
        
        $search = array('a', 'c', 'd', 'f', 'g', 'i', 'l', 'n', 'q', 'x');
        $replace = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);

        $str = str_replace($search, $replace, $params['auth']);

        $value = (int) $str;
        $value -= 354;
        $value /= 698;

        return $value;
    }

}
