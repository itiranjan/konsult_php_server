<?php
/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */
@ob_start();
if($_SERVER['REQUEST_URI'] == '/category/events/' || $_SERVER['REQUEST_URI'] == '/category/'){
header('Location:http://test.konsultapp.com//health-tips/');
exit;
}

?><!DOCTYPE html>
<?php 
	if( $_GET && key_exists('mfn-rtl', $_GET) ):
		echo '<html class="no-js" lang="ar" dir="rtl">';
	else:
?>
<html class="no-js" <?php language_attributes(); ?> <?php mfn_tag_schema(); ?>>
<?php endif; ?>

<!-- head -->
<head>

<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />

  
<?php if( mfn_opts_get('responsive') ) echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
 ?>

<!--<title itemprop="name"><?php
// if( mfn_title() ){
	// echo mfn_title();
// } else {
	// global $page, $paged;
	// wp_title( '|', true, 'right' );
	// bloginfo( 'name' );
	// if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'betheme' ), max( $paged, $page ) );
// }
?></title>-->

<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" href="<?php mfn_opts_show( 'favicon-img', THEME_URI .'/images/favicon.ico' ); ?>" />	
<?php if( mfn_opts_get('apple-touch-icon') ): ?>
<link rel="apple-touch-icon" href="<?php mfn_opts_show( 'apple-touch-icon' ); ?>" />
<?php endif; ?>	

<!-- wp_head() -->
<?php wp_head(); ?>

<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
global $wp;
$current_url = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
			/* fetch meta data from here start */
			global $wpdb;
			global $per_min_charges, $page_url ,$my_wpdb ,$user_id, $docRow ,$name,$address,$city,$area,$email,$experince,$degreeName,$hospitalName,$designationName,$collegeName,$universityName,$specialization,$specialId ;
			$page_url = get_query_var('page_url');

			//$my_wpdb = new WPDB('tusr1', 't2u3r1opK', 't1_konsult', 'localhost');
			$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');

			$user_id = $my_wpdb->get_var("select user_id FROM doctor_profile where page_url = '$page_url';", 0, 0);
			$per_min_charges = $my_wpdb->get_var("select per_min_charges FROM doctor_profile where page_url = '$page_url';", 0, 0);
//$sql  = 'SELECT per_min_charges as fee FROM `doctor_profile` WHERE user=55';
			$isDoctor = $my_wpdb->get_var("select up.user_id FROM user_profile AS up where up.is_doctor = 1 AND up.user_id = $user_id;", 0, 0);

			if(empty($isDoctor)) {
               // header("HTTP/1.1 404 Not Found");
        nocache_headers();
                include( get_query_template( '404' ) );
        //          global $wp_query;
    //$wp_query->set_404();
                status_header( 404 );
      header("HTTP/1.1 404 Not Found");
        die();
				//exit("exiting 404");
			} 
			
			$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, dp.address AS address, up.is_doctor, cm.name AS city, up.photo, up.photo,
dp.total_experience AS total_experience ,dsr.specialization_id as specializationID, dsm.name as specializationName , u.email AS email, dp.area AS area, dp.about as about, dp.other_specializations, dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user_id )
LEFT JOIN doctor_profile dp ON ( up.user_id = dp.user_id )
LEFT JOIN doctor_qualifications dq ON ( up.user_id = dq.user_id )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user_id = dsr.user_id )
LEFT JOIN specialization_master dsm ON ( dsr.specialization_id = dsm.specialization_id )

LEFT JOIN doctor_workplace dex ON ( dp.user_id = dex.user_id )


LEFT JOIN city_master cm ON dp.city_id = cm.city_id
WHERE dp.user_id ='$user_id'");
			
			$name = $docRow->name ;
$address = $docRow->address ;
$city = $docRow->city ;
$area = $docRow->area ;
$email = $docRow->email ;
$experince =  $docRow->total_experience ;
$degreeName =  $docRow->degreeName ;
$hospitalName = $docRow->hospitalName ;
$designationName = $docRow->designationName ;
$collegeName = $docRow->collegeName ;
$universityName = $docRow->universityName ;
$specialization = $docRow->specializationName ;
$specialId = $docRow->specializationID ;

			
			/* fetch meta data from here end */
?>
<title itemprop="name"><?php echo "$name, $specialization, $city | View Profile"; ?></title>
<meta  name="keywords" content="<?php echo "$name"; ?>">
  <meta name="description" content="<?php echo "$name is a $specialization in $city and practices at $hospitalName. $name has $experince years of experience, and is available for tele-consultation on Konsult App"; ?>">
  <meta property="og:title" content="<?php echo "$name, $specialization, $city | View Profile"; ?>"/>
<meta property="og:image" content="<?php echo "http://api.konsultapp.com/photo/".$docRow->photo; ?>"/>
<meta property="og:site_name" content="<?php echo "$name"; ?>"/>
<meta property="og:description" content="<?php echo "$name is a $specialization in $city and practices at $hospitalName. $name has $experince years of experience, and is available for tele-consultation on Konsult App"; ?>"/>
<meta property="og:url" content="<?php echo $current_url; ?>" />
</head>

<!-- body -->
<body <?php body_class(); ?>>
	
	<?php do_action( 'mfn_hook_top' ); ?>
	
	<?php get_template_part( 'includes/header', 'sliding-area' ); ?>
	
	<?php if( mfn_header_style( true ) == 'header-creative' ) get_template_part( 'includes/header', 'creative' ); ?>
	
	<!-- #Wrapper -->
	<div id="Wrapper">
	
		<?php 
			// Header Featured Image ----------
			$header_style = '';
			
			// Image -----
			if( mfn_ID() && ! is_search() ){
				if( ( ( mfn_ID() == get_option( 'page_for_posts' ) ) || ( get_post_type() == 'page' ) ) && has_post_thumbnail( mfn_ID() ) ){
					
					// Pages & Blog Page ---
					$subheader_image = wp_get_attachment_image_src( get_post_thumbnail_id( mfn_ID() ), 'full' );
					$header_style .= ' style="background-image:url('. $subheader_image[0] .');"';

				} elseif( get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) ){

					// Single Post ---
					$header_style .= ' style="background-image:url('. get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) .');"';

				}
			}
			
			// Attachment -----
			if( mfn_opts_get('img-subheader-attachment') == 'fixed' ){
				$header_style .= ' class="bg-fixed"';
			} elseif( mfn_opts_get('img-subheader-attachment') == 'parallax' ){
				$header_style .= ' class="bg-parallax" data-stellar-background-ratio="0.5"';
			}
		?>
		
		<?php if( mfn_header_style( true ) == 'header-below' ) echo mfn_slider(); ?>
	
		<!-- #Header_bg -->
		<div id="Header_wrapper" <?php echo $header_style; ?>>
	
			<!-- #Header -->
			<header id="Header">
				<?php if( mfn_header_style( true ) != 'header-creative' ) get_template_part( 'includes/header', 'top-area' ); ?>	
				<?php if( mfn_header_style( true ) != 'header-below' ) echo mfn_slider(); ?>
			</header>
				
			<?php 
			
			
			
				if( ( mfn_opts_get('subheader') != 'all' ) && ! get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ){
					
					if( is_search() ){
						// Page title -------------------------
						
						echo '<div id="Subheader">';
							echo '<div class="container">';
								echo '<div class="column one">';
								
									global $wp_query;
									$total_results = $wp_query->found_posts;
									
									$translate['search-results'] = mfn_opts_get('translate') ? mfn_opts_get('translate-search-results','results found for:') : __('results found for:','betheme');								
									echo '<h1 class="title">'. $total_results .' '. $translate['search-results'] .' '. get_search_query() .'</h1>';
									
								echo '</div>';
							echo '</div>';
						echo '</div>';
						
					} elseif( ! mfn_slider() || mfn_opts_get( 'subheader-slider-show' ) ){
						// Page title -------------------------
						
						// Subheader | Options
						$subheader_options = mfn_opts_get( 'subheader' );

						if( is_array( $subheader_options ) && isset( $subheader_options['hide-subheader'] ) ){
							$subheader_show = false;
						} elseif( get_post_meta( mfn_ID(), 'mfn-post-hide-title', true ) ){
							$subheader_show = false;
						} else {
							$subheader_show = true;
						}
						
						if( is_array( $subheader_options ) && isset( $subheader_options['hide-breadcrumbs'] ) ){
							$breadcrumbs_show = false;
						} else {
							$breadcrumbs_show = true;
						}
						
						if( is_array( $subheader_options ) && isset( $subheader_options['breadcrumbs-link'] ) ){
							$breadcrumbs_link = 'has-link';
						} else {
							$breadcrumbs_link = 'no-link';
						}
						
						// Subheader | Print
						if( $subheader_show ){
							echo '<div id="Subheader">';
								echo '<div class="container">';
									echo '<div class="column one">';
										
										// Title
										echo '<h1 class="title">'.$name.', '.$specialization.', '.$city.'</h1>';
										//echo "arsh";
										// Breadcrumbs
										if( $breadcrumbs_show ) mfn_breadcrumbs( $breadcrumbs_link );
										
									echo '</div>';
								echo '</div>';
							echo '</div>';
						}
						
					}
					
				}
			?>
		
		</div>
		
		<?php do_action( 'mfn_hook_content_before' ); ?>
		<script>
		//document.querySelector('meta[name="description"]').setAttribute("content", "hello desc");
		</script>