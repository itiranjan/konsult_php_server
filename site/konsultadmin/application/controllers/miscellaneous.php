<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class miscellaneous extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('user_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function makeISDCall() {
        $this->load->view('admin/header');
        $this->load->view("admin/makeISDCall");
    }

    public function connectISDCall() {

        $viewer_id = $this->session->userdata('user_id');

        if ($viewer_id) {
            $query = $this->db->query("SELECT users.mobile, users.token FROM users WHERE user_id = $viewer_id");
            $viewerData = $query->result();
            foreach ($viewerData as $viewer) {
                $token = $viewer->token;
            }
        } else {
            echo '{"message":"Un-authorized user!","status":"0"}';
        }

        $rowData = array();
        $phone = $_POST['phone'];
        $max_call_duration = 500;

        $url = APIURL . "connectISDCall?phone=$phone";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'API_KEY:andapikey', 'APP_VERSION:1.0', 'CONFIG_VERSION:1.0', "AUTH_TOKEN:$token"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("caller_phone" => $viewer->mobile,"receiver_phone" => $phone, 'max_call_duration' => $max_call_duration, 'caller_id' => $viewer_id)));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'make_isd_calls';
        $logData['request'] = array('url' => $url);
        $responseObject = json_decode($output);
        $responseArray = array();
        $responseArray['logging_msg'] = !empty($responseObject->msg) ? $responseObject->msg : $responseObject->errors;
        $responseArray['status'] = $responseObject->status;
        $logData['response'] = $responseArray;
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);

        echo $output;
    }

}
