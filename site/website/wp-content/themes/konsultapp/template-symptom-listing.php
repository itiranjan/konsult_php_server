<?php
/**
 * Template Name: Symptom Listing
 * Description: A Page Template that display Listing items.
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header(symptom);


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
?>



 <script src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jssocials.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/jssocials-theme-flat.css" />

<script>

function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    // Need to decode the URL parameters, including putting in a fix for the plus sign
    // http://stackoverflow.com/a/24417399
    return results ? decodeURIComponent(results[1].replace(/\+/g, '%20')) : null;
}
	//console.log(urlParam("pagee"));
//	let searchParams = new URLSearchParams(window.location.search);
	let searchParams = [];
	if(urlParam("pagee") != null){
		searchParams['pagee'] = urlParam("pagee");
	}
console.log(searchParams['pagee']);
$(function() {
var showTotalChar = 200, showChar = "Read more (+)", hideChar = "Less (-)";
$('.show').each(function() {
var content = $(this).text();
if (content.length > showTotalChar) {
var con = content.substr(0, showTotalChar);
var hcon = content.substr(showTotalChar, content.length - showTotalChar);
var txt= con +  '<span class="dots">...</span><span class="morectnt"><span>' + hcon + '</span>&nbsp;&nbsp;<a href="" class="showmoretxt">' + showChar + '</a></span>';
$(this).html(txt);
}
});
$(".showmoretxt").click(function() {
if ($(this).hasClass("sample")) {
$(this).removeClass("sample");
$(this).text(showChar);
} else {
$(this).addClass("sample");
$(this).text(hideChar);
}
$(this).parent().prev().toggle();
$(this).prev().toggle();
return false;
});
});
	
	$("#share").jsSocials({
    shares: ["twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon", "whatsapp"]
});
</script>
 <!-- #Content -->
<div id="Content">
    <div class="content_wrapper profile_width clearfix">
<?php
            $pageurlq = "";
        $pageurlq = get_query_var( 'page_url', "" );
        $location = "all";
        $location = get_query_var("location","all");
         $linkkk = get_permalink();
      $hhh = str_replace("-"," ",$pageurlq);
        $linkkk = $linkkk."/".$pageurlq;  
        
           ?>
        <!-- .sections_group -->
       <div class="section_wrapper clearfix">
 
           <style>
               .locationheading .col-sm-3wb h1{
               font-size: 14px;
    line-height: 25px;
               }
               .locationheading .col-sm-3wb{width:33%;float:left}
               .locationheading .col-sm-9wb .headcityloc {
                   padding: 10px 20px;
    border: 1px solid;
    margin-left: 10px;
                   cursor:pointer;
               }
               .locationheading .col-sm-9wb{width:67%;float:left}
               .bhhead{
                   text-align: center;
                   margin-top: 50px;
                   margin-bottom: 50px;
               }
               strong{
               text-transform: capitalize;    
               }
           </style>
            
          <?php
           if($location == "all"){
               ?>
           <h2 class="bhhead">Top doctors from <strong>India</strong> specialised in <strong><?php echo $hhh; ?></strong></h2>
           <?php
           }
           else{
               ?>
           <h2 class="bhhead">Top doctors from <strong> <?php echo $location; ?> </strong> specialised in <strong><?php echo $hhh; ?></strong> </h2>
           <?php
           }
           ?>
            <!-- one-ninth part of doctor details -->
				<?php  if(!empty($specializationData->about)) { ?>
				<div class="doctor-detail">
            <div class="row">
                <div class="col-md-9 doc-ful-details">
                <h1 style="text-transform: capitalize;">About <?php echo $tagforpost; ?></h1>
                <p>
<div class="show"><?php  echo $specializationData->about ;  ?></div>
	
	</p>
                
                
				</div>
			 	</div>
				</div>
			 <?php } ?>
<div id="alldoctors" class="avail-doctor"><!--    <p class="result-found">78 doctor(s) found.</p>-->
    <div id="results">   
          
<!-- results appear here -->     
   
		   </div>
    <div class="newresultishere">
    <?php
        $pageurlq = "";
        $pageurlq = get_query_var( 'page_url', "" );
        $paginum = get_query_var( 'pagee', "1" );
        $location = "all";
        $location = get_query_var("location","all");
  //      global $wp_query;
//$location = $wp_query->get( 'location' );
        if(isset($_REQUEST["pagee"])){
            $pagenumber = $_REQUEST["pagee"];
        $current_page = $_REQUEST["pagee"];
        }
        else{
          $pagenumber = 1;
        $current_page = 1;  
        }
        
        symptom_ajax_request_direct($page_url,$pagenumber, $location);
         $totaldoctors = get_doc_count_symptom_direct($page_url, $location);
   $totalpages = $totaldoctors/10; 
        //$current_page = 1;
       
        ?>
        <script>
        console.log("this is location <?php echo $location; ?>");
        </script>
    </div>
  <div class="clrfx"></div>
  <div align="center" class="mmrg">
	  
        <button id="load_more_button"><img src="<?php bloginfo('template_directory'); ?>/images/ajax-loader.gif"  class="animation_image" style="float:left;"><img src="<?php bloginfo('template_directory'); ?>/images/ajax-loader.gif"  class="animation_image" style="float:left;"> Load More</button> <!-- load button -->
    <div id="docpag">
	
	<div class="">
	<?php echo paginate_function_symptom_direct(10, $current_page, $totaldoctors, $totalpages, $pageurlq, $location); ?>
	</div>
	
	
	
	
	</div>
	</div>
	<div class="clrfx"></div>
               
			 
			
		  <?php
		   
		   
		   $args = array(
    'posts_per_page' => 3,
    //'s' => $tagforpost,
    'tag' => $tagforpost,
	'post_type'=> 'post',			   
	'orderby' => 'date',
	'order' => 'DESC'
   // 'cat' => 9,
);
$res = new WP_Query($args);
//$res = query_posts($args);
	//print_r($res);	   
	if ( $res->have_posts() ) :
		   while( $res->have_posts() ): $res->the_post();
       
		   $content = get_the_content();
		   $content =  wp_filter_nohtml_kses( $content );
		   $content = substr($content,0,230);
		   $result = substr($content, 0, strrpos($content, ' '));
		  // '<a href="'.get_the_permalink().'" rel="bookmark">Permalink</a>';
   // echo $urlShare = get_the_permalink();
		   ?>
		   
		  
				
				
		   
				 <div class="doctor-detail">
            <div class="row">
			
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">
				<?php    $imgurl =  get_the_post_thumbnail_url( $post->ID,'thumbnail' ); if (@getimagesize($imgurl)) {	?>
                <img style="float: left; margin: 0px 15px 15px 0px; border-radius: 10px;" src="<?php echo $imgurl ; ?>"  />
					<?php } ?>
					<h1><?php the_title();  ?></h1>
                <p> <?php echo $result ; ?>.....<a href="<?php echo get_the_permalink(); ?>" class="align-right"> Read more</a></p>
                
               <?php /* <div class="art-box">
					<div class="column one-second column_column " id="shareRoundIcons<?php  echo get_the_ID() ; ?>"></div>   
					
                 <div class="column one-second column_column "></div></div>
             <?php /*   <script>
        $("#shareRoundIcons<?php  echo get_the_ID() ; ?>").jsSocials({
    url: "<?php echo get_the_permalink(); ?>",
	showLabel: false,
    showCount: false,
    shares: [ "twitter", "facebook",  "linkedin", "pinterest"]
});

    </script> */ ?>
				</div>
				</div>
				</div>
		   
		   
		   <?php
		   
		   endwhile;
		    endif;
		   wp_reset_query();
		   ?>
				
			
    
    
    </div>
		</div>

<hr />
  <div class="locationheading">
      <div class="col-sm-3wb">
      <h1>Find Best <strong><?php echo $hhh; ?></strong> in your city :</h1>
          </div>
      <div class="col-sm-9wb">
          <span><a href="<?php echo $linkkk; ?>" class="headcityloc">All</a><a href="<?php echo $linkkk; ?>/delhi/" class="headcityloc">Delhi</a><a href="<?php echo $linkkk; ?>/gurgaon/" class="headcityloc">Gurgaon</a><a href="<?php echo $linkkk; ?>/noida/" class="headcityloc">Noida</a><a href="<?php echo $linkkk; ?>/faridabad/" class="headcityloc">Faridabad</a></span>
           </div>
           </div>
           <hr />

</div></div>

	<script type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
	<script type="text/javascript">
        /*
	$(document).ready(function(){
	var track_page = 1; //track user click as page number, righ now page number 1
	//load_contents(track_page); //load content
function chkurl(){
	if(searchParams['pagee']){
		var attrval = searchParams['pagee'];
		//alert(attrval);
		//$( "#docpag ul li" ).removeClass("active");
		$("#docpag ul li").each(function(){
			$(this).removeClass("active");
			//alert("looping");
			var trackpagee = $(this).attr('trackpage');
			//alert(trackpagee);
			//alert(trackpagee + "  " + attrval );
			 if( trackpagee == attrval){
				 //alert();
				 $(this).addClass("active");
			 }
		});
		//$( '#docpag ul li[trackpage="'+ attrval +'"]' ).addClass("active");
	//load content
	load_contents(searchParams['pagee']);
	}
	else{
		var track_page = 1; //track user click as page number, righ now page number 1
	load_contents(track_page); 
	
	}
	}
	$("#load_more_button").click(function (e) { //user clicks on button
		track_page++; //page number increment everytime user clicks load button
		load_contents(track_page); //load content
	});

		//Ajax load function
function load_contents(track_page){
	//alert("calling load content");
	
	
	$('.animation_image').show(); //show loading image
	
	var data = {
		'action': 'symptom_ajax_request',
		'page_url': '<?php// echo $page_url ; ?>',
		'page': track_page,
		
	};
	var ajaxurl = "<?php// echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	$.post(ajaxurl, data, function(response) {
		
		
		if(response.trim().length == 0){
			//display text and disable load button if nothing to load
			$("#load_more_button").text("You have reached end of the record!").prop("disabled", true);
		}
		
		
		$("#results").html(response); //append data into #results element
		
		//$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 100);
	
		//hide loading image
		$('.animation_image').hide(); //hide loading image once data is received
		
		
		
	});
	
	
	
	//return false ;
	/*
	$.post( 'example_ajax_request', {'page': track_page}, function(data){
		
		if(data.trim().length == 0){
			//display text and disable load button if nothing to load
			$("#load_more_button").text("You have reached end of the record!").prop("disabled", true);
		}
		
		$("#results").append(data); //append data into #results element
		
		//scroll page to button element
		$("html, body").animate({scrollTop: $("#load_more_button").offset().top}, 800);
	
		//hide loading image
		$('.animation_image').hide(); //hide loading image once data is received
				});
				*/
		/*	}
			function getdoccount(){
				var data = {
		'action': 'get_doc_count_symptom',
		'page_url': '<?php //echo $page_url ; ?>'
		
	};
	var ajaxurl = "<?php// echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	$.post(ajaxurl, data, function(response) {
		$("#results").attr("doccount",response); //append data into #results element	
		$("#results").attr("pagecount",Math.round(response/100)); //append data into #results element	
		//alert(response%10);
		//console.log(Math.round(response/10));
		if(response < 100){
			for(var i=1;i<=1;i++){
			if(i == 1){
				$("#docpag ul").append("<li class='active' trackpage='"+i+"'>"+i+"</li>");
			}
			else{
				$("#docpag ul").append("<li trackpage='"+i+"'>"+i+"</li>");
			}
			chkurl();
			
		}
		}
		else{
		for(var i=1;i<=Math.round(response/100);i++){
			if(i == 1){
				$("#docpag ul").append("<li class='active' trackpage='"+i+"'>"+i+"</li>");
			}
			else{
				$("#docpag ul").append("<li trackpage='"+i+"'>"+i+"</li>");
			}
			chkurl();
			
		}
		}
	});
				
			}
			
				getdoccount();
			
			$("#docpag ul").on('click', 'li',function (e) { //user clicks on button
			//alert($(this).attr('trackpage'));
		track_page = $(this).attr('trackpage'); //page number increment everytime user clicks load button
		load_contents(track_page); //load content
		$("#docpag ul li").removeClass("active");
		$(this).addClass("active");
		//addURL(track_page);
		if(searchParams['pagee']){
			window.location.href = window.location.pathname+'?pagee='+track_page;
		}
		else{
		window.location.href =window.location.href+'?pagee='+track_page;
		}
	});
	
	if(searchParams['pagee']){
		//alert(searchParams.get('pagee'));

		}
		   $('.prscroll').click(function() {
      
      $('#docpag ul').animate({
        scrollLeft: "+=50px"
      }, "slow");
   });
   
     $('.plscroll').click(function() {
     
      $('#docpag ul').animate({
        scrollLeft: "-=50px"
      }, "slow");
   });
   });*/
</script>
<style>
#docpag ul li:hover {
	background-color: #24cdd9;
color: #fff;
}
#docpag ul li.active {
	background-color: #24cdd9;
color: #fff;
}
#load_more_button{
	display:none;
}
#docpag ul{
	
overflow: hidden;
height: 45px;
/* margin-top: -22px;
margin-bottom: -46px; */
}
#docpag ul li {
	color:#24cdd9;
    display: inline;
    padding: 0px 8px 0px 6px;
    border: 1px #24cdd9 solid;
    border-radius: 12px;
    margin-left: 10px;
    cursor: pointer;
}
.mmrg
{
	margin-bottom:15px;
}
.plscroll:hover{
	background-color: #24cdd9;
color: #fff;
}
.plscroll{
	padding:5px;
	color: #24cdd9;
	/* margin-left: -202px; */
	cursor:pointer;
}
.prscroll:hover{
	background-color: #24cdd9;
color: #fff;
}
.prscroll{
	padding:5px;
	color: #24cdd9;
	/* margin-left: 224px; */
	cursor:pointer;
}
.clrfx {
    height: 20px;
    width: 100%;
}
.pr3 {
	text-align:left;
}
.pr1 {
	text-align:right;
}
.prr {
    width: 33%;
    float: left;
}
.doctor-detail {
    padding: 20px 25px 0px 25px;
	}
	
	.doctor-detail .learn-more-btn.rgt.align_right1 {
    margin-top: -50px;
}
.doctor-img img.scale-with-grid, #Content img {
    max-width: 80%;
    height: auto;
}
.doctor-biography h5{}
.doctor-biography p{
	font-size: 12px;
	line-height: 20px;
}

.doctor-biography .designation{font-size: 14px;}

.doctor-biography .row .round {
    width: 25px;
    height: 25px;
}
.prac-place{
	
	color: rgba(0,0,0,.5);
}
.doctor-biography .fee p:nth-child(2)
{
	color: #333 !important;
}
</style>
<?php
function symptom_ajax_request_direct($pgurl, $pgnum, $location) {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						//$page_url = $_REQUEST['page_url'];
						$page_url = $pgurl;
						//$page_number = filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
						$page_number = $pgnum;
						$item_per_page = 10;
						$position = (($page_number-1) * $item_per_page);
						
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
    if($location == "all")
    {
        $specialIdRand = $my_wpdb->get_results("SELECT DISTINCT(doctor_specialization_rel.user_id) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization_id JOIN doctor_profile ON doctor_profile.user_id = doctor_specialization_rel.user_id JOIN user_profile ON doctor_profile.user_id = user_profile.user_id JOIN city_master ON doctor_profile.city_id = city_master.city_id WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1 ORDER BY doctor_profile.weightage LIMIT $position, $item_per_page ");
        
    }
    else{
        $specialIdRand = $my_wpdb->get_results("SELECT DISTINCT(doctor_specialization_rel.user_id) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization_id JOIN doctor_profile ON doctor_profile.user_id = doctor_specialization_rel.user_id JOIN user_profile ON doctor_profile.user_id = user_profile.user_id JOIN city_master ON doctor_profile.city_id = city_master.city_id WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1 AND city_master.name like '".$location."' ORDER BY doctor_profile.weightage LIMIT $position, $item_per_page ");
        
    }
							
	
	
	
						foreach ($specialIdRand as $doc) {	
$docId = 	$doc->user_id ;
	
$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, dp.address, up.is_doctor, cm.name AS city, up.photo, up.photo,
dp.total_experience,dsr.specialization_id as specializationID, dsm.name as specializationName , u.email,
dp.area, dp.about as about, dp.other_specializations,dp.per_min_charges as charges,dp.page_url as pageUrl,
dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user_id )
LEFT JOIN doctor_profile dp ON ( up.user_id = dp.user_id )
LEFT JOIN doctor_qualifications dq ON ( up.user_id = dq.user_id )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user_id = dsr.user_id )
LEFT JOIN specialization_master dsm ON ( dsr.specialization_id = dsm.specialization_id )

LEFT JOIN doctor_workplace dex ON ( dp.user_id = dex.user_id )


LEFT JOIN city_master cm ON dp.city_id = cm.city_id
WHERE dp.user_id = '$docId' ");	
	?>
<div class="doctor-detail">
            <div class="row">
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">

                    <!-- Doctor Image -->
                    <div class="doctor-img">
 <?php
     $imageavailable = 0;
     if(file_exists(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/konnect/api/public/photo/'.$docRow->photo)){
         $imageavailable = 1;
     }
     else{}
     ?>
                        <img alt="<?php  echo $docRow->name.", ".$docRow->specializationName.", ".$docRow->hospitalName ;  ?>" src="http://api.konsultapp.com/photo/<?php  if($docRow->photo != null AND $imageavailable == 1){ echo $docRow->photo; }else{ echo "user.png"; }  ?>" width="150" height="150">
                    </div>
                    <!-- End Doctor Image -->

                    <!-- Doctor Details -->
                    <div class="doctor-biography">
                        <a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>"><h5 class="doctor-name"> <?php  echo $docRow->name ;  ?></h5></a>
                       <p class="designation"><?php  echo $docRow->specializationName ;  ?></p> 
                        <p><?php  echo $docRow->degreeName ;  ?> - <?php  echo $docRow->hospitalName ;  ?> </p> 
                        

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="round">
                                   <i class="fa fa-user-md" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Experience</p>
                                    <p class=""><?php  echo $docRow->total_experience ;  ?> Years</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                            
                            
                        <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Fee</p>
                                    <p class=""><?php  echo $docRow->charges ;  ?>/min</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                       
                       <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="doc-practice">
                                    <p class="prac-txt">Location</p>
                                    <p class="prac-place"><?php  echo $docRow->area ;  ?> <?php  echo $docRow->city ;  ?></p> 
                                </div>
                            </div>
                    </div>
                    
      <div class="learn-more-btn rgt align_right1">
		<a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>">  <span class="learn-more-txt">View Profile</span> </a>
	</div>
                    </div>
                 </div>
            </div>
        </div>
<?php }
							
							
						//echo "outside foreach";
							//die();
}

function paginate_function_symptom_direct($item_per_page, $current_page, $total_records, $total_pages, $pageurlq, $location) {
    $pagination = '';
    if ($total_pages > 0 && $current_page <= $total_pages) {  //&& $total_pages != 1 //verify total pages and current page number
        if($location == "all"){
          $linkk = get_permalink();
        $linkk = $linkk."/".$pageurlq;  
        }
        else{
          $linkk = get_permalink();
        $linkk = $linkk."/".$pageurlq."/".$location;  
        }
        
        $pagination .= '<ul class="pagination">';

        $right_links = $current_page + 3;
        $previous = $current_page - 3; //previous link 
        $next = $current_page + 1; //next link
        $first_link = true; //boolean var to decide our first link

        if ($current_page > 1) { //echo "<br>current_page:".$current_page."<br>";
            $previous_link = ($previous == 0) ? 1 : $previous;
			$cpg = $current_page - 1;
            $pagination .= '<li class="first" rel="prev"><a href="'.$linkk.'?pagee='.$cpg.'" data-page="'.$cpg.'" title="First">Back</a></li>'; //first link
            //$pagination .= '<li><a href="#" data-page="'.$previous_link.'" title="Previous">&lt;</a></li>'; //previous link
            for ($i = ($current_page - 2); $i < $current_page; $i++) { //Create left-hand side links
                if ($i > 0) {
                    $pagination .= '<li><a href="'.$linkk.'?pagee='.$i.'" data-page="' . $i . '" title="Page' . $i . '">' . $i . '</a></li>';
                }
            }
            $first_link = false; //set first link to false
        }

        if ($first_link) { //if current active page is first link
            $pagination .= '<li class="first active"><span>' . $current_page . '</span></span></li>';
        } else if ($current_page == $total_pages) { //if it's the last active link
            $pagination .= '<li class="last active"><span>' . $current_page . '</span></li>';
        } else { //regular current link
            $pagination .= '<li class="active"><span>' . $current_page . '</span></li>';
        }

        for ($i = $current_page + 1; $i < $right_links; $i++) { //create right-hand side links 
            if ($i <= $total_pages) {
                $pagination .= '<li><a href="'.$linkk.'?pagee='.$i.'" data-page="' . $i . '" title="Page ' . $i . '">' . $i . '</a></li>';
            }
        }
        if ($current_page < $total_pages) {
            $next_link = ($i > $total_pages) ? $total_pages : $i;
            //$pagination .= '<li><a href="#" data-page="'.$next_link.'" title="Next">&gt;</a></li>'; //next link
			$cpg = $current_page + 1;
            $pagination .= '<li class="last"><a href="'.$linkk.'?pagee='.$cpg.'" data-page="' . $cpg . '" title="Last" rel="next">Next</a></li>'; //last link
        }

        $pagination .= '</ul>';
    }
    return $pagination; //return pagination links
}

function get_doc_count_symptom_direct($pgrul, $location) {
	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						//$page_url = $_REQUEST['page_url'];
						$page_url = $pgrul;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
    if($location == "all"){
        
        return $specialIdRa= $my_wpdb->get_var("SELECT COUNT(DISTINCT(doctor_specialization_rel.user_id)) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization_id JOIN doctor_profile ON doctor_specialization_rel.user_id = doctor_profile.user_id WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1");	

    }
    else{
        
        return $specialIdRa= $my_wpdb->get_var("SELECT COUNT(DISTINCT(doctor_specialization_rel.user_id)) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization_id JOIN doctor_profile ON doctor_specialization_rel.user_id = doctor_profile.user_id JOIN user_profile ON doctor_profile.user_id = user_profile.user_id JOIN city_master ON user_profile.city = city_master.city_id WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1 AND city_master.name like '".$location."'");	

    }
						}



get_footer(); ?>
 
