<?php

class specialization_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function specializationList() {

        $sql = "SELECT name FROM specialization_master";

        return $this->db->query($sql);
    }

}
