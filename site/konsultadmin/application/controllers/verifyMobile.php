<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class verifyMobile extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('verifymobile_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/verifyMobile");
    }

    public function verifyMobileList($way) {

        $data = array();
        $resultsData = $this->verifymobile_model->verifyMobileList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'verifymoblog_id' => $row->verifymoblog_id,
                'mobile' => $row->mobile,
                'created_at' => $row->created_at,
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

}
