<?php

class MobileOtp extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'mobile_otp';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'otp_id';

    public function getMobileOtp($params = array()) {
        
        $otpLength = !empty($params['otpLength']) ? $params['otpLength'] : Config::get('constants.DEFAULT_OTP_LENGTH');
        $minLimit = pow(10, $otpLength-1);
        $maxLimit = pow(10, $otpLength) - 1;

        $mobileRaw = $this->where('mobile', '=', $params['mobile'])->first();

        $currentDateTime = date("Y-m-d H:i:s");
        $currentTimestamp = strtotime($currentDateTime);

        if (COUNT($mobileRaw) <= 0) {
            $this->mobile = $params['mobile'];
            $this->otp = mt_rand($minLimit, $maxLimit);
            $this->updated_at = $this->created_at = $currentDateTime;
            $expirtyDate = $currentTimestamp + (60 * 15);
            $this->expired_at = date("Y-m-d H:i:s", $expirtyDate);
            $this->save();

            return $this->otp;
        } else {
            $expirtyTimestamp = strtotime($mobileRaw->expired_at);

            if ($expirtyTimestamp > $currentTimestamp) {
                return $mobileRaw->otp;
            } else {
                $newOtp = mt_rand($minLimit, $maxLimit);

                $expiredAt = date("Y-m-d H:i:s", ($currentTimestamp + (60 * 15)));

                $this::where('mobile', '=', $params['mobile'])->update(array('updated_at' => $currentDateTime, 'expired_at' => $expiredAt, 'otp' => $newOtp));

                return $newOtp;
            }
        }
    }

    public function verifyOtp($params = array()) {

        $currentDateTime = date("Y-m-d H:i:s");

        return $this->where('mobile', '=', $params['mobile'])->where('otp', '=', $params['otp'])->where('expired_at', '>=', $currentDateTime)->pluck('otp_id');
    }

}
