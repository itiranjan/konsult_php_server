<?php

class accesslog_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function insertLog($params = array()) {
        
        $sessionData = $this->session->userdata;

        $logData = array();
        $logData['user_id'] = !empty($params['user_id']) ? $params['user_id'] : (!empty($sessionData['user_id']) ? $sessionData['user_id'] : 0);
        $logData['type'] = $params['type'];
        $params['request'] = !empty($params['request']) ? $params['request'] : array('username' => (!empty($sessionData['username']) ? $sessionData['username'] : NULL));
        $logData['request'] = json_encode($params['request']);
        $logData['response'] = json_encode($params['response']);
        $this->db->insert('admin_users_access_log', $logData);
    }

}
