<?php

class communication_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $ci = get_instance();
        $ci->load->helper('miscellaneous_helper');
    }

    function communicationList($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT pt.name as patient_name, pt.email as patient_email, dr.name as doctor_name, dr.email as doctor_email, ptctm.name AS patient_call_status, drctm.name AS doctor_call_status,calls.call_id, calls.comm_datetime, calls.call_duration, calls.operator_call_duration, calls.telecom_response,spends.total_charges, spends.doctor_charges, spends.virtual_money, spends.real_money, calls.caller_id, calls.receiver_id, calls.followed_by, calls.comment, calls.caller_call_status_id, calls.receiver_call_status_id, pgm.name as payment_gateway_name FROM calls LEFT JOIN spends ON calls.call_id = spends.call_id LEFT JOIN users pt ON pt.user_id = calls.caller_id LEFT JOIN users dr ON dr.user_id = calls.receiver_id LEFT JOIN call_status_master ptctm ON ptctm.call_status_id = calls.receiver_call_status_id LEFT JOIN call_status_master drctm ON drctm.call_status_id = calls.caller_call_status_id LEFT JOIN wallet_master pgm ON pgm.wallet_id = calls.wallet_id ";
        $mainQueryParams['defaultSorting'] = " calls.call_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM calls LEFT JOIN spends ON calls.call_id = spends.call_id LEFT JOIN users pt ON pt.user_id = calls.caller_id LEFT JOIN users dr ON dr.user_id = calls.receiver_id LEFT JOIN call_status_master ptctm ON ptctm.call_status_id = calls.receiver_call_status_id LEFT JOIN call_status_master drctm ON drctm.call_status_id = calls.caller_call_status_id LEFT JOIN wallet_master pgm ON pgm.wallet_id = calls.wallet_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function getUserCommunications($params = array()) {

        $userId = $params['user_id'];
        $sql = "SELECT calls.call_id, calls.comm_datetime AS created_at, calls.call_duration,spends.total_charges, spends.virtual_money, spends.real_money, calls.receiver_id, ptctm.name AS patient_call_status, drctm.name AS doctor_call_status FROM calls LEFT JOIN spends ON calls.call_id = spends.call_id LEFT JOIN call_status_master ptctm ON ptctm.call_status_id = calls.receiver_call_status_id LEFT JOIN call_status_master drctm ON drctm.call_status_id = calls.caller_call_status_id WHERE calls.caller_id = $userId AND (calls.kcc_call_id IS NULL OR calls.kcc_call_id = 0) ORDER BY calls.call_id DESC";

        $query = $this->db->query($sql);

        if ($query->num_rows >= 1) {
            return $query->result();
        }

        return array();
    }

    function kccCallList($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT pt.name as patient_name, pt.email as patient_email, dr.name as doctor_name, dr.email as doctor_email, kcc_calls.kcc_call_id, kcc_calls.user_id, kcc_calls.doctor_user_id, kcc_calls.caller_status, kcc_calls.receiver_status, kcc_calls.created_at, kcc_calls.call_duration, kcc_transactions.amount, kcc_transaction_spend_details.total_charges, kcc_transaction_spend_details.doctor_charges FROM kcc_calls LEFT JOIN kcc_transactions ON kcc_transactions.transaction_details_id = kcc_calls.kcc_call_id LEFT JOIN kcc_transaction_spend_details ON kcc_calls.kcc_call_id = kcc_transaction_spend_details.kcc_call_id LEFT JOIN users pt ON pt.user_id = kcc_calls.user_id LEFT JOIN users dr ON dr.user_id = kcc_calls.doctor_user_id ";
        $mainQueryParams['defaultSorting'] = " kcc_calls.kcc_call_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM kcc_calls LEFT JOIN kcc_transactions ON kcc_transactions.transaction_details_id = kcc_calls.kcc_call_id LEFT JOIN kcc_transaction_spend_details ON kcc_calls.kcc_call_id = kcc_transaction_spend_details.kcc_call_id LEFT JOIN users pt ON pt.user_id = kcc_calls.user_id LEFT JOIN users dr ON dr.user_id = kcc_calls.doctor_user_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function detailsById($id, $format = 'string') {

        $sql = "SELECT name, email FROM users WHERE user_id = $id";

        $params = array();
        $query = $this->db->query($sql, $params);

        if ($query->num_rows == 1) {
            if ($format == 'array') {
                return array('name' => $query->row()->name, 'email' => $query->row()->email);
            } else {
                return $query->row()->name . " ( " . $query->row()->email . " )";
            }
        }

        return NULL;
    }

    function totalActivePatients() {

        $sql = "SELECT COUNT(*) AS total_active_patients FROM calls GROUP BY caller_id LIMIT 1";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_active_patients;
        }

        return 0;
    }

    function totalCalls($params = array()) {

        $sql = "SELECT COUNT(*) AS total_calls FROM calls ";

        if (isset($params['connected'])) {
            $sql .= ' WHERE call_duration IS NOT NULL AND call_duration > 0';
        }

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_calls;
        }

        return 0;
    }

    function totalCallDuration() {

        $sql = "SELECT SUM(call_duration) AS total_call_duration FROM calls WHERE call_duration IS NOT NULL";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_call_duration;
        }

        return 0;
    }

    function totalRevenue() {

        $sql = "SELECT SUM(total_charges-doctor_charges) AS total_revenue FROM spends";

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_revenue;
        }

        return 0;
    }

    function updateCommunicationDetails($rowData = array()) {

        extract($rowData);

        $sql = "UPDATE calls SET $column_name = '$column_value', followed_by = '$followed_by' WHERE call_id = $call_id";

        return $this->db->query($sql);
    }

    function updateRatingFollowup($rowData = array()) {

        extract($rowData);

        $sql = "UPDATE call_ratings SET $column_name = '$column_value', followed_by = '$followed_by' WHERE rating_id = $rating_id";

        return $this->db->query($sql);
    }

    function updateCallBackDetails($rowData = array()) {

        extract($rowData);

        $sql = "UPDATE callback_requests SET $column_name = '$column_value', followed_by = '$followed_by' WHERE callback_request_id = $callback_request_id";

        return $this->db->query($sql);
    }

    function knowlarityList($params = array()) {

        $params['whereCondition'] = "((calls.telecom_response LIKE '%error%') OR (calls.telecom_request IS NOT NULL AND calls.telecom_response IS NULL))";

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT pt.name as patient_name, pt.email as patient_email, dr.name as doctor_name, dr.email as doctor_email, calls.call_id, calls.comm_datetime, calls.telecom_request, calls.telecom_response, calls.caller_id, calls.receiver_id FROM calls LEFT JOIN users pt ON pt.user_id = calls.caller_id LEFT JOIN users dr ON dr.user_id = calls.receiver_id";
        $mainQueryParams['defaultSorting'] = " calls.call_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM calls LEFT JOIN users pt ON pt.user_id = calls.caller_id LEFT JOIN users dr ON dr.user_id = calls.receiver_id";
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function ratingReviews($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT call_ratings.rating_id, call_ratings.call_id, call_ratings.user_id, call_ratings.rating, call_ratings.comment, call_ratings.name, call_ratings.age, call_ratings.sex, call_ratings.created_at, users.name as username, call_ratings.followed_by, call_ratings.followup_comment FROM call_ratings LEFT JOIN users ON call_ratings.user_id = users.user_id ";
        $mainQueryParams['defaultSorting'] = " call_ratings.rating_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM call_ratings LEFT JOIN users ON call_ratings.user_id = users.user_id ";
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function callBackRequests($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT pt.name as patient_name, pt.email as patient_email, dr.name as doctor_name, dr.email as doctor_email, callback_requests.callback_request_id, callback_requests.request_from, callback_requests.request_to, callback_requests.per_min_charges, callback_requests.request_call_id, pgm.name as wallet_name , callback_requests.callback_call_id, callback_requests.created_at, callback_requests.status, callback_requests.followed_by, callback_requests.followup_comment FROM callback_requests LEFT JOIN users pt ON pt.user_id = callback_requests.request_from LEFT JOIN users dr ON dr.user_id = callback_requests.request_to LEFT JOIN wallet_master pgm ON pgm.wallet_id = callback_requests.wallet_id ";
        $mainQueryParams['defaultSorting'] = " callback_requests.callback_request_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM callback_requests LEFT JOIN users pt ON pt.user_id = callback_requests.request_from LEFT JOIN users dr ON dr.user_id = callback_requests.request_to LEFT JOIN wallet_master pgm ON pgm.wallet_id = callback_requests.wallet_id ";
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }
    
    function toggleStatus($data, $id) {
        return $this->db->update('callback_requests', $data, array('callback_request_id' => $id));
    }    

}
