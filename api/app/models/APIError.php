<?php

class APIError extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'error_codes';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'error_code';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = false;

}
