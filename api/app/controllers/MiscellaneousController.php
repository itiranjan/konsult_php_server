<?php

class MiscellaneousController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool $checkAuthToken (To Check Auth token)
     * @return  array
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    /**
     * @author : Anil Kumar
     * @created : 31 dec 2015
     * @description : This method is used for  finding latest Os Version and app Vresion
     * @access public
     * @return  array
     */
    public function priorityspecialization() {

        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $specialization = new Specialization();
            $detailArr = $specialization->priorityspecialization();

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = ($detailArr) ? "Success" : "Not Found";
            $result['response']['list'] = $detailArr;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function verifyMobile() {

        $input = Input::all();

        try {

            $mobileNo = substr($input['mobile'], -10);

            if (preg_match('/^[7-9][0-9]{9}$/', $mobileNo)) {

                $verifyMob = new VerifyMobLog;
                $verifyMob->mobile = $mobileNo;
                $verifyMob->save();

                $result = array('status' => 1, 'msg' => 'Mobile verification details stored successfully.');
            } else {
                $result = array('status' => 0, 'msg' => 'Invalid mobile number.');
            }
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = 'Unable to send the SMS.';
        }

        return Response::make($result, 200, array());
    }

    public function citrusWithdrawalDetails() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $withdrawals = new Withdrawals();

            $data = Input::all();
            $data['user_id'] = $this->getUserIdbyToken($this->authToken);

            if (!$withdrawals->validate($data)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['status'] = 0;
                $result['errors'] = $withdrawals->errors();
            } else {

                $withdrawals->account_holder_name = ucfirst($data['accountHolderName']);
                $withdrawals->bank_account_number = $data['accontNo'];
                $withdrawals->bank_ifsc_code = $data['ifsc'];
                $withdrawals->amount = $data['amount'];
                $withdrawals->user_id = $data['user_id'];
                $withdrawals->status = $data['status'];
                $withdrawals->status_msg = $data['status_msg']; //FOR TESTING PURPOSE ONLY, IT WILL BE GOOD IF WE CAN CATCH SOME ERROR USING THIS.                
                $withdrawals->save();

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = 'Transaction inserted successfully.';
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function generateOTP() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $fullMobile = $data['mobile'];

            if (empty($fullMobile)) {
                $result['status'] = 0;
                $result['msg'] = "Mobile number is required!";
                return Response::make($result, 200, $this->headers);
            }

            //IS MOBILE NUMBER EXIST IN DATABASE
            $first1Chars = substr($fullMobile, 0, 1);
            if ($first1Chars == '+') {
                $fullMobile = substr($fullMobile, 1);
            }

            $first2Chars = substr($fullMobile, 0, 2);
            $internationalCall = 0;
            if ($first2Chars != '91') {
                $internationalCall = 1;
            }

            $mobile = substr($fullMobile, 2);

//            $numberLength = strlen($mobile);
//
//            if ($numberLength != 10 || $internationalCall || !preg_match("/^(6|7|8|9)\d{9}$/", $mobile)) {
//                $result['status'] = 0;
//                $result['msg'] = "International or Invalid mobile number!";
//            } else {

                $otpLength = Config::get('constants.DEFAULT_OTP_LENGTH');

                $mobileOtp = new MobileOtp();
                $getOtp = $mobileOtp->getMobileOtp(array('mobile' => $fullMobile, 'otpLength' => $otpLength));

                //SEND SMS
                $smsText = Config::get('constants.NETCORE_SMS_TEXT_MOBILE_OTP');
                if ($smsText && !empty($getOtp)) {

                    //PREPARE DATA FOR SMS AND SEND
                    $smsParams = array();
                    $smsParams['Text'] = str_replace(array('<OTP>'), array($getOtp), $smsText);
                    $smsParams['To'] = $fullMobile;
                    $this->helper->load('communication')->smsCommunication($smsParams);

                    $result['status'] = 1;
                    $result['otpLength'] = $otpLength;
                    $result['msg'] = "OTP have been sent successfully!";
                } else {
                    $result['status'] = 0;
                    $result['msg'] = "There is some issue with otp generation. Please try again!";
                }
            //}

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function signup() {

        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $data['otp'] = trim($data['otp']);
            $data['password'] = trim($data['password']);
            $data['salutation'] = trim($data['salutation']);
            $data['name'] = trim($data['name']);
            $data['mobile'] = trim($data['mobile']);
            $data['newSignupFlow'] = 1;

            /*
             * status = 0 {signup failed.}
             * status = 1 {signup success.}
             * status = 2 {invalid otp.}
             */
            $user = new User;
            if (!$user->validate($data)) {
                $this->setHeader('RESPONSE_CODE', 0);
                $result['status'] = 0;
                $result['errors'] = $user->errors();
            } else {

                //MISSING OTP AND PASSWORD BOTH
                if (empty($data['otp']) && empty($data['password'])) {
                    $result['status'] = 0;
                    $result['msg'] = 'Inputs are missing!';
                    return Response::make($result, 200, $this->headers);
                }

                //INDAIN MOBILE USER MUST TO LOGIN WITH OTP
                //$isInternationalNumber = $this->isInternationalNumber($data['mobile']);
                if (empty($data['otp'])) {
                    $result['status'] = 0;
                    $result['msg'] = 'Please enter OTP!';
                    return Response::make($result, 200, $this->headers);
                }

                //VALIDATE OTP
                if (!empty($data['otp'])) {
                    $validateOtp = $this->validateOTP($data);
                    if (empty($validateOtp['status'])) {
                        $result['status'] = 2;
                        $result['msg'] = $validateOtp['msg'];
                        return Response::make($result, 200, $this->headers);
                    }
                } elseif (!empty($data['password']) && strlen($data['password']) < 6) {//VA;ODATE PASSWORD
                    $result['status'] = 0;
                    $result['msg'] = "Password must be 6 characters long!";
                    return Response::make($result, 200, $this->headers);
                }

                if (!empty($data['salutation'])) {
                    $user->salutation = trim($data['salutation']);
                }

                if (!empty($data['password'])) {
                    $encryptHelper = $this->helper->load('encrypt');
                    $user->password = $encryptHelper->encryptPWD(array('PWD' => $data['password']));
                }

                $user->name = ucfirst($data['name']);
                $user->mobile = $data['mobile'];
                //$user->status = 1;
                $user->save();

                $userprofile = new UserProfile;
                $userprofile->user_id = $user->user_id;
                $userprofile->save();

                $auth_token = $this->saveAuthToken($user->user_id);
                $this->setHeader('RESPONSE_CODE', '1');
                $this->setHeader('AUTH_TOKEN', $auth_token);

                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.USER_REGISTERED');
                $result['response']['user_id'] = $user->user_id;
                $result['response']['name'] = $user->name;
                $result['response']['mobile'] = $user->mobile;
              
                //SEND SMS ALERT
                $smsText = Config::get('constants.NETCORE_SMS_TEXT_APP_REGISTRATION');
                if ($smsText) {
                    $smsParams = array();
                    $smsParams['Text'] = str_replace('<User>', $user->name, $smsText);
                    $smsParams['To'] = $user->mobile;
                    $this->helper->load('communication')->smsCommunication($smsParams);
                }
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function signin() {
        try {

            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            /*
             * status = 0 {login failed as missing otp or mobile number. login failed as user does not exist with this mobile number.}
             * status = 1 {login success.}
             * status = 2 {invalid otp.}
             */
            if (!empty($data['mobile']) && (!empty($data['otp']) || !empty($data['password']))) {

                $user = User::where('mobile', '=', $data['mobile'])->first();
                if (!count($user)) {
                    $loginFaild = 1;
                    $errorMsg = 'User does not exist with this mobile number!';
                } else {

                    if (!empty($data['otp'])) {
                        $validateOtp = $this->validateOTP($data);
                        if (empty($validateOtp['status'])) {
                            $result['status'] = 2;
                            $result['msg'] = $validateOtp['msg'];
                            return Response::make($result, 200, $this->headers);
                        }
                    } elseif (!empty($data['password'])) {
                        $encryptHelper = $this->helper->load('encrypt');
                        $isPWDMatched = $encryptHelper->validatePWD(array('dbPWD' => $user->password, 'postedPWD' => $data['password']));

                        if (!$isPWDMatched) {
                            $result['status'] = 2;
                            $result['msg'] = "Either mobile or password is wrong!";
                            return Response::make($result, 200, $this->headers);
                        }
                    }

                    //DELETE AUTH-TOKEN ALSO
//                    User::where('user_id', '=', $user->user_id)->where('user_id', '=', $user->user_id)->update(array('token' => NULL));

                    $auth_token = $this->saveAuthToken($user->user_id);
                    $this->setHeader('RESPONSE_CODE', '1');
                    $this->setHeader('AUTH_TOKEN', $auth_token);

                    $userProfile = UserProfile::where('user_id', '=', $user->user_id)->first();

                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.LOGIN_SUCCESS');
                    $result['response']['user_id'] = $user->user_id;
                    $result['response']['name'] = trim($user->salutation . " " . $user->name);
                    $result['response']['email'] = $user->email;
                    $result['response']['mobile'] = $user->mobile;
                    $result['response']['is_doctor'] = $userProfile->is_doctor;
                    $result['response']['is_online'] = Doctor::where('user_id', '=', $user->user_id)->pluck('online_status');
                    $result['response']['is_approved'] = 0;
                    $doc_exist = Doctor::where('user_id', '=', $user->user_id)->exists();
                    if ($doc_exist) {
                        $result['response']['is_approved'] = Doctor::where('user_id', '=', $user->user_id)->pluck('is_approved');
                    }
                    $result['response']['address'] = Doctor::where('user_id', '=', $user->user_id)->pluck('address');
                    $result['response']['photo'] = User::getPhoto(array('photo' => $userProfile->photo, 'is_doctor' => $userProfile->is_doctor));
                    $result['response']['login_paytm'] = $result['response']['login_citrus'] = -1;
                    $walletLogins = UserWallet::select('login', 'wallet_id')->where('user_id', '=', $user->user_id)->get();
                    foreach ($walletLogins as $walletLogin) {
                        if ($walletLogin->wallet_id == 1) {
                            $result['response']['login_paytm'] = $walletLogin->login;
                        } elseif ($walletLogin->wallet_id == 3) {
                            $result['response']['login_citrus'] = $walletLogin->login;
                        }
                    }
                }
            } else {
                $loginFaild = 1;
                $errorMsg = "User login failed as input's are missing!";
            }

            if (!empty($loginFaild)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1029;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = $errorMsg ? $errorMsg : Config::get('config_msg.LOGIN_FAIL');
                $result['errors'] = $error;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function verifyOTP() {
        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $mobile = $data['mobile'];
            $otp = $data['otp'];

            if (empty($mobile) || empty($otp)) {
                $result['status'] = 0;
                $result['msg'] = "Mobile number and OTP are required!";
                return Response::make($result, 200, $this->headers);
            }

            $result = $this->validateOTP($data);

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function validateOTP($params = array()) {

        $mobile = $params['mobile'];
        $otp = $params['otp'];

        if (empty($mobile) || empty($otp)) {
            $result['status'] = 0;
            $result['msg'] = "Mobile number and OTP are required!";
            return $result;
        }

        $first1Chars = substr($mobile, 0, 1);
        if ($first1Chars == '+') {
            $mobile = substr($mobile, 1);
        }

        $mobileOtp = new MobileOtp();
        $verifyOtp = $mobileOtp->verifyOtp(array('mobile' => $mobile, 'otp' => $otp));

        if ($verifyOtp) {
            $result['status'] = 1;
            $result['msg'] = "Valid OTP!";
        } else {
            $result['status'] = 0;
            $result['msg'] = "Invalid OTP!";
            $mobileOtp->where('mobile', $mobile)->increment('attempts', 1);
        }

        $attempts = $mobileOtp->where('mobile', '=', $mobile)->pluck('attempts');

        if ($result['status'] == 1 || $attempts > 3) {
            $mobileOtp->where('mobile', '=', $mobile)->update(array('expired_at' => date("Y-m-d H:i:s"), 'attempts' => 0));
        }

        return $result;
    }

    public function isInternationalNumber($fullMobile) {

        $first1Chars = substr($fullMobile, 0, 1);
        if ($first1Chars == '+') {
            $fullMobile = substr($fullMobile, 1);
        }

        $first2Chars = substr($fullMobile, 0, 2);
        $internationalCall = 0;
        if ($first2Chars != '91') {
            $internationalCall = 1;
        }

        $mobile = substr($fullMobile, 2);

        $numberLength = strlen($mobile);

        if ($numberLength != 10 || $internationalCall || !preg_match("/^(6|7|8|9)\d{9}$/", $mobile)) {
            return 1;
        }

        return 0;
    }

    public function infoPopup() {
        try {
            $requestHeaders = apache_request_headers();
            $checkAuthToken = isset($requestHeaders['AUTH_TOKEN']) ? TRUE : FALSE;
            $result = $this->checkAuth($checkAuthToken);

            if ($result ['config_status'] == - 1) {
                return ($result);
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result ['status'] = 1;
            $result['info_title'] = Config::get('constants.INFO_POPUP_TITLE');
            $result['info_body'] = Config::get('constants.INFO_POPUP_BODY');

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
