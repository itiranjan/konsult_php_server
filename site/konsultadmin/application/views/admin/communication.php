<div id="window3">
    <form id="DetailP">

        <table width="100%">

            <tr>
                <td><div style="width:500px" id="callInformations"/>&nbsp;&nbsp;
                </td>
            </tr>

        </table>
    </form>
</div>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

    <div id="clientsDb">
        <span style="font-size:20px">Call Informations<br/><br/></span>
        <div id="grid" ></div>
    </div>    

    <script>

        var window3 = $("#window3");
        $(document).ready(function() {

            var onClose = function()
            {
                mainGrid.data("kendoGrid").refresh();
            }

            window3.kendoWindow({
                width: "700px",
                visible: false,
                title: "Call Informations"
            });

            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                edit: function(e) {
                    var input = e.container.find(".k-input");
                    input.blur(function(){
                        column_value = input.val();
                        updateCommunicationDetails(e.model.call_id, 'followed_by', column_value);
                    });  

                    var inputArea = e.container.find("textarea");
                    inputArea.blur(function(){
                        column_value = inputArea.val();
                        updateCommunicationDetails(e.model.call_id, 'comment', column_value);
                    });
                },                  
                toolbar:["excel"],
                selectable: "multiple cell",
                allowCopy: true,                    
                excel: {
                    allPages: true,
                    fileName: "call_list_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                    filterable: true
                },
                dataSource: {
//                    type: "json",
//                    //serverPaging: true,
//                    transport: {
//                        read: { 
//                            url: base_url + "/index.php/communication/communicationList",
//                            cache: false
//                        },
//                        type: "POST"
//                    },
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    transport: {
                        read: {
                            type: "POST",
                            url: base_url + "/index.php/communication/communicationList",
                            dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                        },
                        parameterMap: function (options) {
                            if (options.filter) {
                                KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                            }
                            return options;
                        },
                    },                    
                    schema: {
                        data: "list",
                        total: "total",
                        model: {
                            fields: {
                                call_id: {type: "number", editable: false},
                                dr$name: {type: "string", editable: false},
                                pt$name: {type: "string", editable: false},
                                dr$email: {type: "string", editable: false},
                                pt$email: {type: "string", editable: false},               
                                call_duration: {type: "number", editable: false},
                                pgm$name: {type: "string", editable: false},
                                total_charges: {type: "number", editable: false},
                                virtual_money: {type: "number", editable: false},
                                real_money: {type: "number", editable: false},
                                //doctor_charges: {type: "number", editable: false},
                                //konsult_charges: {type: "number", editable: false},
                                comm_datetime: {type: "date", editable: false},
                                ptctm$name: {type: "string", editable: false},
                                drctm$name: {type: "string", editable: false},
                                status: {type: "string", editable: false},
                                followed_by: {type: "string", editable: false},
                                comment: {type: "string" <?php echo (!empty($this->session->userdata('permissions')['communication_updateCommunicationDetails_edit']) ? ",editable: true" : ",editable: false"); ?>},
                            }
                        }
                    },
                    pageSize: 100
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            //eq: "Is equal to",
                            //neq: "Is not equal to"
                        },
                        number: {
                            eq: "Equal To",
                            gt: "Greater Than",
                            //gte: "Greater than or equal",
                            lt: "Less Than",
                            //lte: "Less than or equal",                                
                        }
                    }
                },                     
                sortable: true,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                pageable: {
                    refresh: true,
                    pageSize: 100,
                    numeric: true,
                    buttonCount: 30,
                    info: true
                },
                change: function(e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                columns: [
                        {field: "call_id", title: "Comm Id", width: '8px'},
                        {field: "dr$name", title: "Doctor Name", width: '15px'},
                        {field: "dr$email", title: "Doctor Email", width: '15px'},
                        {field: "pt$name", title: "Patient Name", width: '12px'},
                        {field: "pt$email", title: "Patient Email", width: '12px'},
                        {field: "comm_datetime", title: "Called On", width: '15px',format: "{0:dd/MM/yyyy HH:mm tt}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "call_duration", title: "Duration (Seconds)", width: '7px'},
                        {field: "pgm$name", title: "Gateway", width: '8px'},
                        {field: "total_charges", title: "Patient Debit", width: '7px'},
                        {field: "virtual_money", title: "VC", width: '7px'},
                        {field: "real_money", title: "RC", width: '7px'},
                        //{field: "doctor_charges", title: "Doctor Credit", width: '7px'},
                        //{field: "konsult_charges", title: "Konsult Credit", width: '7px'},
                        {field: "ptctm$name", title: "Pt Status", width: '10px'},
                        {field: "drctm$name", title: "Dr Status", width: '10px'},
                        {field: "status", title: "Call Status", width: '10px', filterable: false, sortable: false},
                        {field: "followed_by", title: "Followed By", width: '8px', filterable: true},
                        {field: "comment", title: "Comments", width: '12px', filterable: true, defaultValue: {}, editor: textEditorInitialize}
                ],
                editable: true,
            }).data("kendoGrid");    
            
//$("#grid").kendoGrid({
//    dataSource: mainGrid,
//    rowTemplate: '<tr class="#:ReportClassDescription==\"Express\"? \"red\" : \"white\"#" data-uid="#= uid #"><td>#: name #</td><td>#:ReportClassDescription #</td></tr>'
//});            

var grid = mainGrid;//.data("kendoGrid");
var data = grid.dataSource.data();console.log(data);
$.each(data, function (i, row) { alert(row.followed_by);
  if (row.followed_by != 'Database Admin')
     $('tr[data-uid="' + row.uid + '"] ').css("background-color", "green");
});

            mainGrid.thead.kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target;
                    return $(target).text();
                }                    
            });
        });
        
            var textEditorInitialize = function(container, options) {
                $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                .appendTo(container);
            };            
            
            function updateCommunicationDetails(call_id, column_name, column_value) {

                var getConfirmation = confirm("Are you sure that you want to update the column.");
                if(!getConfirmation) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    return;
                }

                $.ajax({

                    'url' : base_url + "/index.php/communication/updateCommunicationDetails",
                    'type' : 'POST',
                    'data' : {
                        'call_id' : call_id,
                        'column_name' : column_name,
                        'column_value' : column_value
                    },
                    'success' : function(data) {  
                        alert("Changes has been done succesfully!!");
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    },
                    'error' : function(request,error)
                    {
                        alert("Whoops!! Something might be wrong. Please try again.");
                        //alert("Request: "+JSON.stringify(request));
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    }
                });                
            }            

            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function () {
                        alert(error);
                    }
                });
            }       
    </script>
</div>
</div>