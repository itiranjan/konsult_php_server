<?php

class DoctorQualification extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'doctor_qualifications';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'qualification_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'university' => 'integer',
        'degree' => 'required|integer',
        'start_date' => 'date_format:"Y-m-d"',
        'end_date' => 'date_format:"Y-m-d"',
        'is_pursuing' => 'boolean'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

}
