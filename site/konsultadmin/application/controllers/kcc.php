<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class kcc extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('user_model', '', TRUE);
        $this->load->model('communication_model', '', TRUE);
        $this->load->model('doctor_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function userInfo() {

        $query = $this->db->query("SELECT specialization_id, name FROM `specialization_master` ORDER BY name ASC");

        $this->load->view('admin/header');
        $this->load->view("admin/userInfo", array('specializations' => $query->result()));
    }

    public function showUserInfo() {

        $data = $_POST;
        $mobile = $data['mobile'];
        $promoCode = $data['promo_code'];

        $userDetails = $this->user_model->userDetails(array('mobile' => $data['mobile']));

        if (empty($userDetails)) {
            if (!empty($data['rechargeForm'])) {
                $response = array('status' => 0, 'msg' => 'User does not exist with given mobile number.');
                echo json_encode($response);
            } else {
                echo "0";
            }
        } elseif (!empty($data['rechargeForm'])) {
            if (empty($data['promo_code'])) {
                $response = array('status' => 0, 'msg' => 'Please enter the valid promo code.');
                echo json_encode($response);
            } else {
                $URL = APIURL . "kcc/recharge?mobile=$mobile&promo_code=$promoCode";
                $rechargeResponse = $this->httpGet($URL);

                //LOGGING OF ACTION
                $logData = array();
                $logData['type'] = 'kcc_recharge';
                $logData['request'] = array('url' => $URL);
                $rechargeResponseObject = json_decode($rechargeResponse);
                $rechargeResponseArray = array();
                $rechargeResponseArray['logging_msg'] = $rechargeResponseObject->msg;
                $rechargeResponseArray['status'] = $rechargeResponseObject->status;
                $logData['response'] = $rechargeResponseArray;
                $this->load->model('accesslog_model', '', TRUE);
                $this->accesslog_model->insertLog($logData);

                echo $rechargeResponse;
            }

            $responseArray = array();
        } else {

            $specializationDocs = array();
            $searchedDocs = 0;
            if (!empty($data['p_doctor_search']) && !empty($data['specialization_id'])) {
                $searchedDocs = 1;
                $specialization_id = $data['specialization_id'];
                $query = $this->db->query("SELECT users.user_id, CONCAT(users.salutation, ' ',users.name) as name, dp.per_min_charges, dp.extension, users.mobile FROM doctor_profile AS dp LEFT JOIN users ON users.user_id = dp.user_id LEFT JOIN user_wallet ON user_wallet.user_id = dp.user_id LEFT JOIN doctor_specialization_rel dsr ON dsr.user_id = users.user_id AND dsr.is_default = 1
 JOIN specialization_master dsm ON dsr.specialization_id = $specialization_id WHERE dp.is_approved = 1 AND dp.online_status = 1 AND dp.extension > 0 GROUP BY dp.doctor_id ORDER BY dp.per_min_charges ASC");
                $specializationDocs = $query->result();

                foreach ($specializationDocs as $specializationDoc) {
                    $experience = $this->db->query("select address, hospital_name from doctor_workplace where user_id='$specializationDoc->user_id'");
                    $hospitalDetails = $experience->result();
                    foreach ($hospitalDetails as $hospitalDetail) {
                        $specializationDoc->hospital = empty($hospitalDetail->hospital_name) ? '' : $hospitalDetail->hospital_name;
                        $specializationDoc->address = $hospitalDetail->address;
                    }
                }
            }

            $params = array('user_id' => $userDetails->user_id);
            $callDetails = $this->user_model->getUserCommunications($params);
            foreach ($callDetails as $key => $callDetail) {
                foreach ($callDetail as $key => $value) {
                    $callDetail->$key = $value;
                    if ($key == 'doctor_user_id') {
                        $callDetail->doctor_name = $this->communication_model->detailsById($value);
                        $callDetail->specialization = $this->doctor_model->doctorSpecialityById($value);
                        $callDetail->extension = $this->doctor_model->getExtension($value);
                    }
                }
            }
            $rechargeDetails = $this->user_model->getUserRecharge($params);
            $statementDetails = $this->user_model->getUserStatement($params);
            $this->load->view("admin/showUserInfo", array('userDetails' => $userDetails, 'callDetails' => $callDetails, 'rechargeDetails' => $rechargeDetails, 'statementDetails' => $statementDetails, 'specializationDocs' => $specializationDocs, 'searchedDocs' => $searchedDocs, 'patient_mobile' => $mobile, 'patient_user_id' => $userDetails->user_id, 'dbObject' => $this->db));
        }
    }

    public function callInfos() {
        $this->load->view('admin/header');
        $this->load->view("admin/callInfos");
    }

    public function callInfoList($way) {

        $data = array();

        $resultsData = $this->communication_model->kccCallList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];

        foreach ($result->result() as $row) {
//
//            $doctorDetails = $this->communication_model->detailsById($row->doctor_user_id, 'array');
//            $patientDetals = $this->communication_model->detailsById($row->user_id, 'array');

            if ($row->receiver_status == 'Error in call Transfer') {
                $row->receiver_status = 'Missed / Not-reachable';
            }

            $data[] = array(
                'kcc_calls$kcc_call_id' => $row->kcc_call_id,
                'dr$name' => $row->doctor_name,
                'dr$email' => $row->doctor_email,
                'pt$name' => $row->patient_name,
                'pt$email' => $row->patient_email,
                'caller_status' => $row->caller_status,
                'receiver_status' => $row->receiver_status,
                'kcc_calls$created_at' => $row->created_at,
                'call_duration' => $row->call_duration, // ? round($row->call_duration / 60, 1) : $row->call_duration,
                'total_charges' => $row->total_charges,
                'doctor_charges' => $row->doctor_charges,
                'konsult_charges' => $row->total_charges - $row->doctor_charges
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

    public function docConnect() {

        $mobile = $_REQUEST['mobile'];
        $medium = 'webform';
        $url = "http://api.konsultapp.com/getAppLink?mobile=$mobile&medium=$medium";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);
    }

    public function callConnect() {

        $rowData = array();
        $mobile = $_POST['mobile'];
        $extension = $_POST['extension'];
        $user_id = $_POST['user_id'];

        $url = APIURL . "kcc/docConnect?mobile=$mobile&extension=$extension&kccDashboardCall=1&user_id=$user_id";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'kcc_call_connect';
        $logData['request'] = array('url' => $url);
        $responseObject = json_decode($output);
        $responseArray = array();
        $responseArray['logging_msg'] = !empty($responseObject->msg) ? $responseObject->msg : $responseObject->errors;
        $responseArray['status'] = $responseObject->status;
        $logData['response'] = $responseArray;
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);

        echo $output;
    }

    public function assignMinutes() {
        $query = $this->db->query("SELECT users.user_id, users.salutation, users.name, users.mobile, doctor_profile.per_min_charges FROM users JOIN doctor_profile ON users.user_id = doctor_profile.user_id WHERE allowed_fixed_mins_call = 1 ORDER BY users.name ASC");

        $this->load->view('admin/header');
        $this->load->view("admin/assignMinutes", array('doctors' => $query->result()));
    }

    public function processAssignMinutes() {

        $is_logged_in = $this->session->userdata('user_id');

        if ($is_logged_in) {
            $query = $this->db->query("SELECT users.token FROM users WHERE user_id = $is_logged_in");
            $viewerData = $query->result();
            foreach ($viewerData as $viewer) {
                $token = $viewer->token;
            }
        } else {
            echo '{"message":"Un-authorized user!","status":"0"}';
        }

        $data = $_POST;
        $country = $data['country'];
        $fullName = $data['full_name'];
        $mobile = $data['mobile'];
        $amount = $data['amount'];
        $doctor_user_id = $data['doctor_user_id'];

        //CHECK IF USER EXIST
        $mobile = $country . $mobile;
        $mobile = str_replace('+', '', $mobile);
        $userDetails = $this->user_model->userDetails(array('mobile' => $mobile));

        if (empty($userDetails)) {
            $url = APIURL . 'admin/createKccUser';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'API_KEY:andapikey', 'APP_VERSION:1.0', 'CONFIG_VERSION:1.0', "AUTH_TOKEN:$token"));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("mobile" => "+" . $mobile, 'fullName' => $fullName)));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $output = curl_exec($ch);
            curl_close($ch);
        }

        $userDetails = $this->user_model->userDetails(array('mobile' => $mobile));

        if (!empty($userDetails)) {
            $url = APIURL . 'kcc/assignMinutes';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'API_KEY:andapikey', 'APP_VERSION:1.0', 'CONFIG_VERSION:1.0', "AUTH_TOKEN:$token"));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("mobile" => "+" . $mobile, 'doctor_user_id' => $doctor_user_id, 'amount' => $amount)));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $output = curl_exec($ch);
            curl_close($ch);
            
            $decodeOutput = json_decode($output);
            
            if(!empty($decodeOutput) && !empty($decodeOutput->status)) {
                $response = '{"message":"Talk time assigned successfully!","status":"1"}';
            } else {
                $response = '{"message":"Issue with talk time assignment!","status":"0"}';
            }
        } else {
            $response = '{"message":"Issue with user account!","status":"0"}';
        }
        
        echo $response;
    }

    public function httpGet($url) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}
