<style>
    tr { font-family:tahoma;font-size:11px;height:21px; }
    .red{
        color:red;
    }
    .blue{
        color:blue;
    }
    .green{
        color:green;
    }

    .k-grid td {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
    }    
</style>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

        <div id="clientsDb">
            <span style="font-size:20px">Members List<br/><br/></span>
            <div id="grid" ></div>    
        </div>

        <script>
            $(document).ready(function () {

                var onClose = function ()
                {
                    mainGrid.data("kendoGrid").refresh();
                }

                var selectedRows = [];
                mainGrid = $("#grid").kendoGrid({
                    edit: function(e) {
                        var input = e.container.find(".k-input");
                        input.blur(function(){
                            column_value = input.val();
                            updateUserDetails(e.model.users$user_id, 'followed_by', column_value);
                        });  
                        
                        var inputArea = e.container.find("textarea");
                        inputArea.blur(function(){
                            column_value = inputArea.val();
                            updateUserDetails(e.model.users$user_id, 'comment', column_value);
                        });
                    },                    
                    toolbar:["excel"],
                    selectable: "multiple cell",
                    allowCopy: true,                    
                    excel: {
                        allPages: true,
                        fileName: "member_list_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                        filterable: true
                    },
                    dataSource: {
                        type: "json",
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        transport: {
                            read: {
                                type: "POST",
                                url: base_url + "/index.php/user/userList/1",
                                dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                            },
                            parameterMap: function (options) {
                                if (options.filter) {
                                    KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                                }
                                return options;
                            },
                        },                        
                        schema: {
                            data: "list",
                            total: "total",
                            model: {
                                //id: "id",
                                fields: {
                                    users$user_id: {type: "number", editable: false},
                                    name: {type: "string", editable: false},
                                    users$email: {type: "string", editable: false},
                                    uwi$user_wallet_id: {type: "string", editable: false},
                                    user_wallet$email: {type: "string", editable: false},
                                    mobile: {type: "number", editable: false},
                                    amount_paid: {type: "number", editable: false},
                                    users$created_at: {type: "date", editable: false},
                                    last_accessed: {type: "date", editable: false},
                                    user_profile$followed_by: {type: "string", editable: false},
                                    user_profile$comment: {type: "string" <?php echo (!empty($this->session->userdata('permissions')['user_updateUserDetails_edit']) ? ",editable: true" : ",editable: false"); ?>},
                                    clinic_type: {type: "number", editable: false},
                                }
                            }
                        },
                        pageSize: 100
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                                eq: "Is equal to",
                                neq: "Is not equal to"
                            }
                        }
                    },
                    sortable: true,
                    dataBound: function () {
                        this.expandRow(this.tbody.find("tr.k-master-row").first());
                    },
                    pageable: {
                        refresh: true,
                        pageSize: 100,
                        numeric: true,
                        buttonCount: 20,
                        info: true
                    },
                    change: function (e) {
                        var selected = this.select();
                        for (var i = 0; i < selected.length; i++) {
                            var dataItem = this.dataItem(selected[i]);
                            selectedRows.push(dataItem);
                        }
                    },
                    columns: [
                        {field: "users$user_id", title: "Id", width: '8px'},
                        {field: "name", title: "Name", width: '12px'},
                        {field: "users$email", title: "Email", width: '20px'},
                        {field: "user_wallet$email", title: "Citrus Email", width: '20px'},
                        {field: "uwi$user_wallet_id", title: "PayTm Mapped", width: '12px', filterable: false},
                        {field: "mobile", title: "Mobile", width: '12px'},
                        {field: "amount_paid", title: "Total Amount Spend", width: '6px', filterable: false},
                        {field: "users$created_at", title: "Added On", width: '15px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "last_accessed", title: "Last Accessed", width: '15px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},                
                        {field: "clinic_type", title: "Clinic", width: '8px', template: "<a id='toggle' onclick='<?php echo (!empty($this->session->userdata('permissions')['user_toggleClinic_edit']) ? 'toggleClinic(#= users$user_id#,#= clinic_type#);' : "javascript:void(0);"); ?>' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/#= clinic_type == 0 ? 'employee_month.png' : 'clinic.ico' #' title='Click to change clinic status'></a>", filterable: { ui: clinicFilter, "messages": { "info": "Select:" }, operators: {number: {eq: "Equal To"}}}},                
                        {field: "user_profile$followed_by", title: "Followed By", width: '12px', filterable: true},
                        {field: "user_profile$comment", title: "Comments", width: '20px', filterable: true, defaultValue: {}, editor: textEditorInitialize}
                    ],
                    editable: true,
                }).data("kendoGrid");
            
                mainGrid.thead.kendoTooltip({
                    filter: "th",
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown
                        return $(target).text();
                    }
                });           
            
                $("#grid").kendoTooltip({
                   filter: "td:nth-child(9)", //this filter selects the first column cells
                   position: "left",
                   content: function(e){
                       var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));
                       var content = dataItem.comment;
                       return content;
                   }
                }).data("kendoTooltip");            
            
            });                        
            
            function clinicFilter(element) {

                element.kendoDropDownList({
                    dataTextField: "name",
                    dataValueField: "id",
                    dataSource: [
                        { id: 1, name: "Clinic" },
                        { id: 0, name: "Non-clinic" }
                    ],
                    optionLabel: "--Select--"  
                });
            }    
            
            function toggleClinic(id, value) {
                
                if(value == 1 && !confirm("Are you sure that you want to remove this user from konsult clinic list?")) {
                    return;
                }
                else if(value == 0 && !confirm("Are you sure that you want to add this user in konsult clinic list?")) {
                    return;
                }
                
                callFunction = function(d) {
                    $("#grid").data("kendoGrid").dataSource.read();
                };
                ajaxCall('user/toggleClinic', 'user_id=' + id + "&val=" + value, callFunction);
            }            
            
            var textEditorInitialize = function(container, options) {
                $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                .appendTo(container);
            };            
            
            function updateUserDetails(user_id, column_name, column_value) {

                var getConfirmation = confirm("Are you sure that you want to update the column.");
                if(!getConfirmation) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    return;
                }

                $.ajax({

                    'url' : base_url + "/index.php/user/updateUserDetails",
                    'type' : 'POST',
                    'data' : {
                        'user_id' : user_id,
                        'column_name' : column_name,
                        'column_value' : column_value
                    },
                    'success' : function(data) {  
                        alert("Changes has been done succesfully!!");
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    },
                    'error' : function(request,error)
                    {
                        alert("Whoops!! Something might be wrong. Please try again.");
                        //alert("Request: "+JSON.stringify(request));
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    }
                });                
            }            

            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function () {
                        alert(error);
                    }
                });
            }
        </script>
    </div>
</div>