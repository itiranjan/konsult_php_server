<?php

session_start();
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class VerifyLogin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function index() {

        //This method will have the credentials validation
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');

        if ($this->form_validation->run() == FALSE) {
            echo "Errors:" . validation_errors();
        } else {

            //getting user input
            $username = $this->security->xss_clean($this->input->post('username'));
            $password = $this->security->xss_clean($this->input->post('password'));

            $url = APIURL . 'account/login';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'API_KEY:andapikey', 'APP_VERSION:1.0', 'CONFIG_VERSION:1.0'));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("username" => $username, "password" => $password)));
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
            $output = curl_exec($ch);
            curl_close($ch);

            $decodeOutput = json_decode($output);
            $userId = !empty($decodeOutput->response->user_id) ? $decodeOutput->response->user_id : 0;
            $emailId = !empty($decodeOutput->response->email) ? $decodeOutput->response->email : NULL;
            $name = !empty($decodeOutput->response->name) ? $decodeOutput->response->name : NULL;

            $data = array();
            if (empty($userId) || empty($emailId)) {
                $data['error'] = "User not exist!";
                $validUser = false;
            } else {

                $sql = "SELECT * FROM admin_users WHERE user_id = $userId";

                $query = $this->db->query($sql);

                if ($query->num_rows() == 1) {
                    $row = $query->row();

                    $sqlPermission = "SELECT permissions.name, admin_permissions.permission_id FROM permissions LEFT JOIN admin_permissions ON permissions.permission_id = admin_permissions.permission_id AND admin_id = $row->admin_id";

                    $queryPermissions = $this->db->query($sqlPermission);

                    $permissionsArray = array();
                    $firstAccessiblePage = '';
                    foreach ($queryPermissions->result() as $queryPermission) {
                        $permissionsArray[$queryPermission->name] = empty($queryPermission->permission_id) ? 0 : 1;

                        if (empty($firstAccessiblePage) && $permissionsArray[$queryPermission->name]) {
                            $firstAccessiblePage = $queryPermission->name;
                        }
                    }

                    $data = array(
                        'userid' => $row->admin_id,
                        'user_id' => $userId,
                        'name' => $name,
                        'username' => $emailId,
                        'validated' => true,
                        'permissions' => $permissionsArray,
                    );
                    $this->load->library('session');
                    $this->session->set_userdata($data);

                    $validUser = true;
                } else {
                    $validUser = false;
                }
            }

            //LOGGING OF ACTION
            $logData = array();
            $logData['user_id'] = $userId;
            $logData['type'] = 'login';
            $logData['request'] = array('username' => $username);
            $logData['response'] = array_merge(array('logging_msg' => ($validUser ? 'login successfully' : 'login failed')), $data);
            $this->load->model('accesslog_model', '', TRUE);
            $this->accesslog_model->insertLog($logData);

            if ($validUser) {
                $response = '{"message":"Authorized user!","status":"1","action":"' . $firstAccessiblePage . '"}';
            } else {
                $response = '{"message":"Un-authorized user!","status":"0"}';
            }

            echo $response;
        }
    }

    function autospecialization() {
        include ("dbhelper.php");
        $keyword = $_REQUEST['keyword'];
        //$spexit= "SELECT name FROM specialization_master where name like ('%$keyword%')";
        $spexit = "SELECT name FROM degree_master";
        //echo $spexit;	
        $link = get_dbdata();
        $specid = $link->query($spexit);
        //echo "<option value='$keyword'></option>";
        echo "<option value='Others'></option>";
        $cityid = "";
        while ($spece = mysqli_fetch_array($specid)) {
            $cityid = $spece ["name"];
            if ($cityid != "Others") {
                echo "<option value='$cityid'></option>";
            }
        }
    }

    function logout() {

        $sessionData = $this->session->userdata;

        unset($this->session->userdata);
        $this->session->sess_destroy();

        //LOGGING OF ACTION
        $logData = array();
        $logData['user_id'] = $sessionData['user_id'];
        $logData['type'] = 'logout';
        $logData['request'] = array('username' => $sessionData['username']);
        $logData['response'] = array_merge(array('logging_msg' => 'logged-out successfully!'), $sessionData);
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);

        redirect('login');
    }

    function doctorreg() {
        $this->load->view("admin/doctorRegistration");
    }

    function getcity() {
        include ("dbhelper.php");
        $spexit = "SELECT name FROM city_master";
        //echo $spexit;
        $link = get_dbdata();
        $specid = $link->query($spexit);
        //echo "<option value='$keyword'></option>";
        echo "<option value='Others'></option>";
        $cityid = "";
        while ($spece = mysqli_fetch_array($specid)) {
            $cityid = $spece ["name"];
            if ($cityid != "Others") {
                echo "<option value='$cityid'></option>";
            }
        }
        //echo "hi anil";
    }

    function getspecialization() {
        include ("dbhelper.php");
        $spexit = "SELECT name FROM specialization_master";
        //echo $spexit;
        $link = get_dbdata();
        $specid = $link->query($spexit);
        echo "<option value='Others'></option>";
        $cityid = "";
        while ($spece = mysqli_fetch_array($specid)) {
            $cityid = $spece ["name"];
            if ($cityid != "Others") {
                echo "<option value='$cityid'></option>";
            }
        }
        //echo "hi anil";
    }

    function getdegree() {
        include ("dbhelper.php");
        $spexit = "SELECT name FROM degree_master";
        //echo $spexit;
        $link = get_dbdata();
        $specid = $link->query($spexit);
        echo "<option value='Others'></option>";
        $cityid = "";
        while ($spece = mysqli_fetch_array($specid)) {
            $cityid = $spece ["name"];
            if ($cityid != "Others") {
                echo "<option value='$cityid'></option>";
            }
        }
        //echo "hi anil";
    }

    /**
     * @author : Anil Kumar
     * @created : 10 oct 2015
     * @description : This function get detail by user id
     * @param : int userid
     * @access public
     * @return  array
     */
    function getDoctorEditDataforweb() {
        include ("dbhelper.php");
        $email = $_REQUEST['email'];
        $mobile = $_REQUEST['mob'];
        if (!empty($email) && !empty($mobile)) {
            $increase = 0;
            $link = get_dbdata();
            if ($mobile == "test") {
                $query_condition = "select u.user_id,u.salutation,u.name,dp.address,up.is_doctor,dp.city_id,up.photo,dp.medical_registration_no,dp.info,dp.per_min_charges,dp.total_experience,dp.is_approved,dp.allow_new_user,ds.specialization_id,u.email,u.mobile,dp.info,dp.other_info,dp.area from users u LEFT JOIN user_profile up ON (u.user_id=up.user_id) LEFT JOIN doctor_profile dp ON (up.user_id=dp.user_id) LEFT JOIN doctor_specialization_rel ds ON(dp.user_id=ds.user_id) where u.user_id='$email'";
            } else {
                $query_condition = "select u.user_id,u.salutation,u.name,dp.address,up.is_doctor,dp.city_id,up.photo,dp.medical_registration_no,dp.info,dp.per_min_charges,dp.total_experience,dp.is_approved,dp.allow_new_user,ds.specialization_id,u.email,u.mobile,dp.info,dp.other_info,dp.area from users u LEFT JOIN user_profile up ON (u.user_id=up.user_id) LEFT JOIN doctor_profile dp ON (up.user_id=dp.user_id) LEFT JOIN doctor_specialization_rel ds ON(dp.user_id=ds.user_id) where email='$email' and mobile='$mobile'";
            }

            //echo $query_condition;
            $valid = "No";
            $doclist = Array();
            $doctorquery = $link->query($query_condition);

            //echo count($doctorquery);
            //echo "fasdf".count(mysqli_fetch_array ($doctorquery));
            while ($doct = mysqli_fetch_array($doctorquery)) {
                //$numFound = $doct ["totcount"];
                $valid = "Yes";
                $dbuserid = $doct ["user_id"];
                $isdoctor_status = $doct ["is_doctor"];
                $response1['is_doctor'] = $doct ["is_doctor"];
                $response1['user_id'] = $doct ["user_id"];
                $response1['email'] = $doct ["email"];
                $response1['mobile'] = $doct ["mobile"];
                $_SESSION["user_mobile"] = $doct ["mobile"];
                $_SESSION["user_email"] = $doct ["email"];
                if ($isdoctor_status != 0) {
                    $_SESSION["user_idforedit"] = "modify";
                    $response1['info'] = $doct ["info"];
                    $response1['salutation'] = $doct ["salutation"];
                    $response1['name'] = $doct ["name"];
                    $response1['user_id'] = $doct ["user_id"];
                    $response1['name'] = $doct ["name"];
                    $response1['address'] = $doct ["address"];
                    $response1['is_doctor'] = $doct ["is_doctor"];
                    $response1['city'] = $cityid = $doct ["city_id"];

                    //echo $doct ["name"];
                    $cityquery = "select name from city_master where city_id='$cityid'";
                    //echo $doconline;
                    $doctcityquery = $link->query($cityquery);
                    while ($doctcity = mysqli_fetch_array($doctcityquery)) {
                        $cityname = $doctcity ["name"];
                    }
                    $response1['city_name'] = $cityname;
                    $response1['photo'] = $doct ["photo"];
                    $response1['medical_registration_no'] = $doct ["medical_registration_no"];
                    $response1['info'] = $doct ["info"];
                    $response1['per_min_charges'] = $doct ["per_min_charges"];
                    $response1['other_info'] = $doct ["other_info"];
                    $response1['area'] = $doct ["area"];
                    $response1['total_experience'] = $doct ["total_experience"];
                    $response1['allow_new_user'] = $doct['allow_new_user'];

                    $qulilist = array();
                    $uniidline = "select qualification_id,college,degree from doctor_qualifications where user_id='$dbuserid'";
                    //echo $uniidline;
                    $univerlinequery = $link->query($uniidline);
                    while ($doctuni = mysqli_fetch_array($univerlinequery)) {
                        $university = $doctuni ["college"];
                        $degreeid = $doctuni ["degree"];
                        if (!empty($degreeid)) {
                            $degidline = "select name from degree_master where degree_id='$degreeid'";
                            //echo $doconline;
                            $deglinequery = $link->query($degidline);
                            while ($deg = mysqli_fetch_array($deglinequery)) {
                                $degree = $deg ["name"];
                            }
                        }
                        if (!empty($university)) {
                            $response['university'] = $university;
                        } else {
                            $response['university'] = "";
                        }
                        $response['degree'] = $degree;
                        $response['qualification_id'] = $doctuni ["qualification_id"];
                        ;
                        $json_response = json_encode($response);
                        $quali_decode = json_decode($json_response);
                        array_push($qulilist, $quali_decode);
                    }
                    $expelist = array();
                    $expline = "select dw.workplace_id, hm.hospital_id, hm.name, hm.address, dw.designation_name from doctor_workplace AS dw LEFT JOIN hospital_master AS hm ON dw.hospital_id = hm.hospital_id where user_id='$dbuserid' AND dw.hospital_id IS NOT NULL";
                    //echo $expline;
                    $expelinequery = $link->query($expline);
                    while ($doctex = mysqli_fetch_array($expelinequery)) {
                        $response2['hospital'] = $hopital = $doctex ["name"];
                        $response2['designation'] = $designation = $doctex ["designation_name"];
                        $response2['workplace_id'] = $doctex ["workplace_id"];
                        $json_response2 = json_encode($response2);
                        $quali_decode2 = json_decode($json_response2);
                        array_push($expelist, $quali_decode2);
                    }

                    $speciline = "select specialization_id from doctor_specialization_rel where user_id='$dbuserid'";
                    //echo $speciline;
                    $specilinequery = $link->query($speciline);
                    while ($doctspec = mysqli_fetch_array($specilinequery)) {
                        $specializationid = $doctspec ["specialization_id"];

                        if (!empty($specializationid)) {
                            $speci = "select name from specialization_master where specialization_id='$specializationid'";
                            //echo $speci;
                            $specilinequery = $link->query($speci);
                            while ($doctspeci = mysqli_fetch_array($specilinequery)) {
                                $specialization = $doctspeci ["name"];
                            }
                        }
                    }
                    $response1['specialization_id'] = $specializationid;
                    $response1['specialization_name'] = $specialization;
                    $response1['qualification'] = $qulilist;
                    $response1['experience'] = $expelist;
                    $json_response1 = json_encode($response1);
                    $array_decode = json_decode($json_response1);
                    //print_r($array_decode);
                    array_push($doclist, $array_decode);
                } else {
                    $valid = "diff";
                    $_SESSION["user_idforedit"] = "modify1";
                }
            }
        }
        if ($valid == "Yes") {
            print_r($json_response1);
        } else if ($valid == "diff") {
            $response1['mobile'] = "diff";
            print_r(json_encode($response1));
        } else {
            $response1['email'] = "not";
            print_r(json_encode($response1));
        }
        //print_r($doclist);
    }

    function validateUser() {
        include ("dbhelper.php");
        $link = get_dbdata();
        if ($_REQUEST["email"] && $_REQUEST["mobile"]) {
            $email = $_REQUEST['email'];
            $mobile = $_REQUEST['mobile'];
        }
        //echo $email.$mobile;
        if (!empty($email) && !empty($mobile)) {
            $latlongquery = "SELECT user_id,email,mobile FROM users WHERE email=lower('$email') and mobile = $mobile";
            $latlonquery = $link->query($latlongquery);
            //print_r($latlongquery) ;
            $dbemail = "";
            $dbmobile = "";
            $dbuserid = "";
            $isdoctor = 0;
            $bool = false;
            if (!empty($latlonquery)) {
                while ($recordlat = mysqli_fetch_array($latlonquery)) {
                    $bool = true;
                    $dbuserid = $recordlat ["user_id"];
                    $dbemail = $recordlat ["email"];
                    $dbmobile = $recordlat ["mobile"];
                }
            }
            $doctorexit = "SELECT is_doctor FROM user_profile WHERE user_id=$dbuserid";

            if (!empty($dbuserid)) {
                $doctorquery = $link->query($doctorexit);
                while ($doctorex = mysqli_fetch_array($doctorquery)) {
                    $isdoctor = $doctorex ["is_doctor"];
                }
            }
            //echo $isdoctor;
            if (strtolower($email) == strtolower($dbemail) && $mobile == $dbmobile && $isdoctor != 1) {
                echo "<font color='red'>You are Valid User.</font>";
            } else if ($email == $dbemail && $mobile == $dbmobile && $isdoctor == 1) {
                echo "<font color='red'>You are allready registered as doctor.</font>";
            } else {
                echo "<font color='red'>You are not valid user.</font>";
            }
        } else {
            if (empty($email) && empty($mobile)) {
                echo "<font color='red'>Please Enter Email and Mobile No.</font>";
            } else if (!empty($email) && empty($mobile)) {
                echo "<font color='red'>Please Enter Mobile No.</font>";
            } else if (empty($email) && !empty($mobile)) {
                echo "<font color='red'>Please Enter Email No.</font>";
            }
        }
    }

    function doctorregistration() {
        include ("dbhelper.php");
        //echo "sdafasdf";
        $link = get_dbdata();
        if ($_REQUEST["email"] && $_REQUEST["mobile"]) {
            $salutation = $_REQUEST['salutation'];
            $name = $_REQUEST['name'];
            $otherinfo = $_REQUEST['otherinfo'];
            $area = $_REQUEST['area'];
            $email = $_REQUEST['email'];
            $mobile = $_REQUEST['mobile'];
            $city = $_REQUEST['city'];
            $address = $_REQUEST['address'];
            //$qualification = $_REQUEST['qualification'];
            $experience = $_REQUEST['experience'];
            $specialization = $_REQUEST['specialization'];
            //$wexperience = $_REQUEST['wexperience'];
            $mregistration = $_REQUEST['mregistration'];
            $sregistration = $_REQUEST['sregistration'];
            $pminute = $_REQUEST['pminute'];
            $allowNewUser = $_REQUEST['allowuser'];
            $college0 = $_REQUEST['college0'];
            $degree0 = $_REQUEST['degree0'];
            $qsyear0 = $_REQUEST['qsyear0'];
            $qeyear0 = $_REQUEST['qeyear0'];
            $whospri0 = $_REQUEST['whospri0'];
            $designation0 = $_REQUEST['designation0'];
            $worksyear0 = $_REQUEST['worksyear0'];
            $workeyear0 = $_REQUEST['workeyear0'];
            //echo $college0.$degree0.$qsyear0.$qeyear0;
            $sunday = $_REQUEST['sunday'];
            $sunstarttime = $_REQUEST['sunstarttime'];
            $sunendtime = $_REQUEST['sunendtime'];

            $monday = $_REQUEST['monday'];
            $monstarttime = $_REQUEST['monstarttime'];
            $monendtime = $_REQUEST['monendtime'];

            $tuesday = $_REQUEST['tuesday'];
            $tuestarttime = $_REQUEST['tuestarttime'];
            $tueendtime = $_REQUEST['tueendtime'];

            $wednesday = $_REQUEST['wednesday'];
            $wedstarttime = $_REQUEST['wedstarttime'];
            $wedendtime = $_REQUEST['wedendtime'];

            $thrusday = $_REQUEST['thrusday'];
            $thrustarttime = $_REQUEST['thrustarttime'];
            $thruendtime = $_REQUEST['thruendtime'];

            $friday = $_REQUEST['friday'];
            $fristarttime = $_REQUEST['fristarttime'];
            $friendtime = $_REQUEST['friendtime'];

            $saturday = $_REQUEST['saturday'];
            $satstarttime = $_REQUEST['satstarttime'];
            $satendtime = $_REQUEST['satendtime'];

            $college1 = $_REQUEST['college1'];
            $degree1 = $_REQUEST['degree1'];
            $qsyear1 = '0000-00-00';
            $qeyear1 = '0000-00-00';
            //echo $college1.$degree1.$qsyear1.$qeyear1;
            $is_pursuing1 = 0;

            $college2 = $_REQUEST['college2'];
            $degree2 = $_REQUEST['degree2'];
            $qsyear2 = '0000-00-00';
            $qeyear2 = '0000-00-00';
            $is_pursuing2 = 0;

            $whospri1 = $_REQUEST['whospri1'];
            $designation1 = $_REQUEST['designation1'];
            $worksyear1 = '0000-00-00';
            $workeyear1 = '0000-00-00';
            $is_current2 = 0;
            $is_current1 = 0;

            $whospri2 = $_REQUEST['whospri2'];
            $designation2 = $_REQUEST['designation2'];
            $worksyear2 = $_REQUEST['worksyear2'];
            $workeyear2 = $_REQUEST['workeyear2'];
            $is_current2 = 0;

            $is_pursuing0 = $_REQUEST['curentpersuing0'];
            if ($is_pursuing0 == "Yes") {
                $is_pursuing0 = 1;
            } else if ($is_pursuing0 == "No") {
                $is_pursuing0 = 0;
            } else {
                $is_pursuing0 = 0;
            }
            $is_current0 = $_REQUEST['curentworking0'];
            if ($is_current0 == "Yes") {
                $is_current0 = 1;
            } else if ($is_current0 == "No") {
                $is_current0 = 0;
            } else {
                $is_current0 = 0;
            }
            $incrementq = $_REQUEST['q'];
            $incremente = $_REQUEST['j'];
        }

        if (!empty($email) && !empty($mobile)) {

            $latlongquery = "SELECT user_id,email,mobile FROM users WHERE email='$email' and mobile = $mobile";

            $latlonquery = $link->query($latlongquery);
            $dbemail = "";
            $dbmobile = "";
            $dbuserid = "";
            $message = "";
            $err = "";
            $isdoctor = 0;
            while ($recordlat = mysqli_fetch_array($latlonquery)) {
                $dbuserid = $recordlat ["user_id"];
                $dbemail = $recordlat ["email"];
                $dbmobile = $recordlat ["mobile"];
            }
            $doctorexit = "SELECT is_doctor FROM user_profile WHERE user_id=$dbuserid";
            //echo $doctorexit;
            if (!empty($dbuserid)) {
                $doctorquery = $link->query($doctorexit);
                while ($doctorex = mysqli_fetch_array($doctorquery)) {
                    $isdoctor = $doctorex ["is_doctor"];
                    //$isdoctor = 0;
                }
            }
            //$modify=$_SESSION["user_idforedit"];
            $modify = $_REQUEST['editid'];
            //echo $modify;
            if (strtolower($email) == strtolower($dbemail) && $mobile == $dbmobile && $isdoctor != 1 || $modify = "modify") {

                if ($name) {
                    $docName = "UPDATE users SET name = '$name' WHERE user_id = $dbuserid";
                    $link->query($docName);
                }

                if ($salutation) {
                    $docSalutation = "UPDATE users SET salutation = '$salutation' WHERE user_id = $dbuserid";
                    $link->query($docSalutation);
                }

                $cityid = "";
                if ($dbuserid != 0 && !empty($city)) {
                    //echo "hi".$dbuserid;
                    if (!empty($city)) {
                        $colexit = "SELECT city_id FROM city_master WHERE name='$city'";
                        //echo $doctorexit;
                        $citid = $link->query($colexit);
                        while ($cityaa = mysqli_fetch_array($citid)) {
                            $cityid = $cityaa ["city_id"];
                            //echo "cityid=".$cityid;
                        }
                    }
                    if (empty($cityid)) {
                        $cityid = 999999;
                    }
                    $doctonlile = "update users SET status=1 where user_id=$dbuserid";
                    //echo "update profile".$sql.'<br>';
                    if ($link->query($doctonlile) === TRUE) {
                        $message = "User status updated,";
                        $err = 200;
                    } else {
                        $message = $link->error;
                        $err = 400;
                    }
                    //echo $cityid;
                    $sql = "update doctor_profile SET is_doctor=1,city_id=$cityid where user=$dbuserid";
                    //echo "update profile".$sql.'<br>';
                    if ($link->query($sql) === TRUE) {
                        $message = "User profile succsess,";
                        $err = 200;
                    } else {
                        $message = $link->error;
                        $err = 400;
                    }

                    //echo "update profile".$sql.'<br>';
                    if ($link->query($sql) === TRUE) {
                        $message = "User Settubg relation succsess,";
                        $err = 200;
                    } else {
                        $message = $link->error;
                        $err = 400;
                    }
                }
                if ($dbuserid != 0) {
                    if (empty($pminute)) {
                        $pminute = 0.00;
                    }
                    $created_at = date('Y-m-d h:i:s');
                    //$hospitalid=1;
                    $extension = "qwe";
                    $is_approved = 0;
                    $is_available = 0;
                    $universityid = "";
                    $degreeid = "";
                    $specializationid = "";
                    $is_default = 1;
                    $hospitalid = "";
                    $designationid = "";
                    $status_message1 = "";
                    $status_message2 = "";
                    $err2 = "";
                    $info = "Test Information";

                    //$modify=$_SESSION["user_idforedit"];

                    $allowNewUser = 0;
                    if ($allowNewUser == "Yes") {
                        $allowNewUser = 1;
                    }

                    if ($modify != "modify") {
                        $sql2 = "INSERT INTO doctor_profile(user,medical_registration_no,total_experience,info,per_min_charges,is_approved,created_at,updated_at,other_info,area, allow_new_user) VALUES ($dbuserid,'$mregistration','$sregistration',$experience,'$address',$pminute,$is_approved,$is_available,'$created_at','$created_at','$otherinfo','$area', $allowNewUser)";
                        //echo "insert doctor_profile".$sql2.'<br>';
                        if ($link->query($sql2) === TRUE) {
                            $status_message2 = "doctor profile success,";
                            $err2 = 200;
                        } else {
                            $status_message2 = $link->error;
                            $err2 = 400;
                        }
                    } else {
                        $sql = "update doctor_profile SET medical_registration_no='$mregistration',total_experience=$experience,info='$address',per_min_charges=$pminute,other_info='$otherinfo',area='$area' where user=$dbuserid";
                        //echo "update profile".$sql.'<br>';
                        if ($link->query($sql) === TRUE) {
                            $message = "Doctor Profile is updated,";
                            $err = 200;
                        } else {
                            $message = $link->error;
                            $err = 400;
                        }
                    }
                }
                $created_at = date('Y-m-d h:i:s');
                //echo $incrementq;
                //qualification
                for ($i = 0; $i <= $incrementq; $i++) {
                    //echo $incrementq;
                    $college1 = $_REQUEST['college' . $i];
                    $degree1 = $_REQUEST['degree' . $i];
                    $qsyear1 = '0000-00-00';
                    $qeyear1 = '0000-00-00';
                    $is_pursuing1 = 0;
                    $qualification_id = $_REQUEST['qualid' . $i];

                    if ($dbuserid != 0 && !empty($degree1)) {

                        if (!empty($degree1)) {
                            $degexit = "SELECT degree_id FROM degree_master WHERE name='$degree1'";
                            //echo "select degree_master".$degexit.'<br>';
                            $degid = $link->query($degexit);
                            while ($degrld = mysqli_fetch_array($degid)) {
                                $degreeid = $degrld ["degree_id"];
                            }
                        };
                        $universityid = 1;
                        if (empty($degreeid)) {
                            $degreeid = 999999;
                        }
                        //echo $degreeid.$universityid;
                        // if(empty($qsyear1)){
                        $qsyear1 = '0000-00-00';
                        // }
                        // if(empty($qeyear1)){
                        $qeyear1 = '0000-00-00';
                        // }
                        $collegee = ucwords($college1);
                        if ($qualification_id == 0 || $modify != "modify") {
                            $qsql3 = "INSERT INTO doctor_qualifications(user_id,university,college,degree,start_date,end_date,is_pursuing,created_at,updated_at) VALUES ($dbuserid,$universityid,'$collegee',$degreeid,'$qsyear1','$qeyear1',$is_pursuing1,'$created_at','$created_at')";
                            //echo $qsql3; 
                            if ($link->query($qsql3) === TRUE) {
                                $status_message3 = "Doctor Qualification,";
                                $err3 = 200;
                            } else {
                                $status_message3 = $link->error;
                                $err3 = 400;
                            }
                        } else {
                            $sql = "update doctor_qualifications SET college='$collegee',degree=$degreeid,updated_at='$created_at' where user_id=$dbuserid and qualification_id=$qualification_id";
                            //echo "update profile ".$sql.'<br>';
                            if ($link->query($sql) === TRUE) {
                                $message = "Doctor Qualification is updated,";
                                $err = 200;
                            } else {
                                $message = $link->error;
                                $err = 400;
                            }
                        }

                        //echo "insert doctor_qualifications".$qsql3.'<br>'.$status_message3;
                    }
                    $universityid = "";
                    $degreeid = "";
                }
                //doctor specialization
                if ($dbuserid != 0 && !empty($specialization)) {
                    $specexit = "SELECT specialization_id FROM specialization_master WHERE name='$specialization'";
                    //echo "select degree_master".$degexit.'<br>';
                    $speid = $link->query($specexit);
                    while ($specld = mysqli_fetch_array($speid)) {
                        $specializationid = $specld ["specialization_id"];
                    }
                    if (empty($specializationid)) {
                        $specializationid = 999999;
                    }
                    if ($modify != "modify") {
                        $qsql5 = "INSERT INTO doctor_specialization_rel(doctor,specialization,is_default,created_at,updated_at) VALUES ($dbuserid,$specializationid,$is_default,'$created_at','$created_at')";
                        //echo "insert doctor_specialization_rel".$qsql5.'<br>';
                        if ($link->query($qsql5) === TRUE) {
                            $status_message4 = "Doctor Specialization Success,";
                            $err4 = 200;
                        } else {
                            $status_message4 = $link->error;
                            $err4 = 400;
                        }
                    } else {
                        $sql = "update doctor_specialization_rel SET specialization=$specializationid,updated_at='$created_at' where doctor=$dbuserid";
                        //echo "update profile".$sql.'<br>';
                        if ($link->query($sql) === TRUE) {
                            $message = "Doctor specialization is updated,";
                            $err = 200;
                        } else {
                            $message = $link->error;
                            $err = 400;
                        }
                    }
                }
                //work experience
                for ($j = 0; $j <= $incremente; $j++) {
                    //echo $incremente;
                    //$workplace_id=0;
                    $whospri1 = $_REQUEST['whospri' . $j];
                    $designation1 = $_REQUEST['designation' . $j];
                    $worksyear1 = '0000-00-00';
                    $workeyear1 = '0000-00-00';
                    $is_current1 = 0;
                    $workplace_id = $_REQUEST['hopitalid0'];

                    if ($dbuserid != 0 && !empty($whospri1)) {
                        $hospitalid = 1;
                        $designationid = 1;
                        // if(empty($worksyear1)){
                        $worksyear1 = '0000-00-00';
                        //}
                        // if(empty($workeyear1)){
                        $workeyear1 = '0000-00-00';
                        // }
                        /* if($workplace_id==0){
                          $modify="other";
                          } */
                        $hospitalname = ucwords($whospri1);
                        $designationname = ucwords($designation1);
                        if ($workplace_id == 0 || $modify != "modify") {
                            $dqsql2 = "INSERT INTO doctor_workplace(doctor,hospital,hospital_name,designation,designation_name,join_date,end_date,is_current,created_at,updated_at) VALUES ($dbuserid,$hospitalid,'$hospitalname',$designationid,'$designationname','$worksyear1','$workeyear1',$is_current1,'$created_at','$created_at')";
                            //echo "insert doctor_workplace".$dqsql2.'<br>';
                            if ($link->query($dqsql2) === TRUE) {
                                $status_message5 = "Successfully inserted doctor experience,";
                                $err5 = 200;
                            } else {
                                $status_message5 = $link->error;
                                $err5 = 400;
                            }
                        } else {
                            $sql = "update doctor_workplace SET hospital_name='$hospitalname',designation_name='$designationname',updated_at='$created_at' where doctor=$dbuserid";
                            //echo "update profile ".$sql.'<br>';
                            if ($link->query($sql) === TRUE) {
                                $message = "Doctor experience is updated,";
                                $err = 200;
                            } else {
                                $message = $link->error;
                                $err = 400;
                            }
                        }
                    }
                    $hospitalid = "";
                    $designationid = "";
                }

                if ($modify != "modify") {
                    echo "<font color='red'>You are registered as doctor.</font>";
                } else {
                    echo "<font color='red'>You have Successfully Updated Profile.</font>";
                }
                //echo "enters in insertion";
            } else {
                if ($isdoctor == 1) {
                    echo "<font color='red'>You are allready registered as doctor.</font>";
                } else {
                    echo "<font color='red'>You are not Valid User.</font>";
                }
            }
        } else {
            echo "<font color='red'>Please Enter Email and Mobile No.</font>";
        }
        //$_SESSION["user_idforedit"] = "modify1";
    }

}
