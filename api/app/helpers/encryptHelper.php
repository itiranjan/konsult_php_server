<?php

class encryptHelper {

    /**
     * @author : Parth
     * @created : 28 jan 2015
     * @description : convert string into md5 format
     * @param : string $string
     * @param : string [Optional] $method
     * @access public
     * @return  array 
     */
    public function encrypt($string, $method = 'md5') { // Move it to the common_helper
        $key = '';
        switch ($method) {
            case 'md5':
                $key = md5($string);
                break;
            default:
                $key = md5($string);
        }
        return $key;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 29 jan 2015
     * @description : This function is used for data encryption using key
     * @param : string $encrypt
     * @param : string $key
     * @access public
     * @return  array 
     */
    function mcEncrypt($encrypt, $key) {

        //Encrypt Function
        // comented as IOS having some issue with the alogorithm
        $encrypt = serialize($encrypt);
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
        $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
        $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt . $mac, MCRYPT_MODE_CBC, $iv);
        $encoded = base64_encode($passcrypt) . '|' . base64_encode($iv);

        return $encoded;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 29 jan 2015
     * @description : This function is used for data Decryption using key
     * @param : string $decrypt
     * @param : string $key
     * @access public
     * @return  array 
     */
    function mcDecrypt($decrypt, $key) {

        // Decrypt Function
        try {
            $decrypt = explode('|', $decrypt);
            $decoded = base64_decode($decrypt[0]);
            $iv = base64_decode($decrypt[1]);
            $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
            $mac = substr($decrypted, -64);
            $decrypted = substr($decrypted, 0, -64);
            $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
            if ($calcmac !== $mac) {
                throw new APIError('1018', 'Decryption failed');
            }
            $decrypted = unserialize($decrypted);
            return $decrypted;
        } catch (exception $e) {
            return False;
        }
    }
    
    public function encryptPWD($params = array()) {

        $options = [
            'cost' => 10,//DEFAULT VALUE
        ];

        $postedPWD = trim($params['PWD']);

        return password_hash($postedPWD, PASSWORD_BCRYPT, $options);
    }

    public function validatePWD($params = array()) {

        $dbPWD = $params['dbPWD'];
        $postedPWD = $params['postedPWD'];

        if (password_verify($postedPWD, $dbPWD)) {
            return 1;
        } else {
            return 0;
        }
    }    
    

}
