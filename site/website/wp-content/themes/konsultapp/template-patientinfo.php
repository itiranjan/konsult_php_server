<?php
/**
 * Template Name: Patient Info
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */
//require_once('../../../wp-config.php');
 get_header();

global $wpdb;

	// fetching country list
	$country =  $wpdb->get_results("select country_master.country_id, country_master.name as country_name from country_master", ARRAY_A);
	$totalRows_country = count($country);
	
	// fetching specialization list
	$specialize =  $wpdb->get_results("select specialization_master.specialization_id, specialization_master.name as specialization_name from specialization_master", ARRAY_A);
	$totalRows_specialize = count($specialize);
	
	$form_type = "patient";
	// inserting data to patient_contact table
	
	$patient_contact = "patient_contact"; // Table name
	
	if((isset($_POST['patient_info'])) ) //and (!empty($_POST['patient_info']))
	{
		$wpdb->insert($patient_contact, array(
										'name' => $_POST['patient_name'],
										'email' => $_POST['email'],
										'phone' => $_POST['phone'],
										'age' => $_POST['age'],
										'gender' => $_POST['gender'],
										'country' => $_POST['country'],
										'message' => $_POST['message'],
										'type' => $form_type
									));
		/* =============== Mail Function =================== */
         $to = "info@konsultapp.com";
         $subject = $_POST['patient_name']." is patient and has send an enquiry";
         
         $message = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin-bottom:20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi Admin,</p>
										<p style="font-size:14px Arial;">Name: ' .$_POST["patient_name"]. '</p>
										<p style="font-size:14px Arial;">Email: ' .$_POST["email"]. '</p>
										<p style="font-size:14px Arial;">Phone: ' .$_POST["phone"]. '</p>
										<p style="font-size:14px Arial;">Age: ' .$_POST["age"]. '</p>
										<p style="font-size:14px Arial;">Gender: ' .$_POST["gender"]. '</p>
										<p style="font-size:14px Arial;">Country: ' .$_POST["country_val"]. '</p>
										<p style="font-size:14px Arial;">Message: ' .$_POST["message"].'</p>										
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			//$from = $_POST['email'];
			$from = "info@konsultapp.com";
			$headers = "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=UTF-8\n";
			$headers .= 'From: Konsultapp <'.$from.'>' . "\r\n";
			$headers .= "Reply-To: me <".$to.">\r\n";
			/*$headers .= "X-Priority: 500\n";
			$headers .= "X-MSMail-Priority: High\n";*/
			$headers .= "X-Mailer: My mailer";
			
		$retval = wp_mail($to,$subject,$message,$headers);
			
		/* =============== Patient Mail Function =================== */
        $to_patient = $_POST['email'];
        $subject_patient = "Thank you for your query";
         
        $message_patient = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin-bottom:20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi '.$_POST["patient_name"].',</p>
										<p style="font-size:14px Arial;">You have successfully send the enquiry. We will be in touch soon. </p>
										<p style="font-size: 14px Arial;">Thank you</p>
										<p style="font-size: 14px Arial;">Konsult App Private Limited</p>
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			$from_patient = "noreply@konsultapp.com";
			$header_patient = "MIME-Version: 1.0\n";
			$header_patient .= "Content-type: text/html; charset=UTF-8\n";
			$header_patient .= 'From: Konsultapp <'.$from_patient.'>' . "\r\n";
			$header_patient .= "Reply-To: me <".$to.">\r\n";
			/*$header_patient .= "X-Priority: 500\n";
			$header_patient .= "X-MSMail-Priority: High\n";*/
			$header_patient .= "X-Mailer: My mailer";
         
         $retval_patient = wp_mail($to_patient,$subject_patient,$message_patient,$header_patient);
         
        if( $retval == true ) 
		{
            $message = "Thank you! You have successfully submitted the data. We'll be in touch soon.";
        }
		else 
		{
            $message = "You are wrong somewhere";
        }
	}	
?>
	
<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
		
			<div class="entry-content" itemprop="mainContentOfPage">
				<?php 
					while ( have_posts() ){
						the_post();							// Post Loop
						mfn_builder_print( get_the_ID() );	// Content Builder & WordPress Editor Content
					}
				?>
			</div>
			
			<?php if( mfn_opts_get('page-comments') ): ?>
				<div class="section section-page-comments">
					<div class="section_wrapper clearfix">
					
						<div class="column one comments">
							<?php comments_template( '', true ); ?>
						</div>
						
					</div>
				</div>
			<?php endif; ?>

			<form action="<?php echo home_url()."/patient-information/" ?>" method="post" class="wpcf7-form sent">
				<div style="display: none;">
					<input type="hidden" name="_wpcf7" value="4">
					<input type="hidden" name="_wpcf7_version" value="4.4.2">
					<input type="hidden" name="_wpcf7_locale" value="en_US">
					<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p3115-o1">
					<input type="hidden" name="_wpnonce" value="3604a626a7">
				</div>
				<div class="patient-info">
					<?php if(!empty($message)) {?>
					<div class="display-msg">
						<p><?php echo $message; ?></p>
					</div>
					<?php } ?>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap your-name">
								<input type="text" name="patient_name" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="Name *" required oninvalid="setCustomValidity('Please fill your name')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap your-email">
								<input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" placeholder="Email *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required oninvalid="setCustomValidity('Please fill your valid email id')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap phone">
								<input type="tel" name="phone" pattern="^([0|\+[0-9]{1,5})?([1-9][0-9]{9})$" maxlength="10" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" placeholder="Phone No. *" required oninvalid="setCustomValidity('Please fill your mobile no')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-fourth">
						<p>
							<span class="wpcf7-form-control-wrap age">
								<select name="age" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="contct-age" aria-required="true" aria-invalid="false" required oninvalid="setCustomValidity('Please select your age')" oninput="setCustomValidity('')">
									<option value="">----Select Age----</option>
									<?php
										for($i=1; $i<=100;$i++)	{
									?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
										<?php }?>
								</select>
							</span>
						</p>
					</div>
					<div class="column one-fourth">
						<p style="box-sizing: border-box; display: inline-flex; padding: 5px 0 0; font-size: 14px;">
							<span class="gen">Gender</span>
							<span class="wpcf7-form-control-wrap gender">
								<span class="wpcf7-form-control wpcf7-radio">
									<span class="wpcf7-list-item first">
										<input type="radio" name="gender" value="Male" checked="checked">&nbsp;
										<span class="wpcf7-list-item-label">Male</span>
									</span>
									<span class="wpcf7-list-item last">
										<input type="radio" name="gender" value="Female">&nbsp;
										<span class="wpcf7-list-item-label">Female</span>
									</span>
								</span>
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap country">
								<select name="country" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="country" onchange="document.getElementById('country_selected').value=this.options[this.selectedIndex].text" required oninvalid="setCustomValidity('Please select your country')" oninput="setCustomValidity('')">
									<option value="">-----Country-----</option>
									<?php if($totalRows_country > 0) 
										  {		
											foreach($country as $key => $value ) 
											{ ?>
												<option value="<?php echo $value['country_id']; ?>"><?php echo $value['country_name']; ?></option>
									<?php	}
										} ?>									
								</select>
								<input type="hidden" name="country_val" id="country_selected" value="" />
							</span>
						</p>
					</div>
					<div class="column one">
						<p>
							<span class="wpcf7-form-control-wrap your-message">
								<textarea name="message" cols="40" rows="10" placeholder="Write your query" required oninvalid="setCustomValidity('Please fill the query you have')" oninput="setCustomValidity('')">
								</textarea>
							</span>
						</p>
					</div>
					<div class="column one">
						<input type="submit" value="Submit" name="patient_info" class="pi-btn">
					</div>
				</div>
			</form>
		</div>
		
		<!-- .four-columns - sidebar -->
		<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>