<link rel="stylesheet" href="<?php echo(URL . '/assets/css/stylesheet.css'); ?>" />
<div class="login_bg">
    <?php echo LOADER; ?>
    <div id="login-box">
        <h1>Make ISD Call</h1>
        <form name="form_login" id="form_login" action="verifylogin" method="post" class="login">
            <fieldset>  
                <legend>Enter Phone Number (with country code)</legend>
                <input type="text" id="p_phone" name="phone" class="mediuminput"/>
                <span id="phone_number_mismatch" style="color: red; display: none;">Phone numbers mis-match!</span>
                <span id="phone_invalid" style="color: red; display: none;">Invalid phone number!</span>                
<!--                <legend>Re-enter Phone Number (with country code)</legend>
                <input type="text" id="r_phone" name="r_phone" class="mediuminput"/>-->
            </fieldset>
            <input type="hidden" name="p_loginuser" id="p_loginuser" value="1"/>
            <input type="button" name="btn_login" id="btn_login" value="Call" onclick="makeISDCall()" />
        </form>       
    </div>   
</div> 

<script type="text/javascript" src="<?php echo(JS . "/jquery.min.js"); ?>"></script>
<script>
    $(document).keypress(function (e) {
        if (e.which == 13) {
            assignMinutes();
        }
    });
    
    var error = 'Your request cannot be processed due to some technical problems. Please try after some time!';
    function makeISDCall() {
                
        document.getElementById("phone_number_mismatch").style.display = 'none';
        document.getElementById("phone_invalid").style.display = 'none';           
        
        var p_phone = document.getElementById("p_phone").value;
        //var r_phone = document.getElementById("r_phone").value;
        
        if(p_phone == '') {
            alert("Enter phone number!")
            document.getElementById("phone_invalid").style.display = 'block';
            return false;            
        }
        
//        if(p_phone != r_phone) {
//            alert("Phone numbers mis-match!")
//            document.getElementById("phone_number_mismatch").style.display = 'block';
//            return false;            
//        }

//        if (/^\[1-9]{1}[0-9]{3,14}$/.test(p_phone)) {
//
//        } else {
//            alert("Invalid number!")
//            document.getElementById("phone_invalid").style.display = 'block';
//            return false;
//        }   
        
        if(!confirm("Are you sure, to make a call to " + p_phone)){
            return false;
        }        
        
        // Launch ajax request
        $.ajax({
            
            // The link we are accessing.
            url: '<?php echo URL; ?>/index.php/miscellaneous/connectISDCall',
            // The type of request.
            type: 'POST',
            // The data we are sending.
            data: $("form").serialize(),
            // The type of data that is getting returned.
            dataType: 'html',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                
                alert(data);
                
                location.reload(true);
            },
            error: function () {
                alert(error);
            }
        });
        // Prevent default click
        return false;
    }
    
    callFunction = function(d) {
        alert(d.msg);
    };
    
    function ajaxCall(url, data, customFunction) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            dataType: 'json',
            beforeSend: function() {
                $('#loading').show();
            },
            complete: function() {
                $('#loading').hide();
            },
            success: customFunction,
            error: function() {
                alert(error);
            }
        });
    }
</script>
 



