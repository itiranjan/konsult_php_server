<?php

class CronController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {

        $requestHeaders = apache_request_headers();
        if ($requestHeaders ['API_KEY'] && $requestHeaders ['APP_VERSION'] && $requestHeaders ['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders ['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders ['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders ['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders ['API_KEY']) {
                    $result ['config_status'] = - 1;
                    $result ['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result ['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders ['AUTH_TOKEN'];
            }
        } else {
            $result ['config_status'] = - 1;
            $result ['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    public function updateDoctorsStatus() {

        $currentTime = date("H:i:s");

        if (1/* $key == 'givenKey' */) {
            date_default_timezone_set("Asia/Calcutta");
            $operationDay = date('l');
            if ($operationDay != 'Sunday') {
                $day_id = DB::table('days_master')->where('name', '=', $operationDay)->pluck('day_id');

                $allDoctors = DB::table('operation_hours')->where('day_id', '=', $day_id)->lists('user_id');
                $allDoctors = array_unique($allDoctors);

                $makeOnlineDoctors = DB::table('operation_hours')->where('start_time', '<=', $currentTime)->where('end_time', '>', $currentTime)->where('day_id', '=', $day_id)->lists('user_id');
                $makeOnlineDoctors = array_unique($makeOnlineDoctors);

                $makeOffline = array_diff($allDoctors, $makeOnlineDoctors);

                //MAKE OFFLINE
                if (!empty($makeOffline)) {
                    Doctor::whereIn('user_id', $makeOffline)->update(array('online_status' => 0));
                }

                //MAKE ONLINE
                if (!empty($makeOnlineDoctors)) {
                    Doctor::whereIn('user_id', $makeOnlineDoctors)->update(array('online_status' => 1));
                }
            }
        }
    }

}
