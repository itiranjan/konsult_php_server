<?php

class Degree extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'degree_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'degree_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

}
