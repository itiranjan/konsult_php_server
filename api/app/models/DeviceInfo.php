<?php

class DeviceInfo extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'device_info';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'device_info_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

}
