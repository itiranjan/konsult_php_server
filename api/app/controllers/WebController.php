<?php

$basePath = base_path();
$basePath = substr($basePath, 0, -3);
require_once '/var/www/html/konsult/wp-blog-header.php';

class WebController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function blogInfo() {

        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $blog_id = $data['blog_id'];

            if (empty($blog_id)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['status'] = 0;
                $result['msg'] = 'Blog id missing!';
            } else {
                $this->setHeader('RESPONSE_CODE', '0');

                $blog = DB::connection('mysql1')->table('wp_posts')
                        ->select('ID', 'post_content', 'post_title')
                        ->where('ID', '=', $blog_id)
                        ->where('post_type', '=', 'post')
                        ->where('post_status', '=', 'publish')
                        ->first();

                if (count($blog) <= 0) {
                    $result['status'] = 0;
                    $result['msg'] = 'Blog does not exist or not published!';
                } else {
                    $result['status'] = 1;
                    $result['blog_url'] = get_permalink($blog_id);

                    $imageURL = wp_get_attachment_image_src(get_post_thumbnail_id($blog->ID), 'full');
                    $imageURL = str_replace( 'https://', 'http://', $imageURL );
                    $result ['image_url'] = is_array($imageURL) ? $imageURL[0] : '';
                    $result['post_title'] = $blog->post_title;
                    $result['post_content'] = preg_replace("/<img[^>]+\>/i", "", $blog->post_content);
                    $result['msg'] = 'Blog info successfully retrieved!';
                }
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function blogListing() {

        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $page = !empty($data['page']) ? $data['page'] : 1;
            $limit = 10;

            $this->setHeader('RESPONSE_CODE', '1');

            //GET BLOGS
            $blogs = DB::connection('mysql1')->table('wp_posts')
                    ->select('ID', 'post_title', 'post_content')
                    ->where('post_type', '=', 'post')
                    ->where('post_status', '=', 'publish')
                    ->orderby('post_date', 'desc')
                    ->skip($limit * ($page - 1))->take($limit)
                    ->get();

            if (count($blogs)) {
                $blogListing = array();
                foreach ($blogs as $blog) {
                    $response ['ID'] = $blog->ID;
                    $response ['post_title'] = $blog->post_title;
                    $response ['blog_url'] = get_permalink($blog->ID);
                    $response ['blog_url'] = str_replace( 'https://', 'http://', $response ['blog_url'] );

                    $imageURL = wp_get_attachment_image_src(get_post_thumbnail_id($blog->ID));
                    $response ['image_url'] = is_array($imageURL) ? $imageURL[0] : '';
                    $response ['image_url'] = str_replace( 'https://', 'http://', $response ['image_url'] );

                    $blog_content = strip_tags($blog->post_content);
                    $blog_content = trim($blog_content);
                    $blog_content = ( strlen($blog_content) > 110 ) ? (substr($blog_content, 0, 110) . "......") : $blog_content;

                    $response['post_content'] = $blog_content;

                    $json_response = json_encode($response);
                    $array_decode = json_decode($json_response);
                    array_push($blogListing, $array_decode);
                }

                $result['status'] = 1;
                $result['msg'] = 'Blog listing!';
                $result['blogListing'] = $blogListing;
            } else {
                $result['status'] = 1;
                $result['msg'] = 'No blog found!';
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function getAppLink() {

        $input = Input::all();

        if ($input['medium'] != 'webform' && $input['medium'] != 'missedcall') {
            return array('status' => 0, 'msg' => 'Unknown medium type.');
        }

        try {

            $mobileNo = substr($input['mobile'], -10);

            if (preg_match('/^[7-9][0-9]{9}$/', $mobileNo)) {

                $getAppLink = AppLinkLog::where('mobile', '=', $mobileNo)->first();
                if (!count($getAppLink)) {
                    $getAppLink = new AppLinkLog;
                    $getAppLink->mobile = $mobileNo;
                    $getAppLink->save();
                }

                $getAppLink->medium = $input['medium'];

                //SEND SMS ALERT
                $time_diff = $this->helper->load('date')->time_difference($getAppLink->updated_at, date("Y-m-d H:i:s"));
                if ($getAppLink->sent_count < 4 || $time_diff > 60 * 24) {
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_GETAPPLINK');
                    if ($smsText) {
                        $smsParams = array();
                        $URL = Config::get('constants.APP_DOWNLOAD_URL');
                        $smsParams['Text'] = str_replace('<URL>', $URL, $smsText);
                        $smsParams['To'] = $mobileNo;
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                }

                $getAppLink->sent_count++;
                $getAppLink->save();

                return array('status' => 1, 'msg' => 'SMS sent successfully.');
            }

            return array('status' => 0, 'msg' => 'Invalid mobile number.');
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = 'Unable to send the SMS.';

            return $result;
        }
    }

}
