<?php

class PatDocMinInfo extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'pat_doc_min_info';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'min_info_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

}
