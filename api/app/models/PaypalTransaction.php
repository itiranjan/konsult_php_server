<?php

class PaypalTransaction extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'paypal_transactions';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'transaction_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

}
