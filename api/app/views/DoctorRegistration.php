<!DOCTYPE html>
<html lang="en">
<head>
<title>Registration</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link href="css/style-konsult.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<style>
input {
	font-size: 18px;
	min-height: 0px;
	width: 100%;
	border-radius: 0px;
	line-height: 0px;
	padding: 7px 7px 7px;
	border: none;
	margin-bottom: 10px;
	background-color: #fff /**#e9f0f2**/;
	-webkit-transition: background-color 0.2s;
	transition: background-color 0.2s;
	border: 1px #CCC solid;
}
.heading_h{color:#666;font-size:24px;margin-top:0;padding-bottom:40px;padding-top:20px;color:#0ab2f1;float:left;}
.pic_s{border-radius:100px;float:right;}
.f_div{float:left;width:150px;}
.s_div{padding:5px 10px 5px 10px;border:1px #999 solid;float:left;margin-right:50px;}
.t_div{opacity:0.5;padding:5px 10px 5px 10px;border:1px #999 solid;float:left;}
.f_submit{width:150px;padding-top:20px;padding-bottom:20px;background:#0ab2f1;color:#FFF;}
</style>
</head>
<body>
<?php 
if(isset($_SESSION['Submitbt'])){
	header("Location: http://localhost:8085/konsultdev1/site/admin/application/views/admin/doctRegistration.php");
}
?>
<div id="menu-bar-fixed" style="opacity: 1;top: 0;">
<div class="container"> <a class="logo" href="index.html"><img src="img/logo-sm.png" alt=""></a>
<div style="float:right;">Sudhesh Parasar</div>
</div>
</div>
<div class="container" style="margin-top: 120px;color:#000;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding-right:5%;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="heading_h">Doctor Registration</h1></td>
<td style="float:right;"><span>Name</span><br/>
<input type="text"></td>
</tr>
</table></td>
<td style="padding-left:5%;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="float:left;"><span>Mobile</span><br/>
<input type="text"></td>
<td><img src="img/profile.jpg" height="100"  alt="" class="pic_s"/></td>
</tr>
</table></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>City</span><br/>
<input type="text"></td>
<td style="padding-left:5%;"><span>Address</span><br/>
<input type="text"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Qualification</span><br/>
<input type="text"></td>
<td style="padding-left:5%;"><span>Experience(Years)*</span><br/>
<input type="text"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Specialization*</span><br/>
<input type="text"></td>
<td style="padding-left:5%;"><span>Work Experience</span><br/>
<input type="text"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Medical Registration Number*</span><br/>
<input type="text"></td>
<td style="padding-left:5%;"><span>State Medical Registration Number</span><br/>
<input type="text"></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>

</table>
</div>

<footer id="footer" style="position:static;">
<div class="container">
<div id="footer-content">
<h2><img src="img/logo.png" alt=""></h2>
<!-- <p id='social-links'>
<a href="#" class='icon ion-social-facebook'></a>
<a href="#" class='icon ion-social-twitter'></a>
<a href="#" class='icon ion-social-googleplus-outline'></a>
<a href="#" class='icon ion-social-instagram'></a>
</p> -->
<ul class="f_links">
		<li><a href="terms.html">Terms &amp; Conditions</a></li>
		<li><a href="policy.html">Privacy Policy</a></li>
		</ul>
		<p class="copyright">Copyright � 2015. KONSULT</p>
		</div>
		</div>
		</footer>
		</body>
		</html>