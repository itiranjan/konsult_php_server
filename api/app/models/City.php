<?php

class City extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'city_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'city_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = false;
    
    public function getCityName($city_id) {

        return DB::table('city_master AS cm')
                        ->where('cm.city_id', '=', $city_id)
                        ->pluck('cm.name');
    }    

}
