<?php

class User extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'users';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

    /**
     * To store validation errors
     */
    private $errors;

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined for user registration
     * @var array
     */
    private $rules = array(
        'name' => 'required|min:3|regex:/(^[A-Za-z ]+$)+/',
        'mobile' => 'required|numeric|unique:users',
        // 'mobile' => 'required|digits_between:8,15|min:8|max:15|unique:users|regex:/([+][0-9]{6,13})/',
        'email' => 'required|email|regex:/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
        'password' => 'required|min:3'
    );

    /**
     * validation rules defined for password 
     * @var array
     */
    private $pwdrules = array(
        'password' => 'required|min:3',
        'username' => 'required'
    );

    /**
     * validation rules defined for mobile no 
     * @var array
     */
    private $mobilerules = array(
        //'mobile' => 'digits_between:8,15|min:8|max:15|unique:users',
        'mobile' => 'unique:users|numeric',
        'name' => 'min:3|regex:/(^[A-Za-z ]+$)+/'
    );

    /**
     * validation custom message defined for mobile no 
     * @var array
     */
    private $mobilemessages = array(
        'mobile.regex' => 'Mobile no should start from \'+\' sign'
    );

    /**
     * validation custom message defined for user registration api
     * @var array
     */
    private $messages = array(
        'name.regex' => 'The name may only contain letters and space',
        'mobile.regex' => 'Mobile no should start from \'+\' sign',
        'email.regex' => 'Invalid Email Address'
    );

    /**
     * validation rules defined for model
     * @var array
     */
    private $rulesCallRequest = array(
        'doctor' => 'required|integer|exists:users,user_id',
        'patient' => 'required|integer|exists:users,user_id'
    );

    /**
     * validation rules defined for user status
     * @var array
     */
    private $rulesStatus = array(
        'availability_status' => 'required|boolean'
    );

    /**
     * validation rules defined for Push msg
     * @var array
     */
    private $rulescheckStatus = array(
        'user_id' => 'required|integer|exists:users,user_id'
    );

    /**
     * validation rules defined for mobile no 
     * @var array
     */
    private $resetPasswordrules = array(
        'verify_code' => 'required',
        'newpassword' => 'required|min:3'
    );

    /**
     * validation rules defined for mobile no 
     * @var array
     */
    private $updateprofilerules = array(
        // 'mobile' => 'digits_between:8,15|min:8|max:15|unique:users',
        'mobile' => 'unique:users|numeric',
        'name' => 'min:3|regex:/(^[A-Za-z ]+$)+/',
        'email' => 'email|unique:users|regex:/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/',
        'newpassword' => 'min:3'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function updateprofilerules($data) {

        $v = Validator :: make($data, $this->updateprofilerules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function resetPassword($data) {

        $v = Validator :: make($data, $this->resetPasswordrules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validatecheckStatus($data) {

        $v = Validator :: make($data, $this->rulescheckStatus);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validateStatus($data) {

        $v = Validator :: make($data, $this->rulesStatus);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        if (!empty($data['newSignupFlow'])) {
            $this->rules = array(
                'name' => 'required|min:3|regex:/(^[A-Za-z ]+$)+/',
                'mobile' => 'required|numeric|unique:users',
            );
        }

        $v = Validator :: make($data, $this->rules, $this->messages);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate password for model
     * @param array $data
     * @return boolean
     */
    public function validatepwd($data) {

        $v = Validator :: make($data, $this->pwdrules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate mobile no for model
     * @param array: data
     * @return boolean
     */
    public function validatemobile($data) {

        $v = Validator :: make($data, $this->mobilerules, $this->mobilemessages);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for call Request
     * @param array: data
     * @return boolean
     */
    public function validateCallRequest($data) {

        $v = Validator :: make($data, $this->rulesCallRequest);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 02 Feb 2015
     * @description : This function get user profile detail
     * @access public
     * @return  array 
     */
    public function userProfileDetail() {
        return $this->hasOne('UserProfile', 'user_id', 'user_id');
    }

    /**
     * @author : Shipra Agrawal
     * @created : 21 jan 2015
     * @description : This function get favorite doctor List by user id
     * @param  int $userid
     * @access public
     * @return  array 
     */
    public function favDroctorList($userid) {

        $favDrList = DB::table('users AS u')->where('drs.is_default', '=', 1)->where('df.is_approved', '=', 1)->where('ufd.patient', '=', $userid)->where('ufd.doctor', '!=', $userid)
                ->join('user_profile AS up', 'u.user_id', '=', 'up.user_id')
                ->join('doctor_profile AS df', 'df.user_id', '=', 'u.user_id')
                ->join('user_favorite_doctors AS ufd', 'ufd.doctor', '=', 'u.user_id')
                ->join('doctor_specialization_rel AS drs', 'drs.user_id', '=', 'u.user_id')
                ->join('specialization_master AS sm', 'sm.specialization_id', '=', 'drs.specialization_id')
                ->leftJoin('city_master AS cm', 'df.city_id', '=', 'cm.city_id')
                ->select('u.user_id AS user_id', 'u.name', 'df.online_status as is_online', 'sm.name AS specialization_name', DB::raw('round(df.total_experience) as total_experience'), 'df.per_min_charges', 'df.area', 'cm.name AS city_name', 'photo', 'ufd.updated_at as last_updated_on')
                ->get();

        return $favDrList;
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : Get list of block users.
     * @param int userid
     * @access public
     * @return array 
     */
    public function userBlockList($userid) {

        $blockList1 = DB::table('doctor_patient_rel As d')->where('d.blocked_by', '=', $userid)->where('d.doctor', '=', $userid)
                        ->join('users As u', 'u.user_id', '=', 'd.patient')
                        ->join('user_profile As p', 'p.user_id', '=', 'u.user_id')
                        ->select('u.user_id', 'u.name', 'photo')->get();

        $blockList2 = DB::table('doctor_patient_rel As d')
                        ->where('d.blocked_by', '=', $userid)
                        ->where('d.patient', '=', $userid)
                        ->join('users As u', 'u.user_id', '=', 'd.doctor')
                        ->join('user_profile As p', 'p.user_id', '=', 'u.user_id')
                        ->select('u.user_id', 'u.name', 'photo')->get();

        $blockList = array_merge($blockList2, $blockList1);

        return $blockList;
    }

//    /**
//     * @author : Shipra Agrawal
//     * @created : 02 Feb 2015
//     * @description : Get list of user setting.
//     * @param int userid
//     * @access public
//     * @return  array 
//     */
//    public function userSettingList($userid) {
//
//        $favDrList = DB::table('user_setting_rel AS us')->where('us.user_id', '=', $userid)
//                ->join('setting_master AS s', 's.setting_id', '=', 'us.setting')
//                ->join('users AS u', 'u.user_id', '=', 'us.user_id')
//                ->select('s.setting_id', 's.name')
//                ->get();
//
//        return $favDrList;
//    }

    public function getUserColumn($params = array()) {

        $query = DB::table('users As u')->where('u.user_id', '=', $params['user_id']);

        if ($params['columnName'] == 'name') {
            $query->select(DB::raw('CONCAT(u.salutation, " ", u.name) AS name'));
        } elseif ($params['columnName'] == 'mobile') {
            $query->select(DB::raw('mobile'));
        }

        return $query->get();
    }

    public function isUserExist($params = array()) {
        return $this->where('mobile', '=', $params['mobile'])->pluck('user_id');
    }

    public function createKccUser($params = array()) {

        $params['mobile'] = trim($params['mobile']);
        $params['name'] = !empty($params['name']) ? $params['name'] : $params['mobile'];
        $params['email'] = $params['mobile'] . '@konsultapp.com';

        $postedPWD = '123456';//trim($params['mobile']);
        $params['password'] = password_hash($postedPWD, PASSWORD_BCRYPT, array('cost' => 10));
        DB::beginTransaction();
        try {

            foreach ($params as $key => $value) {
                $this->$key = $value;
            }

            $this->save();

            $userProfile = new UserProfile();
            $userProfile->user_id = $this->user_id;
            $userProfile->save();

            DB::commit();
        } catch (Exception $ex) {
            print_r($ex);
            die;
            DB::rollback();
        }
    }

    public static function getCallTypeData($params = array()) {

        $params['locale'] = !empty($params['locale']) ? strtolower($params['locale']) : '';

        if (!empty($params['mobile'])) {
            $first2Chars = substr($params['mobile'], 0, 2);
            $first3Chars = substr($params['mobile'], 0, 3);
            $internationalCall = 0;
            if ($first2Chars != '91' && $first3Chars != '+91') {
                $internationalCall = 1;
            }
        } elseif (!empty($params['user_id'])) {

            $mobile = User::where('user_id', '=', $params['user_id'])->pluck('mobile');

            if (!empty($mobile)) {
                $first2Chars = substr($mobile, 0, 2);
                $first3Chars = substr($mobile, 0, 3);
                $internationalCall = 0;
                if ($first2Chars != '91' && $first3Chars != '+91') {
                    $internationalCall = 1;
                }
            } else {
                $internationalCall = 0;
            }
        } elseif (!empty($params['locale']) && $params['locale'] != 'in' && $params['locale'] != 'india') {
            $internationalCall = 1;
        } else {
            $internationalCall = 0;
        }

        $currencyFactor = Config::get('constants.INTERNATIONAL_CALL_CHARGE_CURRENCY_FACTOR');

        $data = array();
        $data['internationalCall'] = $internationalCall;
        $data['currency'] = ($internationalCall) ? 'USD' : 'INR';
        $data['currencySymbol'] = ($internationalCall) ? '$' : '₹';
        $data['surcharge'] = ($internationalCall) ? Config::get('constants.INTERNATIONAL_CALL_CHARGE_PER_MIN_RULES') : 1;
        $data['surcharge'] = (!empty($currencyFactor) && $currencyFactor > 0 && $internationalCall) ? ($data['surcharge'] / $currencyFactor) : $data['surcharge'];
        $data['international_per_min_rules'] = Config::get('constants.INTERNATIONAL_CALL_CHARGE_PER_MIN_RULES');
        $data['currencyFactor'] = $currencyFactor;

        return $data;
    }

    public static function getPhoto($params = array()) {

        $publicPath = URL::to('/') . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR;

        if (!empty($params['photo']) && filter_var($params['photo'], FILTER_VALIDATE_URL)) {
            return $params['photo'];
        } elseif (!empty($params['photo']) && $params['photo'] != '(null)') {
            $photoPath = $publicPath . $params['photo'];
        } else {
            $photoPath = $publicPath . (!empty($params['is_doctor']) ? 'doctor.png' : 'user_default.png');
        }

        return $photoPath;
    }

}
