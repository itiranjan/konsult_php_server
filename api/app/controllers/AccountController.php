<?php

class AccountController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool $checkAuthToken (To Check Auth token)
     * @return  array 
     */
    protected function checkAuth($checkAuthToken = TRUE) {

        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                } elseif ($result['config_status'] == 1) {
                    $this->verfiedAuth = true;
                    $this->authToken = $requestHeaders['AUTH_TOKEN'];
                }
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    public function userLogin() {
        try {

            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            if (!empty($data['username']) && !empty($data['password'])) {

                $user = User::where('email', '=', $data['username'])->first();
                $loginFaild = 0;
                if (count($user)) {

                    $encryptHelper = $this->helper->load('encrypt');
                    $isPWDMatched = $encryptHelper->validatePWD(array('dbPWD' => $user->password, 'postedPWD' => $data['password']));

                    if (!$isPWDMatched) {
                        $loginFaild = 1;
                    } else {

                        //DELETE AUTH-TOKEN ALSO
//                        User::where('user_id', '=', $user->user_id)->where('user_id', '=', $user->user_id)->update(array('token' => NULL));

                        $auth_token = $this->saveAuthToken($user->user_id);
                        $this->setHeader('RESPONSE_CODE', '1');
                        $this->setHeader('AUTH_TOKEN', $auth_token);

                        $userProfile = UserProfile::where('user_id', '=', $user->user_id)->first();

                        $result['status'] = 1;
                        $result['msg'] = Config::get('config_msg.LOGIN_SUCCESS');
                        $result['response']['user_id'] = $user->user_id;
                        $result['response']['name'] = trim($user->salutation . " " . $user->name);
                        $result['response']['email'] = $user->email;
                        $result['response']['mobile'] = $user->mobile;
                        $result['response']['is_doctor'] = $userProfile->is_doctor;
                        $result['response']['is_approved'] = 0;
                        $doc_exist = Doctor::where('user_id', '=', $user->user_id)->exists();
                        if ($doc_exist) {
                            $result['response']['is_approved'] = Doctor::where('user_id', '=', $user->user_id)->pluck('is_approved');
                        }
                        $is_online = Doctor::where('user_id', '=', $user->user_id)->pluck('online_status');
                        $result['response']['is_online'] = $is_online ? 1 : 0;
                        $result['response']['address'] = '';
                        $result['response']['photo'] = User::getPhoto(array('photo' => $userProfile->photo, 'is_doctor' => $userProfile->is_doctor));

                        $result['response']['login_paytm'] = $result['response']['login_citrus'] = -1;
                        $walletLogins = UserWallet::select('login', 'wallet_id')->where('user_id', '=', $user->user_id)->get();
                        foreach ($walletLogins as $walletLogin) {
                            if ($walletLogin->wallet_id == 1) {
                                $result['response']['login_paytm'] = $walletLogin->login;
                            } elseif ($walletLogin->wallet_id == 3) {
                                $result['response']['login_citrus'] = $walletLogin->login;
                            }
                        }
                    }
                } else {
                    $loginFaild = 1;
                }
            } else {
                $loginFaild = 1;
            }

            if ($loginFaild) {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1029;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.LOGIN_FAIL');
                $result['errors'] = $error;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 28 jan 2015
     * @description :  User Logout function is used for kill user session.This function can also be destory AUTH_TOKEN (AUTH_TOKEN added in header filed) ;
     * @access  public

     * @return array 
     */
    public function userLogout() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            if ($user_id) {
                DeviceInfo::where('user_id', '=', $user_id)->delete();

                //DELETE AUTH-TOKEN ALSO
//                User::where('user_id', '=', $user_id)->where('user_id', '=', $user_id)->update(array('token' => NULL));

                //FOR CITRUS ONLY NOT FOR PAYTM
                UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 3)->update(array('login' => 0));
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.LOGOUT_SUCCESS');
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1040;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.LOGOUT_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function userExist() {
        try {

            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            if (empty($data['email']) && empty($data['mobile'])) {
                $result['status'] = 0;
                $result['msg'] = "Missing Inputs";
                return Response::make($result, 200, $this->headers);
            }

            if ($data['email']) {
                $emailRow = User::select('user_id', 'salutation', 'name', 'password')->where('email', '=', $data['email'])->first();
                $result['emailExist'] = $emailRow->user_id ? 1 : 0;
                if ($result['emailExist']) {
                    $result['emailUser_salutation'] = $emailRow->salutation;
                    $result['emailUser_name'] = $emailRow->name;
                    $result['passwordExist'] = $emailRow->password ? 1 : 0;
                } else {
                    $result['passwordExist'] = 0;
                }
            }

            if ($data['mobile']) {
                $mobileRow = User::select('user_id', 'salutation', 'name', 'password')->where('mobile', '=', $data['mobile'])->first();
                $result['mobileExist'] = $mobileRow->user_id ? 1 : 0;
                if ($result['mobileExist']) {
                    $result['salutation'] = $mobileRow->salutation;
                    $result['name'] = $mobileRow->name;
                    $result['passwordExist'] = $mobileRow->password ? 1 : 0;
                } else {
                    $result['passwordExist'] = 0;
                }
            }

            $result['status'] = 1;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 19 jan 2015
     * @description :  In User social Login function user login using social input credentials. we get input parameter in array Format.This function can also be created AUTH_TOKEN (AUTH_TOKEN added in header filed) ;
     * @access  public
     * @return array 
     */
    public function userSocialLogin() {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $users = array();
            $userExist = 0;
            if (!empty($data['social_auth_token']) && !empty($data['social_network']) && !empty($data['social_username'])) {
                $user_id = SocialAccounts::whereRaw('social_login_id = \'' . $data['social_username'] . '\' && social_network = \'' . $data['social_network'] . '\'')->pluck('user_id');
                if ($user_id) {
                    SocialAccounts::whereRaw('social_login_id = \'' . $data['social_username'] . '\' && social_network = \'' . $data['social_network'] . '\'')->update(array('social_auth_token' => $data['social_auth_token']));
                    $users = User::with('userProfileDetail')->where('user_id', '=', $user_id)->get();
                    $userExist = 1;
                } else if (!empty($data['email'])) {
                    $users = User::with('userProfileDetail')->where('email', '=', $data['email'])->get();
                    if (count($users)) {
                        foreach ($users as $value) {
                            $socialdata['social_auth_token'] = $data['social_auth_token'];
                            $socialdata['social_network'] = $data['social_network'];
                            $socialdata['social_login_id'] = $data['social_username'];
                            $socialAccount = new SocialAccounts;
                            if ($socialAccount->validate($socialdata)) {
                                $socialAccount->user_id = $value['user_id'];
                                $socialAccount->social_network = $data['social_network'];
                                $socialAccount->social_login_id = $data['social_username'];
                                $socialAccount->social_auth_token = $data['social_auth_token'];
                                $socialAccount->save();
                            } else {
                                $this->setHeader('RESPONSE_CODE', '0');
                                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                                $result['status'] = 0;
                                $result['errors'] = $socialAccount->errors();
                                $this->setHeader('RESPONSE_CODE', 0);
                                return Response::make($result, 200, $this->headers);
                            }
                        }
                        $userExist = 1;
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1043;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.LOGIN_FAIL');
                        $result['errors'] = $error;
                        return Response::make($result, 200, $this->headers);
                    }
                }
                if ($userExist == 1) {
                    foreach ($users as $value) {
                        $auth_token = $this->saveAuthToken($value['user_id']);
                        $this->setHeader('RESPONSE_CODE', '1');
                        $this->setHeader('AUTH_TOKEN', $auth_token);

                        $result['status'] = 1;
                        $result['msg'] = Config::get('config_msg.LOGIN_SUCCESS');
                        $result['response']['user_id'] = $value['user_id'];
                        $result['response']['name'] = $value['name'];
                        $result['response']['email'] = $value['email'];
                        $result['response']['dob'] = '';
                        $result['response']['mobile'] = $value['mobile'];
                        $result['response']['is_doctor'] = $value['userProfileDetail']['is_doctor'];
                        $result['response']['is_approved'] = 0;
                        $doc_exist = Doctor::where('user_id', '=', $value['user_id'])->exists();
                        if ($doc_exist) {
                            $result['response']['is_approved'] = Doctor::where('user_id', '=', $value['user_id'])->pluck('is_approved');
                        }
                        $result['response']['address'] = Doctor::where('user_id', '=', $value['user_id'])->pluck('address');
                        $result['response']['photo'] = User::getPhoto(array('photo' => $value['userProfileDetail']['photo'], 'is_doctor' => $value['userProfileDetail']['is_doctor']));
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1029;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.LOGIN_FAIL');
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1029;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.LOGIN_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 29 jan 2015
     * @description : User can change own password by this function.we get input parameter in array Format and auth token in header field.
     * 
     * @access  public
     * @return array 
     */
    public function changePassword() {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $user = new User();
            if ($user->validatepwd($data)) {
                $user_id = User::whereRaw('(email= "' . $data['username'] . '" or mobile = "' . $data['username'] . '")')->pluck('user_id');
                if ($user_id) {
                    User::where('user_id', '=', $user_id)->update(array('password' => $data['password']));
                    $this->setHeader('RESPONSE_CODE', '1');
                    $this->setHeader('AUTH_TOKEN', $this->authToken);
                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.CHANGE_PASS_SUCCESS');
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1044;
                    $error = $this->getError($error);
                    $result['msg'] = Config::get('config_msg.CHANGE_PASS_FAIL');
                    $result['status'] = 0;
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.CHANGE_PASS_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 30 jan 2015
     * @description : User can change own email address by this function.we get input parameter in array Format and auth token in header field.
     * 
     * @access  public
     * @return array 
     */
    public function editEmail() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $emailpattern = "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/";
            if (empty($data['email']) || !preg_match($emailpattern, $data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1050;
                $error = $this->getError($error);
                $result['msg'] = Config::get('config_msg.CHANGE_EMAIL_REQ_FAIL');
                $result['status'] = 0;
                $result['errors'] = $error;
            } else {
                $user_exist = User::where('email', '=', $data['email'])->exists();
                if ($user_exist) {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1003;
                    $error = $this->getError($error);
                    $result['msg'] = Config::get('config_msg.CHANGE_EMAIL_REQ_FAIL');
                    $result['status'] = 0;
                    $result['errors'] = $error;
                } else {
                    $user_id = $this->getUserIdbyToken($this->authToken);
                    $stringHelper = $this->helper->load('string');
                    $varification_key = $stringHelper->random_string(20);
                    $oldemail = User::where('user_id', '=', $user_id)->pluck('email');
                    User::where('user_id', '=', $user_id)->update(array('varification_key' => $varification_key));

                    $encryptHelper = $this->helper->load('encrypt');
                    $activationData = array('newemail' => $data['email'],
                        'oldemail' => $oldemail,
                        'varification_key' => $varification_key);

                    $json_data = json_encode($activationData);
                    $encrypted_data = base64_encode($encryptHelper->mcEncrypt($json_data, Config::get('constants.PRIVATE_KEY')));

                    $activationLink = URL::to('/') . DIRECTORY_SEPARATOR . 'account' . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'verify' . DIRECTORY_SEPARATOR . urlencode($encrypted_data);

                    $this->setHeader('RESPONSE_CODE', '1');
                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.REMINDER_ACTIVATION_EMAIL');
                    $result['response']['link'] = $activationLink;
                }
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 30 jan 2015
     * @description : This function is used for comfirmed user email address .
      we get input parameter in array Format and auth token in header field.
     * @access public
     * @param string type [ $resetkey ]
     * @return array 
     */
    public function emailConfirmed($resetkey) {
        try {
            $params = new stdclass();
            $encryptHelper = $this->helper->load('encrypt');
            $encryptedText = base64_decode(rawurldecode($resetkey));
            $decryptedText = $encryptHelper->mcDecrypt($encryptedText, Config::get('constants.PRIVATE_KEY'));
            if ($decryptedText) {
                $getJsonRes = json_decode($decryptedText);
                $params = json_decode(json_encode($getJsonRes), FALSE);
                if (is_object($params)) {
                    $verifykey = User::where('varification_key', '=', $params->varification_key)->where('email', '=', $params->oldemail)->exists();
                    if ($verifykey) {
                        User::where('varification_key', '=', $params->varification_key)->where('email', '=', $params->oldemail)->update(array('email' => $params->newemail, 'varification_key', '=', ''));
                        $this->setHeader('RESPONSE_CODE', '1');
                        $result['status'] = 1;
                        $result['msg'] = Config::get('config_msg.EMAIL_UPDATED');
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1051;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.EMAIL_UPDATE_FAIL');
                        $result['errors'] = $error;
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1051;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.EMAIL_UPDATE_FAIL');
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1051;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.EMAIL_UPDATE_FAIL');
                $result['errors'] = $error;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 30 jan 2015
     * @description : This method used to update user mobile number
     * @access public
     * @param  string  type [ define verification type ]
     * @return  array 
     */
    public function editMobile() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);
            if ($user_id) {
                $user = new User();
                if ($user->validatemobile($data)) {
                    User::where('user_id', '=', $user_id)->update(array('mobile' => $data['mobile']));
                    $this->setHeader('RESPONSE_CODE', '1');
                    $result['status'] = 1;
                    $result['msg'] = Config::get('config_msg.MOBILE_NO_CHANGED');
                } else {
                    $result['msg'] = Config::get('config_msg.MOBILE_NO_UPDATE_FAIL');
                    $result['status'] = 0;
                    $result['errors'] = $user->errors();
                    $this->setHeader('RESPONSE_CODE', 0);
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1046;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.MOBILE_NO_UPDATE_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 14 mar 2015
     * @description : User can change own change password by this function.
     * 
     * @access  public
     * @return array 
     */
    public function forgotPassword() {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $user_detail = User::where('email', '=', $data['username'])->orwhere('mobile', '=', $data['username'])->first();
            if (empty($data['username']) || !$user_detail) {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1030;
                $error = $this->getError($error);
                $result['msg'] = Config::get('config_msg.CHANGE_FORGOT_PWD_FAIL');
                $result['status'] = 0;
                $result['errors'] = $error;
            } else {
                User::where('email', '=', $data['username'])->update(array('pwd_reset_status' => 1));
                $encryptHelper = $this->helper->load('encrypt');
                $activationData = array('varification_key' => Config::get('constants.varification_key') . '~' . $user_detail['user_id']);

                $json_data = json_encode($activationData);
                $encrypted_data = base64_encode($encryptHelper->mcEncrypt($json_data, Config::get('constants.PRIVATE_KEY')));
                $activationLink = URL::to('/') . '/account/reset/' . $encrypted_data;

                $emaildata['name'] = $user_detail['name'];
                $emaildata['link'] = $activationLink;
                $emaildata['logo'] = Config::get('constants.LOGO');
                $emailbody = View::make('user_reset_password')->with('data', $emaildata)->render();
                $mailArray = array();
                $mailArray['email'] = $user_detail['email'];
                $mailArray['subject'] = Config::get('config_msg.USER_RESET_PWD_SUBJECT');
                $mailArray['body'] = $emailbody;

                $communicationHelper = $this->helper->load('communication');
                $result['email_response'] = $communicationHelper->emailCommunication1($mailArray);

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.CHANGE_FORGOT_PWD');
                $result['response']['link'] = $activationLink;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 14 mar 2015
     * @description : This function is used for reset password.
     *
     * @access  public
     * @return array 
     */
    public function resetPassword($resetkey) {
        try {
            $data = Input::all();
            $params = new stdclass();
            $encryptHelper = $this->helper->load('encrypt');
            $encryptedText = base64_decode(rawurldecode($resetkey));
            $decryptedText = $encryptHelper->mcDecrypt($encryptedText, Config::get('constants.PRIVATE_KEY'));
            if ($decryptedText) {
                $getJsonRes = json_decode($decryptedText);
                $params = json_decode(json_encode($getJsonRes), FALSE);
                if (is_object($params)) {
                    $user_arr = explode('~', $params->varification_key);
                    $reset_status = User::where('user_id', '=', $user_arr[1])->pluck('pwd_reset_status');
                    if ($user_arr[0] == Config::get('constants.varification_key') && $reset_status == 1) {
                        $stringHelper = $this->helper->load('string');
                        $password = $stringHelper->random_string(4);
                        $data['password'] = $password;

                        $encryptHelper = $this->helper->load('encrypt');
                        $PWD = $encryptHelper->encryptPWD(array('PWD' => $password));

                        User::where('user_id', '=', $user_arr[1])->update(array('password' => $PWD, 'pwd_reset_status' => 0));
                        //TODO: Email Password to user
                        $this->setHeader('RESPONSE_CODE', '1');
                        $emaildata['password'] = $password;
                        $emaildata['logo'] = Config::get('constants.LOGO');
                        $emailbody = View::make('user_reset_pwd_success')->with('data', $emaildata)->render();
                        echo $emailbody;
                        die;
                        $result['status'] = 1;
                        $result['password'] = $password;
                        $result['msg'] = Config::get('config_msg.RESET_PWD');
                    } else {
                        $emaildata['logo'] = Config::get('constants.LOGO');
                        $emailbody = View::make('user_reset_pwd_fail')->with('data', $emaildata)->render();
                        echo $emailbody;
                        die;

                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1051;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.RESET_PWD_FAIL');
                        $result['errors'] = $error;
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1051;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.RESET_PWD_FAIL');
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1051;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.RESET_PWD_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
