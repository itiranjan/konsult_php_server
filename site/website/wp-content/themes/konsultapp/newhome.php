
<?php
global $starttimetoload, $starttimetoloadah; 
 $starttime = microtime(true);
$date = new DateTime();
//echo $date->getTimestamp();
$starttimetoload = $date->getTimestamp();
/**
 * Template Name: New Home Page
 * @author Sahil Ahlawat
 */
 
 get_header();
$endtime1 = microtime(true);
/* phone slider image size should be 180*320 */
?>

<div class="container-fluid mt22">
<div class="container torempadding"><div class="row">
<div class="col-sm-6 torempadding">
<style>
.lefthometop{
margin-top: 20%;
}
.sectionseperator{
	height:100px;
	width:100%;
}
    .speciallogo{
        margin-bottom:95px;
        margin-top:-150px;
    }
    @media only screen and (max-width: 768px)  {
       .speciallogo{
        display: none;
    } 
    }
    
</style>
<div class="lefthometop">
    <img class="speciallogo" width="65%" src="http://www.konsult.com/wp-content/uploads/2017/01/logo-01-1.png" />
<!--<h1>India's #1 Medical Tele-consultation Platform</h1>-->
<h1>Talk To Any Doctor Immediately</h1>
<!--
<h2>More than 100000 consultations done</h2>
-->
<ul class="uspul">
<li><i class="fa fa-check" aria-hidden="true"></i>
Find & select your doctor</li>
<li><i class="fa fa-check" aria-hidden="true"></i>
Talk to your doctor</li>
    <li><i class="fa fa-check" aria-hidden="true"></i>
Chat with your doctor</li>
<li><i class="fa fa-check" aria-hidden="true"></i>
Pay based on talk time</li>

</ul>
<div class="col-sm-12 playiosand">
<div class="col-sm-6 col-xs-6"><a href="https://play.google.com/store/apps/details?id=com.konsult"><img src="http://www.konsult.com/wp-content/uploads/2017/04/androidapp.png" alt="Download Konsult App for Android"/></a></div>
<div class="col-sm-6 col-xs-6 secondimagplay"><a href="https://itunes.apple.com/in/app/konsult-app/id1017239812"><img src="http://www.konsult.com/wp-content/uploads/2017/04/iosapp.png" alt="Download Konsult App for IOS"/></a></div>
</div>
</div></div>
<div class="sectionseperator container"></div>
<div class="col-sm-6 hidden-xs">
<div class="phoneback" style="background:transparent url('http://www.konsult.com/wp-content/uploads/2017/04/iphone-1.png') no-repeat center;">
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators 
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>
-->
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="http://www.konsult.com/wp-content/uploads/2017/04/Screenshot_01.png" alt="Choose your doctor" width="460" height="345">
      </div>

      <div class="item">
        <img src="http://www.konsult.com/wp-content/uploads/2017/04/Screenshot_02.png" alt="Choose your doctor" width="460" height="345">
      </div>
    
      <div class="item">
        <img src="http://www.konsult.com/wp-content/uploads/2017/04/Screenshot_03.png" alt="Check detailed Doctor profile" width="460" height="345">
      </div>

      <div class="item">
        <img src="http://www.konsult.com/wp-content/uploads/2017/04/Screenshot_04.png" alt="Get daily health tips" width="460" height="345">
      </div>
    </div>

    <!-- Left and right controls 
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>-->
  </div>
</div>
</div>
</div></div>
<div class="container-fluid torempadding"><div class="row sectioncolorhome sectionsecond">
<h1 class="shi">Why Konsult App?</h1>
<div class="borderbelowheadings"></div>
<!--<h3 class="subhi">Konsult App provides real-time medical consultations over the phone. Doctors are available round the
clock and one can connect to them without a prior booking or appointment. We let the user choose
from hundreds of qualified doctors based on their availability, specialty, review and years of
experience. There is also the added advantage of sharing medical records and test reports.</h3>
-->
    <style>
        ol.subhi{
            text-align: left;
        }
        .whytabs:hover{
            
         
            box-shadow: 0px 0px 10px 10px rgba(199,197,197,0.25);
    transform: scale(1.05,1.05);
        }
            .whytabs{
                    min-height: 315px;
              padding:20px;  
    background: white;
                    transition: all .6s cubic-bezier(0.165,.84,.44,1);
        }
    </style>
  <!--  <ol class="subhi">
       <li> <strong>CHOOSE YOUR DOCTOR:</strong> Konsult lets you search the doctor by name, speciality or medical symptoms. You can choose from thousands of doctors available on our platform. 
        </li>
<li><strong>IMMEDIATE CONSULTATION:</strong> With konsult, you don’t need any prior appointment as everything happens at real-time. If your doctor is online, you can avail teleconsultation immediately.
        </li>
<li><strong>SHARE MEDICAL REPORTS:</strong> Inbuilt chat feature which allows you to share medical records seamlessly with your doctor. Its completely secure and private.
        </li>
<li><strong>SAVE TIME & MONEY:</strong> Neither you need to travel to the hospital nor you have to wait in queues. Moreover, overall consultation charges would mostly be far below the OPD charges.
        </li>
<li><strong>SEAMLESS PAYMENT:</strong> We have secure integration with leading wallets like Paytm. All the transactions are automated and thus you do not need to click even a button to do so.
        </li>
<li><strong>REGULAR HEALTH TIPS:</strong> Get regular health tips and opinions on your mobile by medical experts from all across the country
    </li>
    </ol>-->
 <div class="col-sm-12">
 
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
     
         
         
         
         <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS04-2.png" alt="online doctor"/>
         <h3 class="subhi">Choose Your Doctor</h3><h4 class="subsubhi">Konsult lets you search the doctor by name, <a href="http://www.konsult.com/specialities" >speciality</a> or <a href="http://www.konsult.com/symptoms">medical symptoms</a>. Choose from thousands of available doctors.</h4></div>
     </div>
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
         <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS-3.png" alt="instantaneous consultation"/>
    <h3 class="subhi">Immediate Consultation</h3><h4 class="subsubhi">With konsult, you don’t need any prior appointment. If your doctor is online, you can avail tele-consultation immediately.</h4></div>
     </div>
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
         
         
          <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS05-5.png" alt="free chat"/>
     <h3 class="subhi">Share Medical Reports</h3><h4 class="subsubhi">Inbuilt chat feature which allows you to share medical records seamlessly with your doctor. Its completely secure and private.</h4></div>
     </div>
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
     <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS02-2.png" alt="talk to your doctor from home"/><h3 class="subhi">Save Time & Money</h3><h4 class="subsubhi">No need to travel to the hospital or to wait in queues. Tele-Consultation charges would mostly be far below the OPD charges.</h4></div>
     </div>
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
     
         
         
         <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS03-2.png" alt=" pay doctor fee using wallet"/>
         <h3 class="subhi">Seamless Payment</h3><h4 class="subsubhi">Secure integration with leading wallets like Paytm. All the transactions are completely secure and automated.</h4></div>
     </div>
 <div class="col-sm-4 col-xs-12 ">
     <div class="whytabs">
     <img src="http://www.konsult.com/wp-content/uploads/2017/05/ICONS01-1.png" alt="daily health tips"/><h3 class="subhi">Regular Health Tips</h3><h4 class="subsubhi">Get regular <a href="http://www.konsult.com/category/health-tips">health tips</a> and <a href="http://www.konsult.com/category/expert-opinion">opinions</a> on your mobile by medical experts from all across the country.</h4></div>
     </div>
 
 </div>
</div></div>
<div class="container-fluid torempadding"><div class="row sectionthird"><h1 class="shi">How It Works</h1>
<div class="borderbelowheadings"></div>
    <div class="col-sm-12 hiwch">
    <div class="col-sm-2 col-xs-12 hiwc"><div class="innerhe">
        <h3 class="subhi">Login</h3>
        <span class="hidden-xs glyphicon glyphicon-log-in" aria-hidden="true"></span><h4 class="subsubhi">Login to application and e-wallet by only providing your name and number</h4></div></div>
    <div class="col-sm-2 col-xs-12 hiwc"><div class="innerhe">
        <h3 class="subhi">Select Doctor</h3>
        <span class="hidden-xs glyphicon glyphicon-search" aria-hidden="true"></span><h4 class="subsubhi">Search & select the doctor you want to consult</h4></div></div>
    <div class="col-sm-2 col-xs-12 hiwc"><div class="innerhe">
        <h3 class="subhi">Receive Call</h3>
        <span class="hidden-xs glyphicon glyphicon-phone" aria-hidden="true"></span><h4 class="subsubhi">Konsult instantaneously bridges the call to your doctor. Receive the call to get consultation</h4></div></div>
    <div class="col-sm-2 col-xs-12 hiwc"><div class="innerhe">
        <h3 class="subhi">Auto Payment</h3>
        <span class="hidden-xs glyphicon glyphicon-credit-card" aria-hidden="true"></span><h4 class="subsubhi"> Consultation charge auto-debited from e-wallet based on consultation time</h4></div></div>
    <div class="col-sm-2 col-xs-12 hiwc"><div class="innerhe">
        <h3 class="subhi">Use free chat</h3>
        <span class="hidden-xs glyphicon glyphicon-comment" aria-hidden="true"></span><h4 class="subsubhi">Share medical reports with your doctor through inbuilt free chat
</h4></div></div>
        
    </div>
    <!--<a class="btnviewmore" href="#">Start Planing</a>-->
<!--<img class="img-responsiveee" src="http://www.konsult.com/wp-content/uploads/2017/04/roadflowchart.jpg" />--></div></div>
<div class="container-fluid recentarticleshome torempadding"><div class="row sectioncolorhome"><h1 class="shi">Recent Articles</h1>
<div class="borderbelowheadings"></div>
<div class="col-sm-12">
<?php

global $post;
$args = array( 'posts_per_page' => 3,
    'category'         => 195);

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
<?php
$content= get_the_title();
  $result = substr($content, 0, 24);
  $result = $content;
  // echo $result;
  ?>
  
<div class="col-sm-4"><div class="ant"><a href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(get_the_ID(), 'thumbnail'); ?>
    <h2><?php echo $result; ?></h2>
<div class="borderbelowimages"></div>
</a>
    
    <h3>Published By <b><?php the_field('doctors_name'); ?></b></h3></div></div>
<?php endforeach; 
wp_reset_postdata();?>

</div>
<a class="btnviewmore" href="http://www.konsult.com/category/expert-opinion/">View All</a>
</div>
</div>
<div class="container lastsection"><div class="row "><h1 class="shi">In News</h1>
<div class="borderbelowheadings"></div>
<div class="col-sm-12  "  id="" >
<!--<div class="col-sm-12 carousel slide"  id="myCarousel1" data-ride="carousel">
 <div class="carousel-inner">-->
  <!--<div class="item active">-->
<div class="col-sm-2 col-xs-6"><a href="http://bwdisrupt.businessworld.in/article/Konsult-Aims-to-Monetize-60-of-All-Medical-Consultations/20-03-2017-114747/"><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/04/logoicon1.png" /></a></div>
      
<div class="col-sm-2 col-xs-6"><a href="http://www.btvi.in/videos/watch/21806/konsult--talk-to-your-doctor"><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/04/logoicon2.png" /></a></div>
      
<div class="col-sm-2 col-xs-6"><a href="http://economictimes.indiatimes.com/small-biz/startups/how-a-bunch-of-healthcare-startups-is-tapping-unserved-niches/articleshow/57949369.cms"><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/04/logoicon3.png" /></a></div>
      
<!--</div>
<div class="item ">
-->
<div class="col-sm-2 col-xs-6"><a href="https://www.vccircle.com/exclusive-health-tech-startup-konsult-raises-seed-funding/?utm_source=twitter&utm_medium=social&utm_campaign=Exclusive%3A+Health-tech+startup+Konsult+raises+seed+funding
" ><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/05/Capture.png" /></a></div>
    
<div class="col-sm-2 col-xs-6"><a href="http://www.thehindubusinessline.com/companies/for-medical-tourists-cash-ban-a-bitter-pill-to-swallow/article9420317.ece" ><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/05/logo.png" /></a></div>
    
<div class="col-sm-2 col-xs-6"><a href="https://yourstory.com/2017/04/konsult-startup-app/"><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/04/logoicon4.png" /></a></div>
<!--
</div>
<div class="item ">
<div class="col-sm-4 col-xs-4"><a href="https://mybigplunge.com/opinion/healthcare-sector-budget-announcements-review-anshul-mittal-konsult/" ><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/05/logo-nav-7.png" /></a></div>
<div class="col-sm-4 col-xs-4"><a href="http://www.siliconindia.com/magazine-articles-in/Online_Consultation_The_Gen_X_of_Medical_Consultation-ISOD331058580.html" ><img class="img-responsive" src="http://www.konsult.com/wp-content/uploads/2017/05/Y6FROvp4Q3.gif" /></a></div>
</div>
</div>-->
 <!-- Left and right controls -->
 <!-- <a class="left carousel-control" href="#myCarousel1" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel1" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>-->
</div>
    
    
    </div></div>
</div>
<style>
    .whytabs a{
        color:#24cdd9;
    }
    @media screen and (max-width: 768px) {
    .hiwc{
        width: 100% !important;
    }
        .whytabs {
    min-height: 225px;
   
    margin-bottom: 20px;
}
}
    .hiwch{
        background: url(http://www.konsult.com/wp-content/uploads/2017/07/linedotted.png) no-repeat scroll center;
        margin-top: -11%;
    }
    .hiwc .subsubhi {
    font-size: 16px;
        
    }
    .hiwc .subhi {
    font-size: 24px;
        font-weight: bold;
}
    .hiwc{
        width: 20% ;
        margin-top:9%;
        
    }
    .hiwc .glyphicon
    {
        color: #24cdd9;
        padding:50px;
        font-size:40px;
    background-color: #fff;
    border: 1px solid #fff;
    border-radius: 50%;
    box-shadow: 1px 1px 10px 5px #f5f5f5;
   /* height: 130px; */
    margin: 0 auto;
    position: relative;
    /* width: 130px; */
    }
    .mt22{
        
        padding:0px !important;
    }
.phoneback .carousel{
	/*padding:  31%;
	padding: 12% 25% 28% 25%;*/
	padding: 9% 31% 0% 31%;
margin: 0 auto;
}
.phoneback{
background-size: auto 100% !important;
}
.lefthometop h1, .lefthometop h2{
	margin-top: 0px;
margin-bottom: 0px;
}
.lefthometop h1 {
	font-size: 35px;
}
.lefthometop h1,.lefthometop h2, .lefthometop ul {
	margin-left: 30px;
}
.lefthometop{
padding: 5% 0% 0% 0%;
}
.playiosand img{
	width: 80%;
}
.secondimagplay{
	margin-left: -12%;
}
.playiosand{
	margin-top: 45px;
}
.sectioncolorhome{
background: #f5f9fc;
text-align: center;
padding: 4% 10% 4% 10%;
}
.shi {
    font-size: 2.857em ;
    text-transform: capitalize;
	//border-bottom: 1px solid #e872a6;
}
.subhi {
    font-size: 24px;
}
.subsubhi {
    font-size: 16px;
    font-weight: 400;
    line-height: 1.6;
}
.sectioncolorhome .col-sm-4 .subhi{
	
	font-weight: bold;
}
.sectioncolorhome .col-sm-6 img{
	width: 35%;
}
.sectionsecond .col-sm-4 img{
	width:30%;
}
.sectioncolorhome .col-sm-4{
	padding: 10px 10px;
	
}
.sectionthird {
    text-align: center;
    width: 80%;
    margin: 0 auto;
    margin-top: 25px;
    margin-bottom: 50px;
}
.recentarticleshome .row .col-sm-4,.recentarticleshome .row .col-sm-4 img{
	 -webkit-transition: all 1s ease; /* Safari */
    transition: all 1s ease;
}
.recentarticleshome .row .col-sm-4 img{
	width: 100%;
}
    .recentarticleshome .row .col-sm-4{
        
        padding: 0px 25px;
    }
    .recentarticleshome .row .col-sm-4 .ant{
        
        
          
                  
              padding:40px;  
    background: white;
                    transition: all .6s cubic-bezier(0.165,.84,.44,1);
       
        
    }
.recentarticleshome .row .col-sm-4 .ant:hover{

	/*
box-shadow: 6px 2px 14px;

box-shadow: 0px 1px 6px -1px;*/
  
            box-shadow: 0px 0px 10px 10px rgba(199,197,197,0.25);
    transform: scale(1.05,1.05);
      
          
}
.recentarticleshome .row{}
.recentarticleshome .row h3{
	font-size: 12px;
	text-align:left;
}
.recentarticleshome .row h2{
	font-size: 14px;
}
.recentarticleshome .row .col-sm-12{
	margin-bottom: 45px;
}
.recentarticleshome .row a:hover{
	text-decoration: none;
}
.recentarticleshome .row .col-sm-4 a{
	color: rgba(0,0,0,.7);
}
    .btnviewmore:hover{
		background:#e872a6;
		color:white;
	}
    .btnviewmore{
		background:#24cdd9;
		color:white;
		padding: 10px 35px;
	}
	.lastsection img{
		    width: 125%;
    max-width: 130%;
	}
	.lastsection{
		text-align:center;
		margin-top: 20px;
margin-bottom: 30px;
	}
	.borderbelowheadings{
		border-bottom: 2px solid #e872a6;
		height:10px;
		width:120px;
		margin:0 auto;
		margin-bottom: 25px;
	}
	.borderbelowimages{
		border-bottom: 3px solid #e872a6;
height: 10px;
width: 120px;
margin-left: 0px;
margin-top: 5px;
margin-bottom: -7px;
	}
	.phoneback{
	 height: 520px;
 }
	@media only screen and (max-width: 667px) {
		.subsubhi {
    font-size: 16px;
}
		.lastsection img {
    width: 100%;
}
		.sectionthird img {
    max-width: 150%;
    height: auto;
    margin-left: -20% !important;
}
		.sectionsecond .col-xs-6 {
    width: 50%;
    max-height: 125px;
    margin-bottom: 25px;
}
 .phoneback{
	 height: 200px;
 }
 .lefthometop li {
    font-size: 16px;
 }
        h1{
    font-size: 26px !important;         
        }
 .subhi {
    font-size: 24px;
}
.sectioncolorhome .col-sm-4 {
    padding: 0px;
}
.borderbelowimages {
    border-bottom: 3px solid rgba(0,0,0,.2);
    height: 10px;
    width: 35px;
    margin-left: 0px;
}
.recentarticleshome .row h2 {
    font-size: 16px;
    font-weight: bold;
    text-align: left;
}
.recentarticleshome .row h3 {
    font-size: 9px;
}
.sectioncolorhome .col-sm-4 {
    padding: 0px;
}
.torempadding{
	padding:0px;
	margin-bottom: 10px;
}
.mt22{
	padding:0px !important;
	margin:0px;
	max-width: none !important;
}
}
@media only screen and (max-width: 1240px) and (min-width: 667px)  {
 .phoneback{
	 height: 308px;
 }
 .sectioncolorhome .col-sm-4 {
    padding: 0;
}
}

.recentarticleshome .row h2 {
    font-size: 14px;
    font-weight: bold;
    text-align: left;
}
/*
.sectionsecond .col-sm-4:hover img{
-webkit-animation:spin 1s ease-in-out; // No more infinite
    -moz-animation:spin 1s linear;
    animation:spin 1s linear;
	} */
@-moz-keyframes spin {
    100% { -webkit-transform: rotate(10deg); transform:rotate(10deg); }
}
@-webkit-keyframes spin {
    100% { -webkit-transform: rotate(10deg); transform:rotate(10deg); }
}
@keyframes spin { 
    100% { -webkit-transform: rotate(10deg); transform:rotate(10deg); } 
}
</style>
<?php
$endtime2 = microtime(true);
get_footer();
 $starttimetoloadah = $date->getTimestamp();


$endtime = microtime(true);
 //$timetakebyheader = $starttimetoloadah - $starttimetoload;
 $timetakebyheader  =  $endtime - $starttime;
 $timetakebyheader1 =  $endtime1 - $starttime;
 $timetakebyheader3 =  $endtime - $endtime2;
 $timetakebyheader2 =  $timetakebyheader - $timetakebyheader1;
 $timetakebyheader4 =  $timetakebyheader3 + $timetakebyheader1;
 $timetakebyheader5 =  $timetakebyheader - $timetakebyheader4;
//printf("Page loaded in %f seconds", $endtime - $starttime );
?>
<script>
console.log("<?php echo 'Time Taken By Footer :'.$timetakebyheader5; ?>");
console.log("<?php echo 'Time Taken By Footer :'.$timetakebyheader3; ?>");
console.log("<?php echo 'Time Taken By Header :'.$timetakebyheader1; ?>");
console.log("<?php echo 'Time Take Without Header :'.$timetakebyheader2; ?>");
console.log("<?php echo 'Time Taken By Whole Page :'.$timetakebyheader; ?>");
console.log("<?php echo 'Time Taken By Header and Footer Only :'.$timetakebyheader4; ?>");
console.log("<?php echo 'Time Taken By Whole Page Without Header and Footer Only :'.$timetakebyheader5; ?>");
</script>