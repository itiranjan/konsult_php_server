<?php

class Login_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function login() {

        //getting user input
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $url = APIURL . 'account/login';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'API_KEY:andapikey', 'APP_VERSION:1.0', 'CONFIG_VERSION:1.0'));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(array("username" => $username, "password" => $password)));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        $decodeOutput = json_decode($output);

        $userId = $decodeOutput->response->user_id;
        $emailId = $decodeOutput->response->email;
        $name = $decodeOutput->response->name;

        if (empty($userId) || empty($emailId)) {
            return false;
        }

        $sql = "SELECT * FROM admin_users WHERE user_id = $userId";

        $query = $this->db->query($sql);

        if ($query->num_rows() == 1) {
            $row = $query->row();

            $sqlPermission = "SELECT permissions.name, admin_permissions.permission_id FROM permissions LEFT JOIN admin_permissions ON permissions.permission_id = admin_permissions.permission_id AND admin_id = $row->admin_id";

            $queryPermissions = $this->db->query($sqlPermission);

            $permissionsArray = array();
            foreach ($queryPermissions->result() as $queryPermission) {
                $permissionsArray[$queryPermission->name] = empty($queryPermission->permission_id) ? 0 : 1;
            }

            $data = array(
                'userid' => $row->admin_id,
                'name' => $name,
                'username' => $emailId,
                'validated' => true,
                'permissions' => $permissionsArray,
            );
            $this->load->library('session');
            $this->session->set_userdata($data);

            return true;
        }
        return false;
    }

}
