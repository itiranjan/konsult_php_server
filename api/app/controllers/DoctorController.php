<?php

class DoctorController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {

        $requestHeaders = apache_request_headers();
        if ($requestHeaders ['API_KEY'] && $requestHeaders ['APP_VERSION'] && $requestHeaders ['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders ['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders ['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders ['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders ['API_KEY']) {
                    $result ['config_status'] = - 1;
                    $result ['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result ['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders ['AUTH_TOKEN'];
            }
        } else {
            $result ['config_status'] = - 1;
            $result ['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    public function docListing() {

        try {
            $requestHeaders = apache_request_headers();
            $checkAuthToken = isset($requestHeaders['AUTH_TOKEN']) ? TRUE : FALSE;
            $result = $this->checkAuth($checkAuthToken);

            if ($result ['config_status'] == - 1) {
                return ($result);
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $cond = Input::all();

            //CHECK FOR INTERNATIONAL CALL
            $loggedInUserId = $this->getUserIdbyToken($this->authToken);
            $locale = isset($cond['locale']) ? $cond['locale'] : NULL;
            $callTypeData = User::getCallTypeData(array('user_id' => $loggedInUserId, 'locale' => $locale));

            $queryString = ' ';
            if (!empty($requestHeaders['AUTH_TOKEN'])) {
                $user_id = $this->getUserIdbyToken($requestHeaders['AUTH_TOKEN']);
                if (!empty($user_id)) {
                    $isClinic = UserProfile::whereRaw('user_id = ' . $user_id)->pluck('clinic_type');
                    if ($isClinic) {
                        $queryString = " AND (b.list_category = $isClinic OR b.list_category = 3) ";
                    }
                }
            }

            if (!empty($cond['docName'])) {
                $docName = str_replace("'", "", $cond['docName']);
                if (!empty($docName)) {
                    $queryString .= " AND a.name like '%" . $docName . "%'";
                }
            }

            $addSalutation = $cond['add_salutation'];

            $countquery = 'select count(a.user_id) as count from users a,doctor_profile b,user_profile c,doctor_specialization_rel ds, doctor_workplace dw where (a.user_id=b.user_id) AND (a.user_id=c.user_id) AND (c.user_id=ds.user_id) AND (dw.user_id = b.user_id) AND c.is_doctor = 1 AND b.is_approved =1' . $queryString;

            $query_condition = 'select (' . $countquery . ') as totcount,a.user_id,a.mobile,a.name,a.salutation,a.email,c.photo,b.per_min_charges,b.total_experience,b.area,b.city_id,ds.specialization_id,b.online_status,b.area from users a,doctor_profile b,user_profile c,doctor_specialization_rel ds, doctor_workplace dw where (a.user_id=b.user_id) AND (a.user_id=c.user_id) AND (c.user_id=ds.user_id)  AND (dw.user_id = b.user_id) AND c.is_doctor = 1 AND b.is_approved =1' . $queryString;

            $city_query_condition = 'select b.city_id from users a,doctor_profile b,user_profile c,doctor_specialization_rel ds, doctor_workplace dw where (a.user_id=b.user_id) and (a.user_id=c.user_id) AND (c.user_id=ds.user_id)  AND (dw.user_id = b.user_id) AND c.is_doctor = 1 AND b.is_approved =1' . $queryString;

            $start = $flag = 0;
            $page = 1;
            if (!empty($cond['city'])) {
                $city = $cond['city'];
                $query_condition .= " AND b.city_id IN($city)";
                $city_query_condition .= " AND b.city_id IN($city)";
                $countquery.= " AND b.city_id IN($city)";
                $flag = 1;
            }

            if (!empty($cond['hospital_id'])) {
                $hospital_id = $cond['hospital_id'];
                $query_condition .= " AND dw.hospital_id = $hospital_id";
                $city_query_condition .= " AND dw.hospital_id = $hospital_id";
                $countquery.= " AND dw.hospital_id = $hospital_id";
            }

            if (!empty($cond['specialization'])) {
                $specialization = $cond['specialization'];
                $specializationModel = new Specialization();
                $specialization = $specializationModel->getSubSpecializations(array('specializationIds' => $specialization));

                $query_condition .= " AND ds.specialization_id IN($specialization)";
                $city_query_condition .= " AND ds.specialization_id IN($specialization)";
                $countquery.=" AND ds.specialization_id IN($specialization)";
                $flag = 1;
            }

            if (!empty($cond['q'])) {
                $name = $cond['q'];
                $query_condition .= " AND name like '%" . $name . "%'";
                $city_query_condition .= " AND name like '%" . $name . "%'";
                $countquery.=" AND name like '%" . $name . "%'";
                $flag = 1;
            }

            $query_condition .= " GROUP BY a.user_id";
            $city_query_condition .= " GROUP BY a.user_id";

            if (!empty($cond['sort'])) {
                $sorting = $cond['sort'];
                if ($sorting == 'name,asc') {
                    $sort = 'order by weightage DESC, name asc';
                } elseif ($sorting == 'name,desc') {
                    $sort = 'order by weightage DESC, name desc';
                }
            } else {
                $sort = 'order by online_status desc, weightage DESC, name asc';
            }

            $query_condition = $query_condition . " " . $sort;

            if (!empty($cond['page'])) {
                $page = $cond['page'];
                $start = ($page == "1") ? 0 : (($page * 10) - 10);
                $end = 10;
            }

            $query_condition = $query_condition . ' limit ' . $start . ',' . $end;
            $countquery = $countquery . ' limit ' . $start . ',' . $end;

            $doclist = $cityData = Array();

            $cityQueryDatas = DB::select($city_query_condition);
            foreach ($cityQueryDatas as $cityQueryData) {
                $cityMaster = new City();
                $cityData[$cityQueryData->city_id] = $cityMaster->getCityName($cityQueryData->city_id);
            }

            $totcountquery = DB::select($countquery);
            foreach ($totcountquery as $totc) {
                $count = $totc->count;
            }

            $doctorquery = DB::select($query_condition);
            foreach ($doctorquery as $doct) {
                $numFound = $doct->totcount;
                $dbuserid = $doct->user_id;
                $response1 ['user_id'] = $doct->user_id;
                if (empty($doct->salutation) || empty($addSalutation)) {
                    $response1 ['name'] = $doct->name;
                } else {
                    $response1 ['name'] = trim($doct->salutation . " " . $doct->name);
                }
                $response1 ['email'] = $doct->email;
                $response1 ['mobile'] = $doct->mobile;
                $response1 ['photo'] = User::getPhoto(array('photo' => $doct->photo, 'is_doctor' => 1));
                $response1 ['is_online'] = $doct->online_status;
                $requestHeaders = apache_request_headers();
                $authToken = $requestHeaders ['AUTH_TOKEN'];
                if (!empty($authToken)) {
                    $patient = $this->getUserIdbyToken($authToken);
                    $addedAsFavourite = FavoriteDoctor::where('doctor', '=', $dbuserid)->where('patient', '=', $patient)->exists();
                    $response1 ['added_as_favourite'] = $addedAsFavourite ? 1 : 0;
                } else {
                    $response1 ['added_as_favourite'] = 2;
                }

                $qulilist = array();
                $univerlinequery = DB::select("select college,degree from doctor_qualifications where user_id='$dbuserid'");
                foreach ($univerlinequery as $doctuni) {
                    $university = $doctuni->college;
                    $degreeid = $doctuni->degree;

                    if (!empty($degreeid)) {
                        $deglinequery = DB::select("select name from degree_master where degree_id='$degreeid'");
                        foreach ($deglinequery as $deg) {
                            $degree = $deg->name;
                        }
                    }

                    $response ['degree'] = $degree;
                    $response ['university'] = !empty($university) ? $university : "";

                    $json_response = json_encode($response);
                    $quali_decode = json_decode($json_response);
                    array_push($qulilist, $quali_decode);
                }

                $specilinequery = DB::select("select specialization_id from doctor_specialization_rel where user_id='$dbuserid'");
                foreach ($specilinequery as $doctspec) {
                    if (!empty($doctspec->specialization_id)) {
                        $specilinequery = DB::select("select name from specialization_master where specialization_id='$doctspec->specialization_id'");
                        foreach ($specilinequery as $doctspeci) {
                            $specialization = $doctspeci->name;
                        }
                    }
                }

                $cityMaster = new City();
                $response1['city_name'] = $cityMaster->getCityName($doct->city_id);
                $expQuery = '';
                if (!empty($cond['hospital_id'])) {
                    $expQuery = " AND dw.hospital_id = " . $cond['hospital_id'];
                }

                $experience = DB::select("select hm.name, hm.address from doctor_workplace AS dw JOIN hospital_master AS hm ON dw.hospital_id = hm.hospital_id where dw.user_id='$dbuserid' $expQuery");
                foreach ($experience as $doctexp) {
                    $response1 ['address'] = empty($doctexp->address) ? '' : $doctexp->address;
                    $response1 ['hospital_name'] = empty($doctexp->name) ? '' : $doctexp->name;
                }
                $response1 ['qualification'] = $qulilist;
                $response1 ['area'] = $doct->area;
                $response1['currency'] = $callTypeData['currency'];
                $response1['currencySymbol'] = $callTypeData['currencySymbol'];
                $response1 ['per_min_charges'] = round($doct->per_min_charges * $callTypeData['surcharge'], 2);
                $response1 ['per_min_charges'] = strval($response1 ['per_min_charges']);
                $response1 ['fix_charges_per_call'] = $response1 ['per_min_charges'] * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                $response1 ['total_experience'] = $doct->total_experience;
                $response1 ['specialization_name'] = $specialization;

                if (!empty($doct->user_id) && !empty($user_id)) {

                    $response1['is_allow_new_user'] = Doctor::where('user_id', '=', $doct->user_id)->pluck('allow_new_user');

                    $relationExist = DoctorPatientRelation::whereRaw('(doctor = ' . $doct->user_id . ' and patient = ' . $user_id . ') or (patient = ' . $doct->user_id . ' and doctor = ' . $user_id . ')')->first();

                    if ($relationExist) {
                        $response1['is_friend'] = $relationExist['is_approved'];
                    } else {
                        $response1['is_friend'] = 0;
                    }
                } else {
                    $response1['is_allow_new_user'] = $response1['is_friend'] = 1;
                }

                $json_response1 = json_encode($response1);
                $array_decode = json_decode($json_response1);
                array_push($doclist, $array_decode);
            }

            $cityResponse = $cityList = array();
            foreach ($cityData as $key => $value) {
                if (empty($value)) {
                    continue;
                }
                $cityResponse['city_id'] = $key;
                $cityResponse['city_name'] = $value;

                $encodedCityResponse = json_encode($cityResponse);
                $decodeCityResponse = json_decode($encodedCityResponse);
                array_push($cityList, $decodeCityResponse);
            }

            $searcharr = Array();
            if ($flag === 0) {
                $searcharr ['city'] = City::select('city_id as id', 'name')->orderBy('name')->get();
                $searcharr ['specialization'] = Specialization::select('specialization_id as id', 'name')->orderBy('name')->get();
                $searcharr ['experience'] [0] ["id"] = "0-2";
                $searcharr ['experience'] [0] ["name"] = "< 2 Years";
                $searcharr ['experience'] [1] ["id"] = "2-5";
                $searcharr ['experience'] [1] ["name"] = "2 Years - 5 Years";
                $searcharr ['experience'] [2] ["id"] = "5-10";
                $searcharr ['experience'] [2] ["name"] = "5 Years - 10 Years";
                $searcharr ['experience'] [3] ["id"] = "10-20";
                $searcharr ['experience'] [3] ["name"] = "10 Years - 20 Years";
                $searcharr ['experience'] [4] ["id"] = "20-40";
                $searcharr ['experience'] [4] ["name"] = "20 Years - 40 Years";
                $searcharr ['experience'] [4] ["id"] = "40-100";
                $searcharr ['experience'] [4] ["name"] = "40 Years - 100 Years";
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result ['status'] = 1;
            $result ['total_count'] = $numFound;
            $result ['count'] = $count;
            if ($count == null) {
                $result ['count'] = $numFound;
            }
            if ($result ['total_count'] == 0) {
                $result ['msg'] = Config::get('config_msg.RECORD_NOT_FOUND');
            } else {
                $result ['msg'] = Config::get('config_msg.DR_LIST');
            }

            $result ['page'] = $page;
            $result ['response'] ['list'] = $doclist;
            $result ['response'] ['cityList'] = $cityList;
            if ($flag === 0) {
                $result ['response'] ['facets'] = $searcharr;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function docSearching() {

        try {
            $checkAuthToken = FALSE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result ['config_status'] == -1) {
                return ($result);
            }
            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $cond = Input::all();

            $queryString = ' ';
            $requestHeaders = apache_request_headers();
            if (!empty($requestHeaders['AUTH_TOKEN'])) {
                $user_id = $this->getUserIdbyToken($requestHeaders['AUTH_TOKEN']);
                if (!empty($user_id)) {
                    $isClinic = UserProfile::whereRaw('user_id = ' . $user_id)->pluck('clinic_type');
                    if ($isClinic) {
                        $queryString = " AND (b.list_category = $isClinic OR b.list_category = 3) ";
                    }
                }
            }

            $countquery = 'select count(a.user_id) as count from users a,doctor_profile b,user_profile c,doctor_specialization_rel ds where (a.user_id=b.user_id) and (a.user_id=c.user_id) and (c.user_id=ds.user_id) AND c.is_doctor = 1 AND b.is_approved =1' . $queryString;

            $query_condition = 'select (' . $countquery . ') as totcount,a.user_id,a.name,a.salutation,a.email,c.photo,b.per_min_charges,b.total_experience,b.city_id,ds.specialization_id,b.online_status,b.area from users a,doctor_profile b,user_profile c,doctor_specialization_rel ds where (a.user_id=b.user_id) and (a.user_id=c.user_id) and (c.user_id=ds.user_id) AND c.is_doctor = 1 AND b.is_approved =1' . $queryString;

            $specializationlist = $specelist = array();
            $query_conditionup = $query_condition;

            $specName = NULL;
            if (!empty($cond['specName'])) {
                $specName = $cond['specName'];
                if ($specName == '*') {
                    $sepcesql = "SELECT sp.name, sp.specialization_id FROM specialization_master AS sp LEFT JOIN doctor_specialization_rel AS dsr ON sp.specialization_id = dsr.specialization_id LEFT JOIN doctor_profile AS dp ON dp.user_id = dsr.user_id LEFT JOIN users AS u ON dp.user_id = u.user_id WHERE dp.online_status = 1 AND dp.is_approved = 1 GROUP BY (sp.specialization_id) ORDER BY sp.name;";

                    $specializationquery = DB::select($sepcesql);
                    foreach ($specializationquery as $doctuni) {
                        if (!in_array($doctuni->specialization_id, $specelist)) {
                            $res ['specialization_name'] = $doctuni->name;
                            $res ['specialization_id'] = $doctuni->specialization_id;
                            $res ['id'] = $doctuni->specialization_id;

                            $json_resp = json_encode($res);
                            $speclist = json_decode($json_resp);
                            array_push($specializationlist, $speclist);
                            array_push($specelist, $doctuni->specialization_id);
                        }
                    }

                    $this->setHeader('AUTH_TOKEN', $this->authToken);
                    $this->setHeader('RESPONSE_CODE', '1');
                    $result ['status'] = 1;
                    $result ['msg'] = 'All Specializations';
                    $result ['response'] ['speclist'] = $specializationlist;

                    return Response::make($result, 200, $this->headers);
                } else {
                    $sepcesql = "SELECT sp.name, sp.specialization_id FROM specialization_master AS sp LEFT JOIN doctor_specialization_rel AS dsr ON sp.specialization_id = dsr.specialization_id LEFT JOIN doctor_profile AS dp ON dp.user_id = dsr.user_id LEFT JOIN users AS u ON dp.user_id = u.user_id WHERE dp.online_status = 1 AND dp.is_approved = 1 AND sp.name like '%" . $specName . "%'GROUP BY (sp.specialization_id) ORDER BY sp.name;";

                    $specializationquery = DB::select($sepcesql);
                    foreach ($specializationquery as $doctuni) {
                        if (!in_array($doctuni->specialization_id, $specelist)) {
                            $res ['specialization_name'] = $doctuni->name;
                            $res ['specialization_id'] = $doctuni->specialization_id;
                            $res ['id'] = $doctuni->specialization_id;

                            $json_resp = json_encode($res);
                            $speclist = json_decode($json_resp);
                            array_push($specializationlist, $speclist);
                            array_push($specelist, $doctuni->specialization_id);
                        }
                    }

                    $speceidlist = implode(",", $specelist);
                    if (!empty($speceidlist)) {
                        $query_conditionsp = " and ds.specialization_id IN($speceidlist)";
                    }
                }

                $query_condition .= $query_conditionsp;
            }

            if (!empty($cond['docName']) && $specName != '*') {
                $docName = $cond['docName'];
                $query_condition = $query_conditionup . " AND name like '%" . $docName . "%'";
                $countquery.=" AND name like '%" . $docName . "%'";

                if (strlen($docName) >= 2) {
                    $symptomQuery = "SELECT sy.name AS symptom, sp.name AS specialization, sp.specialization_id FROM symptom_master as sy INNER JOIN symptom_specialization_rel ssr ON sy.symptom_id = ssr.symptom_id LEFT JOIN specialization_master AS sp ON sp.specialization_id = ssr.specialization_id WHERE sy.name LIKE '%$docName%' ORDER BY sy.name";

                    $symptomDatas = DB::select($symptomQuery);

                    $symptomList = array();
                    $symptomCount = 0;
                    foreach ($symptomDatas as $symptomData) {
                        $symptomResponse ['symptom'] = $symptomData->symptom;
                        $symptomResponse ['specialization'] = $symptomData->specialization;
                        $symptomResponse ['specialization_id'] = $symptomData->specialization_id;
                        $symptomResponseEncoded = json_encode($symptomResponse);
                        $symptomResponseDecoded = json_decode($symptomResponseEncoded);
                        array_push($symptomList, $symptomResponseDecoded);
                        $symptomCount++;
                    }
                }
            }

            if (!empty($cond['hospitalName']) && (strlen($cond['hospitalName']) >= 2 || $cond['hospitalName'] == '*')) {

                $hospitalQuery = "SELECT hp.hospital_id, hp.name FROM hospital_master AS hp INNER JOIN doctor_workplace dw ON hp.hospital_id = dw.hospital_id INNER JOIN doctor_profile b ON b.user_id = dw.user_id AND b.is_approved = 1 $queryString";

                if ($cond['hospitalName'] != '*') {
                    $hospitalName = $cond['hospitalName'];
                    $hospitalQuery .= " WHERE hp.name LIKE '%$hospitalName%' ";
                }

                $hospitalQuery .= " GROUP BY hp.hospital_id ORDER BY hp.name";

                $hospitalDatas = DB::select($hospitalQuery);

                $hospitalList = array();
                foreach ($hospitalDatas as $hospitalData) {
                    $hospitalResponse ['hospital_id'] = $hospitalData->hospital_id;
                    $hospitalResponse ['hospital_name'] = $hospitalData->name;
                    $hospitalResponseEncoded = json_encode($hospitalResponse);
                    $hospitalResponseDecoded = json_decode($hospitalResponseEncoded);
                    array_push($hospitalList, $hospitalResponseDecoded);
                }
            }

            $query_condition .= " GROUP BY a.user_id";

            if (!empty($cond['sort'])) {
                $sorting = $cond['sort'];
                if ($sorting == 'name,asc') {
                    $sort = 'order by weightage DESC, name asc';
                } elseif ($sorting == 'name,desc') {
                    $sort = 'order by weightage DESC, name desc';
                }
            } else {
                $sort = 'order by online_status desc, weightage DESC, name asc';
            }

            $query_condition = $query_condition . " " . $sort;

            $start = 0;
            $page = 1;
            $end = 10;
            if (!empty($cond['page'])) {
                $page = $cond['page'];
                $start = ($page == "1") ? 0 : (($page * 10) - 10);
            }

            $query_condition = $query_condition . ' limit ' . $start . ',' . $end;
            $countquery = $countquery . ' limit ' . $start . ',' . $end;

            $doclist = Array();

            $totcountquery = DB::select($countquery);
            foreach ($totcountquery as $totc) {
                $count = $totc->count;
            }

            $doctorquery = DB::select($query_condition);
            foreach ($doctorquery as $doct) {
                $numFound = $doct->totcount;
                $response1 ['user_id'] = $doct->user_id;
                $response1 ['name'] = trim($doct->salutation . " " . $doct->name);
                $json_response1 = json_encode($response1);
                $array_decode = json_decode($json_response1);
                array_push($doclist, $array_decode);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result ['status'] = 1;
            $result ['total_count'] = $numFound;
            $result ['count'] = $count;
            if ($count == null) {
                $result ['count'] = $numFound;
            }
            if ($result ['total_count'] == 0) {
                $result ['msg'] = Config::get('config_msg.RECORD_NOT_FOUND');
            } else {
                $result ['msg'] = Config::get('config_msg.DR_LIST') . " & Specializations";
            }

            $result ['page'] = $page;
            $result ['response'] ['doclist'] = $doclist;
            $result ['response'] ['speclist'] = $specializationlist;
            $result ['response'] ['symptomList'] = $symptomList;

            if (!empty($cond['hospitalName']) && (strlen($cond['hospitalName']) >= 2 || $cond['hospitalName'] == '*')) {
                $result ['response'] ['hospitalList'] = $hospitalList;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function doctorApprove($user_id, $value = 1) {
        try {

            $this->setHeader('RESPONSE_CODE', '1');
            $doc = new Doctor ();
            $data ['user_id'] = $user_id;
            $data ['is_approved'] = $value;
            if (!$doc->validateDoc($data)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result ['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result ['status'] = 0;
                $result ['errors'] = $doc->errors();
            } else {

                Doctor::where('user_id', '=', $user_id)->update(array(
                    'is_approved' => $value, 'online_status' => $value
                ));

                $user_app = DeviceInfo::where('user_id', '=', $user_id)->first();
                $communicationHelper = $this->helper->load('communication');

                if ($value == 1) {
                    $result ['status'] = 1;
                    $result ['msg'] = Config::get('config_msg.DOC_APPROVE_SUCCESS');

                    $GCMData = array();
                    $GCMData ['msg_text'] = Config::get('config_msg.DOC_APPROVE_SUCCESS');
                    $GCMData ['type'] = 'doc_approve';

                    $params ['msg'] = json_encode($GCMData);
                    if ($user_app ['platform_id'] == 1) {
                        $params ['GCMDeviceIds'] = $user_app ['registartion_id'];
                        $params ['msg'] = json_encode($GCMData);
                        $result ['response'] = $communicationHelper->gcmCommunication($params);
                    } else if ($user_app ['platform_id'] == 2) {
                        $params ['APNSDeviceIds'] = $user_app ['registartion_id'];
                        $params ['push_msg'] = $GCMData;
                        $result ['response'] = $communicationHelper->pushApnsCommunication($params);
                    }

                    //SEND SMS ALERT
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_DOC_APPROVAL');
                    if ($smsText) {

                        $user = User::where('user_id', '=', $user_id)->select('name', 'mobile')->get();
                        if (!empty($user[0]['name']) && !empty($user[0]['mobile'])) {
                            $smsParams = array();
                            $smsParams['Text'] = str_replace('<Doctor Name>', $user[0]['name'], $smsText);
                            $smsParams['To'] = $user[0]['mobile'];
                            $this->helper->load('communication')->smsCommunication($smsParams);
                        }
                    }
                } else {
                    $result ['status'] = 1;
                    $result ['msg'] = Config::get('config_msg.DOC_DISAPPROVE_SUCCESS');
                }
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function doctorStatus() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            $is_approved = Doctor::where('user_id', '=', $user_id)->pluck('is_approved');

            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;

            if (count($is_approved) <= 0) {
                $result['msg'] = 'Doctor profile does not exists.';
                $result['response']['status'] = 2;
            } elseif ($is_approved) {
                $result['msg'] = 'Doctor profile has been approved.';
                $result['response']['status'] = 1;
            } else {
                $result['msg'] = 'Doctor profile has not been approved.';
                $result['response']['status'] = 0;
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function updateSettings() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            Doctor::where('user_id', '=', $user_id)->update(array('per_min_charges' => $data['per_min_charges'], 'allow_new_user' => $data['is_allow_new_user']));

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.DOCTOR_SETTINGS_UPDATE_SUCCESS');
            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function getSettings() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $user_id = $this->getUserIdbyToken($this->authToken);

            $doctor = Doctor::where('user_id', '=', $user_id)->select('allow_new_user', 'per_min_charges')->first();

            $result['is_allow_new_user'] = ($doctor['allow_new_user']) ? TRUE : FALSE;

            $result['per_min_charges'] = $doctor['per_min_charges'];

            $result['status'] = 1;
            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function doctorRegistration() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);
            $requestData = json_decode($data['experience'], TRUE);

            UserProfile::where('user_id', '=', $user_id)->update(array('is_doctor' => 1));

            $doctor = array();
            $doctor['user_id'] = $user_id;
            $doc = Doctor::where('user_id', '=', $doctor['user_id'])->get();

            $doctor['medical_registration_no'] = (!empty($data['medical_registration_no'])) ? trim($data['medical_registration_no']) : $doc[0]->medical_registration_no;

            $doctor['total_experience'] = (isset($data['total_experience'])) ? trim($data['total_experience']) : $doc[0]->total_experience;

            $doctor['per_min_charges'] = (isset($data['per_min_charges'])) ? trim($data['per_min_charges']) : $doc[0]->per_min_charges;

            $doctor['other_info'] = (!empty($data['other_info'])) ? trim($data['other_info']) : $doc[0]->other_info;

            $doctor['area'] = (!empty($data['area'])) ? trim($data['area']) : $doc[0]->area;

            $doctor['other_specializations'] = (!empty($data['other_specializations'])) ? trim($data['other_specializations']) : $doc[0]->other_specializations;

            if (!empty($data['info'])) {
                $doctor['info'] = trim($data['info']);
            }

            if (!empty($data['city'])) {
                $doctor['city_id'] = !empty($data['city']) ? $data['city'] : $doc[0]->city_id;
            }

            $docPro = new Doctor();
            if (!$docPro->validate($doctor)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = "Inputs validation failed!";
                $result['status'] = 0;
                $result['errors'] = $docPro->errors();
                return Response::make($result, 200, $this->headers);
            }

            $docExist = Doctor::where('user_id', '=', $user_id)->exists();

            $newRegistration = 0;
            if ($docExist) {
                Doctor::where('user_id', '=', $user_id)->update($doctor);
            } else {
                DB::table('doctor_profile')->insert($doctor);
                $newRegistration = 1;
            }

            if (!empty($data['qualification'])) {
                if (json_decode($data['qualification'])) {
                    $requestData = json_decode($data['qualification'], TRUE);
                    DoctorQualification::where('user_id', '=', $user_id)->delete();

                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {
                        $qual = array();
                        $qual['user_id'] = $user_id;
                        if ($requestData[$i]['degreeid'] == null || $requestData[$i]['degreeid'] == 0) {
                            $degree = new Degree();
                            $degree->name = $requestData[$i]['degree'];
                            $degree->save();
                            $qual['degree'] = $degree->degree_id;
                        } else {
                            $qual['degree'] = $requestData[$i]['degreeid'];
                        }
                        $qual['university'] = 1;
                        $qual['college'] = $requestData[$i]['university'];
                        $qual['start_date'] = date("Y-m-d");
                        $qual['end_date'] = date("Y-m-d");
                        $qual['is_pursuing'] = $requestData[$i]['is_pursuing'];

                        $qualobj = new DoctorQualification();
                        if ($qualobj->validate($qual)) {
                            $doctorQualification = new DoctorQualification();
                            $doctorQualification->user_id = $qual['user_id'];
                            $doctorQualification->degree = $qual['degree'];
                            $doctorQualification->university = $qual['university'];
                            $doctorQualification->college = $qual['college'];
                            $doctorQualification->start_date = $qual['start_date'];
                            $doctorQualification->end_date = $qual['end_date'];
                            $doctorQualification->is_pursuing = $qual['is_pursuing'];
                            $doctorQualification->save();
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                            $result['status'] = 0;
                            $result['errors'] = $qualobj->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.QUALIFICATION_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }

            $realDoctor = 0;
            if (!empty($data['specialization'])) {
                if (json_decode($data['specialization'])) {
                    $requestData = json_decode($data['specialization'], TRUE);
                    DoctorSpecialization::where('user_id', '=', $user_id)->delete();
                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {
                        $docsp = array();
                        $docSpec = new DoctorSpecialization();
                        $docsp['user_id'] = $user_id;
                        if ($requestData[$i]['specializationid'] == null || $requestData[$i]['specializationid'] === 0) {
                            $spec = new Specialization();
                            $spec->name = $requestData[$i]['specialization'];
                            $spec->save();

                            $docsp['specialization_id'] = $spec->specialization_id;
                        } else {
                            $docsp['specialization_id'] = $requestData[$i]['specializationid'];
                        }
                        if ($count_reqdata == 1) {
                            $requestData[$i]['is_default'] = 1;
                        }
                        $docsp['is_default'] = $requestData[$i]['is_default'];
                        if ($docSpec->validate($docsp)) {

                            $doctorSpecialization = new DoctorSpecialization();
                            $doctorSpecialization->user_id = $docsp['user_id'];
                            $doctorSpecialization->specialization_id = $docsp['specialization_id'];
                            $doctorSpecialization->is_default = $docsp['is_default'];
                            $doctorSpecialization->save();

                            $specName = Specialization::where('specialization_id', '=', $docsp['specialization_id'])->pluck('name');

                            if ($realDoctor == 0 && $specName != 'Arabic Patient Coordinator' && $specName != 'Konsult Helpline' && $specName != 'Yoga Therapist' && $specName != 'Dietitian and Nutritionist') {
                                $realDoctor = 1;
                            }
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                            $result['status'] = 0;
                            $result['errors'] = $docSpec->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.SPECIALIZATION_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }

            if (!empty($data['experience'])) {
                if (json_decode($data['experience'])) {
                    $requestData = json_decode($data['experience'], TRUE);
                    DoctorWorkplace::where('user_id', '=', $user_id)->delete();
                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {

                        $hospital_id = !empty($requestData[$i]['hospital_id']) ? $requestData[$i]['hospital_id'] : NULL;
                        $hospitalExist = 0;
                        if (!empty($hospital_id)) {
                            $hospitalExist = Hospital::whereRaw('hospital_id =' . $hospital_id)->exists();
                            if (empty($hospitalExist)) {
                                continue; //IGNORE IF HOSPITAL ID IS INVALID
                            }
                        }

                        //ADD NEW HOSPITAL
                        if (!empty($requestData[$i]['hospital'])) {
                            $newHospital = new Hospital();
                            $newHospital->name = $requestData[$i]['hospital'];
                            //$newHospital->city = $requestData[$i]['city'];
                            //$newHospital->address = $requestData[$i]['address'];
                            $newHospital->save();
                            $hospital_id = $newHospital->hospital_id;
                        }

                        $docExArr = array();
                        $docExp = new DoctorWorkplace();
                        $docExArr['user_id'] = $user_id;
                        $docExArr['hospital_id'] = $hospital_id;
                        $docExArr['designation_id'] = 1;
                        $docExArr['designation_name'] = $requestData[$i]['designation'];
                        $docExArr['address'] = $requestData[$i]['address'];
                        $docExArr['join_date'] = date("Y-m-d");
                        $docExArr['end_date'] = date("Y-m-d");
                        $docExArr['is_current'] = $requestData[$i]['is_current'];

                        if ($docExp->validate($docExArr)) {
                            $doctorExperience = new DoctorWorkplace();
                            $doctorExperience->user_id = $docExArr['user_id'];
                            $doctorExperience->hospital_id = $docExArr['hospital_id'];
                            $doctorExperience->designation_id = $docExArr['designation_id'];
                            $doctorExperience->designation_name = $docExArr['designation_name'];
                            $doctorExperience->address = $docExArr['address'];
                            $doctorExperience->join_date = $docExArr['join_date'];
                            $doctorExperience->end_date = $docExArr['end_date'];
                            $doctorExperience->is_current = $docExArr['is_current'];
                            $doctorExperience->save();
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = "Invalid hospital data!";
                            $result['status'] = 0;
                            $result['errors'] = $docExp->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.EXPERIENCE_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }

            $salutation = User::where('user_id', '=', $user_id)->pluck('salutation');
            if ($realDoctor && (stristr($salutation, 'mr') || empty($salutation))) {
                User::where('user_id', '=', $user_id)->update(array('salutation' => 'Dr.'));
            }

            $result['status'] = 1;

            $result['msg'] = $newRegistration ? Config::get('config_msg.DR_PROFILE_CREATE_SUCCESS') : Config::get('config_msg.DR_PROFILE_UPDATE_SUCCESS');

            Doctor::where('user_id', '=', $user_id)->where('allow_new_user', '=', 0)->update(array('allow_new_user' => 1));

            //SAVE DOCTOR PROFILE URL
            Doctor::savePageURL(array('user_id' => $user_id));

            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function hospitalDetails($hospital_id = 0) {

        try {

            $requestHeaders = apache_request_headers();
            $checkAuthToken = isset($requestHeaders['AUTH_TOKEN']) ? TRUE : FALSE;
            $result = $this->checkAuth($checkAuthToken);

            if ($result ['config_status'] == - 1) {
                return $result;
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            if (empty($hospital_id)) {
                $result['status'] = 0;
                $result['msg'] = "Hospital id is required!";
                return Response::make($result, 200, $this->headers);
            }

            $hospital = Hospital::where('hospital_id', '=', $hospital_id)->first();
            if (empty($hospital->hospital_id)) {
                $result['status'] = 0;
                $result['msg'] = "Invalid hospital id!";
                return Response::make($result, 200, $this->headers);
            }

            $response['name'] = $hospital->name;
            $response['city'] = $hospital->city;
            $result['hospitalDetails'] = $response;

            $data = Input::all();

            if (!empty($data['specList'])) {

                $specQuery = "SELECT spm.specialization_id, spm.name, spm.photo_url FROM specialization_master AS spm INNER JOIN doctor_specialization_rel AS dsr ON spm.specialization_id = dsr.specialization_id INNER JOIN doctor_profile AS dp ON dp.user_id = dsr.user_id INNER JOIN doctor_workplace AS dw ON dp.user_id = dw.user_id AND hospital_id = $hospital_id INNER JOIN users AS u ON dp.user_id = u.user_id WHERE dp.is_approved = 1 ";

                if ($checkAuthToken) {
                    $this->setHeader('AUTH_TOKEN', $this->authToken);
                    $user_id = $this->getUserIdbyToken($this->authToken);

                    if (!empty($user_id)) {
                        $isClinic = UserProfile::whereRaw('user_id = ' . $user_id)->pluck('clinic_type');
                        if ($isClinic) {
                            $specQuery .= " AND (dp.list_category = $isClinic OR dp.list_category = 3) ";
                        }
                    }
                }

                $specQuery .= " GROUP BY spm.specialization_id;";

                $specDatas = DB::select($specQuery);

                $specList = array();
                foreach ($specDatas as $specData) {
                    $specResponse ['specialization_id'] = $specData->specialization_id;
                    $specResponse ['name'] = $specData->name;
                    $specResponse ['photo_url'] = $specData->photo_url;
                    $specResponseEncoded = json_encode($specResponse);
                    $specResponseDecoded = json_decode($specResponseEncoded);
                    array_push($specList, $specResponseDecoded);
                }

                $result['specList'] = $specList;
            }

//            if (!empty($data['docList'])) {
//                $docQuery = "SELECT dp.doctor_id, dp.user_id, u.name FROM doctor_profile AS dp INNER JOIN doctor_workplace AS dw ON dp.user_id = dw.user_id AND hospital_id = $hospital_id INNER JOIN users AS u ON dp.user_id = u.user_id WHERE dp.is_approved = 1;";
//                $docDatas = DB::select($docQuery);
//
//                $docList = array();
//                foreach ($docDatas as $docData) {
//                    $docResponse ['user_id'] = $docData->user_id;
//                    $docResponse ['doctor_name'] = $docData->name;
//                    $docResponseEncoded = json_encode($docResponse);
//                    $docResponseDecoded = json_decode($docResponseEncoded);
//                    array_push($docList, $docResponseDecoded);
//                }
//                
//                $result['docList'] = $docList;
//            }

            $result['status'] = 1;
            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function setOperationHours() {

        try {
            $result = $this->checkAuth(TRUE);

            if ($result ['config_status'] == - 1) {
                return ($result);
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            if (empty($data['operation_hours'])) {
                $result['status'] = 0;
                $result['msg'] = 'Input data is missing!';
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $doctor_id = Doctor::where('user_id', '=', $user_id)->pluck('doctor_id');
            if (empty($doctor_id)) {
                $result['msg'] = "Un-authroized doctor!";
                $result['status'] = 0;
                return Response::make($result, 200, $this->headers);
            }

            if (json_decode($data['operation_hours'])) {
                $requestData = json_decode($data['operation_hours'], TRUE);
                OperationHours::where('user_id', '=', $user_id)->delete();
                $count = count($requestData);
                for ($i = 0; $count > $i; $i++) {
                    $operationHourArray = array();
                    $operationHourModel = new OperationHours();
                    $operationHourArray['user_id'] = $user_id;
                    $operationHourArray['day_id'] = $requestData[$i]['day_id'];
                    $operationHourArray['start_time'] = $requestData[$i]['start_time'];
                    $operationHourArray['end_time'] = $requestData[$i]['end_time'];
                    $operationHourArray['created_at'] = date("Y-m-d H:i:s");
                    if ($operationHourModel->validate($operationHourArray)) {
                        OperationHours::insert($operationHourArray);
                        $result['msg'] = "Data saved successfully!";
                        $result['status'] = 1;
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $result['msg'] = "Invalid data!";
                        $result['status'] = 0;
                        $result['errors'] = $operationHourModel->errors();
                        return Response::make($result, 200, $this->headers);
                    }
                }
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function getOperationHours() {

        try {
            $result = $this->checkAuth(TRUE);

            if ($result ['config_status'] == - 1) {
                return ($result);
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $doctor_id = Doctor::where('user_id', '=', $user_id)->pluck('doctor_id');
            if (empty($doctor_id)) {
                $result['msg'] = "Un-authroized doctor!";
                $result['status'] = 0;
                return Response::make($result, 200, $this->headers);
            }

            $operationHoursList = array();
            $operationHourModel = new OperationHours();
            $operationHours = $operationHourModel->getOperationHours(array('user_id' => $user_id));

            foreach ($operationHours as $operationHour) {

                $response['day_id'] = $operationHour->day_id;
                $response['start_time'] = $operationHour->start_time;
                $response['end_time'] = $operationHour->end_time;

                $jsonResponse = json_encode($response);
                $array_decode = json_decode($jsonResponse);
                array_push($operationHoursList, $array_decode);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result ['status'] = 1;
            $result['operation_hours'] = $operationHoursList;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
