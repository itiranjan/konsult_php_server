<?php

class UserProfile extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'user_profile';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'profile_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined for name or email
     * @var array
     */
    private $rules = array(
        'name' => 'required|min:3',
        'email' => 'required|email|unique:users',
    );

    /**
     * validation customer message defined for name
     * @var array
     */
    private $messages = array(
        'name.regex' => 'The name may only contain letters and space',
        //'dob.after' => 'Invaild DOB'
    );

    /**
     * validation rule defined for user image
     * @var array
     */
    private $imagerules = array(
        'image' => 'required|image|max:10000|mimes:jpg,jpeg,png'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate user image for model
     * @param array: data
     * @return boolean
     */
    public function validateImage($data) {

        $v = Validator :: make($data, $this->imagerules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

}
