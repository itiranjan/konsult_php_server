<?php
/**
 * Theme Functions
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );

define( 'THEME_NAME', 'betheme' );
define( 'THEME_VERSION', '9.7' );

define( 'LIBS_DIR', THEME_DIR. '/functions' );
define( 'LIBS_URI', THEME_URI. '/functions' );
define( 'LANG_DIR', THEME_DIR. '/languages' );

add_filter( 'widget_text', 'do_shortcode' );


/* ---------------------------------------------------------------------------
 * White Label
 * IMPORTANT: We recommend the use of Child Theme to change this
 * --------------------------------------------------------------------------- */
defined( 'WHITE_LABEL' ) or define( 'WHITE_LABEL', false );


/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * --------------------------------------------------------------------------- */
load_theme_textdomain( 'betheme',  LANG_DIR );
load_theme_textdomain( 'mfn-opts', LANG_DIR );


/* ---------------------------------------------------------------------------
 * Loads the Options Panel
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'mfn_admin_scripts' ) )
{
	function mfn_admin_scripts() {
		wp_enqueue_script( 'jquery-ui-sortable' );
	}
}   
add_action( 'wp_enqueue_scripts', 'mfn_admin_scripts' );
add_action( 'admin_enqueue_scripts', 'mfn_admin_scripts' );
	
require( THEME_DIR .'/muffin-options/theme-options.php' );

$theme_disable = mfn_opts_get( 'theme-disable' );


/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * --------------------------------------------------------------------------- */

// Functions --------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-functions.php' );

// Header -----------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-head.php' );

// Menu -------------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-menu.php' );
if( ! isset( $theme_disable['mega-menu'] ) ){
	require_once( LIBS_DIR .'/theme-mega-menu.php' );
}

// Meta box ---------------------------------------------------------------------
require_once( LIBS_DIR .'/meta-functions.php' );

// Custom post types ------------------------------------------------------------
$post_types_disable = mfn_opts_get( 'post-type-disable' );

if( ! isset( $post_types_disable['client'] ) ){
	require_once( LIBS_DIR .'/meta-client.php' );
}
if( ! isset( $post_types_disable['offer'] ) ){
	require_once( LIBS_DIR .'/meta-offer.php' );
}
if( ! isset( $post_types_disable['portfolio'] ) ){
	require_once( LIBS_DIR .'/meta-portfolio.php' );
}
if( ! isset( $post_types_disable['slide'] ) ){
	require_once( LIBS_DIR .'/meta-slide.php' );
}
if( ! isset( $post_types_disable['testimonial'] ) ){
	require_once( LIBS_DIR .'/meta-testimonial.php' );
}

if( ! isset( $post_types_disable['layout'] ) ){
	require_once( LIBS_DIR .'/meta-layout.php' );
}
if( ! isset( $post_types_disable['template'] ) ){
	require_once( LIBS_DIR .'/meta-template.php' );
}

require_once( LIBS_DIR .'/meta-page.php' );
require_once( LIBS_DIR .'/meta-post.php' );

// Content ----------------------------------------------------------------------
require_once( THEME_DIR .'/includes/content-post.php' );
require_once( THEME_DIR .'/includes/content-portfolio.php' );

// Shortcodes -------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-shortcodes.php' );

// Hooks ------------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-hooks.php' );

// Widgets ----------------------------------------------------------------------
require_once( LIBS_DIR .'/widget-functions.php' );

require_once( LIBS_DIR .'/widget-flickr.php' );
require_once( LIBS_DIR .'/widget-login.php' );
require_once( LIBS_DIR .'/widget-menu.php' );
require_once( LIBS_DIR .'/widget-recent-comments.php' );
require_once( LIBS_DIR .'/widget-recent-posts.php' );
require_once( LIBS_DIR .'/widget-tag-cloud.php' );

// TinyMCE ----------------------------------------------------------------------
require_once( LIBS_DIR .'/tinymce/tinymce.php' );

// Plugins ---------------------------------------------------------------------- 
if( ! isset( $theme_disable['demo-data'] ) ){
	require_once( LIBS_DIR .'/importer/import.php' );
}

require_once( LIBS_DIR .'/class-love.php' );
require_once( LIBS_DIR .'/class-tgm-plugin-activation.php' );

require_once( LIBS_DIR .'/plugins/visual-composer.php' );

// WooCommerce specified functions
if( function_exists( 'is_woocommerce' ) ){
	require_once( LIBS_DIR .'/theme-woocommerce.php' );
}

// Hide activation and update specific parts ------------------------------------

// Slider Revolution
if( ! mfn_opts_get( 'plugin-rev' ) ){
	if( function_exists( 'set_revslider_as_theme' ) ){
		set_revslider_as_theme();
	}
}

// LayerSlider
if( ! mfn_opts_get( 'plugin-layer' ) ){
	add_action('layerslider_ready', 'mfn_layerslider_overrides');
	function mfn_layerslider_overrides() {
		// Disable auto-updates
		$GLOBALS['lsAutoUpdateBox'] = false;
	}
}

// Visual Composer 
if( ! mfn_opts_get( 'plugin-visual' ) ){
	add_action( 'vc_before_init', 'mfn_vcSetAsTheme' );
	function mfn_vcSetAsTheme() {
		vc_set_as_theme();
	}
}



add_filter('query_vars', 'add_state_var', 0, 2);
function add_state_var($vars){
	
    $vars[] = 'page_url';
    $vars[] = 'location';
    $vars[] = 'pagee';
    $vars[] = 'alphabet';
	
    //$vars[] = 'doctor';
    return $vars;
}


/*
function add_rewrite_rules($aRules) {
$aNewRules = array('listing-page/([^/]+)/?$' => 'index.php?pagename=listing-page&page_url=$matches[1]','doctor-profile([^/]+)/?$' => 'index.php?pagename=doctor-profile&page_url=$matches[1]');
$aRules = $aNewRules + $aRules;
return $aRules;
}

add_filter('rewrite_rules_array', 'add_rewrite_rules');
*/
// function fcars() {

  // add_rewrite_rule('listing-page/[/]?([a-zA-Z-]*)[/]?([a-zA-Z-]*)$', 'index.php?pagename=listing-page&page_url=$matches[1]&pagee=$matches[2]');

  // add_rewrite_tag('%page_url%', '[a-zA-Z]+');
  // add_rewrite_tag('%pagee%', '[a-zA-Z]+');
// }

// add_action('init', 'fcars');
function custom_rewrite_tag() {
  add_rewrite_tag('%page_url%', '([^&]+)');
  // add_rewrite_tag('%food%', '([^&]+)');
  // add_rewrite_tag('%variety%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);
add_rewrite_rule('^listing-page/([^/]+)/?$','index.php?pagename=listing-page&page_url=$matches[1]','top');
//add_rewrite_rule('^listing-page/([^/]+)/?$/([^/]+)/?$','index.php?pagename=listing-page&page_url=$matches[1]&pagenum=$matches[2]','top');
 add_rewrite_rule('^symptom/([^/]+)/?$','index.php?pagename=symptom&page_url=$matches[1]','top');
 add_rewrite_rule('^speciality/([^/]+)/?$','index.php?pagename=speciality&page_url=$matches[1]','top');
function custom_rewrite_basic() {
 add_rewrite_rule('^symptom/([^/]+)/?$','index.php?pagename=symptom&page_url=$matches[1]','top');
 add_rewrite_rule('^speciality/([^/]+)/?$','index.php?pagename=speciality&page_url=$matches[1]','top');
}
add_action('init', 'custom_rewrite_basic');




add_rewrite_rule('^doctor-profile/([^/]+)/?$','index.php?pagename=doctor-profile&page_url=$matches[1]','top');

add_rewrite_rule('^symptom-page/([^/]+)/?$','index.php?pagename=symptom-page&page_url=$matches[1]','top');



// ----- Fetch Blog on Doctor Page---


add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );


function example_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$page_number = filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
						$item_per_page = 10;
						$position = (($page_number-1) * $item_per_page);
						
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						$specialIdRand = $my_wpdb->get_results("SELECT doctor_specialization_rel.doctor FROM doctor_specialization_rel JOIN doctor_profile ON doctor_profile.user = doctor_specialization_rel.doctor WHERE doctor_specialization_rel.specialization = '$page_url' AND doctor_profile.is_approved =1 ORDER BY doctor_profile.sponsored LIMIT $position, $item_per_page ");	
	
	
	
						foreach ($specialIdRand as $doc) {	
$docId = 	$doc->doctor ;
	
$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, up.address, up.is_doctor, cm.name AS city, up.photo, up.photo_url,
dp.total_experience,dsr.specialization as specializationID, dsm.name as specializationName , u.email,
dp.area, dp.about as about, dp.other_specializations,dp.per_min_charges as charges,dp.page_url as pageUrl,
dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user )
LEFT JOIN doctor_profile dp ON ( up.user = dp.user )
LEFT JOIN doctor_qualifications dq ON ( up.user = dq.doctor )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user = dsr.doctor )
LEFT JOIN specialization_master dsm ON ( dsr.specialization = dsm.specialization_id )

LEFT JOIN doctor_experience dex ON ( dp.user = dex.doctor )


LEFT JOIN city_master cm ON up.city = cm.city_id
WHERE user_id ='$docId' ");	
	?>
<div class="doctor-detail">
            <div class="row">
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">

                    <!-- Doctor Image -->
                    <div class="doctor-img">
<?php
    $imageavailabl = 0;
    if(file_exists(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/konnect/api/public/photo/'.$docRow->photo)){
        $imageavailabl = 1;
    }
    else{}
    ?>
                        <img src="http://api.konsultapp.com/photo/<?php if($docRow->photo != null AND $imageavailabl == 1){ echo $docRow->photo; }else{ echo "user.png"; } ?>" width="150" height="150">
                    </div>
                    <!-- End Doctor Image -->

                    <!-- Doctor Details -->
                    <div class="doctor-biography">
                       <a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/"> <h5 class="doctor-name"> <?php  echo $docRow->name ;  ?></h5></a>
                       <p class="designation"><?php  echo $docRow->specializationName ;  ?></p> 
                        <p><?php  echo $docRow->degreeName ;  ?> - <?php  echo $docRow->hospitalName ;  ?> </p> 
                        

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="round">
                                   <i class="fa fa-user-md" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Experience</p>
                                    <p class=""><?php  echo $docRow->total_experience ;  ?> Years</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                            
                            
                        <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Fee</p>
                                    <p class=""><?php  echo $docRow->charges ;  ?>/min</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                       
                       <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="doc-practice">
                                    <p class="prac-txt">Location</p>
                                    <p class="prac-place"><?php  echo $docRow->area ;  ?> <?php  echo $docRow->city ;  ?></p> 
                                </div>
                            </div>
                    </div>
                    
      <div class="learn-more-btn rgt align_right1">
		<a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/">  <span class="learn-more-txt">View Profile</span> </a>
	</div>
                    </div>
                 </div>
            </div>
        </div>
<?php }
							
							
							
							die();
}




add_action( 'wp_ajax_symptom_ajax_request', 'symptom_ajax_request' );

add_action( 'wp_ajax_symptom_ajax_request', 'symptom_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_symptom_ajax_request', 'symptom_ajax_request' );


function symptom_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$page_number = filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
						$item_per_page = 10;
						$position = (($page_number-1) * $item_per_page);
						
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						$specialIdRand = $my_wpdb->get_results("SELECT DISTINCT(doctor_specialization_rel.doctor) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization JOIN doctor_profile ON doctor_profile.user = doctor_specialization_rel.doctor WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1 ORDER BY doctor_profile.sponsored LIMIT $position, $item_per_page ");	
	
	
	
						foreach ($specialIdRand as $doc) {	
$docId = 	$doc->doctor ;
	
$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, up.address, up.is_doctor, cm.name AS city, up.photo, up.photo_url,
dp.total_experience,dsr.specialization as specializationID, dsm.name as specializationName , u.email,
dp.area, dp.about as about, dp.other_specializations,dp.per_min_charges as charges,dp.page_url as pageUrl,
dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user )
LEFT JOIN doctor_profile dp ON ( up.user = dp.user )
LEFT JOIN doctor_qualifications dq ON ( up.user = dq.doctor )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user = dsr.doctor )
LEFT JOIN specialization_master dsm ON ( dsr.specialization = dsm.specialization_id )

LEFT JOIN doctor_experience dex ON ( dp.user = dex.doctor )


LEFT JOIN city_master cm ON up.city = cm.city_id
WHERE user_id = '$docId' ");	
	?>
<div class="doctor-detail">
            <div class="row">
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">

                    <!-- Doctor Image -->
                    <div class="doctor-img">
 <?php
     $imageavailable = 0;
     if(file_exists(dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/konnect/api/public/photo/'.$docRow->photo)){
         $imageavailable = 1;
     }
     else{}
     ?>
                        <img src="http://api.konsultapp.com/photo/<?php  if($docRow->photo != null AND $imageavailable == 1){ echo $docRow->photo; }else{ echo "user.png"; }  ?>" width="150" height="150">
                    </div>
                    <!-- End Doctor Image -->

                    <!-- Doctor Details -->
                    <div class="doctor-biography">
                        <a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/"><h5 class="doctor-name"> <?php  echo $docRow->name ;  ?></h5></a>
                       <p class="designation"><?php  echo $docRow->specializationName ;  ?></p> 
                        <p><?php  echo $docRow->degreeName ;  ?> - <?php  echo $docRow->hospitalName ;  ?> </p> 
                        

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="round">
                                   <i class="fa fa-user-md" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Experience</p>
                                    <p class=""><?php  echo $docRow->total_experience ;  ?> Years</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                            
                            
                        <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Fee</p>
                                    <p class=""><?php  echo $docRow->charges ;  ?>/min</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                       
                       <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="doc-practice">
                                    <p class="prac-txt">Location</p>
                                    <p class="prac-place"><?php  echo $docRow->area ;  ?> <?php  echo $docRow->city ;  ?></p> 
                                </div>
                            </div>
                    </div>
                    
      <div class="learn-more-btn rgt align_right1">
		<a href="http://www.konsult.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/">  <span class="learn-more-txt">View Profile</span> </a>
	</div>
                    </div>
                 </div>
            </div>
        </div>
<?php }
							
							
						//echo "outside foreach";
							die();
}


add_action( 'wp_ajax_feedback_ajax_request', 'feedback_ajax_request' );

add_action( 'wp_ajax_feedback_ajax_request', 'feedback_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_feedback_ajax_request', 'feedback_ajax_request' );

function feedback_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$doctor = $_REQUEST['doctor']; $name = $_REQUEST['name']; $email = $_REQUEST['email']; $contact = $_REQUEST['contact']; $message = $_REQUEST['message'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						$my_wpdb->insert("feedback_form", array(
					   "name" => $name,
					   "email" => $email,
					   "contact" => $contact,
						"message" => $message ,	
					   "doctor" => $doctor ,
					   
					));
	
}

add_action( 'wp_ajax_get_doc_count', 'get_doc_count' );

add_action( 'wp_ajax_get_doc_count', 'get_doc_count' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_get_doc_count', 'get_doc_count' );


function get_doc_count() {
	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						echo $specialIdRan= $my_wpdb->get_var("SELECT count(*) FROM doctor_specialization_rel JOIN doctor_profile ON doctor_specialization_rel.doctor = doctor_profile.user where specialization = $page_url AND doctor_profile.is_approved = 1  ");	
}
add_action( 'wp_ajax_get_doc_count_symptom', 'get_doc_count_symptom' );

add_action( 'wp_ajax_get_doc_count_symptom', 'get_doc_count_symptom' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_get_doc_count_symptom', 'get_doc_count_symptom' );


function get_doc_count_symptom() {
	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						echo $specialIdRa= $my_wpdb->get_var("SELECT COUNT(DISTINCT(doctor_specialization_rel.doctor)) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization JOIN doctor_profile ON doctor_specialization_rel.doctor = doctor_profile.user WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1");	
}
function custom_excerpt_length( $length ) {
	return 45;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more( $more ) {
	return "...";
}
add_filter('excerpt_more', 'new_excerpt_more');
    
	
	add_action( 'wp_ajax_what_we_do_ajax_request', 'what_we_do_ajax_request' );
    add_action( 'wp_ajax_what_we_do_ajax_request', 'what_we_do_ajax_request' ); 
    add_action( 'wp_ajax_nopriv_what_we_do_ajax_request', 'what_we_do_ajax_request' );
	function what_we_do_ajax_request(){
		
						//global $wp;
						//global $wpdb;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						//$my_wpdb = new WPDB('root', '', 'Konsult_Prod', 'localhost');
						 $search = $_REQUEST["page"];
						if(isset($search)){
						$specializationsearchData = $my_wpdb->get_results("select * FROM specialization_master WHERE name like '%$search%';");
						if($specializationsearchData){foreach($specializationsearchData as $spsd){
							?>
    
   <li>
 
                              <a href="<?php echo bloginfo('url'); ?>/speciality/<?php echo str_replace(' ', '-', strtolower($spsd->name))?>" >
							  <span class="left"><?php echo $spsd->name; ?></span>
                              <span class="right">Speciality</span>								
								</a>
</li>
   <?php
						}}
						$symptomsearchData = $my_wpdb->get_results("select * FROM symptom_master WHERE name like '%$search%';");
							if($symptomsearchData){foreach($symptomsearchData as $stsd){
								?>
  
   <li>
   
   <a href="<?php echo bloginfo('url'); ?>/symptom/<?php echo str_replace(' ', '-', strtolower($stsd->name))?>-treatment">
                               <span class="left"><?php echo $stsd->name; ?></span>
							   <span class="right">Symptom</span>  
								 </a>
								 </li>
   
   <?php
							}}
                            
                            /* show doctors */
                            
                            $doctorsearchData = $my_wpdb->get_results("select users.name as username, specialization_master.name sname, doctor_profile.page_url as page_url FROM doctor_profile JOIN users on users.user_id = doctor_profile.user_id JOIN doctor_specialization_rel on doctor_specialization_rel.user_id = doctor_profile.user_id JOIN specialization_master on specialization_master.specialization_id = doctor_specialization_rel.specialization_id WHERE doctor_profile.is_approved = 1 AND users.name LIKE '%".$search."%';");
                            
                            if($doctorsearchData){foreach($doctorsearchData as $dtsd){
								?>
  
   <li>
   
   <a href="<?php echo bloginfo('url'); ?>/doctor-profile/<?php echo $dtsd->page_url; ?>">
                               <span class="left"><?php echo "Dr ".$dtsd->username; ?></span>
							   <span class="right">Doctor</span>  
								 </a>
								 </li>
   
   <?php
							}}
                            
						}
	}
	
	/* Regestering Custom Post Type for Internationa Website */
	add_action( 'init', 'codex_book_init' );
function codex_book_init() {
	$labels = array(
		'name'               => _x( 'International-website', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'IW Elements', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'International-website', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'International-website', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'iwelement', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New iwelement', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New iwelement', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit iwelement', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View iwelement', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All iwelements', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search iwelements', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent iwelements:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No iwelement found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No iwelement found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'iwelement' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'iwelement', $args );
}
	/* /Regestering Custom Post Type for Internationa Website */
	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) || in_array('current-menu-ancestor', $classes) ){
        $classes[] = 'active';
    }
    return $classes;
}
/* Regestering Custom Post Type for Internationa Website */
	add_action( 'init', 'register_offer_post' );
function register_offer_post() {
	$labels = array(
		'name'               => _x( 'Offers', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Offer', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Offers', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Offers', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'offerelement', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New offerelement', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New offerelement', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit offerelement', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View offerelement', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Offers', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Offers', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Offers:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No offerelement found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No offerelement found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'offerelement' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'offerelement', $args );
}
/* Adding jobs custom post type 
	add_action( 'init', 'register_job_post' );
function register_job_post() {
	$labels = array(
		'name'               => _x( 'Jobs', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'Job', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Jobs', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'Jobs', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'job', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New job', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New job', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit job', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View job', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Jobs', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Jobs', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Jobs:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Jobs found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Jobs found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'job' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'job', $args );
}

 /Adding jobs custom post type */

// Remove query string from static files
function remove_cssjs_ver( $src ) {
 if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
 return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );
function remove_author_pages_page() {
	if ( is_author() ) {
		global $wp_query;
		$wp_query->set_404();
		status_header( 404 );
	}
}

function remove_author_pages_link( $content ) {
	return get_option( 'home' );
}

add_action( 'template_redirect', 'remove_author_pages_page' );
add_filter( 'author_link', 'remove_author_pages_link' );
//add_filter( 'wpseo_canonical', '__return_false' );

remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'rel_canonical',4523);
 remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
add_filter( 'post_class', 'remove_hentry' );
function remove_hentry( $class ) {
	$class = array_diff( $class, array( 'hentry' ) );	
	return $class;
}
?>
