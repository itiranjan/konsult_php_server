<?php

class KccTransaction extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_transactions';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'transaction_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

    public function getAccountBalance($params = array()) {

        $query = DB::table('kcc_transactions');

        if (!empty($params['transaction_type_id'])) {
            $query->where('transaction_type_id', '=', $params['transaction_type_id']);
        }

        if (!empty($params['created_at'])) {
            $query->where('created_at', '>', $params['created_at']);
        }

        if (!empty($params['transferred_from'])) {
            $query->where('transferred_from', '=', $params['transferred_from']);
        }

        if (!empty($params['transferred_to'])) {
            $query->where('transferred_to', '=', $params['transferred_to']);
        }

        return $query->sum('amount');
    }

    public function getUsersList($params = array()) {

        $list1 = DB::table('kcc_transactions')->groupBy('transferred_from')->lists('transferred_from');

        $list2 = DB::table('kcc_transactions')->groupBy('transferred_to')->lists('transferred_to');

        $finalList = array_merge($list1, $list2);

        return array_unique($finalList);
    }

    public function getBalance($params = array()) {

        $userId = $params['user_id'];

        if (empty($userId)) {
            return 0;
        }

        $kccAccountStatement = new KccAccountStatement();

        //CHECK FOR BALANCE
        $getAccountStatement = $kccAccountStatement->getAccountStatement(array('user_id' => $userId));

        $kccTransactionType = new KccTransactionType();
        
        $balanceParams = array();
        $balanceParams['transaction_type_id'] = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
        $balanceParams['transferred_from'] = $userId;
        if (count($getAccountStatement) > 0) {
            $balanceParams['created_at'] = $getAccountStatement->statement_date;
        }

        $callBalance = $this->getAccountBalance($balanceParams);

        $balanceParams['transferred_to'] = $userId;
        unset($balanceParams['transferred_from']);
        $balanceParams['transaction_type_id'] = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));
        $rechargeBalance = $this->getAccountBalance($balanceParams);

        if (count($getAccountStatement) > 0 && !empty($getAccountStatement->closing_balance)) {
            $closing_balance = $getAccountStatement->closing_balance + $rechargeBalance - $callBalance;
        } else {
            $closing_balance = $rechargeBalance - $callBalance;
        }
        
        return $closing_balance;
    }

}
