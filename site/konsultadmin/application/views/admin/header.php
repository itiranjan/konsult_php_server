<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<style>
    .k-grid-filter {
        z-index: 0 !important;
    }
</style>    

<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="imagetoolbar" content="no">
<title><?= (isset($this->title)) ? SITE . ' [' . CLIENT . '] ' . $this->title : SITE ;?></title>
<link rel="stylesheet" type="text/css" href="<?php echo(CSS.'admin/stylesheet.css'); ?>">
<link href="<?php echo CSS;?>kendo.common.min.css" rel="stylesheet">
<link href="<?php echo CSS;?>kendo.metro.min.css" rel="stylesheet">
<script src="<?php echo JS;?>jquery.min.js"></script>
<script src="<?php echo JS;?>kendo.all.min.js"></script>
<script src="<?php echo JS;?>kendo.datefilter.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/jszip/2.4.0/jszip.js"></script>
</script>
</head>
  
 <body>
<?PHP 
    $permissions = $this->session->userdata('permissions');
    $username = $this->session->userdata('username');
?>
<div id="header">
  <div id="topheader">
        <div class="left">
		<div style="float:left;padding-left:20px;padding-top:5px;margin-right:10px;padding-bottom:5px;">	<img src='<?php echo IMG; ?>admin/logo.png'  /></div>
            <div style="float:left;margin-top:15px;margin-left:15px;Font-size:20px;valign:center;"><b> Website Administration
        </div></div>
    <div class="right">
            <div class="userinfo">
			  <img src="<?= IMG;?>admin/avatar/truck.jpg" width="26px" alt=""><span><?php echo $username; ?></span>
                <div class="submenu">
                <ul>
                    <?php if(!empty($permissions["kcc_assignMinutes_edit"])): ?>
                        <li><a href="<?php echo URL?>/index.php/kcc/assignMinutes">Assign Talk Time</a></li>
                    <?php endif; ?>    
                    <?php if(!empty($permissions["kcc_userInfo_view"]) || !empty($permissions["kcc_userInfo_view"])): ?>
                        <li><a href="<?php echo URL?>/index.php/kcc/userInfo">KCC - Customer Support</a></li>
                    <?php endif; ?>
                        
                    <?php if(!empty($permissions["kcc_callInfos_view"]) || !empty($permissions["kcc_callInfos_view"])): ?>    
                        <li><a href="<?php echo URL?>/index.php/kcc/callInfos">KCC - Call Log</a></li><?php endif; ?>

                    <?php if(!empty($permissions["app_userInfo_view"]) || !empty($permissions["app_userInfo_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/app/userInfo">App Customer Support</a></li><?php endif; ?>
                    
                    <?php if(!empty($permissions["miscellaneous_makeISDCall_view"])): ?>
                        <li><a href="<?php echo URL?>/index.php/miscellaneous/makeISDCall">Make ISD Calls</a></li><?php endif; ?>                    
                    
                    <?php if(!empty($permissions["communication_ratings_view"]) || !empty($permissions["communication_ratings_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/communication/ratings">Review & Ratings</a></li><?php endif; ?>
                    <?php if(!empty($permissions["communication_callBackRequests_view"]) || !empty($permissions["communication_callBackRequests_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/communication/callBackRequests">Call Back Requests</a></li><?php endif; ?>
                    <?php if(!empty($permissions["doctor_index_view"]) || !empty($permissions["doctor_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/doctor">Doctors List</a></li><?php endif; ?>
                    <?php if(!empty($permissions["doctor_doctorAnalytics_view"]) || !empty($permissions["doctor_doctorAnalytics_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/doctor/doctorAnalytics">Doctors Analytics</a></li><?php endif; ?>
                    <?php if(!empty($permissions["user_index_view"]) || !empty($permissions["user_index_view"])): ?>
                        <li><a href="<?php echo URL?>/index.php/user">Users List</a></li>
                    <?php endif; ?>

                    <?php if(!empty($permissions["communication_index_view"]) || !empty($permissions["_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/communication">Call Log</a></li>
                <?php endif; ?>
                    <?php if(!empty($permissions["communication_index_view"]) || !empty($permissions["communication_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/transaction">Payment Failures</a></li><?php endif; ?>
                    <?php if(!empty($permissions["communication_knowlarity_view"]) || !empty($permissions["communication_knowlarity_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/communication/knowlarity">Call Failures</a></li><?php endif; ?>
                    <?php if(!empty($permissions["verifyMobile_index_view"]) || !empty($permissions["verifyMobile_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/verifyMobile">Verify Mobile Log</a></li><?php endif; ?>
                    <?php if(!empty($permissions["appLink_index_view"]) || !empty($permissions["appLink_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/appLink">Get App Link Log</a></li><?php endif; ?>
                    <?php if(!empty($permissions["recharge_mvc_view"]) || !empty($permissions["recharge_mvc_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/recharge/mvc">MVC Recharge Log</a></li><?php endif; ?>
                    <?php if(!empty($permissions["recharge_wallet_view"]) || !empty($permissions["recharge_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/recharge">Recharge Wallet</a></li>
                    <?php endif; ?>
                    <?php if(!empty($permissions["statistics_index_view"]) || !empty($permissions["statistics_index_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/statistics">App Statistics</a></li>
<!--                    <li><a id="doct">Doctor Registration</a></li>    -->

                    <?php endif; ?>
                    <?php if(!empty($permissions["user_auth_view"])): ?>
                    <li><a href="<?php echo URL?>/index.php/user/auth">Authorization Access Control</a></li>
<!--                    <li><a id="doct">Doctor Registration</a></li>    -->

                    <?php endif; ?>
                <li><a href="<?php echo URL?>/index.php/verifylogin/logout">Log out</a></li>
                </ul>
                </div>
            </div> 
		
      </div>
    </div>
	<!-- <div id="navigation">
        <ul>
            <li><a href="#" <?php if (strpos($_SERVER['PHP_SELF'], 'users')){echo 'class="active"';}?>>Users</a></li>
			<li><a href="<?php echo URL;?>/index.php/tortype" <?php if (strpos($_SERVER['PHP_SELF'], 'tortype')){echo 'class="active"';}?>>Tours Type</a></li>
          
            <li><a href="<?php echo URL;?>/index.php/tours" <?php if (strpos($_SERVER['PHP_SELF'], 'tours')){echo 'class="active"';}?>>Tours</a></li>
            
			<li><a href="<?php echo URL;?>/index.php/pkgtype" <?php if (strpos($_SERVER['PHP_SELF'], 'pkgtype')){echo 'class="active"';}?>>Packages Type</a></li>
          
            <li><a href="<?php echo URL;?>/index.php/package" <?php if (strpos($_SERVER['PHP_SELF'], 'package')){echo 'class="active"';}?>>Packages</a></li>
            <li><a href="<?php echo URL;?>/index.php/destinationtype" <?php if (strpos($_SERVER['PHP_SELF'], 'destinationtype')){echo 'class="active"';}?>>Destination Type</a></li>
            
            <li><a href="<?php echo URL;?>/index.php/pkdestination" <?php if (strpos($_SERVER['PHP_SELF'], 'pkdestination')){echo 'class="active"';}?>>Destination</a></li>
            <li><a href="<?php echo URL?>/application/views/admin/doctorRegistration.php">Doctor Registration</a></li>
			
			
			<li><a href="#" <?php if (strpos($_SERVER['PHP_SELF'], 'users')){echo 'class="active"';}?>>Promotions</a></li>
			<li><a href="<?php echo URL;?>/index.php/email" <?php if (strpos($_SERVER['PHP_SELF'], 'email')){echo 'class="active"';}?>>Emailing</a></li>
			<li><a href="#" <?php if (strpos($_SERVER['PHP_SELF'], 'users')){echo 'class="active"';}?>>Events</a></li>
			<li><a href="#" <?php if (strpos($_SERVER['PHP_SELF'], 'users')){echo 'class="active"';}?>>Administrative</a></li>
        </ul>
    </div> -->
<div id="breadcrumb"></div>
</div>
<div style="clear:both;height:10px;"></div>

<script>
$('.submenu').hide();
$('.userinfo').mouseover(function(){
	$('.submenu').show();
});
$('.userinfo').mouseout(function(){
	$('.submenu').hide();
});

/*  function doctorregis()
{
	 window.open("http://localhost:8085/konsultdev1/site/admin/application/views/admin/doctorRegistration.php");
      
} */
 $(document).ready(function() {
	$("#doct").click(function(event){
		var email = "";
    	var mob = "";
       $.get(
    	"http://admin.konsultapp.com/index.php/verifylogin/doctorreg", 
          //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/doctorreg",
          {email: email,mobile: mob},
          function(data) {
           //alert(data);
           //window.open("doctorRegistration.php");
           //window.location.assign("doctorRegistration.php");
           
	 			$.get(
            	  "http://admin.konsultapp.com/index.php/verifylogin/getcity", 
                 //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getcity",
                  {email: email,mobile: mob},
                  function(data1) {
                  	$('#city2').html(data1);
                  }
               );

	 			 $.get(
	 	            	  "http://admin.konsultapp.com/index.php/verifylogin/getspecialization", 
	 	                  //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getspecialization",
	 	                  {email: email,mobile: mob},
	 	                  function(data1) {
	 	                  	$('#country_list_id').html(data1);
	 	                  }
	 	               );
	 			 $.get(
	 	            	  "http://admin.konsultapp.com/index.php/verifylogin/getdegree", 
	 	                  //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getdegree",
	 	                  {email: email,mobile: mob},
	 	                  function(data2) {
	 	                  	$('#degree2').html(data2);
	 	                  }
	 	               );
          $('#breadcrumb').html(data);
         // $('#city').html(data);
          $('#clientsDb').hide();
          }
       );
    });
}); 
</script>	

<style type="text/css">
.k-grid .k-state-selected  {
  background-color: #ffffff !important;
  color: green;
}   
</style>

