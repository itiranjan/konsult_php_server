<?php

class OfferController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {

        $requestHeaders = apache_request_headers();
        if ($requestHeaders ['API_KEY'] && $requestHeaders ['APP_VERSION'] && $requestHeaders ['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders ['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders ['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders ['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders ['API_KEY']) {
                    $result ['config_status'] = - 1;
                    $result ['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result ['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders ['AUTH_TOKEN'];
            }
        } else {
            $result ['config_status'] = - 1;
            $result ['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }

        return $result;
    }

    public function offerListing() {

        try {
            $requestHeaders = apache_request_headers();
            $checkAuthToken = isset($requestHeaders['AUTH_TOKEN']) ? TRUE : FALSE;
            $result = $this->checkAuth($checkAuthToken);

            if ($result ['config_status'] == - 1) {
                return ($result);
            }

            if ($result ['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $offerList = array();
            $offer = new Offer();
            $getOffers = $offer->getOffers();

            foreach ($getOffers as $offer) {

                $response['offer_id'] = $offer->offer_id;
                $response['title'] = $offer->title;
                $response['description'] = $offer->description;
                $response['photo'] = $offer->photo;

                $jsonResponse = json_encode($response);
                $array_decode = json_decode($jsonResponse);
                array_push($offerList, $array_decode);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result ['status'] = 1;
            $result['offerList'] = $offerList;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
