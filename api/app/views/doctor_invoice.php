<!doctype html>
<html lang="en">
    <head>
        <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Konsult App</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
</head>
<body>
<table border="0" style="width:600px; height:100%;margin:0 auto; line-height: 22px; border-collapse: collapse; font-family: 'Roboto', sans-serif; color: #7d7d7d; min-height:430px; background-color: rgba(252, 249, 249, 0.59);">
  <tr style=" background-color: rgba(10, 178, 241, 0.36);">
    <td><img src="<?php echo($data['logo']); ?>" style="  padding: 2px 0px;" width="70px" /></td>
  </tr>
  <tr>
    <td style="  color: #0AB2F1;  font-size: 28px;  margin: 10px 0; line-height: 40px; text-align:center;">Thanks for choosing Konsult App!
</td>
    
  </tr>
  <tr>
    <td style="display: block;  padding: 0 10px;"><strong>Invoice Number</strong> : <?php echo($data['invoice_no']); ?></td>
  </tr>
   <tr>
    <td style="display: block;  padding: 0 10px;"><strong>Your Konsultation charges with <span> <?php echo($data['patient_name']); ?> </span> on  <?php echo($data['date']); ?> : </strong>Rs <?php echo($data['charges']); ?></td>
  </tr>
   <tr>
   <td style="display: block;  padding: 0 10px;"><strong>Your wallet is Credited by : </strong>Rs <?php echo($data['charges']); ?></td>
  </tr>
   <tr>
    <td style="display: block;  padding: 0 10px;">Charges are inclusive of service tax and all other taxes applicable. <br />
Charges do not include fees that may be charged by your bank. Please contact your bank directly for inquiries.</td>
  </tr>
   <tr style="  background-color: rgba(10, 178, 241, 0.36);"> 
    <td><p style="padding:2px 5px;   text-align: center;">In case you need any help feel free to contact us at support@konsultapp.com</p> </td>
  </tr>
</table>
</body>
       
</html>
