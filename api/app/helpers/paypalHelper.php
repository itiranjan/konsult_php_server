<?php

$basePath = base_path();
$basePath = substr($basePath, 0, -3);
include_once $basePath . 'api/vendor/braintree/lib/autoload.php';

class paypalHelper {

    protected $helper;

    public function __construct() {
        $this->helper = new Helper();
        $this->accessToken = Config::get('constants.paypal_accessToken');
//        Braintree_Configuration::environment('production');
//        Braintree_Configuration::merchantId('GSTWCNGPWGZQG');
//        Braintree_Configuration::publicKey('your_public_key');
//        Braintree_Configuration::privateKey('your_private_key');        
    }

    public function generateAccessToken() {

        $gateway = new Braintree_Gateway(array(
            'accessToken' => $this->accessToken,
        ));

        return $gateway->clientToken()->generate();
    }

    public function createTransaction($params = array()) {

        $gateway = new Braintree_Gateway(array(
            'accessToken' => $this->accessToken,
        ));

        $requestParams = [
            "amount" => $params['amount'],
            'merchantAccountId' => 'USD',
            "paymentMethodNonce" => $params['payment_method_nonce'],
            "orderId" => time(), //$params['Mapped to PayPal Invoice Number'],
            "descriptor" => [
                "name" => "Konsult*Konsult MVC"
            ],
//            "shipping" => [
//                "firstName" => "Jen",
//                "lastName" => "Smith",
//                "company" => "Braintree",
//                "streetAddress" => "1 E 1st St",
//                "extendedAddress" => "Suite 403",
//                "locality" => "Bartlett",
//                "region" => "IL",
//                "postalCode" => "60103",
//                "countryCodeAlpha2" => "US"
//            ],
            "options" => [
                'submitForSettlement' => True
//                "paypal" => [
//                    "customField" => $params["PayPal custom field"],
//                    "description" => $params["Description for PayPal email receipt"]
//                ],
            ]
        ];

        //UPDATE PAYPAL REQUEST PARAMS IN TABLE
        PaypalTransaction::where('transaction_id', '=', $params['paypal_transaction_id'])->update(array('request' => json_encode($requestParams)));

        $result = $gateway->transaction()->sale($requestParams);
        if ($result->success) {
            
//            $collection =  $gateway->transaction()->search([
//               //Braintree_TransactionSearch::paypalPayerEmail()->is("ENTER_BUYER_EMAIL_ID"),
//               Braintree_TransactionSearch::id()->is($result->transaction->id),
//            ]);
//
//            foreach($collection as $transaction) {
//                    print_r($transaction);
//                echo "<br/>";//$transaction->amount;
//            }
//die;
//            $result1 = Braintree_Transaction::submitForSettlement($result->transaction->id);
//
//            if ($result1->success) {
//                $settledTransaction = $result1->transaction;print_r($settledTransaction);die;
//            } else {
//                print_r($result1->errors);die;
//            }            
            
            
//            $collection = Braintree_Transaction::search([
//                        Braintree_TransactionSearch::id()->is($result->transaction->id),
////                        Braintree_TransactionSearch::status()->in(
////                                [
////                                    Braintree_Transaction::GATEWAY_REJECTED,
////                                    Braintree_Transaction::SETTLED
////                                ]
////                        )
//            ]);
//print_r($result);echo "Jitendra";
//print_r($collection);die;
////            $transactionObject = $result->transaction();
////            $collection = $transactionObject::search([
////                        $transactionObject::amount()->is($params['amount'])
////            ]);

            return array('status' => 1, 'transaction_id' => $result->transaction->id, 'paypalResponse' => $result);
        } else {
            return array('status' => 0, 'error_msg' => $result->message);
        }
    }

}
