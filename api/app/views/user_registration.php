<!doctype html>
<html lang="en">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Konsult App</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
</head>
<body>
<table border="0" style="width:600px; height:100%;margin:0 auto; line-height: 26px; border-collapse: collapse; font-family: 'Roboto', sans-serif; color: #7d7d7d; min-height:430px; background-color: rgba(252, 249, 249, 0.59);">
  <tr style="background-color: rgba(10, 178, 241, 0.36);">
    <td><img src="<?php echo($data['logo']); ?>" style="  padding: 2px 0px;" width="70px" /></td>
  </tr>
  <tr>
  <td style="  color: #0AB2F1;  font-size: 28px;  margin: 10px 0; line-height: 40px; text-align:center;">Hi <?php echo($data['name']); ?>! </td>    
  </tr>
  <tr>
   <td style="display: block;  padding: 0 10px;">Welcome to Konsult App!</td>
    
  </tr>
  <tr>
    <td style="display: block;  padding: 0 10px;">Thanks for signing up! We're excited to have you join us.</td>
    
  </tr>
   <tr>
   <td style="display: block;  padding: 0 10px; color: #55B2F1;"><strong>Here are your login credentials:</strong></td>
   </tr>
    <td style="display: block;  padding: 0 10px;"><strong>Registered Email Id : </strong><?php echo($data['email']); ?></td>
    <td style="display: block;  padding: 0 10px;"><strong>Registered Mobile : </strong>  <?php echo($data['mobile']); ?></td>
   <!--<td style="display: block;  padding: 0 10px;"><strong>Password : </strong><?php echo($data['password']); ?></td> --> 
  </tr>
   <tr>
    <td style="display: block;  padding: 0 10px;">You can use your email along with your password to login to Konsult App.
Alternatively, you can login through Facebook or Google+</td>
<td style="display: block;  padding: 0 10px;">Looking forward to have you on board and we wish you happy Konsulting :)</td>
  </tr>
  <tr>
  <td style="display: block;  padding: 0 10px;">Love, <br />Team Konsult App</td>
  </tr>

   <tr style="  background-color: rgba(10, 178, 241, 0.36);"> 
       <td><p style="padding:2px 5px;   text-align: center;"><a href="http://konsultapp.com/terms.html" target="blank">Terms of Use</a> and <a href="http://konsultapp.com/policy.html" target="blank">Privacy Policy</a><br/>In case you need any help feel free to contact us at support@konsultapp.com</p> </td>
  </tr>
</table>
</body>
</html>
