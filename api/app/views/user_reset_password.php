<!doctype html>
<html lang="en">
    <head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Konsult App</title>
<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
</head>
<body>
<table border="0" style="width:600px; height:100%;margin:0 auto; line-height: 22px; border-collapse: collapse; font-family: 'Roboto', sans-serif; color: #7d7d7d; min-height:430px; background-color: rgba(252, 249, 249, 0.59);">
  <tr style=" background-color: rgba(10, 178, 241, 0.36);">
    <td><img src="<?php echo($data['logo']); ?>" style="  padding: 2px 0px;" width="70px" /></td>
   
  </tr>
  <tr>
    <td style="  color: #0AB2F1;  font-size: 28px;  margin: 10px 0; line-height: 40px; text-align:center;">Hi   <?php echo($data['name']); ?>!
</td>
   
    
  </tr>
   <tr>
   <td style="display: block;  padding: 0 10px;">You requested a password reset. Please visit this link to reset your password:</td>
    
  </tr>
    <td style="display: block;  padding: 0 10px;"> <a href="<?php echo($data['link']); ?>" target="_blank" style="color: #0AB2F1;"><span style="color:red;"><?php echo($data['link']); ?></a> </span></td>
    
  </tr>
   <tr>
   <tr>
    <td style="display: block;  padding: 0 10px;">
Happy Konsulting :)<br />
Love,<br />
Team Konsult App</td>
    
  </tr>
   <tr style="  background-color: rgba(10, 178, 241, 0.36);"> 
    <td><p style="padding:2px 5px;   text-align: center;">In case you need any help feel free to contact us at support@konsultapp.com</p> </td>
    
  </tr>
</table>


</body>
</html>
