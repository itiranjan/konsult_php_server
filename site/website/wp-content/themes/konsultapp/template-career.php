<?php
/**
 * Template Name: Careers Page
 * Description: A Page Template that display careers page.
 *
 * @package Betheme
 * @author Muffin Group
 */
//require_once('../../../wp-config.php');
 get_header();
 ?>
 <style>
 .careerbackimg{
	 min-height: 600px;
     background-size: cover !important;

 }
 #Subheader{
	/* display:none;*/
     margin-top: -10px;
 }
 h1{font-size: 35px;
 font-weight: bold;}
 hr{
	 background:#e872a6 !important;
	 color:#e872a6 !important;
	 height: 5px;
width: 10%;
 }
 .container{
	 text-align:center;
 }
 p{
	 font-size: 25px;
 }
 .secseperator{
	 height:50px;
	 width:100%;
 }
 .footimgh strong{
	 display:block;
	 text-align:right;
	 }
 .footimgh div{
	 width:50%;
	 margin:0 auto;
 }
 .footimgh{
	 background: #50C1CF;
	 padding:10px;
	 color:white;
	 text-align:center;
 }
 .headimgh{
	 min-height:550px;
 }
 .col-sm-6{
	 color:white;
	 min-height:400px;
	 background:#e872a6;
	 background-size:cover;
	 text-align:left;
	 padding: 0px 20px;
 }
 .col-sm-4{
	 min-height:300px;
	 background-size:cover;
 }
 .ihr{
	 background:white !important;
	 color:white !important;
	 height: 5px;
width: 10%;
margin-left:0px;
 }
 #Header{
	 min-height: 60px !important;
 }
 .lastcre select{
	/* height: 30px;*/
font-size: 15px;
padding: 12px;
 }
 .lastcre .wpcf7-file{
	 font-size:10px;
	 padding:0px;
 }
 .lastcre{
	 text-align:left;
 }
 .lastcre .resume{
	 padding:20px;
 }
 .lastcre .resumediv{
	 padding:20px;
 }
 .lastcre .one-second {
	 border:1px solid darkgrey;
	 border: 1px solid black;
     margin: 0px 0px -1px -1px;
     color: black;
 }
 .lastcre input,.lastcre select {
	 border:none;
	 color:black;
 }
 .lastcre li{
	 /*border:1px solid #e872a6;*/
	 color:#e872a6;
	 margin-top:10px;
 }
 .patient-info {
    border: 1px solid white;
    float: left;
    padding: 25px 15px;
    background-color: white;
}
em{
	color:#e872a6;
}
.piii-btn:hover {
	
    background: #e872a6 !important;
}
.piii-btn {
    width: 150px;
    border: none;
    border-radius: 0px !important;
    background: black !important;
    color: white !important;
}
 </style>
<section class="careerbackimg" style="background-image:url('<?php the_post_thumbnail_url('large'); ?>')">
<div class="headimgh"></div>
<div class="footimgh">
<div>
<?php echo "The best way to predict the future is to create it  <strong>- Abraham Lincoln</strong>"; ?>
</div>
</div>
</section>
<div class="secseperator">
</div>
<section>
<div class="container">
<div class="row">
<h1>Konsult's Culture</h1>
<hr />
<p>The atmosphere at Konsult is very positive. Being in the health care industry Konsult believe in strength, warmth and liveliness. You will never find a dull movement here. The power of Konsult lies in its young emplooyees, full of freshness, vigour and spirit.</p>
<div class="secseperator">
</div>
<div class="col-sm-12">
<div class="col-sm-6" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/01-e1494839779318.jpg')"></div>
<div class="col-sm-6">
<h2>Working Together</h2>
<hr class="ihr">
<p>
Konsult rests on the shoulders of a lawyer from Berklay, a techie from IIT, a marketeer from IIM and an operations head known for building many start-ups. They all have teams working under them. The teams believe in "Smart Work". In a year's time Konsult has evolved from a 5 members team to 25. 
</p>
</div>
<div class="col-sm-6" >
<h2>Knowledge Building</h2>
<hr class="ihr">
<p>
Knowledge increases when shared. We at Konsult believe in learning from each other. No single person handles a single vertical and that's the perk of working in s startup. Our sphere of knowledge extends to 'learning as fun'. 
</p></div>
<div class="col-sm-6" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/05-e1494839815628.jpg')"></div>
</div>
</div>
</div>
</section>
<div class="secseperator">
</div>
<section>
<div class="container">
<div class="row">
<h1>Work is much fun!</h1>
<hr />
<div class="col-sm-12">
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/04-e1494839898613.jpg')"></div>
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/03-e1494839941182.jpg')"></div>
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/06-e1494839983890.jpg')"></div>
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/02-e1494840033997.jpg')"></div>
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/07-e1494840079901.jpg')"></div>
<div class="col-sm-4" style="background-image:url('http://www.konsult.com/wp-content/uploads/2017/05/08-e1494840139728.jpg')"></div>
</div>
</div>
</section>
<div class="secseperator">
</div>
<section>
<div class="container">
<div class="row">
<h1>Current Opening</h1>
<hr />
<div class="col-sm-12 lastcre">
<?php echo get_the_content(); ?>

<div class="secseperator">
</div>
<?php
echo do_shortcode('[contact-form-7 id="3340" title="Career"]');
?>
</div>
</div>
</section>
<?php
get_footer();

?>
