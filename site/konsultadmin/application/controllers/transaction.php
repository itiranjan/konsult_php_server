<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class transaction extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('transaction_model', '', TRUE);
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('userid');
        
        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/transaction");
    }

    public function transaction() {
        $this->load->view('admin/header');
        $this->load->view("admin/transaction");
    }

    public function transactionList($way) {

        $data = array();
        $resultsData = $this->transaction_model->transactionList($_POST);
        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'transaction_id' => $row->transaction_id,
                'call_id' => $row->call_id,
                'name' => $row->name,
                'created_at' => $row->created_at,
                'pgm$name' => $row->payment_gateway_name,
                'pg_request' => $row->pg_request,
                'pg_response' => $row->pg_response
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

}
