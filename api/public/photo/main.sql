select t.transaction_id,t.order_no,t.transaction_no,pg.name as payment_gateway,tt.name as transaction_type,
t.amount,t.balance,sm.name as transaction_status,t.trans_datetime,
from transactions t inner join payment_gateway_master pg  on pg.payment_gateway_id=t.payment_gateway
inner join transaction_type_master tt on tt.id=t.`type`
inner join transaction_status_master sm on sm.trans_status_id=t.trans_status
left join transaction_spend_details sd on t.spend_details=sd.spend_id