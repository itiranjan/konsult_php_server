<?php

class doctor_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $ci = get_instance();
        $ci->load->helper('miscellaneous_helper');
    }

    function doctorList($params = array()) {

        $params['whereCondition'] = " (is_doctor = 1) ";

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT doctor_profile.online_status, users.user_id,users.name, users.email,mobile,photo,city_master.name AS city, is_approved,users.created_at, user_wallet.email as wallet_email FROM users INNER JOIN user_profile ON users.user_id=user_profile.user_id LEFT JOIN doctor_profile ON users.user_id=doctor_profile.user_id LEFT JOIN city_master ON doctor_profile.city_id=city_master.city_id LEFT JOIN user_wallet ON user_wallet.user_id = users.user_id ";
        $mainQueryParams['groupBy'] = " users.user_id ";
        $mainQueryParams['defaultSorting'] = " users.user_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM users INNER JOIN user_profile ON users.user_id = user_profile.user_id LEFT JOIN doctor_profile ON users.user_id = doctor_profile.user_id LEFT JOIN city_master ON doctor_profile.city_id = city_master.city_id LEFT JOIN user_wallet ON user_wallet.user_id = users.user_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function doctorAnalytics($params = array()) {

        $params['whereCondition'] = " (is_doctor = 1) ";

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT users.user_id, users.name, users.mobile, users.email, COUNT(calls.call_id) as total_calls, SUM(tsd.doctor_charges) total_earning, COUNT(DISTINCT(calls.caller_id)) AS patient_count, list_category, doctor_profile.weightage, doctor_profile.extension FROM users INNER JOIN user_profile ON users.user_id=user_profile.user_id LEFT JOIN doctor_profile ON users.user_id=doctor_profile.user_id LEFT JOIN calls ON users.user_id = calls.receiver_id LEFT JOIN spends tsd ON tsd.call_id = calls.call_id ";
        $mainQueryParams['groupBy'] = " users.user_id ";
        $mainQueryParams['defaultSorting'] = " users.created_at DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results, COUNT(calls.call_id) as total_calls, SUM(tsd.doctor_charges) total_earning, COUNT(DISTINCT(calls.caller_id)) AS patient_count FROM users INNER JOIN user_profile ON users.user_id=user_profile.user_id LEFT JOIN doctor_profile ON users.user_id=doctor_profile.user_id LEFT JOIN calls ON users.user_id = calls.receiver_id LEFT JOIN spends tsd ON tsd.call_id = calls.call_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

    function patientsList($id) {
        $sql = "SELECT u.user_id, u.name, u.email FROM calls c LEFT JOIN users u ON u.user_id = c.caller_id WHERE receiver_id = $id GROUP BY c.caller_id ORDER BY c.call_id desc";
        $params = array();
        return $query = $this->db->query($sql, $params);
    }

    function doctorDetailById($id) {
        $sql = "SELECT u.user_id,u.name, email,mobile,photo,cm.name AS city,is_approved,u.created_at,medical_registration_no,total_experience,info,per_min_charges,sm.name as specialization,dp.other_info,dp.area FROM users u INNER JOIN user_profile up ON u.user_id=up.user_id LEFT JOIN doctor_profile dp ON u.user_id=dp.user_id LEFT JOIN city_master cm ON dp.city_id=cm.city_id LEFT JOIN doctor_specialization_rel sd on u.user_id=sd.user_id and sd.is_default=1 LEFT JOIN specialization_master sm on sd.specialization_id=sm.specialization_id WHERE u.user_id=" . $id;
        $params = array();
        return $query = $this->db->query($sql, $params);
    }

    function doctorSpecialityById($id) {

        $specQuery = "SELECT name AS specialization FROM specialization_master sm RIGHT JOIN doctor_specialization_rel sd ON sd.specialization_id = sm.specialization_id WHERE user_id = $id";
        $specResults = $this->db->query($specQuery, array());
        $specsArray = array();
        foreach ($specResults->result() as $specResult) {
            $specsArray[] = $specResult->specialization;
        }

        if (!empty($specsArray)) {
            return implode(', ', $specsArray);
        } else {
            return 'Not Mapped';
        }
    }

    function getQualifications($id) {
        $sql = "select dq.college AS university,dm.name as degree,dq.start_date,dq.end_date from doctor_qualifications dq INNER JOIN degree_master dm on dm.degree_id=dq.degree WHERE user_id = " . $id;
        $params = array();
        return $this->db->query($sql, $params);
    }

    function getExperience($id) {
        $sql = "select de.hospital_name AS hospital,de.designation_name as designation,de.join_date,de.end_date,de.is_current from doctor_workplace de WHERE user_id = " . $id;
        $params = array();
        return $query = $this->db->query($sql, $params);
    }

    function toggleApproved($data, $id) {
        return $this->db->update('doctor_profile', $data, array('user_id' => $id));
    }
    
    function toggleStatus($data, $id) {
        return $this->db->update('doctor_profile', $data, array('user_id' => $id));
    }    

    function totalDoctors($params = array()) {

        $sql = "SELECT COUNT(*) AS total_doctors FROM doctor_profile";

        if (isset($params['total_approved_doctors'])) {
            $sql .= ' WHERE is_approved = ' . $params['total_approved_doctors'];
        }

        $query = $this->db->query($sql);

        if ($query->num_rows == 1) {
            return $query->row()->total_doctors;
        }

        return 0;
    }

    function updateDoctorProfileColumn($params = array()) {

        $columnName = $params['columnName'];
        $columnValue = $params['columnValue'];
        $user_id = $params['user_id'];

        if (empty($user_id))
            return;

        if ($columnName == 'extension') {

            $getDoctor = $this->db->select('doctor_id')
                    ->where('extension', $columnValue)
                    ->get('doctor_profile');

            if ($getDoctor->num_rows > 0) {
                return 0;
            }
        }

        $sql = "UPDATE doctor_profile SET $columnName = $columnValue WHERE user_id = $user_id";

        return $this->db->query($sql);
    }

    function updateDoctorDetails($rowData = array()) {

        extract($rowData);

        $sql = "UPDATE doctor_profile SET $column_name = '$column_value' WHERE user_id = $user_id";

        return $this->db->query($sql);
    }

    function getExtension($id) {

        $sql = "SELECT extension FROM doctor_profile WHERE user_id = $id";

        $params = array();
        $query = $this->db->query($sql, $params);

        if ($query->num_rows == 1) {
            return $query->row()->extension;
        }

        return NULL;
    }

}

?>