<?php

class VerifyMobLog extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'verifymob_log';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'verifymoblog_id';

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }
}
