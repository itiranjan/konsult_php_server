<link rel="stylesheet" href="<?php echo(URL . '/assets/css/stylesheet.css'); ?>" />

<div class="login_bg">
    <?php echo LOADER; ?>
    <div id="login-box">
        <h1>Konsult Call Center</h1>
        <form name="form_login" id="form_login" action="verifylogin" method="post" class="login">
            <fieldset>  
                <legend>Enter Mobile Number</legend>
                <input type="text" id="p_mobile" name="mobile" class="mediuminput" value="91"/>
                <span id="user_does_not_exist" style="color: red; display: none;">User does not exist with given mobile number.</span>
            </fieldset>
            <input type="hidden" name="p_loginuser" id="p_loginuser" value="1"/>
            <input type="button" name="btn_login" id="btn_login" value="Get User Details" onclick="getUserInfo()" />
        </form>
        <br/>
        <form name="form_recharge" id="form_recharge" action="recharge" method="post" class="recharge">
            <fieldset>  
                <legend>Enter Promo Code</legend>
                <input type="text" id="p_promo_code" name="promo_code" class="mediuminput" value=""/>
                <p id="invalid_promo_code" style="color: red;"></p>
                <p id="valid_promo_code" style="color: green;"></p>
            </fieldset>
            <input type="hidden" name="p_recharge" id="p_recharge" value="1"/>
            <input type="button" name="btn_recharge" id="btn_recharge" value="Recharge" onclick="recharge()" />
        </form> 
        
        <br/>
        <form name="form_doctor_search" id="form_doctor_search" action="doctor_search" method="post" class="doctor_search">
            <fieldset>  
                <legend>Select Specialization</legend>
                
                <select name="specialization_id" id="specialization_id">
                    <option value="0">---Select Specialization---</option>
                    <?php foreach($specializations as $specialization): ?>
                        <option value="<?php echo $specialization->specialization_id; ?>"><?php echo $specialization->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </fieldset>
            <input type="hidden" name="p_doctor_search" id="p_doctor_search" value="1"/>
            <input type="button" name="btn_doctor_search" id="btn_doctor_search" value="Search Doctors" onclick="doctor_search()" />
        </form>        
        
    </div>   
</div> 

<div id="showUserInfo"></div>

<script type="text/javascript" src="<?php echo(JS . "/jquery.min.js"); ?>"></script>
<script>
    $(document).keypress(function (e) {
        if (e.which == 13) {
            getUserInfo();
        }
    });
    
    var error = 'Your request cannot be processed due to some technical problems. Please try after some time!';
    function getUserInfo() {
        
        document.getElementById("user_does_not_exist").style.display = 'none';
        
        document.getElementById("invalid_promo_code").innerHTML = '';
        document.getElementById("valid_promo_code").innerHTML = '';
        document.getElementById("p_promo_code").value = '';
        
        var p_mobile = document.getElementById("p_mobile").value;

        if (/^\d{10,12}$/.test(p_mobile)) {

        } else {
            alert("Invalid mobile number!")
            document.getElementById("p_mobile").focus();
            return false;
        }
        
        // Launch ajax request
        $.ajax({
            
            // The link we are accessing.
            url: '<?php echo URL; ?>/index.php/kcc/showUserInfo',
            // The type of request.
            type: 'POST',
            // The data we are sending.
            data: $("form").serialize(),
            // The type of data that is getting returned.
            dataType: 'html',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                
                if(data == 0) {
                    document.getElementById("user_does_not_exist").style.display = 'block';
                    document.getElementById("showUserInfo").innerHTML = '';
                    return;
                }

                document.getElementById("showUserInfo").innerHTML = data;
            },
            error: function () {
                alert(error);
            }
        });
        // Prevent default click
        return false;
    }
    
    function recharge() {
        
        document.getElementById("user_does_not_exist").style.display = 'none';
        
        var p_mobile = document.getElementById("p_mobile").value;

        if (/^\d{10,12}$/.test(p_mobile)) {

        } else {
            alert("Invalid mobile number!")
            document.getElementById("p_mobile").focus();
            return false;
        }
        
        var formData = $("form").serialize() + '&rechargeForm=1'
        
        // Launch ajax request
        $.ajax({
            
            // The link we are accessing.
            url: '<?php echo URL; ?>/index.php/kcc/showUserInfo',
            // The type of request.
            type: 'POST',
            // The data we are sending.
            data: formData,
            // The type of data that is getting returned.
            dataType: 'html',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                
                if(data == 0) {
                    document.getElementById("user_does_not_exist").style.display = 'block';
                    document.getElementById("showUserInfo").innerHTML = '';
                    return;
                }
                
                var dataParse = jQuery.parseJSON(data);

                if(dataParse.status == 1) {
                    document.getElementById("invalid_promo_code").innerHTML = '';
                    document.getElementById("valid_promo_code").innerHTML = dataParse.msg;
                    return;
                }
                else {
                    document.getElementById("valid_promo_code").innerHTML = '';
                    document.getElementById("invalid_promo_code").innerHTML = dataParse.msg;
                    return;
                }

                document.getElementById("showUserInfo").innerHTML = data;
            },
            error: function () {
                alert(error);
            }
        });
        // Prevent default click
        return false;
    } 
    
    function doctor_search() {
    
        var specialization_id = document.getElementById("specialization_id").value;
        
        var p_mobile = document.getElementById("p_mobile").value;

        if (/^\d{10,12}$/.test(p_mobile)) {

        } else {
            alert("Invalid mobile number!")
            document.getElementById("p_mobile").focus();
            return false;
        }        
        
        if(specialization_id <= 0) {
            alert("Please select specialization!")
            document.getElementById("specialization_id").focus();
            return false;
        }
        
        $.ajax({
            
            // The link we are accessing.
            url: '<?php echo URL; ?>/index.php/kcc/showUserInfo',
            // The type of request.
            type: 'POST',
            // The data we are sending.
            data: $("form").serialize(),
            // The type of data that is getting returned.
            dataType: 'html',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                if(data == 0) {
                    document.getElementById("user_does_not_exist").style.display = 'block';
                    document.getElementById("showUserInfo").innerHTML = '';
                    return;
                }

                document.getElementById("showUserInfo").innerHTML = data;
            },
            error: function () {
                alert(error);
            }
        });          
    }
    
    function connect_doc(patient_mobile, extension) {
        
        var getConfirmation = confirm("Are you sure that you want to connect this user to doctor.");
        if(!getConfirmation) {
            return;
        }        
        
        var user_id = <?php echo $this->session->userdata('user_id'); ?>;
        
        $.ajax({
            // The link we are accessing.
            url: 'callConnect',
            
            data: 'mobile='+patient_mobile+'&extension='+extension+'&kccDashboardCall=1'+'&user_id='+user_id,
            // The type of request.
            type: 'POST',
            // The data we are sending.
            //data: $("form").serialize(),
            // The type of data that is getting returned.
            dataType: 'json',
            beforeSend: function () {
                $('#loading').show();
            },
            complete: function () {
                $('#loading').hide();
            },
            success: function (data) {
                //var dataParse = jQuery.parseJSON(data);
                alert(data.msg);
            },
            error: function () {
                alert(error);
            }
        });        
    }         
</script>
 



