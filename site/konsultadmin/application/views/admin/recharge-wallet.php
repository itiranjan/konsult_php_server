<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
body{background: #FCFCFC;font-family: Arial,sans-serif;}
.form_section{border: 1px solid #ccc;
background-color: #fff;
text-align: left;
width: 450px;
margin: 50px auto 0;
padding: 20px;}
</style>
</head>

<body>
<div id="Sign-In" class="form_section">
	<h1 style="font-size: 16px;font-weight: bold;">Mechant Virtual Currency</h1>
    <form method="POST" action="javascript:void(0);" style="margin-top: 15px; padding-top:15px; border-top:1px #e5e4e2 solid;" onsubmit="makeRecharge();" id="mvc_recharge">
		<output style="font-size: 12px;font-weight: bold;">Enter Email-id</output>
		<input id="email" type="email" name="email" style="margin-left: 20px;" /><br/><br/>
		<output style="font-size: 12px;font-weight: bold;">Enter Mobile no</output>
		<input id="mobile" type="text" name="mobile" style="margin-left: 10px;"  /><br/><br/>
		<output style="font-size: 12px;font-weight: bold;">Enter Pass Key</output>
		<input id="password" type="text" name="password" style="margin-left: 10px;"  /><br/><br/>             <output style="font-size: 12px;font-weight: bold;">Recharge Amt</output>
		<input id="amount" type="text" name="amount" style="margin-left: 10px;"  /><br/><br/>
        <font color="red">(Clinic User: Maximum Allowed Limit INR 700/-)</font><br/>
        <font color="red">(Normal User: Maximum Allowed Limit INR 500/-)</font><br/>
		<input type="submit" value="Submit"/>
	</form>	
</body>
</html>

<script>
    function makeRecharge() { 
        
        if(!document.getElementById('email').value || !document.getElementById('mobile').value || !document.getElementById('amount').value || !document.getElementById('password').value) { 
            alert("All fields are required!!");
            return;
        }
        
        if(document.getElementById('amount').value > 3000) {
            alert("Amount can't be more than INR 3000/-");
            return;
        }
        
        var base_url = "<?php echo URL; ?>";
        $.ajax({
            'url' : base_url + "/index.php/recharge/wallet",
            'type' : 'POST',
            'data' : {
                format: 'json',
                'email' : document.getElementById('email').value,
                'mobile' : document.getElementById('mobile').value,
                'password' : document.getElementById('password').value,
                'amount' : document.getElementById('amount').value,
            },
            'success' : function(data) {  
                var returnedData = jQuery.parseJSON(data);
                alert(returnedData.message);
                if(returnedData.status == 1) {
                    location.reload(true);
                }
            },
            'error' : function(request,error)
            {
                alert("Whoops!! Something might be wrong. Please try again.");
            }
        }); 
    }
</script>

