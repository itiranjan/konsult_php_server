<?php

class AdminController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function getBalance() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            if (!empty($data['user_id'])) {
                $user_id = $data['user_id'];
            } elseif (!empty($data['email'])) {
                $user_id = User::where('email', '=', $data['email'])->pluck('user_id');
            } elseif (!empty($data['mobile'])) {
                $user_id = User::where('mobile', '=', $data['mobile'])->pluck('mobile');
            } else {
                $user_id = 0;
            }

            if (empty($user_id)) {
                $result ['status'] = 0;
                $result ['errors'] = 'User does not exist.';
            } else {
                $citrusHelper = $this->helper->load('citrus');

                $user_token = UserWallet::where('user_id', '=', $user_id)->pluck('token');
                if ($user_token) {
                    $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $user_token));
                    if (!empty($mainBalance['error'])) {
                        $result ['Main_Balance'] = $mainBalance;
                    } else {
                        $result ['Main_Balance'] = $mainBalance['value'];
                    }
                } else {
                    $result ['Main_Balance'] = 'Missing citrus auth token.';
                }

                $citrusModel = new UserWallet();
                $emailID = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'columnName' => 'email'));

                if ($emailID) {
                    $mvcBalance = $citrusHelper->fetchMVCBalance(array('emailId' => $emailID));
                    if (empty($mvcBalance['d'])) {
                        $result ['Promo_Balance'] = $mvcBalance;
                    } else {
                        $result ['Promo_Balance'] = $mvcBalance['mvcAmount'];
                    }
                } else {
                    $result ['Promo_Balance'] = 'Missing user email id.';
                }

                $result['status'] = 1;
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function debitAmount() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            if (!empty($data['user_id'])) {
                $user_id = $data['user_id'];
            } elseif (!empty($data['email'])) {
                $user_id = User::where('email', '=', $data['email'])->pluck('user_id');
            } elseif (!empty($data['mobile'])) {
                $user_id = User::where('mobile', '=', $data['mobile'])->pluck('mobile');
            } else {
                $user_id = 0;
            }

            if (empty($user_id)) {
                $result ['status'] = 0;
                $result ['errors'] = 'User does not exist.';
            } else {

                $citrusdata = array();
                $citrusdata['user_id'] = $user_id;
                $citrusdata['transactionId'] = '9636';
                $citrusdata['currency'] = 'INR';
                $citrusdata['amount'] = $data['amount'];
                $citrusdata['comment'] = 'Our records indicate that due to some technical glitch certain amounts were credited to your Citrus wallet. We have rectified the error and the amount has been debited now from your citrus wallet. Soon one of our executive will call you for the same. We thank you for your patience and continued support.';

                $citrusHelper = $this->helper->load('citrus');
                $debitMoney = $citrusHelper->debitRealMoney($citrusdata);

                $result['debitMoney'] = $debitMoney;
                $result['msg'] = "Amount debited successfully from doctor's wallet.";
                $result['status'] = 1;
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function debitVirtualAmount() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            if (!empty($data['user_id'])) {
                $user_id = $data['user_id'];
            } elseif (!empty($data['email'])) {
                $user_id = User::where('email', '=', $data['email'])->pluck('user_id');
            } elseif (!empty($data['mobile'])) {
                $user_id = User::where('mobile', '=', $data['mobile'])->pluck('mobile');
            } else {
                $user_id = 0;
            }

            if (empty($user_id)) {
                $result ['status'] = 0;
                $result ['errors'] = 'User does not exist.';
            } else {

                $citrusdata = array('amount' => $data['amount'], 'emailId' => $data['email'], 'mobile' => '', 'transactionId' => '8976556');

                $citrusHelper = $this->helper->load('citrus');
                $debitMoney = $citrusHelper->redeemVirtualMoney($citrusdata);

                $result['debitMoney'] = $debitMoney;
                $result['msg'] = "Amount debited successfully from wallet.";
                $result['status'] = 1;
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function creditAmount() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $input = Input::all();
            $payment_gateway_id = $input['payment_gateway_id'];

            if (empty($payment_gateway_id)) {
                return array('status' => 0, 'msg' => 'Missing payment gateway id!');
            }

            $response = array();
            if ($payment_gateway_id == 3) {
                $doctorDetails = array(
                    'jitu.lnmiit@gmail.com' => 5,
                );

                $citrusHelper = $this->helper->load('citrus');
                $citrusdata_doc = array();
                $citrusdata_doc['currency'] = 'INR';
                $citrusdata_doc['message'] = 'Sorry! Due to some Citrus Wallet issue certain amounts were not credited to your wallet. We have transferred the amount and same has been credited now.';

                foreach ($doctorDetails as $email => $amount) {
                    $citrusdata_doc['to'] = $email;
                    $citrusdata_doc['amount'] = $amount;
                    $pushMoney = $citrusHelper->pushMoney($citrusdata_doc);
                    $response[$email] = $pushMoney;
                }
            } elseif ($payment_gateway_id == 1) {

                $doctorDetails = array(
                    '9555840484' => 2,
                );

                foreach ($doctorDetails as $mobile => $amount) {

                    $autoDebitRequestData = array();
                    $autoDebitRequestData['amount'] = $amount;
                    $autoDebitRequestData['order_id'] = 'JUN_JULY_17_FAILURES_' . time();
                    $autoDebitRequestData['mobile'] = $mobile;

                    $paytmHelper = $this->helper->load('paytm');
                    $response[$mobile] = $cashbackStatus = $paytmHelper->salesToUserCredit($autoDebitRequestData);

                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_PAYTM_FAILURES');
                    if ($smsText) {
                        $smsParams = array();
                        $smsParams['Text'] = $smsText;
                        $smsParams['To'] = $mobile;
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                }
            }

            $result['response'] = $response;
            //$result['msg'] = "Amount credited successfully to doctor's wallet.";
            $result['status'] = 1;

            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function setDefaultPassword() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $data['newPWD'] = trim($data['newPWD']);
            $pwdLength = strlen($data['newPWD']);

            $user_id = $this->getUserIdbyToken($this->authToken);

            if (empty($user_id)) {
                $result ['status'] = 0;
                $result ['errors'] = 'User does not exist.';
            } else if ($pwdLength < 3) {
                $result ['status'] = 0;
                $result ['errors'] = 'Password length is short';
            } else {

                $encryptHelper = $this->helper->load('encrypt');

                $newPWD = ($data['newPWD']) ? $data['newPWD'] : '123456';

                $newPWD = $encryptHelper->encryptPWD(array('PWD' => $newPWD));

                User::where('user_id', '=', $user_id)->update(array('password' => $newPWD));

                $result ['msg'] = 'Password reset successfully!';
                $result ['status'] = 1;
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function createKccUser() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            //WHO CAN CALL THIS API
            $user_id = $this->getUserIdbyToken($this->authToken);
            $admin_id = DB::table('admin_users')->where('user_id', '=', $user_id)->pluck('admin_id');

            if (empty($admin_id)) {
                $result = array('status' => 0, 'msg' => 'Un-authorized access!');
                return Response::make($result, 200, $this->headers);
            }

            $admin_permission_id = DB::table('admin_permissions')->where('admin_id', '=', $admin_id)->where('permission_id', '=', 27)->pluck('admin_id');

            if (empty($admin_permission_id)) {
                $result = array('status' => 0, 'msg' => 'Un-authorized access!');
                return Response::make($result, 200, $this->headers);
            }
            
            $isValidName = preg_match('/(^[A-Za-z ]+$)+/', $data['fullName']);
            if (empty($isValidName)) {
                $result = array('status' => 0, 'msg' => 'Enter valid name!');
                return Response::make($result, 200, $this->headers);
            }            

            //CHECK FOR VALID MOBILE
            $mobile = trim($data['mobile']);
            if (empty($mobile)) {
                $result = array('status' => 0, 'msg' => 'Mobile number is missing!');
                return Response::make($result, 200, $this->headers);
            }

            $isValidMobile = preg_match('/^\+[1-9]{1}[0-9]{3,14}$/', $mobile);
            if (empty($isValidMobile)) {
                $result = array('status' => 0, 'msg' => 'Mobile number is not valid!');
                return Response::make($result, 200, $this->headers);
            }

            //CHECK FOR THE USER
            $userModel = new User();
            $mobile = str_replace('+', '', $mobile);
            $userId = $userModel->isUserExist(array('mobile' => $mobile));

            if (!empty($userId)) {
                $result = array('status' => 0, 'msg' => 'User is already exist with this mobile number!');
                return Response::make($result, 200, $this->headers);
            }

            //CREATE USER
            if (empty($userId)) {
                $userId = $userModel->createKccUser(array('mobile' => $mobile, 'name' => $data['fullName']));
            }
            
            $result['user_id'] = $userId;
            $result['status'] = 1;
            $result['msg'] = 'User has been created successfully!';

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
