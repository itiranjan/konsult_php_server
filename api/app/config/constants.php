<?php

return [
    //Email Logo URL
     'LOGO' => 'http://api.konsultapp.com/images/konsult_app_icon_96.png',
    //use for encryption and decryption data.
    'PRIVATE_KEY' => 'dsajhfbiulsi3792bhwjef9302ur',
    
    //use for solr
   //'SOLR_URL' => 'http://192.99.169.232:8983/solr/collection1/', //konsult dev
    'SOLR_URL' => 'http://52.74.211.110:8983/solr/collection1/',  // konsult app client
	//'SOLR_URL' => 'http://localhost:8983/solr/collection1/',			
    //'SOLR_URL'=>'http://localhost:8983/solr/collection1/',
    
    //use for KNOWLARITY 
//    'KNOWLARITY' => 'http://etsdom.kapps.in/webapi/konsult/api/konsult_c2c.py?',
//	'KNOWLARITYINT' => 'http://etsdom.kapps.in/webapi/konsult/api/konsult_c2c_usa.py?',
    //'KNOWLARITYINT' => 'http://etsrds.kapps.in/webapi/konsult/api/konsult_c2c_usa_op.py?',
    'KNOWLARITYINT' => 'http://etsrds.kapps.in/webapi/konsult/api/konsult_c2c_usa_op_test.py?',
    'KNOWLARITY_AUTH_KEY' => '6842c1ee-ac05-4e90-8b6f-0d40e40240c7',
    'OPERATOR_NUMBER' => '+919999016769',
    'KNOWLARITY_KCC_DASHBOARD' => 'http://etsrds.kapps.in/webapi/konsult/api/konsult_c2c_usa_ib_test.py?',   
    
    //    'KNOWLARITYINT' => 'http://etsrds.kapps.in/webapi/konsult/api/konsult_c2c_13010_mobileapp.py?', //DEV SERVER
//    'KNOWLARITY_AUTH_KEY' => 'FdhcGUFVDgv_BHDXVCTYcvsc_YYCDdshy',//DEV SERVER    
    
    //'SANDBOX_CITRUS' => 'https://sandboxadmin.citruspay.com',
    //'PRODUCTION_CITRUS' => 'https://admin.citruspay.com',
    'CITRUS_URL'=>'https://admin.citruspay.com',
    'CITRUS_BALANCE' => '/service/v2/mycard',
    'CITRUS_PAY' => '/service/v2/prepayment/pay',
    'CITRUS_REFUND' => '/service/v2/prerefund/refund',
	'CITRUS_TRANSFER' => '/service/v2/prepayment/transfer',
    'CITRUS_AUTHTOKEN_PREFIX' => 'Bearer ',
    'CITRUS_ACCESS_KEY' => 'MQ526OM21YGFL8OW0Z10', ///merchantAccessKey
    'CITRUS_SECRET_KEY' => 'd098eb1e880a01e419fac438061c45c1adee01c8',
    'CITRUS_PREFIX' => 'KTNL',    //For Development : KTND & For Live : KTNL
    
    'KONSULT_AUTHTOKEN' =>   '639a30b5-3bd9-45ff-90ff-ce5fc717a60d',//'20510358-c7a8-4028-9158-9a00b6c094b7',
	//'KONSULT_AUTHTOKEN' =>'78365a92-79a3-4bb5-aada-1c4b1f405fff',
    
    'PAYTM_oauthUrl' => 'https://accounts.paytm.com/',
    'PAYTM_walletUrl' => 'https://trust.paytm.in/',
    'PAYTM_transactionUrl' => 'https://secure.paytm.in/oltp',
    'PAYTM_addMoneyUrl' => 'https://secure.paytm.in/oltp-web/processTransaction',
    'PAYTM_clientId' => 'merchant-konsult-app',
    'PAYTM_clientSecret' => '283e7c87-56b7-40b3-b365-c01d29f33183',
    'PAYTM_authorization' => 'Basic bWVyY2hhbnQta29uc3VsdC1hcHA6MjgzZTdjODctNTZiNy00MGIzLWIzNjUtYzAxZDI5ZjMzMTgz', //base64_encode("merchant-konsult-app:283e7c87-56b7-40b3-b365-c01d29f33183");
    'PAYTM_MID' => 'KonApp23243453392052',
    'PAYTM_MERCHANT_KEY' => '8IUwJXxXxz%W0J5!',
    'PAYTM_CALLBACK_URL' => 'http://konsult.com',
    'PAYTM_AppIP' => '52.77.103.165',
    'PAYTM_GRATIFICATION_MID' => 'KONSUL52043108868609',
    'PAYTM_AESKEY_MerchantKey' => 'VgHbz9q%GokXSTxH', //GRATIFICATION API
    'PAYTM_MerchantGuid' => '6cf3dfde-1e7c-43fe-8c14-b498585f27bd', //GRATIFICATION API
    'PAYTM_MARKETING_DEALS_SalesWalletGuid' => '3fcdb22f-cc39-4a7b-9b10-bb02bb7a7e3d',//'DDD999F03B9911E7A0E51866DA858C62',
    'PAYTM_INDUSTRY_TYPE_ID' => 'Retail106',
    'PAYTM_CHANNEL_ID' => 'WAP',
    'PAYTM_WEBSITE' => 'KonAppWAP',
    'PAYTM_PaymentMode' => 'PPI',
    'PAYTM_AuthMode' => 'USRPWD',
    'PAYTM_platformName' => 'payTM',
    'PAYTM_REQUESTTYPE_ADD_MONEY' => 'ADD_MONEY',
    'PAYTM_REQUESTTYPE_WITHDRAW' => 'WITHDRAW',
    'PAYTM_operationType' => 'SALES_TO_USER_CREDIT',
    'PAYTM_currency' => 'INR',
    'PAYTM_salesWalletName' => 'Konsult App Gratification',

//    'PAYTM_oauthUrl' => 'https://accounts-uat.paytm.com/',
//    'PAYTM_walletUrl' => 'https://trust-uat.paytm.in/',
//    'PAYTM_transactionUrl' => 'https://pguat.paytm.com/oltp',
//    'PAYTM_addMoneyUrl' => 'https://pguat.paytm.com/oltp-web/processTransaction',
//    'PAYTM_clientId' => 'merchant-konsult-staging',
//    'PAYTM_clientSecret' => '760277b1-78fc-40a7-b6aa-7c767e1903d0',
//    'PAYTM_authorization' => 'Basic bWVyY2hhbnQta29uc3VsdC1zdGFnaW5nOjc2MDI3N2IxLTc4ZmMtNDBhNy1iNmFhLTdjNzY3ZTE5MDNkMA==', //base64_encode("merchant-konsult-staging:760277b1-78fc-40a7-b6aa-7c767e1903d0");
//    'PAYTM_MID' => 'KONSUL77756565905486',
//    'PAYTM_MERCHANT_KEY' => 'ug8Y@_FkHQXyq6&i',
//    'PAYTM_CALLBACK_URL' => 'http://konsultapp.com',
//    'PAYTM_AppIP' => '127.0.0.1',
//    'PAYTM_AESKEY_MerchantKey' => 'J9r@&&Qo9cYe@u8J', //GRATIFICATION API
//    'PAYTM_MerchantGuid' => '61e46803-3f18-46de-926f-748f38962d8a', //GRATIFICATION API
//    'PAYTM_MARKETING_DEALS_SalesWalletGuid' => '1baea01c-86b7-4dea-98ec-8c05c96c0b9f',
//    'PAYTM_INDUSTRY_TYPE_ID' => 'Retail',
//    'PAYTM_PAYTM_CHANNEL_ID' => 'WAP',
//    'PAYTM_WEBSITE' => 'APP_STAGING',    
//    'PAYTM_PaymentMode' => 'PPI',
//    'PAYTM_AuthMode' => 'USRPWD',
//    'PAYTM_platformName' => 'payTM',
//    'PAYTM_REQUESTTYPE_ADD_MONEY' => 'ADD_MONEY',
//    'PAYTM_REQUESTTYPE_WITHDRAW' => 'WITHDRAW',
//    'PAYTM_operationType' => 'SALES_TO_USER_CREDIT',
//    'PAYTM_currency' => 'INR',    
//    'PAYTM_salesWalletName' => '',               
			
	//CITRUS VIRTUAL CRRENCY
	'CITRUS_VIRTUAL_URL'=>'https://coupons.citruspay.com/cms_api.asmx',
	'CITRUS_VIRTUAL_SANDBOX_URL'=>'https://sandboxcoupons.citruspay.com/cms_api.asmx',
	'Issue_Virtual_Currency'=>'/IssueCoupons',
	'Get_Balance'=>'/FetchCampaignBalance',
	'Redeem_Virtual_Currency'=>'/RedeemCampaign',
	'Rollback_Redemption'=>'/RollbackCampaign',
	
	'Content_Type'=>'application/json',
	'Merchant_Id'=>'haovrnrhhn',
	'Compaign_Code'=>'KONSULTCASH',
	'Parterner_Id'=>'haovrnrhhn',
	'PassWord'=>'MQ526OM21YGFL8OW0Z10',
    
    // use for pagination
    'DR_LIMIT' => '10',
    
    // use for email verification
    'varification_key' => 'Konsult123',
    
    //use for call api
    'call_info_key' => 'cc03e747a6afbbcbf8be7668acfebee5',
    'MIN_CALL_DURATION' => 10, // 15 seconds
    'KONSULT_PERCENTAGE' => 30, // In percentege
    'INSTITUTE_PERCENTAGE' => 0, // In percentege
    //
    //PUSH GCM API URL & KEY Configuration URL and Query String
    'PUSH_GCMAPI_URL' => 'https://android.googleapis.com/gcm/send',
    'PUSH_GCMAPI_APIKEY' => 'AIzaSyD2QzMvd1uNVQPHpxclygvcDnCPuPnQ4u4',
    
    //PUSH APNS API URL & KEY Configuration URL and Query String
    'PUSH_APNSAPI_POST' => '2195',
    'PUSH_APNSAPI_URL_SANDBOX' => 'gateway.sandbox.push.apple.com', //SANDBOX DETAIL
    'PUSH_APNSAPI_CERT_SANDBOX' => '/cert/Certificates.pem', //SANDBOX DETAIL
    'PUSH_APNSAPI_URL' => 'ssl://gateway.push.apple.com:2195', //LIVE DETAIL
    //'PUSH_APNSAPI_CERT' => '/cert/Prod_Certificates.pem',   //LIVE DETAIL
	//'PUSH_APNSAPI_CERT' => '/var/www/html/konnect/api/public/kon.pem',   //LIVE DETAIL
	'PUSH_APNSAPI_CERT' => 'apn.pem',   //local DETAIL
    
    //SENDGRID - MAIL SERVER Configuration URL, PARAMETER etc
    'SENDGRID_URL' => 'https://api.sendgrid.com/',
    'SENDGRID_PARAMETER'=>'api/mail.send.json',
    'SENDGRID_API_USERNAME' => 'konsultapp123',
    'SENDGRID_API_PASSWORD' => 'Kon$ult@123',
    'FROM_EMAIL' => 'support@konsultapp.com',
    'FROM_NAME' => 'Konsult App',
    'NO_REPLY' => 'support@konsultapp.com', 
		
	//other
	'MainUrl' => 'http://api.konsultapp.com//photo/',
	//otherffsdfsdf
    
    'LARAVEL_INFO_LOG' => 1,
    'LARAVEL_MAX_LOG_SIZE' => 10,
    'PROMO_CODE_FOR_CAMPAIGN' => 'PROMOCODECOMPAIGN300',
    'MAX_CALL_DURATION_WHEN_PER_MIN_ZERO' => 15,
    'MAX_CALL_DURATION_CAPPING' => 900,//IN SECONDS
    'MAX_CALL_DURATION_CAPPING_FOR_FIX_CALL_CHARGES' => 720,//IN SECONDS
    'INTERNATIONAL_CALL_CHARGE_PER_MIN_RULES'=>2.5,
    'INTERNATIONAL_CALL_CHARGE_CURRENCY_FACTOR'=>65,
    'ENABLED_FIX_CHARGES_PER_CALL_FOR_INTERNATIONAL' => 1,
    'FIX_CHARGES_PER_CALL_TOTAL_MINS' => 5,
    
    'NETCORE_SMS_URL' => 'http://bulkpush.mytoday.com/BulkSms/SingleMsgApi',
    'NETCORE_SMS_USERNAME' => 8447441780,
    'NETCORE_SMS_PASSWORD' => 'KonSMSPlan@#$2017',
    'NETCORE_SMS_FEEDID' => 356453,
    'APP_DOWNLOAD_URL' => 'http://bit.ly/1Rtq6VC',
    
    'THRESHOLD_CHARGES_KONSULT_CLINIC' => 40,//40,
    'CHARGES_DEDUCTION_CRITERIA' => 'SECOND',//MINUTE//SECOND
    
    'KNOWLARITY_DOWN' => 0,//900
    'CITRUS_DOWN' => 0,//900
    'DATABASE_SERVER_DOWN' => 0,//900  
    
    'DEFAULT_OTP_LENGTH' => 4,
    
    'INFO_POPUP_TITLE' => 'Info Title!',
    'INFO_POPUP_BODY' => 'Here the info body will come.',
    
    'twilio_enabled' => 1,
    'twilio_account_sid' => 'ACa2669286e8ed8aeed4f02428cf46de71',
    'twilio_auth_token' => '0f2e13b2af67afdec3451c9c7fc093af',
    'twilio_number' => '+12516168656',
    'twilio_api_key' => 'SK34c3837cae017f8a94be3f2b62a1c91e',
    'twilio_secret_key' => '9r3TfvAkFPN4YfOrdGf7fJPg4VgNrtKD',
    'twilio_outgoing_sid' => 'APf76c17fbde5a83c9310e1a2ab7116538',
    
    'paypal_accessToken_sandbox' => 'access_token$sandbox$dm6c5ndhfdt6pbtm$258039328fca523b4cf2bc8132a4f641',    'paypal_accessToken' => 'access_token$production$8kq75rdyrbzp5j3q$4e1a01c7665ad513f37b859b5c19b551',
    
    'NETCORE_SMS_TEXT_APP_REGISTRATION_OFFER' => 'Dear <User>, thank you for registering with Konsult App. Now you can connect with hundreds of highly qualified doctors over call. Simply create WALLET and get first consultation free.',    
    
    'NETCORE_SMS_TEXT_APP_REGISTRATION' => 'Dear <User>, thank you for registering with Konsult App. Now you can connect with hundreds of highly qualified doctors over call.',
    
    'NETCORE_SMS_TEXT_RECHARGE_WALLET' => 'Dear <User>, your wallet has been successfully recharged by <Value1>. Your total available balance is <Value2>.',
    
    'NETCORE_SMS_TEXT_CALL_DEBIT_PATIENT' => 'Dear <User>, your last call charges with Dr. <Doctor Name> were <Value1>. Your total available balance is <Value2>.',
    
//    'NETCORE_SMS_TEXT_CALL_CREDIT_DOCTOR' => 'Dear Dr. <Doctor Name>, you just spoke to <User> and your wallet has been credited by INR <Value1>. Your total available balance is INR <Value2>.',
    
    'NETCORE_SMS_TEXT_CALL_CREDIT_DOCTOR' => 'Dear Dr. <Doctor Name>, you just spoke to <User> and your wallet has been credited by INR <Value1>.',
    
    'NETCORE_SMS_TEXT_MISSED_ALERT_PATIENT' => 'Dear <User>, it seems that Dr. <Doctor Name> is unavailable to take calls right now. We have informed the doctor, please try again later.',    
    
    'NETCORE_SMS_TEXT_MISSED_ALERT_DOCTOR' => 'Dear Dr. <Doctor Name>, you just missed a call from <User>. We have asked the patient to call you again later.',
    
    'NETCORE_SMS_TEXT_DOC_APPROVAL' => 'Dear Dr. <Doctor Name>, welcome to Konsult App, your doctor registration has been successfully approved.',	
    
    'NETCORE_SMS_TEXT_GETAPPLINK' => 'Consult and share medical reports with your doctor from the comfort of your home. Download Konsult App from <URL>',
    
    'NETCORE_SMS_TEXT_CALLBACK_PATIENT_INSUFFICIENT_BALANCE' => "Dear Konsult Customer, <DOCTOR_NAME> tried to call you but couldn't connect as your Konsult Wallet balance is low. Kindly recharge your Wallet and contact doctor again.",
    
    'NETCORE_SMS_TEXT_CALLBACK_PATIENT_MISSED_DOCTOR_CALL' => "Dear Konsult Customer, <DOCTOR_NAME> tried to call you but couldn't talk since you were unavailable. Kindly contact doctor again.",   

    'NETCORE_SMS_TEXT_DOCTOR_CALLBACK_REQUEST' => 'Dear <DOCTOR_NAME>, <PATIENT_NAME> tried calling you for consultation. You can call him back by accessing the callback request from the app home page. Charges will be deducted from patient wallet.',        

    'NETCORE_SMS_TEXT_MOBILE_OTP' => '<OTP> is your OTP for signing into Konsult. It will expire in 15 minutes.',
    
    'NETCORE_SMS_TEXT_PAYTM_CASHBACK' => 'Ta-dah! You have received cashback of INR <amount> in your paytm wallet against your last call on Konsult.',
    
    'NETCORE_SMS_TEXT_PAYTM_FAILURES' => 'Sorry! Due to Paytm Wallet issue, certain consultation amounts were not credited to your wallet. We have transferred the amount and same has been credited now.',
    
    'SMS_FIRST_COMMUNICATION' => 'Hi <NAME>, a new patient <PATIENT_NUMBER_NAME> case has been assigned to you. Please, establish the first communication and update the case within <TIME>',
    

];

