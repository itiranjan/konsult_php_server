<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('parseFilters')) {

    function parseQuery($params = array()) {

        $query = $params['baseQuery'];

        $where = '';
        if (!empty($params['filter']) || !empty($params['whereCondition'])) {
            $where = parseFilters($params);
        }
        $query .= $where;
        
        if(!empty($params['groupBy'])) {
            $query .= " GROUP BY ".$params['groupBy'];
        }

        if (!empty($params['sort']) && isset($params['sort'][0])) {
            $field = str_replace('$', '.', $params['sort'][0]['field']);
            $query .= " ORDER BY " . $field . " " . $params['sort'][0]['dir'];
        } elseif (!empty($params['defaultSorting'])) {
            $query .= " ORDER BY " . $params['defaultSorting'];
        }

        if (!empty($params['take']) && isset($params['skip'])) {
            $query .= "  LIMIT " . $params['skip'] . ',' . $params['take'];
        }

        return $query;
    }

    function parseFilters($params, $count = 0) {

        $filters = !empty($params['filter']) ? $params['filter'] : array();

        $where = "";
        $intcount = 0;
        $noend = false;
        $nobegin = false;

        if (isset($filters['filters'])) {
            $itemcount = count($filters['filters']);
            if ($itemcount == 0) {
                $noend = true;
                $nobegin = true;
            } elseif ($itemcount == 1) {
                $noend = true;
                $nobegin = true;
            } elseif ($itemcount > 1) {
                $noend = false;
                $nobegin = false;
            }
            foreach ($filters['filters'] as $key => $filter) {

                $filter['field'] = str_replace('$', '.', $filter['field']);

                if (isset($filter['field'])) {
                    switch ($filter['operator']) {
                        case 'startswith':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "%' ";
                            break;
                        case 'contains':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = " '%" . $filter['value'] . "%' ";
                            break;
                        case 'doesnotcontain':
                            $compare = " NOT LIKE ";
                            $field = $filter['field'];
                            $value = " '%" . $filter['value'] . "%' ";
                            break;
                        case 'endswith':
                            $compare = " LIKE ";
                            $field = $filter['field'];
                            $value = "'%" . $filter['value'] . "' ";
                            break;
                        case 'eq':
                            $compare = " = ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'gt':
                            $compare = " > ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'lt':
                            $compare = " < ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'gte':
                            $compare = " >= ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'lte':
                            $compare = " <= ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                        case 'neq':
                            $compare = " <> ";
                            $field = $filter['field'];
                            $value = "'" . $filter['value'] . "'";
                            break;
                    }
                    if ($count == 0 && $intcount == 0) {
                        $before = "";
                        $end = " " . $filters['logic'] . " ";
                    } elseif ($count > 0 && $intcount == 0) {
                        $before = "";
                        $end = " " . $filters['logic'] . " ";
                    } else {
                        $before = " " . $filters['logic'] . " ";
                        $end = "";
                    }
                    $where .= ( $nobegin ? "" : $before ) . $field . $compare . $value . ( $noend ? "" : $end );
                    $count ++;
                    $intcount ++;
                } else {
                    $where .= " ( " . parseFilters($filter, $count) . " )";
                }
                $where = str_replace(" or  or ", " or ", $where);
                $where = str_replace(" and  and ", " and ", $where);
            }
        }

        if (!empty($where) && empty($params['whereCondition'])) {
            $where = " WHERE " . $where;
        } elseif (!empty($where) && !empty($params['whereCondition'])) {
            $where = " WHERE " . $where . " AND " . $params['whereCondition'];
        } elseif (empty($where) && !empty($params['whereCondition'])) {
            $where = " WHERE " . $params['whereCondition'];
        }

        return $where;
    }

}