<?php
/**
 * Template Name: Template New Default
 *
 * @package Betheme
 * @author Muffin Group
 */
get_header(newtemp);
?>
	<!-- #Content -->
	<div id="Content">
		<div class="content_wrapper clearfix">
	
			<!-- .sections_group -->
			<div class="sections_group">
				<?php 
					while ( have_posts() ){
						the_post();							// Post Loop	
						mfn_builder_print( get_the_ID() );	// Content Builder & WordPress Editor Content
					}
				?>
			</div>
			
			<!-- .four-columns - sidebar -->
			<?php get_sidebar(); ?>
	
		</div>
	</div>
	
	<?php //do_action( 'mfn_hook_content_after' ); ?>
	
	<?php// do_action( 'mfn_hook_bottom' ); ?>

<!-- wp_footer() -->
<?php get_footer(); ?>
