function KendoGrid_FixFilter(kendoDataSource, kendoFilter, depth) {

    if ((!kendoDataSource) || (!kendoFilter))
        return;

    if (!depth)
        depth = 0;
    // console.log(depth + " - FixDatesInFilter:" + JSON.stringify(kendoFilter));

    $.each(kendoFilter.filters, function (idx, filter) {
        //console.log("filter = " + idx + " = " + JSON.stringify(filter));

        if (filter.hasOwnProperty("filters")) {
            depth++;
            KendoGrid_FixFilter(kendoDataSource, filter, depth)
        }
        else {
            $.each(kendoDataSource.schema.model.fields, function (propertyName, propertyValue) {
                if (filter.field == propertyName && propertyValue.type == 'date') {
                    filter.value = kendo.toString(filter.value, "yyyy-MM-dd HH:mm:ss");
                }
            });
        }
    });
}            