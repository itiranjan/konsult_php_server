<?php

class CallController extends ApiBaseController {

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function callmvc() { 

        $data = Input::all();
        $twilioHelper = $this->helper->load('twilio');
        
        //MAINTENANCE CHECK
        $maintainance = array();
        $maintainance['knowlarity_down'] = Config::get('constants.KNOWLARITY_DOWN');
        $maintainance['citrus_down'] = Config::get('constants.CITRUS_DOWN');
        $maintainance['database_server_down'] = Config::get('constants.DATABASE_SERVER_DOWN');
        foreach ($maintainance as $key => $value) {
            if (!empty($value)) {
                
                $errorMsg = "Apologies! Server is down for maintenance. Please try again later.";
                if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                    $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => $errorMsg));
                    return Response::make($twiml, 200, $this->headers);
                } else {
                    $result['status'] = $value;
                    $result['error_type'] = "$key";
                    $result['error_msg'] = $errorMsg;
                    return Response::make($result, 200, $this->headers);
                }
                
            }
        }

        try {
            
            //TWILIO VOIP CALL CONDITION
            if(!empty($data['voipCall'])) {
                $user_id = $data['patient'];
            } else {
                $checkAuthToken = True;
                $result = $this->checkAuth($checkAuthToken);
                if ($result['config_status'] == -1) {
                    return ($result);
                } elseif ($result['config_status'] == 0) {
                    $this->setHeader('RESPONSE_CODE', 0);
                    return Response::make($result, 200, $this->headers);
                }

                $data['patient'] = $user_id = $this->getUserIdbyToken($this->authToken);
            }
            
            if (empty($data['patient']) || empty($data['doctor']) || $data['patient'] == $data['doctor']) {
                $errorMsg = "You can't be your own doctor!";
                if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                    $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => $errorMsg));
                    return Response::make($twiml, 200, $this->headers);
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1040;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = $errorMsg;
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }
            
            $user = new User();
            if ($user->validateCallRequest($data)) {
                
                //VARIABLE INTIALIZATION FOR INTERNATIONAL CALL OF FIXED MINS CONCEPT
                $fixedMinsCall = 0;
                
                $this->setHeader('AUTH_TOKEN', $this->authToken);

                $blockId = UserBlock::whereRaw('blocker_id = ' . $data['doctor'] . ' AND ' . 'blocked_id = ' . $user_id)->pluck('block_id');

                if (!empty($blockId)) {
                    
                    $errorMsg = "You have been blocked by this doctor.";
                    if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                        $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => $errorMsg));
                        return Response::make($twiml, 200, $this->headers);
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1040;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = $errorMsg;
                        $result['errors'] = $error;
                        return Response::make($result, 200, $this->headers);
                    }
                }

                $user = UserProfile::where('user_id', '=', $user_id)->select('clinic_type', 'photo')->first();

                $user_detail = User::where('user_id', '=', $user_id)->select('mobile', 'user_id', 'name')->first();

                $patientMob = $user_detail['mobile'];
                $locale = isset($data['locale']) ? $data['locale'] : '';
                $callTypeData = User::getCallTypeData(array('mobile' => $patientMob, 'locale' => $locale));
                $domesticCall = !$callTypeData['internationalCall'];

                $doctorRateInfo = Doctor::where('user_id', '=', $data['doctor'])->select('per_min_charges', 'fix_call_charges')->first();

                $per_min_charges = $doctorRateInfo['per_min_charges'];
                if (!$domesticCall) {
                    $per_min_charges = $per_min_charges * (floatval($callTypeData['international_per_min_rules']));
                    $per_min_charges = round($per_min_charges, 2);
                }

                $communicationHelper = $this->helper->load('communication');
                $placeCallflag = 0;

                $thresholdChargesKonsultClinic = Config::get('constants.THRESHOLD_CHARGES_KONSULT_CLINIC');

                $fix_call_charges = 0;
                if ($user['clinic_type'] == 2) {
                    $per_min_charges = 0;
                    $fix_call_charges = $doctorRateInfo['fix_call_charges'];
                }

                $doctorPerMinCharges = ($user['clinic_type'] == 1 && $per_min_charges > $thresholdChargesKonsultClinic) ? $thresholdChargesKonsultClinic : $per_min_charges;

                $minAmountForCall = ($user['clinic_type'] == 2) ? $fix_call_charges : $doctorPerMinCharges;

                $user['photo'] = User::getPhoto(array('photo' => $user['photo'], 'is_doctor' => 0));

                $httpHelper = $this->helper->load('http');
                $wallet_id = !empty($data['payment_gateway_id']) ? $data['payment_gateway_id'] : 3;
                if ($domesticCall) {

//                    //USER MUST BE LOGIN IN WALLET
//                    $walletLogin = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', $wallet_id)->pluck('login');
//                    if (empty($walletLogin)) {
//                        $this->setHeader('RESPONSE_CODE', '0');
//                        $result['status'] = 0;
//                        $result['msg'] = "Login to wallet first!";
//                        $result['errors'] = "User is not logged in wallet!";
//                        return Response::make($result, 200, $this->headers);
//                    }

                    $walletTokenParams = array('user_id' => $user_id, 'wallet_id' => $wallet_id, 'columnName' => 'token');

                    if ($wallet_id == 1) {
                        $walletTokenParams = array_merge($walletTokenParams, array('checkExpiry' => 1));
                    }

                    $citrusModel = new UserWallet();

                    $user_token = $citrusModel->getWalletInfoColumn($walletTokenParams);

                    if ($wallet_id == 1 && (empty($user_token['status']) || $user_token['status'] == 2)) {
                                            
                        if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                            $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => "There is some issue with wallet. Please try again after login to your wallet."));
                            return Response::make($twiml, 200, $this->headers);
                        } else {

                            $result = array();
                            $result['status'] = $user_token['status'];
                            $result['msg'] = ($user_token['status'] == 2) ? 'Token expired!' : 'Missing token';
                            return Response::make($result, 200, $this->headers);
                        }
                    } elseif (empty($user_token)) {
                        
                        if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                            $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => "There is some issue with wallet. Please try again after login to your wallet."));
                            return Response::make($twiml, 200, $this->headers);
                        } else {
                        
                            $this->setHeader('RESPONSE_CODE', '0');
                            $error[] = 1040;
                            $error = $this->getError($error);
                            $result['status'] = 0;
                            $result['msg'] = Config::get('config_msg.INVAILD_CITRUS_AUTHTOKEN');
                            $result['errors'] = $error;
                            return Response::make($result, 200, $this->headers);
                        }
                    }

                    if ($wallet_id == 1) {
                        $paytmHelper = $this->helper->load('paytm');
                        $userTotalBalance = $paytmHelper->walletWebCheckBalance(array('ssotoken' => $user_token['token'], 'getBalanceOnly' => 1));

                        if (is_array($userTotalBalance) && isset($userTotalBalance['status'])) {
                            
                            if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                                $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => "There is some issue with wallet. Please try again after login to your wallet."));
                                return Response::make($twiml, 200, $this->headers);
                            } else {

                                $this->setHeader('RESPONSE_CODE', '0');
                                $result['status'] = $userTotalBalance['status'];
                                $result['msg'] = "Unable to fetch balance!";
                                $result['errors'] = $userTotalBalance['response'];
                                return Response::make($result, 200, $this->headers);
                            }
                        }
                    } else {
                        $citrusHelper = $this->helper->load('citrus');
                        $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $user_token));

                        if (!empty($mainBalance['error'])) {
                            
                            if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                                $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => "There is some issue with wallet. Please try again after login to your wallet."));
                                return Response::make($twiml, 200, $this->headers);
                            } else {
                                $this->setHeader('RESPONSE_CODE', '0');
                                $result['status'] = 0;
                                $result['msg'] = $mainBalance['errorMsg'];
                                $result['errors'] = $mainBalance;
                                return Response::make($result, 200, $this->headers);
                            }
                        }

                        $emailID = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'columnName' => 'email'));

                        $mvcBalance = $citrusHelper->fetchMVCBalance(array('emailId' => $emailID));
                        if (empty($mvcBalance['d'])) {
                            
                            if(Config::get('constants.twilio_enabled') && !empty($data['voipCall'])) {
                                $twiml = $twilioHelper->errorMsgInVoip(array('errorMsg' => "There is some issue with wallet. Please try again after login to your wallet."));
                                return Response::make($twiml, 200, $this->headers);
                            } else {
                                $this->setHeader('RESPONSE_CODE', '0');
                                $result['status'] = 0;
                                $result['msg'] = $mvcBalance['errorMsg'];
                                $result['errors'] = $mvcBalance;
                                return Response::make($result, 200, $this->headers);
                            }
                        }

                        $userTotalBalance = $totbal = $mvcBalance['mvcAmount'] + $mainBalance['value'];
                    }
                } else {
                    
                    //CHECK IF MINS ARE AVAILABLE
                    $patDocMapping = PatDocMinInfo::where('patient_user_id', '=', $user_id)->where('doctor_user_id', '=', $data['doctor'])->first();
                    if (!empty($patDocMapping) && !empty($patDocMapping->min_info_id) && $patDocMapping->available_seconds >= 60) {
                        $fixedMinsCall = 1;
                    }
                    
                    $kccTransaction = new KccTransaction();
                    $userTotalBalance = $kccBalance = $kccTransaction->getBalance(array('user_id' => $user_id));
                }

                $enabled_fix_charges_per_call_for_international = Config::get('constants.ENABLED_FIX_CHARGES_PER_CALL_FOR_INTERNATIONAL');
                if($enabled_fix_charges_per_call_for_international && !$domesticCall) {
                    $fix_call_charges = $minAmountForCall = $minAmountForCall * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                }
                
                if ($minAmountForCall <= $userTotalBalance || $fixedMinsCall) {

                    $user_app = DeviceInfo::where('user_id', '=', $data['doctor'])->select('platform_id', 'registartion_id')->first();

                    $rel_exist = DoctorPatientRelation::whereRaw('(doctor = ' . $data['doctor'] . ' and patient = ' . $user_id . ') or (patient = ' . $data['doctor'] . ' and doctor = ' . $user_id . ')')->first();
                    if ($rel_exist['is_approved'] == 1) {
                        $placeCallflag = 1;
                    } else {
                        $allow_new_user = Doctor::where('user_id', '=', $data['doctor'])->pluck('allow_new_user');
                        if (!empty($allow_new_user)) {
                            if (!empty($rel_exist)) {
                                DoctorPatientRelation::where('doctor_patient_id', ' = ', $rel_exist['doctor_patient_id'])->update(array('is_approved' => 1));
                            } else {
                                $doctorPatientRelation = new DoctorPatientRelation();
                                $doctorPatientRelation->doctor = $data['doctor'];
                                $doctorPatientRelation->patient = $user_id;
                                $doctorPatientRelation->is_approved = 1;
                                $doctorPatientRelation->save();
                            }
                            $placeCallflag = 1;
                        } else {
                            if (empty($rel_exist)) {
                                $doctorPatientRelation = new DoctorPatientRelation();
                                $doctorPatientRelation->doctor = $data['doctor'];
                                $doctorPatientRelation->patient = $user_id;
                                $doctorPatientRelation->is_approved = 0;
                                $doctorPatientRelation->save();
                            }
                            $GCMData = array();
                            $GCMData['from_user_id'] = $user_detail['user_id'];
                            $GCMData['from_user_name'] = $user_detail['name'];
                            $GCMData['from_user_photo'] = $user['photo'];
                            $GCMData['msg_id'] = 0;
                            $GCMData['msg_time'] = time();
                            $GCMData['type'] = 'call_request';
                            if ($user_app['platform_id'] == 1) {
                                $params['GCMDeviceIds'] = $user_app['registartion_id'];
                                $params['msg'] = json_encode($GCMData);
                                $result['response'] = $communicationHelper->gcmCommunication($params);
                            } else if ($user_app['platform_id'] == 2) {
                                $params['APNSDeviceIds'] = $user_app['registartion_id'];
                                $GCMData['msg_text'] = $user_detail['name'] . " " . Config::get('config_msg.CALL_REQUEST');
                                $params['push_msg'] = $GCMData;
                                $result['response'] = $communicationHelper->pushApnsCommunication($params);
                            }

                            $this->setHeader('RESPONSE_CODE', '0');
                            $error[] = 1027;
                            $error = $this->getError($error);
                            $result['status'] = 0;
                            $rel_exist['new_user_allowed'] = 0;
                            $result['msg'] = Config::get('config_msg.NOT_AllOWED_NEW_USER');
                            $result['errors'] = $error;
                        }
                    }
                    
                    if ($placeCallflag == 1) {
                        $dr_status = Doctor::where('user_id', '=', $data['doctor'])->pluck('online_status');
                        if ($dr_status == 1) {

                            //INSERT DATA IN KCC COMMUNICATION TABLE IF INTERNATIONAL CALL
                            $kccCallId = 0;
                            if (!$domesticCall) {
                                $kccCall = new KccCall();
                                $kccCall->user_id = $user_id;
                                $kccCall->doctor_user_id = $data['doctor'];
                                $kccCall->per_min_charges = $doctorPerMinCharges;
                                $kccCall->save();
                                $kccCallId = $kccCall->kcc_call_id;
                            }

                            $commData = new Call();
                            $commData->caller_id = $user_id;
                            $commData->receiver_id = $data['doctor'];
                            $commData->caller_call_status_id = 4;
                            $commData->receiver_call_status_id = 4;
                            $commData->per_min_charges = $doctorPerMinCharges;
                            $commData->fix_call_charges = ($fixedMinsCall ? 0 : $fix_call_charges);
                            $commData->fixed_min_call = $fixedMinsCall;
                            $commData->kcc_call_id = $kccCallId;
                            $commData->wallet_id = $wallet_id;
                            $commData->save();
                            $GCMData = array();
                            $GCMData['from_user_id'] = $user_detail['user_id'];
                            $GCMData['from_user_name'] = $user_detail['name'];
                            $GCMData['from_user_photo'] = $user['photo'];
                            $GCMData['msg_id'] = $commData->call_id;
                            $GCMData['msg_time'] = time();
                            $GCMData['type'] = 'call_initiate';

                            if ($user_app['platform_id'] == 1) {
                                $params['GCMDeviceIds'] = $user_app['registartion_id'];
                                $params['msg'] = json_encode($GCMData);
                                $result['response'] = $communicationHelper->gcmCommunication($params);
                            } else if ($user_app['platform_id'] == 2) {
                                $params['APNSDeviceIds'] = $user_app['registartion_id'];
                                $GCMData['msg_text'] = $user_detail['name'] . " " . Config::get('config_msg.CALL_INITIATE');
                                $params['push_msg'] = $GCMData;
                                $result['response'] = $communicationHelper->pushApnsCommunication($params);
                            }

                            if ($user['clinic_type'] == 2) {
                                $max_call_duration = Config::get('constants.MAX_CALL_DURATION_CAPPING_FOR_FIX_CALL_CHARGES');
                            } else {
                                $bal = floatval($userTotalBalance);
                                if ($bal >= $doctorPerMinCharges) {
                                    if ($doctorPerMinCharges > 0) {
                                        $tot_mins = floatval($bal) / floatval($doctorPerMinCharges);
                                        $max_call_duration = intval($tot_mins) * 60;
                                    } else {
                                        $max_call_duration = 15 * 60;
                                    }
                                } else {
                                    $max_call_duration = ($doctorPerMinCharges > 0) ? 0 : 15 * 60;
                                }
                            }

                            $doctor_detail = User::where('user_id', '=', $data['doctor'])->first();
                            $patMobile = '+' . $user_detail['mobile'];
                            $docMobile = '+' . $doctor_detail['mobile'];

                            //DISCONNECT THE CALL AFTER 15 MINS 
                            $MAX_CALL_DURATION_CAPPING = Config::get('constants.MAX_CALL_DURATION_CAPPING');
                            if ($max_call_duration > $MAX_CALL_DURATION_CAPPING) {
                                $max_call_duration = $MAX_CALL_DURATION_CAPPING;
                            }
                            
                            if($fixedMinsCall) {
                                $max_call_duration = $patDocMapping->available_seconds;
                            }

                            $operatorNumber = Config::get('constants.OPERATOR_NUMBER');
                            $connectOperator = 0;

                            date_default_timezone_set("Asia/Calcutta");
                            $operationHours = date("H");
                            $operationDay = date('l');
                            if ($operatorNumber && !$user['clinic_type'] && ($operationHours >= 9 && $operationHours <= 17 && $operationDay != 'Sunday')) {
                                $connectOperator = 1;
                            }

                            if (Config::get('constants.twilio_enabled')) {

                                $twilioParams = array('call_id' => $commData->call_id, 'patMobile' => $patMobile, 'docMobile' => $docMobile, 'max_call_duration' => $max_call_duration, 'connectOperator' => $connectOperator, 'clinic_type' => $user['clinic_type']);
                                $telecom_id = 2;
                                if(!empty($data['voipCall'])) {
                                    $telecom_id = 3;
                                }
                                
                                Call::where('call_id', '=', $commData->call_id)->update(array('telecom_id' => $telecom_id, 'telecom_request' => json_encode($twilioParams)));
                                                                
                                if(empty($data['voipCall'])) {
                                    $result['twilio'] = $twilioHelper->initiateCall($twilioParams);
                                    if (isset($result['twilio']['status']) && !empty($result['twilio']['status'])) {
                                        $result['status'] = 1;
                                        $result['msg'] = Config::get('config_msg.CALL_PLACED');
                                    } else {
                                        $result['status'] = 0;
                                        $result['error_msg'] = $result['twilio']['message'];
                                        $result['msg'] = 'Service provider failure!';
                                    }                         
                                    Call::where('call_id', '=', $commData->call_id)->update(array('telecom_response' => json_encode($result)));
                                }
                                else {
                                    $twiml = $twilioHelper->voipCall($twilioParams);
                                    return Response::make($twiml, 200, $this->headers);
                                }

                            } else {
                                $url = Config::get('constants.KNOWLARITYINT') . "customer_number=" . $patMobile . "&agent_number=" . $docMobile . "&call_id=" . $commData->call_id . "&max_call_duration=" . $max_call_duration . "&auth_key=" . Config::get('constants.KNOWLARITY_AUTH_KEY');

                                if ($connectOperator) {
                                    $url .= "&operator=$operatorNumber";
                                }

                                Call::where('call_id', '=', $commData->call_id)->update(array('telecom_request' => $url));

                                if (!$domesticCall) {
                                    KccCall::where('kcc_call_id', '=', $kccCallId)->update(array('request_details' => $url));
                                }

                                $httpHelper = $this->helper->load('http');
                                $doc = $httpHelper->httpGet($url);
                                $xml = simplexml_load_string($doc);
                                $json = json_encode($xml);
                                $responseArray = json_decode($json, TRUE);
                                $result['url'] = $url;
                                if ($responseArray['status'] == 'Success') {
                                    $this->setHeader('RESPONSE_CODE', '1');
                                    $result['status'] = 1;
                                    $result['msg'] = Config::get('config_msg.CALL_PLACED');
                                    $result['response'] = $responseArray;

                                    Call::where('call_id', '=', $commData->call_id)->update(array('telecom_response' => $json));
                                } else {
                                    $this->setHeader('RESPONSE_CODE', '0');
                                    $result['status'] = 0;
                                    $result['url'] = $url;
                                    $result['msg'] = "Service Provider Failure!";
                                    $result['response'] = $responseArray;
                                    Call::where('call_id', '=', $commData->call_id)->update(array('telecom_response' => $json));

                                    //ERROR LOGGING                                
                                    $errorLog = new ErrorLogs();
                                    $errorLog->type = 'knowlarity_error_callapihit';
                                    $errorLog->request = $url;
                                    $errorLog->response = json_encode($responseArray);
                                    $errorLog->save();
                                }
                            }
                        } else {
                            $commData = new Call();
                            $commData->caller_id = $user_id;
                            $commData->receiver_id = $data['doctor'];
                            $commData->caller_call_status_id = 2;
                            $commData->per_min_charges = $doctorPerMinCharges;
                            $commData->save();

                            $this->setHeader('RESPONSE_CODE', '1');
                            $result['status'] = 1;
                            $result['msg'] = Config::get('config_msg.DR_OFFlINE');

                            $GCMData = array();
                            $GCMData['from_user_id'] = $user_detail['user_id'];
                            $GCMData['from_user_name'] = $user_detail['name'];
                            $GCMData['from_user_photo'] = $user['photo'];
                            $GCMData['msg_id'] = $commData->call_id;
                            $GCMData['msg_time'] = time();
                            $GCMData['type'] = 'call_missed';

                            if ($user_app['platform_id'] == 1) {
                                $params['GCMDeviceIds'] = $user_app['registartion_id'];
                                $params['msg'] = json_encode($GCMData);
                                $result['response'] = $communicationHelper->gcmCommunication($params);
                            } else if ($user_app['platform_id'] == 2) {
                                $params['APNSDeviceIds'] = $user_app['registartion_id'];
                                $GCMData['msg_text'] = Config::get('config_msg.CALL_MISSED') . $user_detail['name'];
                                $params['push_msg'] = $GCMData;

                                $result['response'] = $communicationHelper->pushApnsCommunication($params);
                            }
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1028;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = "Insufficient Balance!";
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.CALL_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
            }
            
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function callinfoMvc() {

        $input = Input::all();
        $callId = !empty($input['twilio_call_id']) ? $input['twilio_call_id'] : 0;
        $isTwilioCall = 0;
        if ($callId) {
            $telecomId = Call::where('call_id', '=', $callId)->pluck('telecom_id');
            $isTwilioCall = ($telecomId == 2 || $telecomId == 3) ? 1 : 0;
            if ($isTwilioCall) {

                $callFrom = DB::table('calls')->join('users', 'users.user_id', '=', 'calls.caller_id')->where('call_id', '=', $callId)->pluck('mobile');

                $input['Transactionid'] = $callId;
                $input['CallSid'] = $input['CallSid'];
                $input['CallTo'] = $input['To'];
                $input['CallFrom'] = $callFrom;
                $input['Created'] = $input['Timestamp'];

                if ($input['CallStatus'] == 'completed') {
                    $input['CallStatus'] = $input['customerCallStatus'] = 'Connected';
                    $input['total_call_duration'] = $input['CallDuration'];
                    $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['total_call_duration'];
                    if (isset($input['RecordingDuration'])) {
                        $input['DialCallDuration'] = $input['doctor_call_duration'] = $input['RecordingDuration'];
                    }
                } else {

                    if ($input['CallStatus'] == 'no-answer' || $input['CallStatus'] == 'busy') {
                        $input['CallStatus'] = 'Missed';
                    } else {
                        $input['CallStatus'] = 'NotConnected';
                    }

//                    $twilioHelper = $this->helper->load('twilio');
//                    $twilioHelper->twilioOperatorOption1();

                    $input['customerCallStatus'] = 'Connected';

                    $input['total_call_duration'] = $input['CallDuration'];
                    $input['DialCallDuration'] = $input['doctor_call_duration'] = 0;
                }
            }
        }

        $requestHeaders = apache_request_headers();
        if (($requestHeaders['key'] == Config::get('constants.call_info_key')) || $isTwilioCall) {

            $data = Array();
            if (!empty($input['CallSid'])) {
                $data['CallSid'] = $input['CallSid'];
            } if (!empty($input['CallFrom'])) {
                $data['CallFrom'] = $input['CallFrom'];
            } if (!empty($input['CallTo'])) {
                $data['CallTo'] = $input['CallTo'];
            } if (!empty($input['CallStatus'])) {
                $data['CallStatus'] = $input['CallStatus'];
            } if (!empty($input['Created'])) {
                $data['Created'] = $input['Created'];
            } if (!empty($input['DialCallDuration'])) {
                $data['DialCallDuration'] = $input['doctor_call_duration'];
            } if (!empty($input['StartTime'])) {
                $data['StartTime'] = $input['StartTime'];
            } if (!empty($input['EndTime'])) {
                $data['EndTime'] = $input['EndTime'];
            } if (!empty($input['CallType'])) {
                $data['CallType'] = $input['CallType'];
            } if (!empty($input['Transactionid'])) {
                $data['Transactionid'] = $input['Transactionid'];
            } if (!empty($input['customerCallStatus'])) {
                $data['customerCallStatus'] = $input['customerCallStatus'];
            } if (!empty($input['total_call_duration'])) {
                $data['total_call_duration'] = $input['total_call_duration'];
            }

            if (!empty($input['dtmf'])) {
                $data['dtmf'] = 0;
                if ($input['dtmf'] == '1' || $input['dtmf'] == 1) {
                    $data['dtmf'] = 1;
                }
            }

            $data['CallStatus'] = strtolower($data['CallStatus']);
            $data['CallStatus'] = ucfirst($data['CallStatus']);
            $data['customerCallStatus'] = strtolower($data['customerCallStatus']);
            $data['customerCallStatus'] = ucfirst($data['customerCallStatus']);

            if (empty($data['Transactionid'])) {
                $result['status'] = 0;
                $result['msg'] = "Missing communication data!";
                $this->setHeader('RESPONSE_CODE', '0');
                return Response::make($result, 200, $this->headers);
            } else {

                $isCommunicationExist = Call::where('call_id', '=', $data['Transactionid'])->pluck('call_id');

                if ($isCommunicationExist) {
                    $isTransactionExist = Transactions::where('call_id', '=', $data['Transactionid'])->pluck('transaction_id');
                    if ($isTransactionExist) {
                        $result['status'] = 0;
                        $result['msg'] = "Communication data already inserted!";
                        $this->setHeader('RESPONSE_CODE', '0');
                        return Response::make($result, 200, $this->headers);
                    }
                } else {
                    $result['status'] = 0;
                    $result['msg'] = "Communication doesn't exist!";
                    $this->setHeader('RESPONSE_CODE', '0');
                    return Response::make($result, 200, $this->headers);
                }
            }

            //SWAPPING OF VALUES FOR CALLBACK FEATURE
            $callBackRequestId = CallBackRequest::whereRaw('callback_call_id = ' . $data['Transactionid'])->pluck('callback_request_id');
            if ($callBackRequestId) {
                $swapCallFrom = $data['CallFrom'];
                $data['CallFrom'] = $data['CallTo'];
                $data['CallTo'] = $swapCallFrom;

                if ($data['CallStatus'] == 'Connected' || $data['CallStatus'] == 'Missed') {
                    CallBackRequest::where('callback_request_id', '=', $callBackRequestId)->update(array('status' => 'closed'));
                }
            }

            $comm = array();
            $communication = Call::whereRaw('call_id = ' . $data['Transactionid'])->first();

            if (!empty($data['customerCallStatus'])) {
                $CallStatusMaster = new CallStatusMaster();
                $comm['receiver_call_status_id'] = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['customerCallStatus']));
            }

            if (!empty($data['CallStatus'])) {
                $comm['caller_call_status_id'] = $CallStatusMaster->getCommunicationTypeId(array('name' => $data['CallStatus']));
            }

            //IF DOCTOR MISSED THE CALL
            if (!empty($data['dtmf'])) {
                $comm['caller_call_status_id'] = 2; //MISSED
                if ($data['CallStatus'] == 'Not Connected' || $data['CallStatus'] == 'NotConnected') {
                    $comm['caller_call_status_id'] = 7; //NOT CONNECTED
                }
            }

            $comm['call_duration'] = $data['DialCallDuration'] ? $data['DialCallDuration'] : 0;
            $comm['operator_call_duration'] = intval($data['total_call_duration']) - intval($comm['call_duration']);
            $comm['telecom_response'] = json_encode($input);

            $communicationHelper = $this->helper->load('communication');
            Call::whereRaw('call_id = ' . $data['Transactionid'])->update($comm);

            $data['CallFrom'] = str_replace('+', '', $data['CallFrom']);
            $data['CallFrom'] = trim($data['CallFrom']);

            //START - PUSH MESSAGE FOR CALL ALERT //WILL REMOVE do_rating later
            $patient = User::where('user_id', '=', $communication->caller_id)->select('user_id', 'name', 'email', 'mobile')->first();
            $patientIsClinic = UserProfile::where('user_id', '=', $patient['user_id'])->pluck('clinic_type');
            $patient_app = DeviceInfo::where('user_id', '=', $patient['user_id'])->select('platform_id', 'registartion_id')->first();

            if ($patientIsClinic) {
                $comment = "Clinic - " . $patient['name'];
                if ($patientIsClinic == 2) {
                    $comment = "Pilot Run Clinic - " . $patient['name'];
                }
                Call::whereRaw('call_id = ' . $data['Transactionid'])->update(array('followed_by' => 'Database Admin', 'comment' => $comment));
            } elseif ($callBackRequestId) {
                Call::whereRaw('call_id = ' . $data['Transactionid'])->update(array('followed_by' => 'Database Admin', 'comment' => "Call Back - By Doctor"));
            }

            if ($communication->kcc_call_id) {
                $kccCall = KccCall::where('kcc_call_id', '=', $communication->kcc_call_id)->first();
                $kccCall->call_duration = $data['DialCallDuration'];
                $kccCall->caller_status = ucfirst($data['customerCallStatus']);
                $kccCall->receiver_status = ucfirst($data['CallStatus']);
                $kccCall->call_starttime = $data['StartTime'];
                $kccCall->response_details = json_encode($input);
                $kccCall->save();
            }

            $data['CallTo'] = str_replace('+', '', $data['CallTo']);
            $data['CallTo'] = trim($data['CallTo']);

            $doctor = User::where('user_id', '=', $communication->receiver_id)->select('user_id', 'name', 'email', 'mobile')->first();
            $doctor_app = DeviceInfo::where('user_id', '=', $doctor['user_id'])->select('platform_id', 'registartion_id')->first();

            if ($data['CallStatus'] == 'Connected') {
                $seconds = $data['DialCallDuration'];

                if (Config::get('constants.MIN_CALL_DURATION') < $seconds) {

                    if($communication->fixed_min_call) {
                        
                        //REDUCE AVAILABLE MINUTES
                        $patDocMapping = PatDocMinInfo::where('patient_user_id', '=', $communication->caller_id)->where('doctor_user_id', '=', $communication->receiver_id)->first();
                        $patDocMapping->available_seconds -= $seconds;
                        $patDocMapping->save();        
                        
                        //MAKE OTHER ENTRIES
                        $spend_detail = new Spend();
                        $spend_detail->call_id = $data['Transactionid'];
                        $spend_detail->total_charges = 0;
                        $spend_detail->doctor_charges = 0;
                        $spend_detail->created_at = date('Y-m-d h:i:s');
                        $spend_detail->save();           
                        
                        $kccTransactionType = new KccTransactionType();
                        $kccTransaction = new KccTransaction();
                        $kccTransaction->transferred_to = $kccCall->doctor_user_id;
                        $kccTransaction->transferred_from = $kccCall->user_id;
                        $kccTransaction->amount = 0;
                        $kccTransaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
                        $kccTransaction->transaction_details_id = $kccCall->kcc_call_id;
                        $kccTransaction->save();      
                        
                        $trans_detail = new Transactions();
                        $trans_detail->call_id = $data['Transactionid'];
                        $trans_detail->call_uuid = $data['CallSid'];
                        $trans_detail->user_id = $patient['user_id'];
                        $trans_detail->wallet_id = $communication->wallet_id;
                        $trans_detail->type = 2;
                        $trans_detail->spend_id = $spend_detail->spend_id;
                        $trans_detail->amount = 0;
                        $trans_detail->balance = 0;
                        $trans_detail->trans_status = 1;
                        $trans_detail->save();                        

                        $trans_detail1 = new Transactions();
                        $trans_detail1->call_id = $data['Transactionid'];
                        $trans_detail1->call_uuid = $data['CallSid'];
                        $trans_detail1->user_id = $doctor['user_id'];
                        $trans_detail1->wallet_id = 4;
                        $trans_detail1->type = 1;
                        $trans_detail1->spend_id = $spend_detail->spend_id;
                        $trans_detail1->amount = 0;
                        $trans_detail1->balance = 0;
                        $trans_detail1->trans_status = 1;
                        $trans_detail1->save();                        
                        
                    }
                    else {
                    
                        $chargesDeductionCriteria = Config::get('constants.CHARGES_DEDUCTION_CRITERIA');

                        $httpHelper = $this->helper->load('http');
                        $citrusHelper = $this->helper->load('citrus');
                        $paytmHelper = $this->helper->load('paytm');

                        $citrusModel = new UserWallet();

                        if ($kccCall->kcc_call_id) {
                            $kccTransaction = new KccTransaction();
                            $user_mvc_bal = $kccTransaction->getBalance(array('user_id' => $patient['user_id']));
                            $totbal = $user_mvc_bal;
                        } else {

                            $user_token = $citrusModel->getWalletInfoColumn(array('user_id' => $patient['user_id'], 'wallet_id' => $communication->wallet_id, 'columnName' => 'token'));

                            if ($communication->wallet_id == 1) {

                                if (empty($user_token)) {
                                    $this->setHeader('RESPONSE_CODE', '0');
                                    $error[] = 1040;
                                    $error = $this->getError($error);
                                    $result['status'] = 0;
                                    $result['msg'] = Config::get('config_msg.INVAILD_CITRUS_AUTHTOKEN');
                                    $result['errors'] = $error;
                                    return Response::make($result, 200, $this->headers);
                                    //NEED TO DO LOGGING HERE
                                }

                                $mvcBalance['mvcAmount'] = $user_mvc_bal = 0;

                                $mainBalance['value'] = $totbal = $paytmHelper->walletWebCheckBalance(array('ssotoken' => $user_token, 'getBalanceOnly' => 1));
                            } else {
                                $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $user_token));

                                if (!empty($mainBalance['error'])) {
                                    $this->setHeader('RESPONSE_CODE', '0');
                                    $result['status'] = 0;
                                    $result['msg'] = $mainBalance['errorMsg'];
                                    $result['errors'] = $mainBalance;
                                    return Response::make($result, 200, $this->headers);
                                    //NEED TO DO LOGGING HERE
                                }

                                $emailID = $citrusModel->getWalletInfoColumn(array('user_id' => $patient['user_id'], 'columnName' => 'email'));
                                $mvcBalance = $citrusHelper->fetchMVCBalance(array('emailId' => $emailID));
                                if (empty($mvcBalance['d'])) {
                                    $this->setHeader('RESPONSE_CODE', '0');
                                    $result['status'] = 0;
                                    $result['msg'] = $mvcBalance['errorMsg'];
                                    $result['errors'] = $mvcBalance;
                                    return Response::make($result, 200, $this->headers);
                                    //NEED TO DO LOGGING HERE
                                }

                                $user_mvc_bal = $mvcBalance['mvcAmount'];
                                $totbal = $mvcBalance['mvcAmount'] + $mainBalance['value'];
                            }
                        }

                        $doc_citrus_acc = UserWallet::where('user_id', '=', $doctor['user_id'])->first(); //NEED TO PUT wallet_id CONDITION FOR CITRUS
                        $doc_token = $doc_citrus_acc['token'];

                        $doc_citrus = $citrusHelper->fetchRealBalance(array('citrusToken' => $doc_token));
                        $doctorRealBalance = $doc_citrus['value'];

                        $paytm_doctor_token = $citrusModel->getWalletInfoColumn(array('user_id' => $doctor['user_id'], 'wallet_id' => 1, 'columnName' => 'token'));
                        if ($paytm_doctor_token) {
                            $doctorRealBalance = $paytmHelper->walletWebCheckBalance(array('ssotoken' => $paytm_doctor_token, 'getBalanceOnly' => 1));
                        }

                        $umob = $data['CallFrom'];
                        $locale = isset($data['locale']) ? $data['locale'] : '';
                        $callTypeData = User::getCallTypeData(array('mobile' => $umob, 'locale' => $locale));
                        $domesticCall = !$callTypeData['internationalCall'];

                        $enabled_fix_charges_per_call_for_international = Config::get('constants.ENABLED_FIX_CHARGES_PER_CALL_FOR_INTERNATIONAL');                   

                        if (($enabled_fix_charges_per_call_for_international && !$domesticCall) || $patientIsClinic == 2) {
                            $charges = $communication->fix_call_charges;

                            if ($user_mvc_bal >= $charges) {
                                $remain_user_mvc_bal = $user_mvc_bal - $charges;
                                $user_bal = floatval($mainBalance['value']) + $remain_user_mvc_bal;
                            } else if ($user_mvc_bal < $charges) {
                                $user_bal = floatval($mainBalance['value']) - ($charges - $user_mvc_bal);
                                $de_citrus_bal = ($charges - $user_mvc_bal);
                            }
                        } else {

                            if ($chargesDeductionCriteria == 'SECOND') {
                                $charges = ($communication->per_min_charges / 60) * $seconds;
                            } else {
                                $charges = ($communication->per_min_charges) * ceil($seconds / 60);
                            }

                            $charges = round($charges);
                            if ($user_mvc_bal >= $charges) {
                                $remain_user_mvc_bal = $user_mvc_bal - $charges;
                                $user_bal = floatval($mainBalance['value']) + $remain_user_mvc_bal;
                            } else if ($user_mvc_bal < $charges) {
                                $user_bal = floatval($mainBalance['value']) - ($charges - $user_mvc_bal);
                                $de_citrus_bal = ($charges - $user_mvc_bal);
                            }
                        }

                        $charges = round($charges);
                        $konsult_share = (floatval(Config::get('constants.KONSULT_PERCENTAGE')) * $charges) / 100;
                        $konsult_share = round($konsult_share);
                        $doc_bal = $charges - $konsult_share;

                        $tot_doc_bal = $doc_bal + floatval($doctorRealBalance);
                        $spend_detail = new Spend();
                        $spend_detail->call_id = $data['Transactionid'];
                        $spend_detail->total_charges = $kccCall->kcc_call_id ? ($charges) : $charges;
                        $spend_detail->doctor_charges = $doc_bal;
                        $spend_detail->created_at = date('Y-m-d h:i:s');
                        $spend_detail->save();

                        $trans_detail = new Transactions();
                        $trans_detail->call_id = $data['Transactionid'];
                        $trans_detail->call_uuid = $data['CallSid'];
                        $trans_detail->user_id = $patient['user_id'];
                        $trans_detail->wallet_id = $communication->wallet_id;
                        $trans_detail->type = 2;
                        $trans_detail->spend_id = $spend_detail->spend_id;
                        $trans_detail->amount = $charges;
                        $trans_detail->balance = $user_bal;
                        $trans_detail->trans_status = 1;
                        $trans_detail->save();

                        //INSERT KCC TRANSACTION DETAILS
                        if ($kccCall->kcc_call_id) {

                            $chargesFromKccWallet = $charges;
                            if ($user_mvc_bal <= $charges) {
                                $chargesFromKccWallet = $user_mvc_bal;
                            }

                            $kccTransactionType = new KccTransactionType();
                            $kccTransaction = new KccTransaction();
                            $kccTransaction->transferred_to = $kccCall->doctor_user_id;
                            $kccTransaction->transferred_from = $kccCall->user_id;
                            $kccTransaction->amount = $chargesFromKccWallet;
                            $kccTransaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
                            $kccTransaction->transaction_details_id = $kccCall->kcc_call_id;
                            $kccTransaction->save();

                            if ($charges) {
                                //WE SHOULD FETCH PAYMENT GATEWAY VALUES FROM DATABASE
                                if ($user_mvc_bal >= $charges || $charges <= 0) {
                                    $trans_detail->wallet_id = 4;
                                } elseif ($charges > $user_mvc_bal && $user_mvc_bal > 0) {
                                    $trans_detail->wallet_id = 5;
                                } else {
                                    $trans_detail->wallet_id = 6;
                                }
                                $trans_detail->save();
                            }
                        } else {
                            if ($user_mvc_bal < $charges) {

                                if ($communication->wallet_id == 1) {

                                    $autoDebitRequestData = array();
                                    $autoDebitRequestData['amount'] = $spend_detail->real_money = $de_citrus_bal;
                                    $autoDebitRequestData['order_id'] = $communication->call_id;

                                    $first2Chars = substr($umob, 0, 2);
                                    $first3Chars = substr($umob, 0, 3);
                                    if ($first2Chars == '91' || $first3Chars == '+91') {
                                        $autoDebitRequestData['mobile'] = substr($umob, 2);
                                    } else {
                                        $autoDebitRequestData['mobile'] = $umob;
                                    }

                                    $autoDebitRequestData['sso_token'] = $user_token;
                                    $autoDebitRequestData['user_id'] = $patient['user_id'];
                                    $autoDebitResponse = $paytmHelper->oltpHandlerffWithdrawScw($autoDebitRequestData);

                                    $myreq['paytm_request'] = $autoDebitRequestData;
                                    $myreq['paytm_mvc_request'] = array();
                                    $citrus_req = json_encode($myreq, JSON_UNESCAPED_SLASHES);
                                    $myres['paytm_response'] = $autoDebitResponse;
                                    $myres['paytm_mvc_response'] = array();
                                    $myRes = json_encode($myres);

                                    //CASHBACK FLOW START
//                                    $isFirstCall = 0;
//                                    if (!$patientIsClinic) {
//                                        $communicationModel = new Call();
//                                        $isFirstCall = $communicationModel->where('caller_id', '=', $patient['user_id'])->where('call_duration', '>', 10)->where('operator_call_duration', '<=', 0)->pluck('call_id');
//                                        $isFirstCall = $isFirstCall ? 0 : 1;
//                                    }
    
                                    $autoDebitRequestData['amount'] = (int) ($autoDebitRequestData['amount'] * .15);
                                    if ($autoDebitRequestData['amount'] > 0 && (!$patientIsClinic)) {
                                        if (strstr($autoDebitResponse, 'TXN_SUCCESS')) {
                                            $autoDebitRequestData['spend_id'] = $trans_detail->spend_id;
                                            $autoDebitRequestData['CallSid'] = $data['CallSid'];
                                            
                                            $cashbackStatus = $paytmHelper->cashback($autoDebitRequestData);
    
                                            $spend_detail->cashback = $autoDebitRequestData['amount'];
                                            $spend_detail->save();
    
                                            if ($cashbackStatus == 1) {
                                                $smsText = Config::get('constants.NETCORE_SMS_TEXT_PAYTM_CASHBACK');
                                                if ($smsText && $spend_detail->cashback > 0) {
    
                                                    //PREPARE DATA FOR SMS AND SEND
                                                    $smsParams = array();
                                                    $smsParams['Text'] = str_replace(array('<amount>'), array($spend_detail->cashback), $smsText);
                                                    $smsParams['To'] = $patient['mobile'];
                                                    $this->helper->load('communication')->smsCommunication($smsParams);
                                                }
                                            }
                                        }
                                    }
                                    //CASHBACK FLOW END
                                } else {
                                    $citrusdata = array();
                                    $citrusdata['transactionId'] = $trans_detail->transaction_id;
                                    $citrusdata['amount'] = $spend_detail->real_money = $de_citrus_bal;
                                    $citrusdata['user_id'] = $patient['user_id'];

                                    $citrusHelper = $this->helper->load('citrus');
                                    $user_citrus_json = $citrusHelper->debitRealMoney($citrusdata);

                                    $httpHelper = $this->helper->load('http');

                                    if ($user_mvc_bal > 0) {
                                        $Amount = $spend_detail->virtual_money = $user_mvc_bal;
                                        $spend_detail->save();

                                        $user_citrus_mvc = $citrusHelper->redeemVirtualMoney(array('amount' => $Amount, 'emailId' => $emailID, 'mobile' => '', 'transactionId' => $trans_detail->transaction_id));
                                    }

                                    $myreq['citrus_request'] = $citrusdata;
                                    $myreq['citrus_mvc_request'] = $user_citrus_mvc['requestData'];
                                    $citrus_req = json_encode($myreq, JSON_UNESCAPED_SLASHES);
                                    $myres['citrus_response'] = $user_citrus_json;
                                    $myres['citrus_mvc_response'] = $user_citrus_mvc;
                                    $myRes = json_encode($myres);
                                }

                                $transStatus = $this->_getTransactionStatus($myRes);
                                $user_invoice_no = 'KONPAT/' . date("m") . '/' . date("Y") . '/' . $trans_detail->transaction_id;
                                Transactions::where('transaction_id', '=', $trans_detail->transaction_id)->update(array('pg_request' => $citrus_req, 'pg_response' => $myRes, 'invoice_no' => $user_invoice_no, 'trans_status' => $transStatus));
                            } else if ($user_mvc_bal >= $charges) {

                                if ($charges != 0 || $charges != 0.00) {
                                    $httpHelper = $this->helper->load('http');

                                    $Amount = $spend_detail->virtual_money = $charges;
                                    $spend_detail->save();

                                    $user_citrus_mvc = $citrusHelper->redeemVirtualMoney(array('amount' => $Amount, 'emailId' => $emailID, 'mobile' => '', 'transactionId' => $trans_detail->transaction_id));

                                    $requestData = json_encode($user_citrus_mvc['requestData'], JSON_UNESCAPED_SLASHES);
                                    Transactions::where('transaction_id', '=', $trans_detail->transaction_id)->update(array('pg_request' => $requestData));

                                    $user_invoice_no = 'KONPAT/' . date("m") . '/' . date("Y") . '/' . $trans_detail->transaction_id;
                                    $userCitrusMvc = json_encode($user_citrus_mvc);

                                    $transStatus = $this->_getTransactionStatus($userCitrusMvc);

                                    Transactions::where('transaction_id', '=', $trans_detail->transaction_id)->update(array('pg_response' => $userCitrusMvc, 'invoice_no' => $user_invoice_no, 'trans_status' => $transStatus));
                                }
                            }
                        }

                        $trans_detail1 = new Transactions();
                        $trans_detail1->call_id = $data['Transactionid'];
                        $trans_detail1->call_uuid = $data['CallSid'];
                        $trans_detail1->user_id = $doctor['user_id'];
                        $trans_detail1->wallet_id = ($paytm_doctor_token ? 1 : 3);
                        $trans_detail1->type = 1;
                        $trans_detail1->spend_id = $spend_detail->spend_id;
                        $trans_detail1->amount = $doc_bal;
                        $trans_detail1->balance = $tot_doc_bal;
                        $trans_detail1->trans_status = 1;
                        $trans_detail1->save();

                        if ($charges > 0) {

                            $doc_invoice_no = 'KONPAT/' . date("m") . '/' . date("Y") . '/' . $trans_detail1->transaction_id;

                            if (1 /*$paytm_doctor_token*/) {

                                $paytmDocPush = array();
                                $paytmDocPush['amount'] = $doc_bal;
                                $paytmDocPush['user_id'] = $doctor['user_id'];
                                $paytmDocPush['order_id'] = $communication->call_id;

                                $first2Chars = substr($data['CallTo'], 0, 2);
                                $first3Chars = substr($data['CallTo'], 0, 3);
                                if ($first2Chars == '91' || $first3Chars == '+91') {
                                    $paytmDocPush['mobile'] = substr($data['CallTo'], 2);
                                } else {
                                    $paytmDocPush['mobile'] = $data['CallTo'];
                                }

                                Transactions::where('transaction_id', '=', $trans_detail1->transaction_id)->update(array('pg_request' => json_encode($paytmDocPush), 'invoice_no' => $doc_invoice_no));

                                $creditDocMoney = $paytmHelper->salesToUserCredit($paytmDocPush);

                                Transactions::where('transaction_id', '=', $trans_detail1->transaction_id)->update(array('pg_response' => $creditDocMoney, 'trans_status' => $transStatus));

                                $creditDocMoney = json_decode($creditDocMoney);
                            } else {
                                $citrusdata_doc = array();
                                $citrusdata_doc['to'] = $doc_citrus_acc['email'];
                                $citrusdata_doc['amount'] = $doc_bal;

                                Transactions::where('transaction_id', '=', $trans_detail1->transaction_id)->update(array('pg_request' => json_encode($citrusdata_doc), 'invoice_no' => $doc_invoice_no));

                                $doc_citrus_json = $citrusHelper->pushMoney($citrusdata_doc);

                                $docCitrusJson = json_encode($doc_citrus_json);

                                $transStatus = $this->_getTransactionStatus($docCitrusJson);

                                Transactions::where('transaction_id', '=', $trans_detail1->transaction_id)->update(array('pg_response' => $docCitrusJson, 'trans_status' => $transStatus));
                            }

                            //SEND SMS ALERT TO DOCTOR
                            $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALL_CREDIT_DOCTOR');
                            if ($smsText && !empty($doc_bal)) {

                                $userName = $patient['name'];
                                if ($patientIsClinic) {
                                    $userName = "Konsult Clinic ($userName)";
                                }

                                //PREPARE DATA FOR SMS AND SEND
                                $smsParams = array();
                                $totalAvailableBalance = (int) $doc_bal + (int) $doctorRealBalance;
                                $smsParams['Text'] = str_replace(array('<Doctor Name>', '<User>', '<Value1>', '<Value2>'), array($doctor['name'], $userName, $doc_bal, $totalAvailableBalance), $smsText);
                                $smsParams['To'] = $doctor['mobile'];
                                $this->helper->load('communication')->smsCommunication($smsParams);
                            }

                            //SEND SMS ALERT TO PATIENT
                            $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALL_DEBIT_PATIENT');
                            if (!empty($doc_bal) && $smsText) {

                                //PREPARE DATA FOR SMS AND SEND
                                $smsParams = array();
                                $smsParams['Text'] = str_replace(array('<User>', '<Doctor Name>', '<Value1>', '<Value2>'), array($patient['name'], $doctor['name'], "INR " . $charges, "INR " . $user_bal), $smsText);
                                $smsParams['To'] = $patient['mobile'];
                                $this->helper->load('communication')->smsCommunication($smsParams);
                            }
                        } else {
                            Transactions::where('call_id', '=', $trans_detail1->call_id)->update(array('pg_response' => 'ZERO_CHARGES', 'pg_request' => 'ZERO_CHARGES'));
                        }

                        //MAKE DOCTOR PATIENT MAPPING TO ENABLE CHAT
                        $doctorPatientMapping = new DoctorPatientMapping();
                        $doctorPatientMapping->doMapping(array('doctor_id' => $doctor['user_id'], 'patient_id' => $patient['user_id'], 'type' => 'calling'));

                        //Push msg for doctor
                        $GCMData = array();
                        $GCMData['initiated_by'] = $patient['user_id'];
                        $GCMData['from_user_id'] = $patient['user_id'];
                        $GCMData['from_user_name'] = $patient['name'];
                        $GCMData['msg_id'] = $data['Transactionid'];
                        $GCMData['msg_time'] = time();
                        $GCMData['call_duration'] = $data['DialCallDuration'];
                        $GCMData['call_charges'] = $doc_bal;
                        $GCMData['type'] = 'call_complete';
                        if ($doctor_app['platform_id'] == 1) {
                            $params['GCMDeviceIds'] = $doctor_app['registartion_id'];
                            $params['msg'] = json_encode($GCMData);
                            $result['response'] = $communicationHelper->gcmCommunication($params);
                        } else if ($doctor_app['platform_id'] == 2) {
                            $params['APNSDeviceIds'] = $doctor_app['registartion_id'];
                            $GCMData['msg_text'] = Config::get('config_msg.CALL_COMPLETE') . " Rs: " . $doc_bal . " is credited in your wallet.";
                            $params['push_msg'] = $GCMData;

                            $result['response'] = $communicationHelper->pushApnsCommunication($params);
                        }

                        if ($doc_bal > 0) {
                            $emaildata = array();
                            $emaildata['patient_name'] = $patient['name'];
                            $emaildata['invoice_no'] = $doc_invoice_no;
                            $emaildata['name'] = $doctor['name'];
                            $emaildata['date'] = date('Y-m-d');
                            $emaildata['charges'] = $doc_bal;
                            $emaildata['logo'] = Config::get('constants.LOGO');
                            $emailbody = View::make('doctor_invoice')->with('data', $emaildata)->render();
                            $mailArray = array();
                            $mailArray['email'] = $doctor['email'];
                            $mailArray['subject'] = Config::get('config_msg.DOC_INVOICE_EMAIL_SUBJECT');
                            $mailArray['body'] = $emailbody;
                            $result['email_response'] = $communicationHelper->emailCommunication($mailArray);
                        }

                        //Push msg for patient
                        $GCMData['initiated_by'] = $patient['user_id'];
                        $GCMData['from_user_id'] = $patient['user_id'];
                        $GCMData['from_user_name'] = $patient['name'];
                        $GCMData['msg_id'] = $data['Transactionid'];
                        $GCMData['msg_time'] = time();
                        $GCMData['call_duration'] = $data['DialCallDuration'];
                        $GCMData['call_charges'] = $charges;
                        $GCMData['type'] = 'call_complete';
                        if ($patient_app['platform_id'] == 1) {
                            $params['GCMDeviceIds'] = $patient_app['registartion_id'];
                            $params['msg'] = json_encode($GCMData);
                            $result['response'] = $communicationHelper->gcmCommunication($params);
                        } else if ($patient_app['platform_id'] == 2) {
                            $params['APNSDeviceIds'] = $patient_app['registartion_id'];
                            if (!$domesticCall) {
                                $charges = round($charges / $callTypeData['currencyFactor'], 2);
                                $GCMData['msg_text'] = Config::get('config_msg.CALL_COMPLETE') . " $" . $charges . " is Debited from Your Account.";
                            } else {
                                $GCMData['msg_text'] = Config::get('config_msg.CALL_COMPLETE') . " Rs: " . $charges . " is Debited from Your Account.";
                            }
                            $params['push_msg'] = $GCMData;
                            $result['response'] = $communicationHelper->pushApnsCommunication($params);
                        }

                        if (!$patientIsClinic) {
                            $emaildata = array();
                            $emaildata['name'] = $patient['name'];
                            $emaildata['doc_name'] = $doctor['name'];
                            $emaildata['invoice_no'] = $user_invoice_no;
                            $emaildata['date'] = date('Y-m-d');
                            $emaildata['charges'] = $charges;
                            $emaildata['logo'] = Config::get('constants.LOGO');
                            $emaildata['internationalCall'] = $callTypeData['internationalCall'];
                            $emailbody = View::make('patient_invoice')->with('data', $emaildata)->render();
                            $mailArray = array();
                            $mailArray['email'] = $patient['email'];
                            $mailArray['subject'] = Config::get('config_msg.PATIENT_INVOICE_EMAIL_SUBJECT');
                            $mailArray['body'] = $emailbody;
                            $result['email_response'] = $communicationHelper->emailCommunication($mailArray);
                        }
                    }
                }
                //ADD AS A FAVOURITE
                $callDatas = Call::whereRaw('call_id = ' . $data['Transactionid'])->select('caller_id', 'receiver_id')->first();
                $favouriteData = array();
                $favouriteData['patient'] = $callDatas['caller_id'];
                $favouriteData['doctor'] = $callDatas['receiver_id'];
                $result['favs'] = $favouriteData;
                $favDoctor = new FavoriteDoctor();
                if ($favDoctor->validate($favouriteData)) {
                    $doc_exist = Doctor::whereRaw('user_id =' . $favouriteData['doctor'])->exists();
                    if ($doc_exist) {
                        $favdoc = FavoriteDoctor::whereRaw('patient = ' . $favouriteData['patient'] . ' && doctor =' . $favouriteData['doctor'])->exists();
                        if (!$favdoc) {
                            $favDoctor->patient = $favouriteData['patient'];
                            $favDoctor->doctor = $favouriteData['doctor'];
                            $favDoctor->save();
                            $result['add_favourite_msg'] = Config::get('config_msg.DR_ADD_FAV_LIST');
                        } else {
                            $favDoctor->save();
                            $result['add_favourite_msg'] = Config::get('config_msg.DR_ALREADY_ADD_FAV_LIST');
                        }
                    }
                }
            } elseif (( $comm['receiver_call_status_id'] != 7 && $data['CallStatus'] == 'Missed') && !empty($data['Transactionid'])) {

                //CHECK FOR CALLBACK CASE
                if (empty($callBackRequestId)) {

                    //SEND SMS ALERT TO DOCTOR
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_MISSED_ALERT_DOCTOR');
                    if ($smsText && !empty($doctor['name']) && !empty($patient['name']) && !empty($doctor['mobile'])) {
                        $userName = $patient['name'];
                        if ($patientIsClinic) {
                            $userName = "Konsult Clinic ($userName)";
                        }

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $smsParams['Text'] = str_replace(array('<Doctor Name>', '<User>'), array($doctor['name'], $userName), $smsText);
                        $smsParams['To'] = $doctor['mobile'];
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }

                    //SEND SMS ALERT TO PATIENT
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_MISSED_ALERT_PATIENT');
                    if (!$patientIsClinic && $smsText && !empty($doctor['name']) && !empty($patient['name']) && !empty($patient['mobile'])) {

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $smsParams['Text'] = str_replace(array('<User>', '<Doctor Name>'), array($patient['name'], $doctor['name']), $smsText);
                        $smsParams['To'] = $patient['mobile'];
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                } else {
                    //SEND SMS ALERT TO PATIENT
                    $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALLBACK_PATIENT_MISSED_DOCTOR_CALL');
                    if (!$patientIsClinic && $smsText && !empty($doctor['name']) && !empty($patient['name']) && !empty($patient['mobile'])) {

                        //PREPARE DATA FOR SMS AND SEND
                        $smsParams = array();
                        $smsParams['Text'] = str_replace(array('<User>', '<Doctor Name>'), array($patient['name'], $doctor['name']), $smsText);
                        $smsParams['To'] = $patient['mobile'];
                        $this->helper->load('communication')->smsCommunication($smsParams);
                    }
                }
            }

            //MANIPULATING DTMF PARAM AS SOME DOCTORS DO NOT HAVE THE LATEST VERSION OF APP (TEMPORARY CHECK)
            $doctorAppDetails = DeviceInfo::where('user_id', '=', $doctor['user_id'])->select('platform_id', 'current_app_version')->first();
            if (($doctorAppDetails['platform_id'] == 1 && $doctorAppDetails['current_app_version'] < 43) || ($doctorAppDetails['platform_id'] == 2 && $doctorAppDetails['current_app_version'] < 1.15)) {
                $data['dtmf'] = 2;
            }

            $GCMData = array();
            $GCMData ['msg_text'] = "Call Info";
            $GCMData ['type'] = 'call_info';
            $GCMData ['communication_id'] = $data['Transactionid'];
            $GCMData ['call_id'] = $data['Transactionid'];
            $GCMData ['clinic_type'] = $patientIsClinic ? 1 : 0;
            $GCMData ['call_id'] = $data['Transactionid'];
            $GCMData ['call_duration'] = ($data['DialCallDuration']) ? $data['DialCallDuration'] : 0;
            $GCMData ['doctorCallStatus'] = $data['CallStatus'];
            $GCMData ['patientCallStatus'] = $data['customerCallStatus'];
            $GCMData ['doctorUserId'] = $doctor['user_id'];
            $GCMData ['dtmf'] = $data['dtmf'];
            $GCMData ['callback_request_id'] = $callBackRequestId ? $callBackRequestId : 0;
            $params ['msg'] = json_encode($GCMData);
            if ($patient_app ['platform_id'] == 1) {
                $params ['GCMDeviceIds'] = $patient_app ['registartion_id'];
                $params ['msg'] = json_encode($GCMData);
                $result ['response'] = $communicationHelper->gcmCommunication($params);
            } else if ($patient_app ['platform_id'] == 2) {
                $params ['APNSDeviceIds'] = $patient_app ['registartion_id'];
                $params ['push_msg'] = $GCMData;
                $result ['response'] = $communicationHelper->pushApnsCommunication($params);
            }
            //START - PUSH MESSAGE FOR CALL ALERT     
            //START - PUSH MESSAGE FOR DO RATING
            if ($data['DialCallDuration'] && ($data['CallStatus'] == 'Connected')) {
                $GCMData = array();
                $GCMData ['msg_text'] = "Rate your last call.";
                $GCMData ['type'] = 'do_rating';
                $GCMData ['clinic_type'] = $patientIsClinic ? 1 : 0;
                $GCMData ['call_id'] = $data['Transactionid'];
                $GCMData ['call_duration'] = $data['DialCallDuration'];
                $params ['msg'] = json_encode($GCMData);
                if ($patient_app ['platform_id'] == 1) {
                    $params ['GCMDeviceIds'] = $patient_app ['registartion_id'];
                    $params ['msg'] = json_encode($GCMData);
                    $result ['response'] = $communicationHelper->gcmCommunication($params);
                } else if ($patient_app ['platform_id'] == 2) {
                    $params ['APNSDeviceIds'] = $patient_app ['registartion_id'];
                    $params ['push_msg'] = $GCMData;
                    $result ['response'] = $communicationHelper->pushApnsCommunication($params);
                }
            }
            //END - PUSH MESSAGE FOR DO RATING             

            $result['status'] = 1;
            $result['msg'] = "Data Successfully inserted";
            $this->setHeader('RESPONSE_CODE', '1');
        } else {
            $result['status'] = 0;
            $result['msg'] = "Invalid Key";
            $this->setHeader('RESPONSE_CODE', '0');
        }
        return Response::make($result, 200, $this->headers);
    }

    public function saveCitrusAuthToken() {
        try {
            $checkAuthToken = True;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data = Input::all();
            $data['token'] = $data['citrus_authtoken'];
            $data['email'] = $data['citrus_email_address'];
            $user_id = $this->getUserIdbyToken($this->authToken);
            $citrusAuthTokenInsertion = false;

            if (!empty($data['token']) && $data['token'] != '(null)' && !empty($data['email'])) {
                $exists = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 3)->exists();

                if (empty($exists)) {
                    $isCitrusEmailExist = UserWallet::where('email', '=', $data['email'])->where('wallet_id', '=', 3)->pluck('email');

                    $walletInfoModel = new UserWallet();
                    $walletInfoModel->user_id = $user_id;
                    $walletInfoModel->token = $data['token'];
                    $walletInfoModel->email = $data['email'];
                    $walletInfoModel->wallet_id = 3;
                    $walletInfoModel->save();

                    $citrusAuthTokenInsertion = empty($isCitrusEmailExist) ? true : false;
                } else {
                    UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 3)->update(array('token' => $data['token'], 'email' => $data['email']));
                }

                //RUN PROMO CODE CAMPAIGN 
                $this->_runPromoCodeCampaign(array('user_id' => $user_id, 'citrusAuthTokenInsertion' => $citrusAuthTokenInsertion));

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.AUTHTOKEN_UPDATE');
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1040;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    protected function _runPromoCodeCampaign($data = array()) {

        $user_id = $data['user_id'];
        $citrusAuthTokenInsertion = $data['citrusAuthTokenInsertion'];
        $promoCode = Config::get('constants.PROMO_CODE_FOR_CAMPAIGN');

        if (empty($user_id) || empty($citrusAuthTokenInsertion) || empty($promoCode)) {
            return false;
        }

        $queryResult = DB::select("SELECT * FROM coupons WHERE code = '$promoCode' AND valid_from <= CURDATE() AND valid_till >= CURDATE()");
        foreach ($queryResult as $record) {
            $promoCodeValue = $record->code;
            $promoCodeStartDate = $record->valid_from;
            $promoCodeEndDate = $record->valid_till;
        }

        if (!empty($promoCodeValue)) {

            $created_at = UserWallet::where('user_id', '=', $user_id)->where('wallet_id', '=', 3)->pluck('created_at');
            $created_at = date('Y-m-d', strtotime($created_at));

            if ($created_at >= $promoCodeStartDate && $created_at <= $promoCodeEndDate) {
                $citrusModel = new UserWallet();
                $data['user_id'] = $user_id;
                $data['Email'] = $citrusModel->getWalletInfoColumn(array('user_id' => $user_id, 'columnName' => 'email'));
                $data['promoCode'] = $promoCodeValue;
                $data['connect'] = $this->helper->load('citrus');
                $data['user_id'] = $user_id;

                $promoValidate = new CouponUsage();
                $promoValidate->promo_code_model($data);
            }
        }

        return true;
    }

    protected function _getTransactionStatus($pg_response) {

        if (stristr($pg_response, 'failed') || stristr($pg_response, 'FAILURE')) {
            return 2;
        } elseif (stristr($pg_response, 'error')) {
            return 3;
        }

        return 1;
    }

    public function callStatus() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            $communication = Call::whereRaw('caller_id = ' . $user_id)->orderBy('call_id', 'DESC')->first();

            if (!empty($communication) && $communication->call_duration > 0) {
                $result['call_duration'] = $communication->call_duration;
                $result['call_status'] = 1;
                $result['msg'] = "Call completed successfully.";
            } else {
                $result['call_duration'] = 0;
                $result['call_status'] = 0;
                $result['msg'] = "Call does not exist or call duration is zero seconds.";
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function rating() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data['user_id'] = $user_id = $this->getUserIdbyToken($this->authToken);
            $call_id = $data['call_id'] = $data['communication_id'];
            $userRatingObj = new CallRating();
            if ($userRatingObj->validate($data)) {
                $this->setHeader('RESPONSE_CODE', '1');
                $communication = Call::whereRaw('call_id = ' . $call_id)->first();

                if (!empty($communication) && $communication->call_duration > 0) {
                    $ratingExist = CallRating::whereRaw('call_id = ' . $communication->call_id)->exists();
                    if (!$ratingExist) {
                        $userRatingObj->call_id = $communication->call_id;
                        $userRatingObj->user_id = $user_id;
                        $userRatingObj->rating = $data['rating'];
                        $userRatingObj->comment = $data['comment'];
                        $userRatingObj->name = ucfirst($data['name']);
                        $userRatingObj->age = $data['age'];
                        $userRatingObj->sex = ucfirst($data['sex']);
                        $userRatingObj->save();
                        $result['status'] = 1;
                        $result['msg'] = Config::get('config_msg.RATING_ADD_SUCCESS');
                    } else {
                        $result['msg'] = "Rating is already given for this call.";
                        $result['status'] = 0;
                        $this->setHeader('RESPONSE_CODE', 0);
                    }
                } else {
                    $result['msg'] = "Call does not exist or call duration is zero.";
                    $result['status'] = 0;
                    $this->setHeader('RESPONSE_CODE', 0);
                }
            } else {
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $userRatingObj->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
