<?php

class applink_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $ci = get_instance();
        $ci->load->helper('miscellaneous_helper');
    }

    function appLinkList($params = array()) {

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT * FROM applink_log ";
        $mainQueryParams['defaultSorting'] = " applinklog_id DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM applink_log ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

}
