<?php

class DoctorPatientMapping extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'doctor_patient_mapping';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'mapping_id';

    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'doctor_id' => 'required|integer',
        'patient_id' => 'required|numeric'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }
    
    public function doMapping($params = null) {
        
        $type = !empty($params['type']) ? $params['type'] : '';
        
        if($this->validate($params) && $type == 'calling') {
            
            $mapping = $this::whereRaw('doctor_id = ' . $params['doctor_id'] . ' AND '. 'patient_id = ' . $params['patient_id'])->first();
            
            if(count($mapping)) {
                $mapping->$type = 1;
                $mapping->save();
            }
            else {
                $this->doctor_id = $params['doctor_id'];
                $this->patient_id = $params['patient_id'];
                $this->$type = 1;
                $this->save();
            }
        }
    }
    
    public function checkMapping($params = array()) {

        if(empty($params['doctor_id']) || empty($params['patient_id']) || $params['type'] != 'calling') {
            return 0;
        }
        
        $mappingId1 = $this::whereRaw('doctor_id = ' . $params['doctor_id'] . ' AND '. 'patient_id = ' . $params['patient_id'] . ' AND '. $params['type']. " = 1")->pluck('mapping_id');
        
        $mappingId2 = $this::whereRaw('doctor_id = ' . $params['patient_id'] . ' AND '. 'patient_id = ' . $params['doctor_id'] . ' AND '. $params['type']. " = 1")->pluck('mapping_id');

        return ($mappingId1 || $mappingId2) ? 1 : 0;
    }

}
