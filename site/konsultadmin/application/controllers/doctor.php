<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class doctor extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('doctor_model', '', TRUE);
        $this->load->model('specialization_model', '', TRUE);
    }

    function is_logged_in() {
        $is_logged_in = $this->session->userdata('userid');
        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/doctor");
    }

    public function doctorAnalytics() {
        $this->load->view('admin/header');
        $this->load->view("admin/doctorAnalytics");
    }

    public function doctorAnalyticsList() {

        $data = array();
        $resultsData = $this->doctor_model->doctorAnalytics($_POST);
        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];

        foreach ($result->result() as $row) {
            $data[] = array(
                'users$user_id' => $row->user_id,
                'name' => $row->name,
                'email' => $row->email,
                'mobile' => $row->mobile,
                'patient_count' => $row->patient_count,
                'total_calls' => $row->total_calls,
                'total_earning' => $row->total_earning,
                'list_category' => $row->list_category,
                'weightage' => $row->weightage,
                'extension' => $row->extension
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }

    public function doctorList() {

        $data = array();
        $resultsData = $this->doctor_model->doctorList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];

        foreach ($result->result() as $row) {
            $data[] = array(
                'users$user_id' => $row->user_id,
                'users$name' => $row->name,
                'users$email' => $row->email,
                'user_wallet$email' => $row->wallet_email ? $row->wallet_email : '',
                'specialization' => $this->doctor_model->doctorSpecialityById($row->user_id),
                'mobile' => $row->mobile,
                'photo' => $row->photo,
                'city_master$name' => $row->city,
                'online_status' => $row->online_status,
                'is_approved' => $row->is_approved,
                'created_at' => $row->created_at
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }

    public function toggleApproved1() {
        if ($_POST['val'] == '1')
            $val = 0;
        else
            $val = 1;
        $data = array(
            'is_approved' => $val
        );
        echo $this->doctor_model->toggleApproved($data, $_POST['id']);
    }
    
    public function toggleStatus() {

        if ($_POST['val'] == '1')
            $val = 0;
        else
            $val = 1;

        $data = array(
            'online_status' => $val
        );

        $doctorStatus = $this->doctor_model->toggleStatus($data, $_POST['id']);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'doctor_status';
        $logData['request'] = array('status' => $val, 'user_id' => $_POST['id']);
        $logData['response'] = array('logging_msg' => (($doctorStatus) ? ($val ? 'Online!' :'Offline!') : "Unable to change online/offline status!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);      
        
        echo $doctorStatus;
    }    

    public function toggleApproved() {

        $val = ($_POST['val'] == '1') ? 0 : 1;

        $data = array(
            'is_approved' => $val
        );

        $URL = APIURL . 'doctor/approve/' . $_POST['id'] . '/' . $val;
        $result = $this->httpGet($URL);
        $result = json_decode($result, true);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'doctor_approval';
        $logData['request'] = array('is_approved' => $val, 'url' => $URL);
        $logData['response'] = array_merge(array('logging_msg' => (($result['status']) ? ($val ? 'Approved!' :'Dis-approved!') : "Unable to change approval status!")), $result);
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);        

        if ($result['status'] == 1) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function updateDoctorProfileColumn() {

        $columnValue = !empty($_POST['column_value']) ? $_POST['column_value'] : 0;
        $columnName = !empty($_POST['column_name']) ? $_POST['column_name'] : 'weightage';
        $user_id = $_POST['user_id'];

        $updateColumn = $this->doctor_model->updateDoctorProfileColumn(array('columnName' => $columnName, 'columnValue' => $columnValue, 'user_id' => $user_id));

        $result = json_encode($updateColumn);

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = "doctor_".(($columnName == 'weightage') ? 'weight' : 'extension');
        $logData['request'] = $_POST;
        $logData['response'] = array('logging_msg' => ($updateColumn ? 'Updated successfully!' :'Updation failed!'));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);          
        
        echo $result;
        
    }

    public function httpGet($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function escapeString($string) {

        global $connection;

        // depreciated function
        if (version_compare(phpversion(), "4.3.0", "<")) {
            return mysqli_escape_string($connection, $string);
        } else {
            return mysqli_real_escape_string($connection, $string);
        }
    }

    public function DoctorDetailById($id) {
        $data = array();
        $result = $this->doctor_model->doctorDetailById($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->user_id,
                'name' => $row->name,
                'email' => $row->email,
                'mobile' => $row->mobile,
                'photo' => $row->photo,
                'city' => $row->city,
                'is_approved' => $row->is_approved == 1 ? 'YES' : 'NO',
                'created_at' => $row->created_at,
                'medical_registration_no' => $row->medical_registration_no,
                'total_experience' => $row->total_experience,
                'info' => $row->info,
                'per_min_charges' => $row->per_min_charges,
                'other_info' => $row->other_info,
                'area' => $row->area,
                'specialization' => $row->specialization,
                'qualification' => $this->getQualifications($id),
                'experience' => $this->getExperience($id)
            );
        }

        echo json_encode($data);
    }

    public function viewPatientsList($id) {
        $data = array();
        $result = $this->doctor_model->patientsList($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->user_id,
                'name' => $row->name,
                'email' => $row->email
            );
        }

        echo json_encode($data);
    }

    public function getQualifications($id) {
        $data = array();
        $result = $this->doctor_model->getQualifications($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'university' => $row->university,
                'degree' => $row->degree,
                'start_date' => $row->start_date,
                'end_date' => $row->end_date
            );
        }

        return $data;
        //echo json_encode($data);
    }

    public function getExperience($id) {
        $data = array();
        $result = $this->doctor_model->getExperience($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'hospital' => $row->hospital,
                'designation' => $row->designation,
                'join_date' => $row->join_date,
                'end_date' => $row->end_date,
                'is_current' => $row->is_current == 1 ? 'YES' : 'NO'
            );
        }

        return $data;
    }

    public function toggleClinic() {

        $rowData = array();
        $rowData['user_id'] = $_POST['user_id'];
        $rowData['column_name'] = 'list_category';
        $rowData['column_value'] = ($_POST['val'] == '1') ? 0 : 1;

        $this->doctor_model->updateDoctorDetails($rowData);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = "doctor_toggleClinic_edit";
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => 'Updated successfully!');
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);         

        echo 1;
    }

}
