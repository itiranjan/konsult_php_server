<?php
/**
 * Template Name: Doctor Info
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */

 get_header();

global $wpdb;

	// fetching country list
	$country =  $wpdb->get_results("select country_master.country_id, country_master.name as country_name from country_master", ARRAY_A);
	$totalRows_country = count($country);
    
    $my_wpdb = new WPDB( 'root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
	
	// fetching specialization list
	$specialize =  $my_wpdb->get_results("select specialization_master.specialization_id, specialization_master.name as specialization_name from specialization_master", ARRAY_A);
	$totalRows_specialize = count($specialize);
	
	// inserting data to patient_contact table
	
	$doctor_table = "doctor-info"; // Table name
	
	if((isset($_POST['doctor-info'])) ) //and (!empty($_POST['patient_info']))
	{
		$wpdb->insert($doctor_table, array(
										'doctor_name' => $_POST['doctor-name'],
										'email' => $_POST['doctor-email'],
										'phone' => $_POST['doctor-phone'],
										'age' => $_POST['doctor-age'],
										'gender' => $_POST['gender'],
										'specialization_id' => $_POST['speciality'],
										'country' => $_POST['country'],
										'additional_info' => $_POST['additional-message']
									));
				
		/* =============== Mail Function =================== */
        $to = "info@konsultapp.com";
         $subject = "New Doctor Registration from Dr. ".$_POST['doctor-name'];
         
         $message = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin-bottom: 20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi Admin,</p>
										<p style="font-size:14px Arial;">Name: ' .$_POST["doctor-name"]. '</p>
										<p style="font-size:14px Arial;">Email: ' .$_POST['doctor-email']. '</p>
										<p style="font-size:14px Arial;">Phone: ' .$_POST['doctor-phone']. '</p>
										<p style="font-size:14px Arial;">Age: ' .$_POST['doctor-age']. '</p>
										<p style="font-size:14px Arial;">Gender: ' .$_POST['gender']. '</p>
										<p style="font-size:14px Arial;">Specialization: '.$_POST['speciality_val']. '</p>
										<p style="font-size:14px Arial;">Country: ' .$_POST['country_val']. '</p>
										<p style="font-size:14px Arial;">Message: ' .$_POST['additional-message'].'</p>										
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			//$from = $_POST['doctor-email'];
			$from = "info@konsultapp.com";
			$headers = "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=UTF-8\n";
			$headers .= 'From: Konsultapp <'.$from.'>' . "\r\n";
			$headers .= "Reply-To: me <".$to.">\r\n";
			$headers .= "X-Priority: 500\n";
			$headers .= "X-MSMail-Priority: High\n";
			$headers .= "X-Mailer: My mailer";
         
        $retval = wp_mail($to,$subject,$message,$headers);
        
		/* =============== Mail Function to client =================== */
		$to_email = $_POST['doctor-email'];
        $subject_client = "Thank you for showing interest";
         
        $message_client = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin-bottom: 20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi '.$_POST["doctor-name"].',</p>
										<p style="font-size:14px Arial;">You have successfully Registered. We will be in touch soon. </p>
										<p style="font-size: 14px Arial;">Thanks</p>
										<p style="font-size: 14px Arial;">Konsult App Private Limited</p>
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			$from_client = "info@konsultapp.com";
			$header = "MIME-Version: 1.0\n";
			$header .= "Content-type: text/html; charset=UTF-8\n";
			$header .= 'From: Konsultapp <'.$from_client.'>' . "\r\n";
			$header .= "Reply-To: me <".$to.">\r\n";
			$header .= "X-Priority: 500\n";
			$header .= "X-MSMail-Priority: High\n";
			$header .= "X-Mailer: My mailer";
         
        $retval_client = wp_mail($to_email,$subject_client,$message_client,$header);		
		
        if( $retval == true ) 
		{
            $message = "Thank you! You have successfully submitted the data. We'll be in touch soon.";
        }
		else 
		{
            $message = "You are wrong somewhere";
        }
      
	}	
?>
	
<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
		
			<div class="entry-content" itemprop="mainContentOfPage">
				<?php 
					while ( have_posts() ){
						the_post();							// Post Loop
						mfn_builder_print( get_the_ID() );	// Content Builder & WordPress Editor Content
					}
				?>
			</div>
			
			<?php if( mfn_opts_get('page-comments') ): ?>
				<div class="section section-page-comments">
					<div class="section_wrapper clearfix">
					
						<div class="column one comments">
							<?php comments_template( '', true ); ?>
						</div>
						
					</div>
				</div>
			<?php endif; ?>

			<form id="doctor-registration" name="doctor" action="<?php echo home_url()."/doctor-registration/" ?>" method="post" class="wpcf7-form sent"><!-- onSubmit="return formValidation();" -->
				<div style="display: none;">
					<input type="hidden" name="_wpcf7" value="4">
					<input type="hidden" name="_wpcf7_version" value="4.4.2">
					<input type="hidden" name="_wpcf7_locale" value="en_US">
					<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p3115-o1">
					<input type="hidden" name="_wpnonce" value="3604a626a7">
				</div>
				<div class="patient-info">
					<?php if(!empty($message)) {?>
					<div class="display-msg">
						<p><?php echo $message; ?></p>
					</div>
					<?php } ?>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap your-name">
								<input type="text" name="doctor-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="Name *" required oninvalid="setCustomValidity('Please fill your name')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap your-email">
								<input type="email" name="doctor-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="Email *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required oninvalid="setCustomValidity('Please fill your valid email id')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap phone">
								<input type="tel" name="doctor-phone" pattern="^([0|\+[0-9]{1,5})?([1-9][0-9]{9})$" maxlength="10" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" aria-required="true" aria-invalid="false" placeholder="Phone No. *" required oninvalid="setCustomValidity('Please fill your mobile no')" oninput="setCustomValidity('')">
							</span>
						</p>
					</div>
					<div class="column one-fourth">
						<p>
							<span class="wpcf7-form-control-wrap age">
								<select name="doctor-age" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="contct-age" aria-required="true" aria-invalid="false" required oninvalid="setCustomValidity('Please select your age')" oninput="setCustomValidity('')">
									<option value="">----Select Age----</option>
									<?php
										for($i=1; $i<=100;$i++)	{
									?>
											<option value="<?php echo $i;?>"><?php echo $i;?></option>
										<?php }?>					
								</select>
							</span>
						</p>
					</div>
					<div class="column one-fourth">
						<p style="box-sizing: border-box; display: inline-flex; padding: 5px 0 0; font-size: 14px;">
							<span class="gen">Gender</span>
							<span class="wpcf7-form-control-wrap gender">
								<span class="wpcf7-form-control wpcf7-radio">
									<span class="wpcf7-list-item first">
										<input type="radio" name="gender" value="Male" checked="checked">&nbsp;
										<span class="wpcf7-list-item-label">Male</span>
									</span>
									<span class="wpcf7-list-item last">
										<input type="radio" name="gender" value="Female">&nbsp;
										<span class="wpcf7-list-item-label">Female</span>
									</span>
								</span>
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap speciality">
								<select name="speciality" class="wpcf7-form-control wpcf7-select" id="medicl-speciality" aria-invalid="false" onchange="document.getElementById('speciality_selected').value=this.options[this.selectedIndex].text" required oninvalid="setCustomValidity('Please select your specialization')" oninput="setCustomValidity('')">
									<option value="">-----Speciality-----</option>
									<?php if($totalRows_specialize > 0) 
										  {		
											foreach($specialize as $key => $value ) 
											{ ?>
												<option value="<?php echo $value['specialization_id']; ?>"><?php echo $value['specialization_name']; ?></option>
									<?php	}
										} ?>
								</select>
								<input type="hidden" name="speciality_val" id="speciality_selected" value="" />
							</span>
						</p>
					</div>
					<div class="column one-second">
						<p>
							<span class="wpcf7-form-control-wrap country">
								<select name="country" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="country" aria-required="true" aria-invalid="false" onchange="document.getElementById('country_selected').value=this.options[this.selectedIndex].text" required oninvalid="setCustomValidity('Please select your country')" oninput="setCustomValidity('')">
									<option value="">-----Country-----</option>
									<?php if($totalRows_country > 0) 
										  {		
											foreach($country as $key => $value ) 
											{ ?>
												<option value="<?php echo $value['country_id']; ?>"><?php echo $value['country_name']; ?></option>
									<?php	}
										} ?>
								</select>
								<input type="hidden" name="country_val" id="country_selected" value="" />
							</span>
						</p>
					</div>
					<div class="column one">
						<p>
							<span class="wpcf7-form-control-wrap your-message">
								<textarea name="additional-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Additional Information">
								</textarea>
							</span>
						</p>
					</div>
					<div class="column one">
						<input type="submit" id="doc" value="Submit" name="doctor-info" class="wpcf7-form-control wpcf7-submit pi-btn">
					</div>
				</div>
			</form>
		</div>
		
		<!-- .four-columns - sidebar -->
		<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>