<?php
/**
 * Template Name: Downlaod App Template.
 * Description: A Page Template To Display Download Page.
 * @author Sahil Ahlawat
 */
    get_header();
?>
<div class="container-fluid">
<div class="row">
    <div class="col-sm-12">
        <h1>Get konsult app to consult any doctor from the comfort of your home</h1>
    <div class="col-sm-4 col-xs-hidden col-left-">
        <img src="http://www.konsult.com/wp-content/uploads/2017/01/konsult-buddy01.png" class="" />
        </div>
    <div class="col-sm-8 col-right-">
        <h2></h2>
        <ul>
            
            <li><i class="fa fa-check" aria-hidden="true"></i> Search & select leading doctors across the country</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> Talk to any doctor from the comfort of your home</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> Chat & share medical reports with your doctor for free</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> Doctors set their call charges and you pay accordingly</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> Automatic payment through Paytm & Citrus wallets</li>
            <li><i class="fa fa-check" aria-hidden="true"></i> Get regular health tips to stay healthy</li>
        </ul>
        <div class="col-sm-12 sociallinks">
        <div class="col-sm-6 col-xs-6 text-align-right"><a href="https://play.google.com/store/apps/details?id=com.konsult"><img src="http://www.konsult.com/wp-content/uploads/2017/04/androidapp.png"></a></div>
<div class="col-sm-6 col-xs-6 secondimagplay"><a href="https://itunes.apple.com/in/app/konsult-app/id1017239812"><img src="http://www.konsult.com/wp-content/uploads/2017/04/iosapp.png"></a></div>
        </div>
        <div class="col-sm-8 col-sm-offset-2 sendlinkform">
        <form>
           <div class="col-sm-8"> <input type="text" name="mobile-no" class="textfieldsms" placeholder="Send Link To Your Mobile"/></div>
            <div class="col-sm-4"> <input type="submit" class="sendmessagelink" name="submit" value="Send Link" /></div>
            </form>
            <div class="sendingmessage">Sending Link to you...</div>
            
        </div>
        </div>
    
    </div>
    <div class="sectionseperator"></div>
    </div>
</div>
<script>
jQuery(".sendmessagelink").click(function(e){
    e.preventDefault();
    jQuery(".sendingmessage").show();
    jQuery(".sendingmessage").text("Sending Link to you...");
    phonenumber();
});
</script>
<style>
    #Top_bar #logo img {
    max-height: 120%;
}
    .top_bar_left.clearfix {
        margin:0px !important;
    }
    
    input.sendmessagelink{
        padding:0px !important;
    }
    input.textfieldsms {
    border: 1px solid;
}
        .text-align-right{
        text-align: right;
    }
    .sectionseperator{
        height:40px;
        width: 100%;
        clear: both;
    }
    .sendingmessage{
        border:1px solid #24cdd9;
        color:#24cdd9;
            clear: both;
    padding: 10px;
        display:none;
    }
    .sendlinkform{
        margin-top: 20px;
    }
    .sendlinkform input{
        width:100%;
        
    }
    h1{
        text-align: center;
        font-size:34px;
    }
    #Header {
    min-height: 50px !important;
}
    .col-right- ul li{
        margin-top: 10px;
        font-size:1.2em;
    }
   .col-right- ul{
        
    margin-left: 20%;
    margin-bottom: 20px;

    }
    .col-left-{
        text-align: right;
    }
    .col-right-{
        
        margin-top:5%;
    }
    .sociallinks{
        margin-bottom: 20px;
    }
    h2{
        font-size:20px;
    }
    @media screen and (min-width: 786px) {
        
        .col-left- img {
    margin-right: -40px;
}
        
    }
    @media screen and (max-width: 786px) {
   .col-right- ul{
        
    margin-left: 0%;
    margin-bottom: 20px;

    }
        .col-sm-12.sociallinks {
    margin-bottom: 65px;
}
        .sociallinks .col-sm-6{
            padding:0px;
        }
        .sociallinks{
            padding:0px;
        }
        .col-right-{
        padding:0px;
        }
}
</style>
<?php
get_footer();
?>