<?php

class KccAccountStatement extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_account_statements';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'statement_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = false;
    
    public function getAccountStatement($params = array()) {
        return $this->select('closing_balance', 'statement_date')->where('user_id', '=', $params['user_id'])->orderby('statement_id', 'DESC')->first();
    }

}
