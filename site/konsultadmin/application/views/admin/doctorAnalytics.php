<div id="window3">
    <form id="DetailP">

        <table width="100%">

            <tr>
                <td><div style="width:500px" id="patientDetails"/>&nbsp;&nbsp;
                </td>
            </tr>

        </table>
    </form>
</div>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

    <div id="clientsDb">
        <span style="font-size:20px">Doctors Analytics<br/><br/></span>
        <div id="grid" ></div>
    </div>    

    <script>

        var window3 = $("#window3");
        $(document).ready(function() {

            var onClose = function()
            {
                mainGrid.data("kendoGrid").refresh();
            }

            window3.kendoWindow({
                width: "700px",
                visible: false,
                title: "Patients List"
            });

            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                edit: function(e) {
                    
                    var input = e.container.find(".k-input");
                    input.blur(function(){
                        column_value = input.val();
                        updateDoctorProfileColumn(e.model.users$user_id, 'extension', column_value);
                    });                       
                    
                    var inputArea = e.container.find("textarea");
                    inputArea.blur(function(){
                        column_value = inputArea.val();
                        updateDoctorProfileColumn(e.model.users$user_id, 'weightage', column_value);
                    });
                },                  
                toolbar:["excel"],
                selectable: "multiple cell",
                allowCopy: true,                    
                excel: {
                    allPages: true,
                    fileName: "doctor_analytics_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                    filterable: true
                },
                dataSource: {
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    transport: {
                        read: {
                            type: "POST",
                            url: base_url + "/index.php/doctor/doctorAnalyticsList",
                            dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                        },
                        parameterMap: function (options) {
                            if (options.filter) {
                                KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                            }
                            return options;
                        },
                    },                      
                    schema: {
                        data: "list",
                        total: "total",
                        model: {
                            fields: {
                                users$user_id: { type: "number", editable: false},
                                name: {type:"string", editable: false},
                                email: {type:"string", editable: false},
                                mobile: {type: "string", editable: false},
                                patient_count: {type: "number", editable: false},
                                total_calls: {type: "number", editable: false},
                                total_earning: {type: "number", editable: false},
                                list_category: {type: "number", editable: false},
                                extension: {type: "string" <?php echo (!empty($this->session->userdata('permissions')['doctor_updateDoctorProfileColumn_edit']) ? ",editable: true" : ",editable: false"); ?>},
                                weightage: {type: "number" <?php echo (!empty($this->session->userdata('permissions')['doctor_updateDoctorProfileColumn_edit']) ? ",editable: true" : ",editable: false"); ?>},
                            }
                        }
                    },
                    pageSize: 100
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                },                     
                sortable: true,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                pageable: {
                    refresh: true,
                    pageSize: 100,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function(e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                columns: [
                    {field: "users$user_id", title: "Id", width: '7px'},
                    {field: "name", title: "Name", width: '15px'},
                    {field: "email", title: "Email", width: '15px'},
                    {field: "mobile", title: "Mobile", width: '12px'},
                    {field: "patient_count", title: "# Patients", width: '8px', filterable: false, template: "#if(patient_count > 0) {#<a id='edit' onclick='viewPatientsList(#= users$user_id#);' style='cursor:pointer' title='Click to view patient list'>#: patient_count #</a>#} else{##:patient_count##}#"},
                    {field: "total_calls", title: "Total Calls", width: '8px', filterable: false},
                    {field: "total_earning", title: "Total Earning", width: '8px', filterable: false},
                    {field: "list_category", title: "Clinic", width: '8px', template: "<a id='toggle' onclick='<?php echo (!empty($this->session->userdata('permissions')['doctor_toggleClinic_edit']) ? 'toggleClinic(#= users$user_id#,#= list_category#);' : "javascript:void(0)"); ?>' style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/#= list_category == 0 ? 'employee_month.png' : 'clinic.ico' #' title='Click to change clinic status'></a>", filterable: { ui: clinicFilter, "messages": { "info": "Select:" }, operators: {number: {eq: "Equal To"}}}},
                    {field: "extension", title: "Extension", width: '8px', filterable: true, defaultValue: {}},  
                    {field: "weightage", title: "Weight", width: '6px', filterable: true, defaultValue: {}, editor: textEditorInitialize},
                ],
                editable: true,
            }).data("kendoGrid");              

            mainGrid.thead.kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target;
                    return $(target).text();
                }                    
            });
        });
        
        function clinicFilter(element) {

            element.kendoDropDownList({
                dataTextField: "name",
                dataValueField: "id",
                dataSource: [
                    { id: 1, name: "Clinic" },
                    { id: 0, name: "Non-clinic" }
                ],
                optionLabel: "--Select--"  
            });
        }        
        
        function toggleClinic(id, value) {

            if(value == 1 && !confirm("Are you sure that you want to remove this doctor from konsult clinic list?")) {
                return;
            }
            else if(value == 0 && !confirm("Are you sure that you want to add this doctor in konsult clinic list?")) {
                return;
            }

            callFunction = function(d) {
                $("#grid").data("kendoGrid").dataSource.read();
            };
            ajaxCall(base_url + '/index.php/doctor/toggleClinic', 'user_id=' + id + "&val=" + value, callFunction);
        }            

        function viewPatientsList(id) {
            $.ajax({
                type: 'POST',
                url: "<?php echo URL; ?>/index.php/doctor/viewPatientsList/" + id,
                dataType: 'json',
                success: function(result) {

                    window3.data("kendoWindow").center();
                    window3.data("kendoWindow").open();

                    if(result.length > 0) {

                        str = '<table width="100%" border="1" ><tr><th>Id</th><th>Name</th><th> Email</th></tr>';
                        for (i = 0; i < result.length; i++) {
                            str += "<tr><td>" + result[i].id + "</td><td>" + result[i].name + "</td><td>" + result[i].email + "</td></tr>";
                        }
                        str += '</table>';

                    } else {

                        str = 'No patients has been connected to this doctor at yet.';
                    }

                    $('#patientDetails').html(str);
                }
            });
        }                      

        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&apos');
        }
        
        var textEditorInitialize = function(container, options) {
            $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
            .appendTo(container);
        };       
        
        function isInt(value) {
          return !isNaN(value) && 
                 parseInt(Number(value)) == value && 
                 !isNaN(parseInt(value, 10));
        }        
        
        function updateDoctorProfileColumn(user_id, column_name, column_value) {
            //alert(column_name);alert(column_value);
            if (!isInt(column_value)) {
                alert("Entered value is not an integer");
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
                return;
            }
            
            if (column_name == 'extension' && (column_value < 100 || column_value > 9999)) {
                alert("Entered value must be between 100 and 9999");
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
                return;
            }

            var getConfirmation = confirm("Are you sure that you want to update the column.");
            if(!getConfirmation) {
                $('#grid').data('kendoGrid').dataSource.read();
                $('#grid').data('kendoGrid').refresh();
                return;
            }

            $.ajax({

                'url' : base_url + "/index.php/doctor/updateDoctorProfileColumn",
                'type' : 'POST',
                'data' : {
                    'user_id' : user_id,
                    'column_name' : column_name,
                    'column_value' : column_value
                },
                'success' : function(data) {  
                    
                    if(column_name == 'extension' && data == 0) {
                        alert("Sorry!! Extension number "+ column_value +" is already assigned to some other doctor!");
                    } else {
                        alert("Changes has been done succesfully!!");
                    }
                    
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();

                },
                'error' : function(request,error)
                {
                    alert("Whoops!! Something might be wrong. Please try again.");
                    //alert("Request: "+JSON.stringify(request));
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();

                }
            });                
        }             
        
        function ajaxCall(url, data, customFunction) {
            //alert(data);
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function() {
                    $('#loading').show();
                },
                complete: function() {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();                    
                    $('#loading').hide();
                },
                success: customFunction,
//                error: function() {
//                    alert(error);
//                }
            });
        }
    </script>
</div>
</div>