<?php

class Doctor extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'doctor_profile';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'doctor_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'medical_registration_no' => 'required|min:3|max:250',
        'total_experience' => 'required|integer|between:0,900',
        'per_min_charges' => array('regex:/^[0-9]+(\.[0-9]{1,2})?$/', 'required', 'numeric', 'between:0,5000')
    );

    /**
     * validation rules defined
     * @var array
     */
    private $rulesdoc = array(
        'user_id' => 'required|integer|exists:users,user_id'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validateDoc($data) {
        // make a new validator object
        $v = Validator :: make($data, $this->rulesdoc);

        // check for failure
        if ($v->fails()) {
            // set errors and return false
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        if(!empty($data ['is_approved'])) {
            $citrusModel = new UserWallet();
            $citrusEmail = $citrusModel->getWalletInfoColumn(array('user_id' => $data ['user_id'], 'columnName' => 'email', 'wallet_id' => 3));
            
            $paytmMapping = $citrusModel->getWalletInfoColumn(array('user_id' => $data ['user_id'], 'columnName' => 'token', 'wallet_id' => 1, 'checkExpiry' => 1));
            
            if (empty($citrusEmail) && (empty($paytmMapping) || (!empty($paytmMapping) && $paytmMapping['status'] != 1))) {
                $this->errors = array('doctor' => array('0' => "Wallet is not mapped with user!"));
                return false;
            }
        }
        
        return true;
    }

    public function getDoctorDetails($params = array()) {
        return DB::table('doctor_profile')->join('users', 'users.user_id', '=', 'doctor_profile.user_id')->select('doctor_profile.per_min_charges', 'users.mobile', 'users.user_id')->where('extension', '=', $params['extension'])->first();
    }

    public static function savePageURL($params = array()) {

        $userId = $params['user_id'];

        if (empty($userId)) {
            return;
        }

        $userName = trim($params['name']);
        $salutation = trim($params['salutation']);
        if (empty($userName) || empty($salutation)) {
            $user = User::where('user_id', '=', $userId)->select('salutation', 'name')->first();
            $salutation = trim($user->salutation);
            $name = trim($user->name);
            $fullName = $salutation . " " . $name;
        } else {
            $fullName = trim($salutation . $userName);
        }

        $specialization = DB::table('specialization_master')->join('doctor_specialization_rel', 'doctor_specialization_rel.specialization_id', '=', 'specialization_master.specialization_id')->select('specialization_master.name')->where('user_id', '=', $userId)->orderBy('doctor_specialization_rel.is_default', 'DESC')->limit(1)->first();

        if (!empty($specialization->name)) {
            $specialization = trim($specialization->name);
        } else {
            $specialization = '';
        }

        $pageURL = Doctor::getSlug($fullName . " " . $specialization);

        $pageURLExist = 0;
        $i = 1;
        do {
            $pageURLExist = Doctor::where('page_url', '=', $pageURL)->pluck('page_url');
            if (!empty($pageURLExist)) {
                $pageURL = $pageURL . "-$i";
                $i++;
            }
        } while (!empty($pageURLExist));

        Doctor::where('user_id', '=', $userId)->update(array(
            'page_url' => $pageURL
        ));

        return 1;
    }
    
    public static function getSlug($str, $limit = 256) {

        if (strlen($str) > $limit) {
            $str = Engine_String::substr($str, 0, $limit) . '...';
        }

        $slugString = $str;

        //CASE 1:
        $search = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ',
            'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
        $replace = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', '
U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
        $str = str_replace($search, $replace, $str);

        $str = preg_replace('/([a-z])([A-Z])/', '$1 $2', $slugString);
        $str = strtolower($str);
        $str = preg_replace('/[^a-z0-9-]+/i', '-', $str);
        $str = preg_replace('/-+/', '-', $str);
        $str = trim($str, '-');

        //CASE 2:
        if (empty($str) || $str == '-') {
            setlocale(LC_CTYPE, 'pl_PL.utf8');
            $str = @iconv('UTF-8', 'ASCII//TRANSLIT', $slugString);
            $str = strtolower($str);
            $str = strtr($str, array('&' => '-', '"' => '-', '&' . '#039;' => '-', '<' => '-', '>' => '-', '\'' => '-'));
            $str = preg_replace('/^[^a-z0-9]{0,}(.*?)[^a-z0-9]{0,}$/si', '\\1', $str);
            $str = preg_replace('/[^a-z0-9\-]/', '-', $str);
            $str = preg_replace('/[\-]{2,}/', '-', $str);

            //CASE 3:
            if (empty($str) || $str == '-') {

                $cyrillicArray = array(
                    "Є" => "YE", "І" => "I", "Ї" => "YI", "Ѓ" => "G", "і" => "i", "№" => "#", "є" => "ye", "ѓ" => "g",
                    "А" => "A", "Б" => "B", "В" => "V", "Г" => "G", "Д" => "D",
                    "Е" => "E", "Ё" => "YO", "Ж" => "ZH",
                    "З" => "Z", "И" => "I", "Й" => "J", "К" => "K", "Л" => "L",
                    "М" => "M", "Н" => "N", "О" => "O", "П" => "P", "Р" => "R",
                    "С" => "S", "Т" => "T", "У" => "U", "Ф" => "F", "Х" => "X",
                    "Ц" => "C", "Ч" => "CH", "Ш" => "SH", "Щ" => "SHH", "Ъ" => "'",
                    "Ы" => "Y", "Ь" => "", "Э" => "E", "Ю" => "YU", "Я" => "YA",
                    "а" => "a", "б" => "b", "в" => "v", "г" => "g", "д" => "d",
                    "е" => "e", "ё" => "yo", "ж" => "zh",
                    "з" => "z", "и" => "i", "ї" => "yi", "й" => "j", "к" => "k", "л" => "l",
                    "м" => "m", "н" => "n", "о" => "o", "п" => "p", "р" => "r",
                    "с" => "s", "т" => "t", "у" => "u", "ф" => "f", "х" => "x",
                    "ц" => "c", "ч" => "ch", "ш" => "sh", "щ" => "shh", "ъ" => "",
                    "ы" => "y", "ь" => "", "э" => "e", "ю" => "yu", "я" => "ya", "«" => "", "»" => "", "—" => "-"
                );

                $str = strtr($slugString, $cyrillicArray);
                $str = preg_replace('/\W+/', '-', $str);
                $str = strtolower(trim($str, '-'));
            }
        }

        if (!$str) {
            $str = '-';
        }

        return $str;
    }    

}
