<?php

class KccTransactionType extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_transaction_types';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'transaction_type_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = false;
    
    public function getTransactionTypeId($params = array()) {
        return $this->where('transaction_type_name', '=', $params['transaction_type_name'])->pluck('transaction_type_id');
    }

}
