<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class app extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('user_model', '', TRUE);
        $this->load->model('communication_model', '', TRUE);
        $this->load->model('doctor_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function userInfo() {

        $query = $this->db->query("SELECT specialization_id, name FROM `specialization_master` ORDER BY name ASC");

        $this->load->view('admin/header');
        $this->load->view("admin/appUserInfo", array('specializations' => $query->result()));
    }

    public function showAppUserInfo() {

        $data = $_POST;
        $mobile = $data['mobile'];

        $userDetails = $this->user_model->userDetails(array('mobile' => $data['mobile']));

        if (empty($userDetails)) {

            $response = array('status' => 0, 'msg' => 'User does not exist with given mobile number.');
            echo json_encode($response);
        } else {

            $specializationDocs = array();
            $searchedDocs = 0;
            if (!empty($data['p_doctor_search']) && !empty($data['specialization_id'])) {
                $searchedDocs = 1;
                $specialization_id = $data['specialization_id'];
                $query = $this->db->query("SELECT users.user_id, CONCAT(users.salutation, ' ',users.name) as name, dp.per_min_charges, dp.extension, users.mobile FROM doctor_profile AS dp LEFT JOIN users ON users.user_id = dp.user_id LEFT JOIN user_wallet ON user_wallet.user_id = dp.user_id LEFT JOIN doctor_specialization_rel dsr ON dsr.user_id = users.user_id AND dsr.is_default = 1
 JOIN specialization_master dsm ON dsr.specialization_id = $specialization_id WHERE dp.is_approved = 1 AND dp.online_status = 1 GROUP BY dp.doctor_id ORDER BY dp.weightage DESC, dp.per_min_charges ASC");
                $specializationDocs = $query->result();

                foreach ($specializationDocs as $specializationDoc) {
                    $experience = $this->db->query("select address, hospital_name from doctor_workplace where user_id='$specializationDoc->user_id'");
                    $hospitalDetails = $experience->result();
                    foreach ($hospitalDetails as $hospitalDetail) {
                        $specializationDoc->hospital = empty($hospitalDetail->hospital_name) ? '' : $hospitalDetail->hospital_name;
                        $specializationDoc->address = $hospitalDetail->address;
                    }
                }
            }

            $params = array('user_id' => $userDetails->user_id);
            $callDetails = $this->communication_model->getUserCommunications($params);

            foreach ($callDetails as $key => $callDetail) {
                foreach ($callDetail as $key => $value) {
                    $callDetail->$key = $value;
                    if ($key == 'receiver_id') {
                        $callDetail->doctor_name = $this->communication_model->detailsById($value);
                        $callDetail->specialization = $this->doctor_model->doctorSpecialityById($value);
                        $callDetail->extension = $this->doctor_model->getExtension($value);
                    }
                }
            }

            $this->load->helper('citrus');

            $promoBalance = fetchMVCBalance(array('emailId' => $userDetails->email));
            $userDetails->promo_balance = !empty($promoBalance['mvcAmount']) ? $promoBalance['mvcAmount'] : 'Unable to fetch';

            $user_token = $this->user_model->getCitrusToken(array('user_id' => $userDetails->user_id));
            if (!empty($user_token)) {
                $realBalance = fetchRealBalance(array('citrusToken' => $user_token));
                $userDetails->real_balance = !empty($realBalance['value']) ? $realBalance['value'] : 'Unable to fetch';
            }

            $userDetails->auth_token = $this->user_model->getUserAuthToken(array('user_id' => $userDetails->user_id));

            $this->load->view("admin/showAppUserInfo", array('userDetails' => $userDetails, 'callDetails' => $callDetails, 'specializationDocs' => $specializationDocs, 'searchedDocs' => $searchedDocs, 'patient_mobile' => $mobile, 'patient_user_id' => $userDetails->user_id, 'dbObject' => $this->db));
        }
    }

    public function callInfos() {
        $this->load->view('admin/header');
        $this->load->view("admin/appCallInfos");
    }

    public function callConnect() {

        $rowData = array();
        $patient = $_POST['patient'];
        $doctor = $_POST['doctor'];
        $authToken = $_POST['AUTH_TOKEN'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, APIURL . "call?patient=$patient&doctor=$doctor&C2cDashboardCall=1");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("API_KEY:andapikey", "APP_VERSION:1.0", "CONFIG_VERSION:1.0", "AUTH_TOKEN:$authToken"));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'app_call_connect';
        $logData['request'] = array('url' => APIURL . "call?patient=$patient&doctor=$doctor&C2cDashboardCall=1");
        $responseObject = json_decode($output);
        $responseArray = array();
        $responseArray['logging_msg'] = !empty($responseObject->msg) ? $responseObject->msg : $responseObject->errors;
        $responseArray['status'] = $responseObject->status;
        $logData['response'] = $responseArray;
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);        

        echo $output;
    }

    public function callBackRequest() {

        $rowData = array();
        $request_to = $_POST['receiver_id'];
        $authToken = $_POST['AUTH_TOKEN'];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, APIURL . "callBackRequest");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("API_KEY:andapikey", "APP_VERSION:1.0", "CONFIG_VERSION:1.0", "AUTH_TOKEN:$authToken"));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array('receiver_id' => $request_to));
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'callback_request';
        $logData['request'] = array('url' => APIURL . "callBackRequest", 'request_to' => $request_to);
        $responseObject = json_decode($output);
        $responseArray = array();
        $responseArray['logging_msg'] = !empty($responseObject->msg) ? $responseObject->msg : $responseObject->errors;
        $responseArray['status'] = $responseObject->status;
        $logData['response'] = $responseArray;
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);

        echo $output;
    }

}
