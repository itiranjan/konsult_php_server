<?php
/**
 * Template Name: Contact
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */
//require_once('../../../wp-config.php');
 get_header();

global $wpdb;

	// fetching country list
	$country =  $wpdb->get_results("select country_master.country_id, country_master.name as country_name from country_master", ARRAY_A);
	$totalRows_country = count($country);
	
	$form_type = "contact";
	// inserting data to patient_contact table
	
	$patient_contact = "patient_contact"; // Table name
	
	if((isset($_POST['contact_info'])) ) //and (!empty($_POST['patient_info']))
	{
		$wpdb->insert($patient_contact, array(
										'name' => $_POST['contact-name'],
										'email' => $_POST['email'],
										'phone' => $_POST['phone'],
										'age' => $_POST['age'],
										'gender' => $_POST['gender'],
										'country' => $_POST['country'],
										'subject' => $_POST['subject'],
										'message' => $_POST['message'],
										'type' => $form_type
									));
		/* =============== Mail Function =================== */
         $to = "info@konsultapp.com";
		 //$to = "ekta@detecvision.com";
         $subject = $_POST['subject'];
         
         $message = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin_bottom:20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi Admin,</p>
										<p style="font-size:14px Arial;">Name: ' .$_POST["contact-name"]. '</p>
										<p style="font-size:14px Arial;">Email: ' .$_POST["email"]. '</p>
										<p style="font-size:14px Arial;">Phone: ' .$_POST["phone"]. '</p>
										<p style="font-size:14px Arial;">Age: ' .$_POST["age"]. '</p>
										<p style="font-size:14px Arial;">Gender: ' .$_POST["gender"]. '</p>
										<p style="font-size:14px Arial;">Country: ' .$_POST["country_val"]. '</p>
										<p style="font-size:14px Arial;">Message: ' .$_POST["message"].'</p>										
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			$from = "info@konsultapp.com";
			$headers = "MIME-Version: 1.0\n";
			$headers .= "Content-type: text/html; charset=UTF-8\n";
			$headers .= 'From: Konsultapp <'.$from.'>' . "\r\n";
			$headers .= "Reply-To: me <".$to.">\r\n";
			/*$headers .= "X-Priority: 500\n";
			$headers .= "X-MSMail-Priority: High\n";*/
			$headers .= "X-Mailer: My mailer";
         
         $retval = wp_mail($to,$subject,$message,$headers);
		 
		 /* =============== User Mail Function =================== */
         $to_contact = $_POST["email"];
         $subject_contact = "Thank you for your query";
         
         $message_contact = '<html>
						<head>
							<title>HTML email</title>
						</head>
						<body>
							<table style="width: 80%; margin: 0 auto; border: 15px solid #f1f1f1;margin_bottom:20px;">
								<tr>
									<td style="padding: 25px 0 20px; text-align: center;">
										<img src="http://konsultapp.com/wp-content/uploads/2016/04/logo.png">
									</td>
								</tr>
								<tr>
									<td style="padding: 0 20px;">
										<p style="font-size:14px Arial;">Hi '.$_POST["contact-name"].',</p>
										<p style="font-size:14px Arial;">You have successfully sent the query. We will be in touch soon. </p>
										<p style="font-size: 14px Arial;">Thanks</p>
										<p style="font-size: 14px Arial;">Konsult App Private Limited</p>
									</td>
								</tr>
							</table>
						</body>
					</html>';
         
			$from_contact = "noreply@konsultapp.com";
			$headers_contact = "MIME-Version: 1.0\n";
			$headers_contact .= "Content-type: text/html; charset=UTF-8\n";
			$headers_contact .= 'From: Konsultapp <'.$from_contact.'>' . "\r\n";
			$headers_contact .= "Reply-To: me <".$to.">\r\n";
			/*$headers_contact .= "X-Priority: 500\n";
			$headers_contact .= "X-MSMail-Priority: High\n";*/
			$headers_contact .= "X-Mailer: My mailer";
         
         $retval_contact = wp_mail($to_contact,$subject_contact,$message_contact,$headers_contact);
		 
         
        if( $retval == true ) 
		{
            $message = "Thank you! You have successfully submitted the data. We'll be in touch soon.";
        }
		else 
		{
            $message = "You are wrong somewhere";
        }
	}
	/*else
	{
		$message = "You are wrong somewhere.";
	}*/	
?>
	
<style>
	h1.cntct { padding: 20px 0 0;}
</style>
<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
			
			<div class="section_wrapper clearfix">
					<h1 class="lam">Please leave your valuable feedback</h1>
					
					<form action="<?php echo home_url()."/contact-us/" ?>" method="post" class="wpcf7-form">  
						<div style="display: none;">
							<input type="hidden" name="_wpcf7" value="447">
							<input type="hidden" name="_wpcf7_version" value="4.4.2">
							<input type="hidden" name="_wpcf7_locale" value="en_US">
							<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f447-p3061-o1">
							<input type="hidden" name="_wpnonce" value="5befd833bc">
						</div>
						<div class="contact-form">
							<?php if(!empty($message)) {?>
								<div class="display-msg">
									<p><?php echo $message; ?></p>
								</div>
							<?php } ?>						
							<div class="column one-third">
								<span class="wpcf7-form-control-wrap your-name">
									<input type="text" name="contact-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" placeholder="Name *" required oninvalid="setCustomValidity('Please fill your name')" oninput="setCustomValidity('')">
								</span>
							</div>
							<div class="column one-third">
								<span class="wpcf7-form-control-wrap your-email">
									<input type="email" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" placeholder="Email *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required oninvalid="setCustomValidity('Please fill your email')" oninput="setCustomValidity('')">
								</span>
							</div>
							<div class="column one-third">
								<span class="wpcf7-form-control-wrap phone">
									<input type="tel" name="phone" pattern="^([0|\+[0-9]{1,5})?([1-9][0-9]{9})$" maxlength="10" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-required wpcf7-validates-as-tel" placeholder="Mobile *" required oninvalid="setCustomValidity('Please fill your mobile no')" oninput="setCustomValidity('')">
								</span>
							</div>

							<div class="column one-second">
								<span class="wpcf7-form-control-wrap menu-872">
									<select name="country" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="country" aria-required="true" aria-invalid="false" onchange="document.getElementById('country_selected').value=this.options[this.selectedIndex].text" required oninvalid="setCustomValidity('Please select your country')" oninput="setCustomValidity('')">
											<option value="">-----Country-----</option>
											<?php if($totalRows_country > 0) 
												  {		
													foreach($country as $key => $value ) 
													{ ?>
														<option value="<?php echo $value['country_id']; ?>"><?php echo $value['country_name']; ?></option>
											<?php	}
												} ?>											
										</select>
										<input type="hidden" name="country_val" id="country_selected" value="" />
								</span>
							</div>
							<div class="column one-fourth">
								<span class="wpcf7-form-control-wrap age">
									<select name="age" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" id="contct-age" aria-required="true" aria-invalid="false" required oninvalid="setCustomValidity('Please select  your age')" oninput="setCustomValidity('')">
											<option value="">----Select Age----</option>
											<?php
												for($i=18; $i<=100;$i++)	{
											?>
													<option value="<?php echo $i;?>"><?php echo $i;?></option>
												<?php }?>
										</select>
								</span>
							</div>
							<div class="column one-fourth">
								<p style="box-sizing: border-box; display: inline-flex; padding: 5px 0 0;">
									<span class="gndr">Gender</span> 
									<span class="wpcf7-form-control-wrap gender">
										<span class="wpcf7-form-control wpcf7-radio">
											<span class="wpcf7-list-item first">
												<input type="radio" name="gender" value="Male" checked="checked">&nbsp;
												<span class="wpcf7-list-item-label">Male</span>
											</span>
											<span class="wpcf7-list-item last">
												<input type="radio" name="gender" value="Female">&nbsp;
												<span class="wpcf7-list-item-label">Female</span>
											</span>
										</span>
									</span>
								</p>
							</div>
							<div class="column one">
								<span class="wpcf7-form-control-wrap your-subject">
									<input type="text" name="subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" placeholder="Subject *" required oninvalid="setCustomValidity('Please fill the subject of your query')" oninput="setCustomValidity('')">
								</span>
							</div>
							<div class="column one">
								<span class="wpcf7-form-control-wrap your-message">
									<textarea name="message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false" placeholder="Message" required oninvalid="setCustomValidity('Please fill the query you have')" oninput="setCustomValidity('')">
									</textarea>
								</span>
							</div>
							<div class="column one btn-align">
								<input type="submit" name="contact_info" value="Send Message" class="wpcf7-form-control wpcf7-submit contact-btn">
							</div>
						</div>
					</form>	
				</div>
		<!--	
			<div class="entry-content" itemprop="mainContentOfPage">
				//<?php 
					//while ( have_posts() ){ the_post();							// Post Loop
						//mfn_builder_print( get_the_ID() );	// Content Builder & WordPress Editor Content
					//}
				//?>
			</div>
			-->
            <div class="entry-content" itemprop="mainContentOfPage">
				<div class="section    " style="padding-top:0px; padding-bottom:0px; background-color:"><div class="section_wrapper clearfix"><div class="items_group clearfix"><div class="column one column_column "><div class="column_attr" style=""><h1 class="cntct">Contact Us</h1></div></div><div class="column one column_column "><div class="column_attr" style=""><div class="column one-fourth three-divider"> 
	<div class="locate">
		<img src="http://www.konsult.com/wp-content/uploads/2016/06/building.png" alt="">
	</div>
	<div class="adres">
		<p class="contact-content">Konsult App Private Limited</p>
	</div>
</div>
<div class="column one-fourth three-divider ">
	<div class="locate">
		<i class="icon-location"></i>
	</div>
	<div class="adres">
		<p>D-136, Saket, New Delhi, India – 110017</p>
	</div>
</div>
<div class="column one-fourth three-divider ">
	<div class="locate">
		<i class="icon-call"></i>
	</div>
	<div class="adres">
		<p>+91-9599281202</p>
	</div>
</div>
<div class="column one-fourth">
	<div class="locate">
		<i class="icon-email"></i>
	</div>
	<div class="adres">
		<p><a href="mailto:info@konsultapp.com">info@konsultapp.com</a></p>
	</div>
</div></div></div></div></div></div><div class="section the_content no_content"><div class="section_wrapper"><div class="the_content_wrapper"></div></div></div>			</div>
			<?php if( mfn_opts_get('page-comments') ): ?>
				<div class="section section-page-comments">
					<div class="section_wrapper clearfix">
						<div class="column one comments">
							<?php comments_template( '', true ); ?>
						</div>
						
					</div>
				</div>
			<?php endif; ?>
		</div>
		
		<!-- .four-columns - sidebar -->
		<?php get_sidebar(); ?>

	</div>
</div>

<?php get_footer(); ?>