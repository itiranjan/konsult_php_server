<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class user extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('user_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/user");
    }

    public function auth() {

        $admin_id = isset($_POST['admin_id']) ? $_POST['admin_id'] : 0;

        $this->load->view('admin/header');
        $query = $this->db->query("SELECT admin_id, name, email, users.user_id FROM admin_users JOIN users ON users.user_id = admin_users.user_id");

        $userEmailArray = array();

        $userEmailArray[0] = '---- Selct User ----';

        foreach ($query->result() as $row) {
            $userEmailArray[$row->admin_id] = $row->email;
        }

        $this->load->view("admin/auth", array('userEmailArray' => $userEmailArray));
    }

    public function saveAuthPermissions() {

        $admin_id = $_POST['admin_id'];

        $query = $this->db->query("SELECT admin_id, name, email, users.user_id FROM admin_users JOIN users ON users.user_id = admin_users.user_id AND admin_id = $admin_id");

        $userPermissions = $userEmailArray = array();

        $userEmailArray[0] = '---- Selct User ----';

        foreach ($query->result() as $row) {

            $userEmailArray[$row->admin_id] = $row->email;

            $userPermissions[$row->admin_id]['user_id'] = $row->user_id;
            $userPermissions[$row->admin_id]['name'] = $row->name;
            $userPermissions[$row->admin_id]['email'] = $row->email;

            $sqlPermission = "SELECT permissions.name, permissions.description, admin_permissions.permission_id, permissions.permission_id as permission_master_id FROM permissions LEFT JOIN admin_permissions ON permissions.permission_id = admin_permissions.permission_id AND admin_id = $row->admin_id WHERE permissions.name != 'user_auth_view'";

            $queryPermissions = $this->db->query($sqlPermission);

            $permissionsArray = array();
            foreach ($queryPermissions->result() as $queryPermission) {
                $value = empty($queryPermission->permission_id) ? 0 : 1;
                $permissionsArray[$queryPermission->permission_master_id] = array('description' => $queryPermission->description, 'value' => $value, 'name' => $queryPermission->name);
            }

            $userPermissions[$row->admin_id]['permissions'] = $permissionsArray;
        }
        $this->load->view("admin/saveAuthPermissions", array('userPermissions' => $userPermissions));
    }

    public function changeAuthorization() {

        $checkList = $_POST['check_list'];
        $admin_id = strtolower($_POST['admin_id']);

        $query = $this->db->query("SELECT permission_id FROM permissions WHERE name != 'user_auth_view'");
        $permissionIds = array();
        foreach ($query->result() as $row) {
            $permissionIds[] = $row->permission_id;
        }

        foreach ($permissionIds as $permissionId) {
            if (in_array($permissionId, $checkList)) {
                $this->db->query("INSERT IGNORE INTO admin_permissions (`admin_id`, `permission_id`) VALUES ($admin_id, $permissionId)");
            } else {
                $this->db->query("DELETE FROM admin_permissions WHERE admin_id = $admin_id AND permission_id = $permissionId");
            }
        }

        $response = '{"message":"Changes done successfully!!","status":"1"}';

        echo $response;
        die;
    }

    public function userList($way) {

        $data = array();
        $resultsData = $this->user_model->userList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'users$user_id' => $row->user_id,
                'name' => $row->name,
                'users$email' => $row->email,
                'uwi$user_wallet_id' => $row->paytm,
                'user_wallet$email' => $row->wallet_email,
                'mobile' => $row->mobile,
                'users$created_at' => $row->created_at,
                'last_accessed' => $row->last_accessed,
                'amount_paid' => $row->amount_paid,
                'user_profile$followed_by' => $row->followed_by,
                'user_profile$comment' => $row->comment,
                'clinic_type' => $row->clinic_type
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

    public function updateUserDetails() {

        $rowData = array();
        $rowData['user_id'] = $_POST['user_id'];
        $rowData['column_name'] = $_POST['column_name'];
        $rowData['column_value'] = $_POST['column_value'];
        $rowData['followed_by'] = $this->session->userdata('username');

        if (!empty($rowData['user_id'])) {
            $this->user_model->updateUserDetails($rowData);
        }

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'user_followup';
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => (($rowData['user_id']) ? 'Comment inserted successfully!' : "Undefined user id!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);
    }

    public function toggleClinic() {

        $rowData = array();
        $rowData['user_id'] = $_POST['user_id'];
        $rowData['column_name'] = 'clinic_type';
        $rowData['column_value'] = ($_POST['val'] == '1') ? 0 : 1;
        $rowData['followed_by'] = '';

        $this->user_model->updateUserDetails($rowData);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = "user_toggleClinic_edit";
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => 'Updated successfully!');
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);        

        echo 1;
    }

}
