<?php
/**
 * Theme Functions
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );

define( 'THEME_NAME', 'betheme' );
define( 'THEME_VERSION', '9.7' );

define( 'LIBS_DIR', THEME_DIR. '/functions' );
define( 'LIBS_URI', THEME_URI. '/functions' );
define( 'LANG_DIR', THEME_DIR. '/languages' );

add_filter( 'widget_text', 'do_shortcode' );


/* ---------------------------------------------------------------------------
 * White Label
 * IMPORTANT: We recommend the use of Child Theme to change this
 * --------------------------------------------------------------------------- */
defined( 'WHITE_LABEL' ) or define( 'WHITE_LABEL', false );


/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * --------------------------------------------------------------------------- */
load_theme_textdomain( 'betheme',  LANG_DIR );
load_theme_textdomain( 'mfn-opts', LANG_DIR );


/* ---------------------------------------------------------------------------
 * Loads the Options Panel
 * --------------------------------------------------------------------------- */
if( ! function_exists( 'mfn_admin_scripts' ) )
{
	function mfn_admin_scripts() {
		wp_enqueue_script( 'jquery-ui-sortable' );
	}
}   
add_action( 'wp_enqueue_scripts', 'mfn_admin_scripts' );
add_action( 'admin_enqueue_scripts', 'mfn_admin_scripts' );
	
require( THEME_DIR .'/muffin-options/theme-options.php' );

$theme_disable = mfn_opts_get( 'theme-disable' );


/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * --------------------------------------------------------------------------- */

// Functions --------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-functions.php' );

// Header -----------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-head.php' );

// Menu -------------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-menu.php' );
if( ! isset( $theme_disable['mega-menu'] ) ){
	require_once( LIBS_DIR .'/theme-mega-menu.php' );
}

// Meta box ---------------------------------------------------------------------
require_once( LIBS_DIR .'/meta-functions.php' );

// Custom post types ------------------------------------------------------------
$post_types_disable = mfn_opts_get( 'post-type-disable' );

if( ! isset( $post_types_disable['client'] ) ){
	require_once( LIBS_DIR .'/meta-client.php' );
}
if( ! isset( $post_types_disable['offer'] ) ){
	require_once( LIBS_DIR .'/meta-offer.php' );
}
if( ! isset( $post_types_disable['portfolio'] ) ){
	require_once( LIBS_DIR .'/meta-portfolio.php' );
}
if( ! isset( $post_types_disable['slide'] ) ){
	require_once( LIBS_DIR .'/meta-slide.php' );
}
if( ! isset( $post_types_disable['testimonial'] ) ){
	require_once( LIBS_DIR .'/meta-testimonial.php' );
}

if( ! isset( $post_types_disable['layout'] ) ){
	require_once( LIBS_DIR .'/meta-layout.php' );
}
if( ! isset( $post_types_disable['template'] ) ){
	require_once( LIBS_DIR .'/meta-template.php' );
}

require_once( LIBS_DIR .'/meta-page.php' );
require_once( LIBS_DIR .'/meta-post.php' );

// Content ----------------------------------------------------------------------
require_once( THEME_DIR .'/includes/content-post.php' );
require_once( THEME_DIR .'/includes/content-portfolio.php' );

// Shortcodes -------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-shortcodes.php' );

// Hooks ------------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-hooks.php' );

// Widgets ----------------------------------------------------------------------
require_once( LIBS_DIR .'/widget-functions.php' );

require_once( LIBS_DIR .'/widget-flickr.php' );
require_once( LIBS_DIR .'/widget-login.php' );
require_once( LIBS_DIR .'/widget-menu.php' );
require_once( LIBS_DIR .'/widget-recent-comments.php' );
require_once( LIBS_DIR .'/widget-recent-posts.php' );
require_once( LIBS_DIR .'/widget-tag-cloud.php' );

// TinyMCE ----------------------------------------------------------------------
require_once( LIBS_DIR .'/tinymce/tinymce.php' );

// Plugins ---------------------------------------------------------------------- 
if( ! isset( $theme_disable['demo-data'] ) ){
	require_once( LIBS_DIR .'/importer/import.php' );
}

require_once( LIBS_DIR .'/class-love.php' );
require_once( LIBS_DIR .'/class-tgm-plugin-activation.php' );

require_once( LIBS_DIR .'/plugins/visual-composer.php' );

// WooCommerce specified functions
if( function_exists( 'is_woocommerce' ) ){
	require_once( LIBS_DIR .'/theme-woocommerce.php' );
}

// Hide activation and update specific parts ------------------------------------

// Slider Revolution
if( ! mfn_opts_get( 'plugin-rev' ) ){
	if( function_exists( 'set_revslider_as_theme' ) ){
		set_revslider_as_theme();
	}
}

// LayerSlider
if( ! mfn_opts_get( 'plugin-layer' ) ){
	add_action('layerslider_ready', 'mfn_layerslider_overrides');
	function mfn_layerslider_overrides() {
		// Disable auto-updates
		$GLOBALS['lsAutoUpdateBox'] = false;
	}
}

// Visual Composer 
if( ! mfn_opts_get( 'plugin-visual' ) ){
	add_action( 'vc_before_init', 'mfn_vcSetAsTheme' );
	function mfn_vcSetAsTheme() {
		vc_set_as_theme();
	}
}



add_filter('query_vars', 'add_state_var', 0, 2);
function add_state_var($vars){
	
    $vars[] = 'page_url';
	
    //$vars[] = 'doctor';
    return $vars;
}


/*
function add_rewrite_rules($aRules) {
$aNewRules = array('listing-page/([^/]+)/?$' => 'index.php?pagename=listing-page&page_url=$matches[1]','doctor-profile([^/]+)/?$' => 'index.php?pagename=doctor-profile&page_url=$matches[1]');
$aRules = $aNewRules + $aRules;
return $aRules;
}

add_filter('rewrite_rules_array', 'add_rewrite_rules');
*/
// function fcars() {

  // add_rewrite_rule('listing-page/[/]?([a-zA-Z-]*)[/]?([a-zA-Z-]*)$', 'index.php?pagename=listing-page&page_url=$matches[1]&pagee=$matches[2]');

  // add_rewrite_tag('%page_url%', '[a-zA-Z]+');
  // add_rewrite_tag('%pagee%', '[a-zA-Z]+');
// }

// add_action('init', 'fcars');
add_rewrite_rule('^listing-page/([^/]+)/?$','index.php?pagename=listing-page&page_url=$matches[1]','top');
//add_rewrite_rule('^listing-page/([^/]+)/?$/([^/]+)/?$','index.php?pagename=listing-page&page_url=$matches[1]&pagenum=$matches[2]','top');
function custom_rewrite_basic() {
 add_rewrite_rule('^symptom/([^/]+)/?$','index.php?pagename=symptom&page_url=$matches[1]','top');
 add_rewrite_rule('^speciality/([^/]+)/?$','index.php?pagename=speciality&page_url=$matches[1]','top');
}
add_action('init', 'custom_rewrite_basic');
function custom_rewrite_tag() {
  add_rewrite_tag('%page_url%', '([^&]+)');
  // add_rewrite_tag('%food%', '([^&]+)');
  // add_rewrite_tag('%variety%', '([^&]+)');
}
add_action('init', 'custom_rewrite_tag', 10, 0);



add_rewrite_rule('^doctor-profile/([^/]+)/?$','index.php?pagename=doctor-profile&page_url=$matches[1]','top');

add_rewrite_rule('^symptom-page/([^/]+)/?$','index.php?pagename=symptom-page&page_url=$matches[1]','top');



// ----- Fetch Blog on Doctor Page---


add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );

add_action( 'wp_ajax_example_ajax_request', 'example_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_example_ajax_request', 'example_ajax_request' );


function example_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$page_number = filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
						$item_per_page = 10;
						$position = (($page_number-1) * $item_per_page);
						
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						$specialIdRand = $my_wpdb->get_results("SELECT doctor_specialization_rel.doctor FROM doctor_specialization_rel JOIN doctor_profile ON doctor_profile.user = doctor_specialization_rel.doctor WHERE doctor_specialization_rel.specialization = '$page_url' AND doctor_profile.is_approved =1 ORDER BY doctor_profile.sponsored LIMIT $position, $item_per_page ");	
	
	
	
						foreach ($specialIdRand as $doc) {	
$docId = 	$doc->doctor ;
	
$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, up.address, up.is_doctor, cm.name AS city, up.photo, up.photo_url,
dp.total_experience,dsr.specialization as specializationID, dsm.name as specializationName , u.email,
dp.area, dp.about as about, dp.other_specializations,dp.per_min_charges as charges,dp.page_url as pageUrl,
dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user )
LEFT JOIN doctor_profile dp ON ( up.user = dp.user )
LEFT JOIN doctor_qualifications dq ON ( up.user = dq.doctor )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user = dsr.doctor )
LEFT JOIN specialization_master dsm ON ( dsr.specialization = dsm.specialization_id )

LEFT JOIN doctor_experience dex ON ( dp.user = dex.doctor )


LEFT JOIN city_master cm ON up.city = cm.city_id
WHERE user_id ='$docId' ");	
	?>
<div class="doctor-detail">
            <div class="row">
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">

                    <!-- Doctor Image -->
                    <div class="doctor-img">
 
                        <img src="http://api.konsultapp.com/photo/<?php  echo $docRow->photo ;  ?>" width="150" height="150">
                    </div>
                    <!-- End Doctor Image -->

                    <!-- Doctor Details -->
                    <div class="doctor-biography">
                       <a href="http://www.konsultapp.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/"> <h5 class="doctor-name"> <?php  echo $docRow->name ;  ?></h5></a>
                       <p class="designation"><?php  echo $docRow->specializationName ;  ?></p> 
                        <p><?php  echo $docRow->degreeName ;  ?> - <?php  echo $docRow->hospitalName ;  ?> </p> 
                        

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="round">
                                   <i class="fa fa-user-md" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Experience</p>
                                    <p class=""><?php  echo $docRow->total_experience ;  ?> Years</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                            
                            
                        <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Fee</p>
                                    <p class=""><?php  echo $docRow->charges ;  ?>/min</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                       
                       <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="doc-practice">
                                    <p class="prac-txt">Location</p>
                                    <p class="prac-place"><?php  echo $docRow->area ;  ?> <?php  echo $docRow->city ;  ?></p> 
                                </div>
                            </div>
                    </div>
                    
      <div class="learn-more-btn rgt align_right1">
		<a href="http://www.konsultapp.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/">  <span class="learn-more-txt">View Profile</span> </a>
	</div>
                    </div>
                 </div>
            </div>
        </div>
<?php }
							
							
							
							die();
}




add_action( 'wp_ajax_symptom_ajax_request', 'symptom_ajax_request' );

add_action( 'wp_ajax_symptom_ajax_request', 'symptom_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_symptom_ajax_request', 'symptom_ajax_request' );


function symptom_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$page_number = filter_var($_REQUEST["page"], FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_HIGH);
						$item_per_page = 10;
						$position = (($page_number-1) * $item_per_page);
						
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						$specialIdRand = $my_wpdb->get_results("SELECT DISTINCT(doctor_specialization_rel.doctor) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization JOIN doctor_profile ON doctor_profile.user = doctor_specialization_rel.doctor WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1 ORDER BY doctor_profile.sponsored LIMIT $position, $item_per_page ");	
	
	
	
						foreach ($specialIdRand as $doc) {	
$docId = 	$doc->doctor ;
	
$docRow = $my_wpdb->get_row("SELECT u.user_id, CONCAT( u.salutation, ' ', u.name ) AS name, up.address, up.is_doctor, cm.name AS city, up.photo, up.photo_url,
dp.total_experience,dsr.specialization as specializationID, dsm.name as specializationName , u.email,
dp.area, dp.about as about, dp.other_specializations,dp.per_min_charges as charges,dp.page_url as pageUrl,
dgm.name AS degreeName,
um.name as universityName,dq.college as collegeName,
dex.hospital_name as hospitalName,dex.designation_name as designationName

FROM users u
LEFT JOIN user_profile up ON ( u.user_id = up.user )
LEFT JOIN doctor_profile dp ON ( up.user = dp.user )
LEFT JOIN doctor_qualifications dq ON ( up.user = dq.doctor )
LEFT JOIN degree_master dgm ON ( dq.degree = dgm.degree_id )
LEFT JOIN university_master um ON ( dq.university = um.university_id )


LEFT JOIN doctor_specialization_rel dsr ON ( dp.user = dsr.doctor )
LEFT JOIN specialization_master dsm ON ( dsr.specialization = dsm.specialization_id )

LEFT JOIN doctor_experience dex ON ( dp.user = dex.doctor )


LEFT JOIN city_master cm ON up.city = cm.city_id
WHERE user_id = '$docId' ");	
	?>
<div class="doctor-detail">
            <div class="row">
                <!-- one-ninth part of doctor details -->
                <div class="col-md-9 doc-ful-details">

                    <!-- Doctor Image -->
                    <div class="doctor-img">
 
                        <img src="http://api.konsultapp.com/photo/<?php  echo $docRow->photo ;  ?>" width="150" height="150">
                    </div>
                    <!-- End Doctor Image -->

                    <!-- Doctor Details -->
                    <div class="doctor-biography">
                        <a href="http://www.konsultapp.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/"><h5 class="doctor-name"> <?php  echo $docRow->name ;  ?></h5></a>
                       <p class="designation"><?php  echo $docRow->specializationName ;  ?></p> 
                        <p><?php  echo $docRow->degreeName ;  ?> - <?php  echo $docRow->hospitalName ;  ?> </p> 
                        

                       
                        <div class="row">
                           
                            <div class="col-md-3">
                                <div class="round">
                                   <i class="fa fa-user-md" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Experience</p>
                                    <p class=""><?php  echo $docRow->total_experience ;  ?> Years</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                            
                            
                        <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-inr" aria-hidden="true"></i>
                                </div>
                                <div class="fee">
                                    <p class="fee-txt">Fee</p>
                                    <p class=""><?php  echo $docRow->charges ;  ?>/min</p> <!-- Doctor Fees -->
                                </div>
                            </div>
                       
                       <div class="col-md-3">
                                <div class="round">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </div>
                                <div class="doc-practice">
                                    <p class="prac-txt">Location</p>
                                    <p class="prac-place"><?php  echo $docRow->area ;  ?> <?php  echo $docRow->city ;  ?></p> 
                                </div>
                            </div>
                    </div>
                    
      <div class="learn-more-btn rgt align_right1">
		<a href="http://www.konsultapp.com/doctor-profile/<?php  echo $docRow->pageUrl ;  ?>/">  <span class="learn-more-txt">View Profile</span> </a>
	</div>
                    </div>
                 </div>
            </div>
        </div>
<?php }
							
							
						//echo "outside foreach";
							die();
}


add_action( 'wp_ajax_feedback_ajax_request', 'feedback_ajax_request' );

add_action( 'wp_ajax_feedback_ajax_request', 'feedback_ajax_request' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_feedback_ajax_request', 'feedback_ajax_request' );

function feedback_ajax_request() {
	
						
						ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$doctor = $_REQUEST['doctor']; $name = $_REQUEST['name']; $email = $_REQUEST['email']; $contact = $_REQUEST['contact']; $message = $_REQUEST['message'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						$my_wpdb->insert("feedback_form", array(
					   "name" => $name,
					   "email" => $email,
					   "contact" => $contact,
						"message" => $message ,	
					   "doctor" => $doctor ,
					   
					));
	
}

add_action( 'wp_ajax_get_doc_count', 'get_doc_count' );

add_action( 'wp_ajax_get_doc_count', 'get_doc_count' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_get_doc_count', 'get_doc_count' );


function get_doc_count() {
	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						echo $specialIdRan= $my_wpdb->get_var("SELECT count(*) FROM doctor_specialization_rel JOIN doctor_profile ON doctor_specialization_rel.doctor = doctor_profile.user where specialization = $page_url AND doctor_profile.is_approved = 1  ");	
}
add_action( 'wp_ajax_get_doc_count_symptom', 'get_doc_count_symptom' );

add_action( 'wp_ajax_get_doc_count_symptom', 'get_doc_count_symptom' );    // If called from admin panel
    add_action( 'wp_ajax_nopriv_get_doc_count_symptom', 'get_doc_count_symptom' );


function get_doc_count_symptom() {
	                    ini_set('display_errors', 1);
						ini_set('display_startup_errors', 1);
						error_reporting(E_ALL);
	
						$page_url = $_REQUEST['page_url'];
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						
						echo $specialIdRa= $my_wpdb->get_var("SELECT COUNT(DISTINCT(doctor_specialization_rel.doctor)) FROM doctor_specialization_rel JOIN symptom_specialization_rel on symptom_specialization_rel.specialization_id = doctor_specialization_rel.specialization JOIN doctor_profile ON doctor_specialization_rel.doctor = doctor_profile.user WHERE symptom_specialization_rel.symptom_id = $page_url AND doctor_profile.is_approved = 1");	
}
function custom_excerpt_length( $length ) {
	return 45;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );
function new_excerpt_more( $more ) {
	return "...";
}
add_filter('excerpt_more', 'new_excerpt_more');
    
	
	add_action( 'wp_ajax_what_we_do_ajax_request', 'what_we_do_ajax_request' );
    add_action( 'wp_ajax_what_we_do_ajax_request', 'what_we_do_ajax_request' ); 
    add_action( 'wp_ajax_nopriv_what_we_do_ajax_request', 'what_we_do_ajax_request' );
	function what_we_do_ajax_request(){
		
						//global $wp;
						//global $wpdb;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						//$my_wpdb = new WPDB('root', '', 'Konsult_Prod', 'localhost');
						 $search = $_REQUEST["page"];
						if(isset($search)){
						$specializationsearchData = $my_wpdb->get_results("select * FROM specialization_master WHERE name like '%$search%' limit 3;");
						if($specializationsearchData){foreach($specializationsearchData as $spsd){
							?>
    
   <li>
 
                              <a href="<?php echo bloginfo('url'); ?>/listing-page/<?php echo str_replace(' ', '-', strtolower($spsd->name))?>" >
							  <span class="left"><?php echo $spsd->name; ?></span>
                              <span class="right">Speciality</span>								
								</a>
</li>
   <?php
						}}
						$symptomsearchData = $my_wpdb->get_results("select * FROM symptom_master WHERE name like '%$search%' limit 3;");
							if($symptomsearchData){foreach($symptomsearchData as $stsd){
								?>
  
   <li>
   
   <a href="<?php echo bloginfo('url'); ?>/symptom-page/<?php echo str_replace(' ', '-', strtolower($stsd->name))?>">
                               <span class="left"><?php echo $stsd->name; ?></span>
							   <span class="right">Symptom</span>  
								 </a>
								 </li>
   
   <?php
							}}
						}
	}
?>