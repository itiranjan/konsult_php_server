<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/var/www/html/konnect/site/website/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define('DB_NAME', 'konsultappweb');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '$#KonsultApp@2015#$');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm?Rp4NS!k(R^L9_/-Ig@cYIdft8s1G3bw>#C%h_{;[i.+IY2Hn0.7)qK`[X2D@bM');
define('SECURE_AUTH_KEY',  '4nZV|PmN;*_naq^s#Pk{EgJE zYbZl_A~|ciz6zxtnm9rtY{!A{edu<+7vcwQv~;');
define('LOGGED_IN_KEY',    'idXMKpAN!bg4<+F(KOlhqXkyITVp1FA>F7EZskY#wD)BD@*3jr=^Xw#v|G$%:uhn');
define('NONCE_KEY',        '~+,6C?=V_dFE:N<H3@+;s>gtYs/Y ?p_a}Ers@?j4rPqKcOH|zLC|e9-h&-R&=7x');
define('AUTH_SALT',        'XMYXguNl^_t.S!6>zj{]vC:#LA%nnE%mRRRHkw%Kd9.yG3635LN5*1iS4:Iyf0pu');
define('SECURE_AUTH_SALT', 'CjUhhy[Km@*=Z1]g?J@jm}#B0m=T u o:*Sjo(*u/2TL#j42[B[V*EuPzJCeNQmD');
define('LOGGED_IN_SALT',   'c[FYU*+^GgNO&_K*m]+`dr1[JAFc|5~Ls>lq ePE^e4]7ofG{aaI8ulT?RH(^.1s');
define('NONCE_SALT',       '`6ggJ~)KPO^ISgy4]Jt#: p$c!)4G~|4enQKe{YN&rH,RDpZq8r+sbV<Rk$Y9CSQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
