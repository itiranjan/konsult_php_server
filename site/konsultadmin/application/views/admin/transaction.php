<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

        <div id="clientsDb">
            <span style="font-size:20px">Payment Failures<br/><br/></span>
            <div id="grid" ></div>    
        </div>

        <script>
            $(document).ready(function () {

                var onClose = function ()
                {
                    mainGrid.data("kendoGrid").refresh();
                }

                var selectedRows = [];
                mainGrid = $("#grid").kendoGrid({                   
                    toolbar:["excel"],
                    selectable: "multiple cell",
                    allowCopy: true,                    
                    excel: {
                        allPages: true,
                        fileName: "transaction_logs_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                        filterable: true
                    },
                    dataSource: {
                        type: "json",
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        transport: {
                            read: {
                                type: "POST",
                                url: base_url + "/index.php/transaction/transactionList/1",
                                dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                            },
                            parameterMap: function (options) {
                                if (options.filter) {
                                    KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                                }
                                return options;
                            },
                        },                        
                        schema: {
                            data: "list",
                            total: "total",
                            model: {
                                id: "id",
                                fields: {
                                    transaction_id: {type: "number", editable: false},
                                    call_id: {type: "number", editable: false},
                                    created_at: {type: "date", editable: false},
                                    name: {type: "string", editable: false},
                                    pgm$name: {type: "string", editable: false},
                                    pg_request: {type: "string", editable: false},
                                    pg_response: {type: "string", editable: false},
                                }
                            }
                        },
                        pageSize: 100
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contains",
                            },
                            number: {
                                eq: "Is equal to",
                                gt: "Grater than",
                                lt: "Less than",
                            }
                        }
                    },
                    sortable: true,
                    dataBound: function () {
                        this.expandRow(this.tbody.find("tr.k-master-row").first());
                    },
                    pageable: {
                        refresh: true,
                        pageSize: 100,
                        numeric: true,
                        buttonCount: 20,
                        info: true
                    },
                    change: function (e) {
                        var selected = this.select();
                        for (var i = 0; i < selected.length; i++) {
                            var dataItem = this.dataItem(selected[i]);
                            selectedRows.push(dataItem);
                        }
                    },
                    columns: [
                        {field: "transaction_id", title: "Trans. Id", width: '4px'},
                        {field: "call_id", title: "Comm. Id", width: '4px'},
                        {field: "created_at", title: "Date", width: '10px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "name", title: "Status", width: '6px'},
                        {field: "pgm$name", title: "Gateway", width: '6px'},
                        {field: "pg_request", title: "Request", width: '30px'},
                        {field: "pg_response", title: "Response", width: '30px'},
                    ],
                    editable: false,
                }).data("kendoGrid");
            
                mainGrid.thead.kendoTooltip({
                    filter: "th",
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown
                        return $(target).text();
                    }
                });           
            
//                $("#grid").kendoTooltip({
//                   filter: "td:nth-child(6)", //this filter selects the first column cells
//                   position: "left",
//                   content: function(e){
//                       var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));
//                       var content = dataItem.pg_response;
//                       return content;
//                   }
//                }).data("kendoTooltip");            
            
            });                        
        </script>
    </div>
</div>