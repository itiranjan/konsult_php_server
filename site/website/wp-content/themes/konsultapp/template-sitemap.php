<?php
/**
 * Template Name: Sitemap
 *
 * @package Betheme
 * @author Muffin Group
 */

get_header();
?>

<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
		
			<div class="section">
				<div class="section_wrapper clearfix">

					<?php 
						if( have_posts() ){
							the_post();
// 							get_template_part( 'content', 'page' );
						} 
					?>
					
					<div class="one column">
						<ul class="list">
							<?php
                            $args = array(
	'depth'        => 0,
	'show_date'    => '',
	'date_format'  => get_option('date_format'),
	'child_of'     => 0,
	'exclude'      => '4505,3502,3309,3664,4175,4512,3309,4140,4144,3061,3753,3717,4111,4116',
	'include'      => '',
	'title_li'     => __(''),
	'echo'         => 1,
	'authors'      => '',
	'sort_column'  => 'menu_order, post_title',
	'link_before'  => '',
	'link_after'   => '',
	'walker'       => '',
	'post_type'    => 'page',
        'post_status'  => 'publish' 
);
                           //  wp_list_pages( $args );
                            //wp_list_pages( 'title_li=','exclude=' ); ?>
                            
							<li class="page_item page-item-4493"><a href="http://www.konsult.com/about-us">About Us</a></li>
<li class="page_item page-item-4226"><a href="http://www.konsult.com/download-app">App Download</a></li>
<li class="page_item page-item-344 page_item_has_children"><a href="http://www.konsult.com/careers">Careers</a>
<!--<ul class="children">
	<li class="page_item page-item-4520"><a href="http://www.konsult.com/careers/digital-marketing-executive-gurugram">Digital Marketing Executive Vacancy</a></li>
</ul>-->
</li>
<li class="page_item page-item-3115"><a href="http://www.konsult.com/doctor-registration">Doctor Registration</a></li>
<li class="page_item page-item-4523"><a href="http://www.konsult.com/doctor-sitemap-alphabetical">Doctors</a></li>
<li class="page_item page-item-3106"><a href="http://www.konsult.com/health-faqs">Health FAQ’s</a></li>
<li class="page_item page-item-1162"><a href="http://www.konsult.com/health-tips">Health Feed</a></li>
<li class="page_item page-item-4219"><a href="http://www.konsult.com/">Home</a></li>
<li class="page_item page-item-3431"><a href="http://www.konsult.com/partner-hospital">Partner Hospitals</a></li>
<li class="page_item page-item-4480"><a href="http://www.konsult.com/specialities">Specialities</a></li>
<li class="page_item page-item-4132"><a href="http://www.konsult.com/symptoms">Symptoms</a></li>
<li class="page_item page-item-4120"><a href="http://www.konsult.com/what-we-treat">What We Treat</a></li>
					
						</ul>
					</div>
		
				</div>
			</div>
			<div class="section">
            <div class="section_wrapper clearfix text-center" style="display:none;">
                <h2>Special Links</h2>
                <hr />
            <ul class="linksspecial">
                <li><a class="btn btn-primary vall" href="http://www.konsult.com/doctor-sitemap-alphabetical">View All Doctors</a></li>
                <li><a class="btn btn-primary vall" href="http://www.konsult.com/specialities">View All Specialities</a></li>
                <li><a class="btn btn-primary vall" href="http://www.konsult.com/symptoms">View All Symptoms</a></li>
                
                </ul>
                </div>
            </div>
		</div>

	</div>
</div>
<style>
    
    .sub-menu{
            min-width: 185px;
    }
    .text-center{
        text-align:center;
    }
    ul,li{display:inline;}
    h2{
        margin-top:50px;
        text-align: center;
        
    }
    .vall:hover{
        background: #e872a6;
    color: white;
    }
    .vall{
        background: #24cdd9;
    color: white;
    padding: 10px 35px;
    }
    .list li{
        width:33%;
        float:left;
        text-align: left;
    }
    @media screen and (max-width: 786px) {
        #menu{
        width:60%;
        margin:0 auto;
    }
        .sub-menu{
            display: none;
        }
    }
    @media screen and (min-width: 786px) {
        .page_item a {
    margin-left: 40%;
    font-size: 12px;
} 
    }
    .page_item a {
    
    font-size: 12px;
}
    .list{
        width:60%;
            margin:0 auto;
        text-align: center;
    }
</style>
<?php get_footer(); ?>