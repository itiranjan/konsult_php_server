<?php

class kccCall extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_calls';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'kcc_call_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

}
