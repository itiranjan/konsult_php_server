<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class communication extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('communication_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/communication");
    }

    public function communicationList() {

        $data = array();

        $resultsData = $this->communication_model->communicationList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];

        foreach ($result->result() as $row) {

            $operatorCall = 0;
            if (strstr($row->telecom_response, '"dtmf":"1"')) {
                $operatorCall = 1;
            }

            $status = 'Unrecognized';

            if ($row->operator_call_duration > 0) {
                $status = 'Picked by operator';
            } elseif ($operatorCall && $row->operator_call_duration <= 0) {
                $status = 'Missed by operator';
            } elseif ($row->caller_call_status_id == 1 && $row->receiver_call_status_id == 1) {
                $status = 'Connected';
            } elseif ($row->caller_call_status_id == 2 && $row->receiver_call_status_id == 1) {
                $status = 'Missed by doctor';
            } elseif ($row->caller_call_status_id == 7 && $row->receiver_call_status_id == 1) {
                $status = 'Not connected - doctor side';
            } elseif ($row->caller_call_status_id == 4 && $row->receiver_call_status_id == 4) {
                $status = 'Ongoing Call / Knowlarity Failure';
            } elseif ($row->receiver_call_status_id == 2) {
                $status = 'Missed by patient';
            } elseif ($row->receiver_call_status_id == 7) {
                $status = 'Not connected - patient side';
            }

            $data[] = array(
                'call_id' => $row->call_id,
                'dr$name' => $row->doctor_name,
                'dr$email' => $row->doctor_email,
                'pt$name' => $row->patient_name,
                'pt$email' => $row->patient_email,
                'comm_datetime' => $row->comm_datetime,
                'call_duration' => $row->call_duration, // ? round($row->call_duration / 60, 1) : $row->call_duration,
                'pgm$name' => $row->payment_gateway_name,
                'total_charges' => $row->total_charges,
                'virtual_money' => $row->virtual_money,
                'real_money' => $row->real_money,
                //'doctor_charges' => $row->doctor_charges,
                //'konsult_charges' => $row->total_charges - $row->doctor_charges,
                'drctm$name' => $row->doctor_call_status,
                'ptctm$name' => $row->patient_call_status,
                'status' => $status,
                'followed_by' => $row->followed_by,
                'comment' => $row->comment,
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }

    public function updateCommunicationDetails() {

        $rowData = array();
        $rowData['call_id'] = $_POST['call_id'];
        $rowData['column_name'] = $_POST['column_name'];
        $rowData['column_value'] = $_POST['column_value'];
        $rowData['followed_by'] = $this->session->userdata('username');

        if (!empty($rowData['call_id'])) {
            $this->communication_model->updateCommunicationDetails($rowData);
        }

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'call_followup';
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => (($rowData['call_id']) ? 'Comment inserted successfully!' : "Undefined call id!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);
    }

    public function updateRatingFollowup() {

        $rowData = array();
        $rowData['rating_id'] = $_POST['rating_id'];
        $rowData['column_name'] = $_POST['column_name'];
        $rowData['column_value'] = $_POST['column_value'];
        $rowData['followed_by'] = $this->session->userdata('username');

        if (!empty($rowData['rating_id'])) {
            $this->communication_model->updateRatingFollowup($rowData);
        }

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'rating_followup';
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => (($rowData['rating_id']) ? 'Comment inserted successfully!' : "Undefined rating id!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);
    }

    public function updateCallBackDetails() {

        $rowData = array();
        $rowData['callback_request_id'] = $_POST['callback_request_id'];
        $rowData['column_name'] = $_POST['column_name'];
        $rowData['column_value'] = $_POST['column_value'];
        $rowData['followed_by'] = $this->session->userdata('username');

        if (!empty($rowData['callback_request_id'])) {
            $this->communication_model->updateCallBackDetails($rowData);
        }

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'callback_followup';
        $logData['request'] = $rowData;
        $logData['response'] = array('logging_msg' => (($rowData['callback_request_id']) ? 'Comment inserted successfully!' : "Undefined callback request id!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);
    }

    public function knowlarity() {
        $this->load->view('admin/header');
        $this->load->view("admin/knowlarity");
    }

    public function knowlarityList() {

        $data = array();
        $resultsData = $this->communication_model->knowlarityList($_POST);
        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'call_id' => $row->call_id,
                'dr$name' => $row->doctor_name,
                'dr$email' => $row->doctor_email,
                'pt$name' => $row->patient_name,
                'pt$email' => $row->patient_email,
                'comm_datetime' => $row->comm_datetime,
                'telecom_request' => $row->telecom_request,
                'telecom_response' => $row->telecom_response,
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }

    public function ratings() {
        $this->load->view('admin/header');
        $this->load->view("admin/ratings");
    }

    public function ratingList() {

        $data = array();
        $resultsData = $this->communication_model->ratingReviews($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'rating_id' => $row->rating_id,
                'call_ratings$user_id' => $row->user_id,
                'call_id' => $row->call_id,
                'rating' => $row->rating,
                'comment' => $row->comment,
                'name' => $row->name ? "Clinic: $row->username ($row->name)" : $row->username,
                'age' => $row->age,
                'sex' => $row->sex,
                'call_ratings$created_at' => $row->created_at,
                'followed_by' => $row->followed_by,
                'followup_comment' => $row->followup_comment,
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }

    public function callBackRequests() {
        $this->load->view('admin/header');
        $this->load->view("admin/callBackRequests");
    }

    public function callBackRequestList() {

        $data = array();
        $resultsData = $this->communication_model->callBackRequests($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {

            $data[] = array(
                'callback_request_id' => $row->callback_request_id,
                'dr$name' => $row->doctor_name,
                'dr$email' => $row->doctor_email,
                'pt$name' => $row->patient_name,
                'pt$email' => $row->patient_email,
                'request_call_id' => $row->request_call_id,
                'callback_call_id' => $row->callback_call_id,
                'callback_requests$status' => $row->status,
                'pgm$name' => $row->wallet_name,
                'callback_requests$created_at' => $row->created_at,
                'followed_by' => $row->followed_by,
                'followup_comment' => $row->followup_comment,
            );
        }

        header("Content-type: application/json");
        echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
    }
    
    public function toggleCallbackrequestStatus() {

        $callback_request_id = $_POST['id'];
        $query = $this->db->query("SELECT status FROM `callback_requests` WHERE callback_request_id = $callback_request_id");
        $data = $query->result();
        $status = $data[0]->status;
        
        if ($status == 'open')
            $val = 'close';
        else
            $val = 'open';

        $data = array(
            'status' => $val
        );

        $requestStatus = $this->communication_model->toggleStatus($data, $_POST['id']);
        
        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'callbackrequest_status';
        $logData['request'] = array('status' => $val, 'callback_request_id' => $_POST['id']);
        $logData['response'] = array('logging_msg' => (($requestStatus) ? ($val) : "Unable to change open/closed status!"));
        $this->load->model('accesslog_model', '', TRUE);
        $this->accesslog_model->insertLog($logData);      
        
        echo $requestStatus;
    }    

}
