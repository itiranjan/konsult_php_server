<?php

$basePath = base_path();
$basePath = substr($basePath, 0, -3);
include_once $basePath . 'api/vendor/Paytm/PaytmKit/lib/encdec_paytm.php';

class paytmHelper {

    protected $helper;

    public function __construct() {
        $this->helper = new Helper();
        $this->oauthUrl = Config::get('constants.PAYTM_oauthUrl');
        $this->walletUrl = Config::get('constants.PAYTM_walletUrl');
        $this->transactionUrl = Config::get('constants.PAYTM_transactionUrl');
        $this->addMoneyUrl = Config::get('constants.PAYTM_addMoneyUrl');
        $this->clientId = Config::get('constants.PAYTM_clientId');
        $this->clientSecret = Config::get('constants.PAYTM_clientSecret');
        $this->authorization = Config::get('constants.PAYTM_authorization');
        $this->MID = Config::get('constants.PAYTM_MID');
        $this->MERCHANT_KEY = Config::get('constants.PAYTM_MERCHANT_KEY');
        $this->callBackUrl = Config::get('constants.PAYTM_CALLBACK_URL');
        $this->AppIP = Config::get('constants.PAYTM_AppIP');
        $this->GRATIFICATION_MID = Config::get('PAYTM_GRATIFICATION_MID');
        $this->AESKEY_MerchantKey = Config::get('constants.PAYTM_AESKEY_MerchantKey');
        $this->MerchantGuid = Config::get('constants.PAYTM_MerchantGuid');
        $this->MARKETING_DEALS_SalesWalletGuid = Config::get('constants.PAYTM_MARKETING_DEALS_SalesWalletGuid');
        $this->INDUSTRY_TYPE_ID = Config::get('constants.PAYTM_INDUSTRY_TYPE_ID');
        $this->CHANNEL_ID = Config::get('constants.PAYTM_CHANNEL_ID');
        $this->WEBSITE = Config::get('constants.PAYTM_WEBSITE');

        $this->PaymentMode = Config::get('constants.PAYTM_PaymentMode');
        $this->AuthMode = Config::get('constants.PAYTM_AuthMode');
        $this->platformName = Config::get('constants.PAYTM_platformName');

        $this->REQUESTTYPE_WITHDRAW = Config::get('constants.PAYTM_REQUESTTYPE_WITHDRAW');
        $this->REQUESTTYPE_ADD_MONEY = Config::get('constants.PAYTM_REQUESTTYPE_ADD_MONEY');
        $this->operationType = Config::get('constants.PAYTM_operationType');
        $this->currency = Config::get('constants.PAYTM_currency');

        $this->salesWalletName = Config::get('constants.PAYTM_salesWalletName');
    }

    public function signinOtp($params = array()) {

        $httpHelper = $this->helper->load('http');

        $url = $this->oauthUrl . "signin/otp";

        $header = array('Content-Type:application/json');
        
        $walletParams = array('phone' => $params['mobile'], 'clientId' => $this->clientId, 'scope' => 'wallet', 'responseType' => 'token');
        
        if(!empty($params['email'])) {
            $walletParams['email'] = $params['email'];
        }

        $postData = json_encode($walletParams);

        $output = $httpHelper->httpPostWithHeader($url, $header, 1, $postData);

        return $output;
    }

    public function signinValidateOtp($params = array()) {

        $url = $this->oauthUrl . "signin/validate/otp";

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type:application/json', 'Authorization:' . $this->authorization);

        $postData = json_encode(array('otp' => $params['otp'], 'state' => $params['state']));

        $output = $httpHelper->httpPostWithHeader($url, $header, 1, $postData);

        return $output;
    }

    public function userDetails($params = array()) {

        $url = $this->oauthUrl . "user/details";

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type:application/json', 'session_token:' . $params['session_token']);

        $output = $httpHelper->httpGetWithHeader($url, $header);

        return $output;
    }

    public function walletWebCheckBalance($params = array()) {

        $url = $this->walletUrl . "wallet-web/checkBalance";

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type:application/json', 'ssotoken:' . $params['ssotoken']);

        $output = $httpHelper->httpGetWithHeader($url, $header);

        if ($params['getBalanceOnly']) {
            $decodeOutput = json_decode($output);
            if (isset($decodeOutput->response->amount)) {
                return $decodeOutput->response->amount;
            } else {
                $status = 0;
                //404 => User doesn't exist.  403 => Unauthorized Access
                if ($decodeOutput->statusCode == 403 || $decodeOutput->statusCode == 404) {
                    $status = 2;
                }

                return array('status' => $status, 'response' => $output);
            }
        }

        return $output;
    }

    //ADD MONEY TO WALLET
    public function initiateAddMoney($params = array()) {

        $paramList = array();
        $paramList["MID"] = $this->MID;
        $paramList["ORDER_ID"] = 'AM_' . $params['user_id'] . "_" . time();
        $paramList["CUST_ID"] = "KON" . $params['user_id'];
        $paramList["INDUSTRY_TYPE_ID"] = $this->INDUSTRY_TYPE_ID;
        $paramList["CHANNEL_ID"] = $this->CHANNEL_ID;
        $paramList["TXN_AMOUNT"] = $params['amount'];
        $paramList["WEBSITE"] = $this->WEBSITE;
        $paramList["SSO_TOKEN"] = $params['sso_token'];
        $paramList["REQUEST_TYPE"] = $this->REQUESTTYPE_ADD_MONEY;
        $paramList["CALLBACK_URL"] = $this->callBackUrl;

        //GENERATE CHECKSUM
        $checkSum = getChecksumFromArray($paramList, $this->MERCHANT_KEY);

        $paramList["CHECKSUMHASH"] = $checkSum;
        $paramList["ADDMONEY_URL"] = $this->addMoneyUrl;

        $paramList['status'] = 1;

        return json_encode($paramList);
    }

    //AUTO DEBIT FROM PATIENT WALLET
    public function oltpHandlerffWithdrawScw($params = array()) {

        $paramList = array();
        $paramList["MID"] = $this->MID;
        $paramList["ReqType"] = $this->REQUESTTYPE_WITHDRAW;
        $paramList["TxnAmount"] = sprintf("%.2f", $params['amount']);
        $paramList['AppIP'] = $this->AppIP;
        $paramList["OrderId"] = "CALL_" . $params['order_id'];
        $paramList['Currency'] = $this->currency;
        $paramList['DeviceId'] = $params['mobile'];
        $paramList["SSOToken"] = $params['sso_token'];
        $paramList["PaymentMode"] = $this->PaymentMode;
        $paramList["CustId"] = $params['user_id'];
        $paramList["IndustryType"] = $this->INDUSTRY_TYPE_ID;
        $paramList["Channel"] = $this->CHANNEL_ID;
        $paramList["AuthMode"] = $this->AuthMode;

        $checkSum = getChecksumFromArray($paramList, $this->MERCHANT_KEY);

        $paramList["CheckSum"] = urlencode($checkSum);

        $postData = 'JsonData=' . json_encode($paramList);

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type: application/json');

        $url = $this->transactionUrl . "/HANDLER_FF/withdrawScw";

        $output = $httpHelper->httpPostWithHeader($url, $header, 1, $postData);

        $this->getTxnStatus(array('ORDERID' => $paramList["OrderId"]));

        return $output;
    }

    //ADD AMOUNT TO DOCTOR WALLET
    public function salesToUserCredit($params = array()) {
        
        $paramList['request']['requestType'] = null;
        $paramList['request']["merchantGuid"] = $this->MerchantGuid;
        $paramList['request']["merchantOrderId"] = "CALL_" . $params['order_id'];
        $paramList['request']["salesWalletName"] = $this->salesWalletName;
        $paramList['request']["salesWalletGuid"] = $this->MARKETING_DEALS_SalesWalletGuid;
        //$paramList['request']["payeeEmailId"] = $params['email'];
        $paramList['request']["payeePhoneNumber"] = $params['mobile'];
        //$paramList['request']["payeeSsoId"] = $params['user_id'];
        $paramList['request']["appliedToNewUsers"] = "N";
        $paramList['request']["amount"] = $params['amount'];
        $paramList['request']["currencyCode"] = $this->currency;
        $paramList["ipAddress"] = $this->AppIP;
        $paramList["platformName"] = $this->platformName;
        $paramList["operationType"] = $this->operationType;

        $data_string = json_encode($paramList);

        $checkSum = getChecksumFromString($data_string, $this->AESKEY_MerchantKey);

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type: application/json', 'mid:' . $this->MerchantGuid, 'checksumhash:' . $checkSum);

        $url = $this->walletUrl . "/wallet-web/salesToUserCredit";

        $output = $httpHelper->httpPostWithHeader($url, $header, 1, $data_string);

        return $output;
    }

    public function getTxnStatus($params = array()) {

        $paramList = array();
        $paramList["MID"] = $this->MID;
        $paramList["ORDERID"] = $params['ORDERID'];

        $checkSum = getChecksumFromArray($paramList, $this->MERCHANT_KEY);

        $paramList["CHECKSUMHASH"] = urlencode($checkSum);

        $postData = 'JsonData=' . json_encode($paramList);

        $httpHelper = $this->helper->load('http');

        $header = array('Content-Type: application/json');

        $url = $this->transactionUrl . "/HANDLER_INTERNAL/getTxnStatus";

        $output = $httpHelper->httpPostWithHeader($url, $header, 1, $postData);

        $output = json_decode($output);

        //INSERT DATA IN LOG TABLE
        if (!empty($output) && $output->STATUS != 'TXN_SUCCESS') {                               
            $errorLog = new ErrorLogs();
            $errorLog->type = 'paytm_getTxnStatus';
            $errorLog->request = $postData;
            $errorLog->response = json_encode($output);
            $errorLog->save();
        }

        return $output;
    }

    public function cashback($params = array()) {

        $call_id = $params['order_id'];
        $params['order_id'] = 'CASHBACK_' . $call_id;

        $request = json_encode($params);

        $cashbackTrans = new Transactions();
        $cashbackTrans->call_id = $call_id;
        $cashbackTrans->call_uuid = $params['CallSid'];
        $cashbackTrans->user_id = $params['user_id'];
        $cashbackTrans->wallet_id = 1;
        $cashbackTrans->type = 4;
        $cashbackTrans->spend_id = $params['spend_id'];
        $cashbackTrans->amount = $params['amount'];
        $cashbackTrans->trans_status = 1;
        $cashbackTrans->save();

        $response = $this->salesToUserCredit($params);

        $cashbackInvoice = 'KONPAT/' . date("m") . '/' . date("Y") . '/' . $cashbackTrans->transaction_id;
        $walletBalance = $this->walletWebCheckBalance(array('ssotoken' => $params['sso_token'], 'getBalanceOnly' => 1));
        $walletBalance = ($walletBalance >= 0) ? $walletBalance : NULL;

        $transStatus = 2;
        if (strstr($response, 'SUCCESS')) {
            $transStatus = 1;
        }

        Transactions::where('transaction_id', '=', $cashbackTrans->transaction_id)->update(array('balance' => $walletBalance, 'pg_request' => $request, 'pg_response' => $response, 'invoice_no' => $cashbackInvoice, 'trans_status' => $transStatus));

        return $transStatus;
    }

}
