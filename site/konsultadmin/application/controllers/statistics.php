<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class statistics extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('doctor_model', '', TRUE);
        $this->load->model('user_model', '', TRUE);
        $this->load->model('communication_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function index() {

        $this->load->view('admin/header');

        $data = array();
        $data['total_doctors'] = $this->doctor_model->totalDoctors(array());
        $data['total_approved_doctors'] = $this->doctor_model->totalDoctors(array('total_approved_doctors' => 1));
        $data['total_unapproved_doctors'] = $this->doctor_model->totalDoctors(array('total_approved_doctors' => 0));
        $data['total_patients'] = $this->user_model->totalPatients();
        $data['total_active_patients'] = $this->communication_model->totalActivePatients();
        $data['total_calls'] = $this->communication_model->totalCalls(array());
        $data['total_connected_calls'] = $this->communication_model->totalCalls(array('connected' => 1));
        $data['total_call_duration'] = (int) ($this->communication_model->totalCallDuration() / 60);
        $data['total_revenue'] = (int) ($this->communication_model->totalRevenue());

        $this->load->view("admin/statistics", $data);
    }

}
