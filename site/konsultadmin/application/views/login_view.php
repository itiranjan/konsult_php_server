<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
   <title>Simple Login with CodeIgniter</title>
 </head>
 <script language='JavaScript' type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js'></script>
 <body>
   <h1>Simple Login with CodeIgniter</h1>
   <?php echo validation_errors(); ?>
   <?php echo form_open('verifylogin'); ?>
     <label for="username">Username:</label>
     <input type="text" size="20" id="username" name="username"/>
     <br/>
     <label for="password">Password:</label>
     <input type="password" size="20" id="password" name="password"/>
     <br/>
     <input type="submit" value="Login"/>
	 <input type="button" id="login" value="Ajax Login" />
   </form>
   <input type='button' value='Add' id='add'/>
   <div id='result'>
   </div>
   <script type='text/javascript' language='javascript'>
   $(document).ready(function(){
	$('#login').click(function(){
	var form_data = {
		username : $('#username').val(),
		password : $('#password').val(),
		ajax : '1'
	};
    $.ajax({
            url: 'index.php/verifylogin',
            type:'POST',
			async : false,
			data: form_data,
            success: function(data){
                    //$('#result').append($data);
					if(data != '1')
					{
					//alert(data);
					$('#result').html(data);
					}
					else
					{
					window.location = 'index.php/home';
					}
                },
			error: function (xhr, ajaxOptions, thrownError) {
        //alert(xhr.status);
        //alert(thrownError);
		$('#result').html(xhr.status+" : "+thrownError);
        }
            });
 
	});
	});
</script>
</body>
</html>
