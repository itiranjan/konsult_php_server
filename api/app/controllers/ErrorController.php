<?php

class ErrorController extends BaseController {

    public function getError403() {
        echo "error 403";
        exit;
    }

    public function getError404() {
        echo "error 404";
        exit;
    }

    public function getError500() {
        echo "error 500";
        exit;
    }

    public function getError() {
        echo "error default";
        exit;
    }

}
