<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */

// prev & next post -------------------
$in_same_term = ( mfn_opts_get( 'prev-next-nav' ) == 'same-category' ) ? true : false;
$post_prev = get_adjacent_post( $in_same_term, '', true );
$post_next = get_adjacent_post( $in_same_term, '', false );
$blog_page_id = get_option('page_for_posts');

// post classes -----------------------
$classes = array();
if( ! mfn_post_thumbnail( get_the_ID() ) ) $classes[] = 'no-img';
if( get_post_meta(get_the_ID(), 'mfn-post-hide-image', true) ) $classes[] = 'no-img';
if( post_password_required() ) $classes[] = 'no-img';
if( ! mfn_opts_get( 'blog-title' ) ) $classes[] = 'no-title';
if( ! mfn_opts_get( 'share' ) ) $classes[] = 'no-share';

$translate['published'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-published','Published by') : __('Published by','betheme');
$translate['at'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-at','at') : __('at','betheme');
$translate['tags'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-tags','Tags') : __('Tags','betheme');
$translate['categories'] 	= mfn_opts_get('translate') ? mfn_opts_get('translate-categories','Categories') : __('Categories','betheme');
$translate['all'] 			= mfn_opts_get('translate') ? mfn_opts_get('translate-all','Show all') : __('Show all','betheme');
$translate['related'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-related','Related posts') : __('Related posts','betheme');
$translate['readmore'] 		= mfn_opts_get('translate') ? mfn_opts_get('translate-readmore','Read more') : __('Read more','betheme');
?>

<div id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>

	<div class="section section-post-header">
		<div class="section_wrapper clearfix">
			
			<?php if( mfn_opts_get('prev-next-nav') ): ?>
			<div class="column one post-nav">
				
				<?php 
					// prev & next post navigation
					echo mfn_post_navigation( $post_prev, 'prev', 'icon-left-open-big' ); 
					echo mfn_post_navigation( $post_next, 'next', 'icon-right-open-big' ); 
				?>
				
				<ul class="next-prev-nav">
					<?php if( $post_prev ): ?>
						<li class="prev"><a class="button button_js" href="<?php echo get_permalink( $post_prev ); ?>"><span class="button_icon"><i class="icon-left-open"></i></span></a></li>
					<?php endif; ?>
					<?php if( $post_next ): ?>
						<li class="next"><a class="button button_js" href="<?php echo get_permalink( $post_next ); ?>"><span class="button_icon"><i class="icon-right-open"></i></span></a></li>
					<?php endif; ?>
				</ul>
				
				<?php if( $blog_page_id ): ?>
					<a class="list-nav" href="<?php echo get_permalink( $blog_page_id ); ?>"><i class="icon-layout"></i><?php echo $translate['all']; ?></a>
				<?php endif; ?>
				
			</div>
			<?php endif; ?>

			<div class="column one post-header">
			
				<div class="button-love"><?php echo mfn_love() ?></div>
				
				<div class="title_wrapper">
				
					<?php
						if( mfn_opts_get( 'blog-title' ) ){ 
							if( get_post_format() == 'quote'){
								echo '<blockquote>'. get_the_title() .'</blockquote>';
							} else {
								$h = mfn_opts_get( 'title-heading', 1 );
								echo '<h'. $h .' class="entry-title" itemprop="headline">'. get_the_title() .'</h'. $h .'>';
							}
						}
					?>
					
					<?php 
						if( get_post_format() == 'link'){
							$link = get_post_meta(get_the_ID(), 'mfn-post-link', true);
							echo '<a href="'. $link .'" target="_blank">'. $link .'</a>';
						}
					?>
					
					<?php if( mfn_opts_get( 'blog-meta' ) ): ?>
						<div class="post-meta clearfix">
						
							<div class="author-date">
								<span class="vcard author post-author">
									<?php echo $translate['published']; ?> <i class="icon-user"></i>
									<span class="fn">Konsultapp</span>
									<!--<span class="fn"><a href="<?php //echo get_author_posts_url( get_the_author_meta('ID') ); ?>"><?php //the_author_meta( 'display_name' ); ?></a></span>-->
								</span> 
								<span class="date">
									<?php echo $translate['at']; ?> <i class="icon-clock"></i>
									<time class="entry-date" datetime="<?php the_date('c'); ?>" itemprop="datePublished" pubdate><?php echo get_the_date(); ?></time>
								</span>	
							</div>
							
							<div class="category meta-categories">
								<span class="cat-btn"><?php echo $translate['categories']; ?> <i class="icon-down-dir"></i></span>
								<div class="cat-wrapper"><?php echo get_the_category_list(); ?></div>
							</div>
							
							<div class="category mata-tags">
								<span class="cat-btn"><?php echo $translate['tags']; ?> <i class="icon-down-dir"></i></span>
								<div class="cat-wrapper">
									<ul>
										<?php
											if( $terms = get_the_terms( false, 'post_tag' ) ){
												foreach( $terms as $term ){
													$link = get_term_link( $term, 'post_tag' );
													echo '<li><a href="' . esc_url( $link ) . '">' . $term->name .'</a></li>';
												}
											}
										?>
									</ul>
								</div>
							</div>

						</div>
					<?php endif; ?>
					
				</div>
				
			</div>
			
			<div class="column one single-photo-wrapper">
				
				<?php if( mfn_opts_get( 'share' ) ): ?>
					<div class="share_wrapper">
						<span class='st_facebook_vcount' displayText='Facebook'></span>
						<span class='st_twitter_vcount' displayText='Tweet'></span>
						<span class='st_pinterest_vcount' displayText='Pinterest'></span>						
						
						<script src="http<?php mfn_ssl(1); ?>://w<?php mfn_ssl(1); ?>.sharethis.com/button/buttons.js"></script>
						<script>stLight.options({publisher: "1390eb48-c3c3-409a-903a-ca202d50de91", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
					</div>
				<?php endif; ?>
				
				<?php if( ! post_password_required() ): ?>
					<!--<div class="image_frame scale-with-grid <?php if( ! mfn_opts_get('blog-single-zoom') ) echo 'disabled'; ?>">
						<div class="image_wrapper">
							<?php //echo mfn_post_thumbnail( get_the_ID() ); ?>
						</div>
					</div>-->
				<?php endif; ?>
				
			</div>
			
		</div>
	</div>

	<div class="post-wrapper-content">

		<?php
			// Content Builder & WordPress Editor Content
			mfn_builder_print( $post->ID );	
		?>

		<div class="section section-post-footer">
			<div class="section_wrapper clearfix">
			
				<div class="column one post-pager">
					<?php
						// List of pages
						wp_link_pages(array(
							'before'			=> '<div class="pager-single">',
							'after'				=> '</div>',
							'link_before'		=> '<span>',
							'link_after'		=> '</span>',
							'next_or_number'	=> 'number'
						));
					?>
				</div>
				
			</div>
		</div>
		
		<div class="section section-post-about">
			<div class="section_wrapper clearfix">
			
				<?php if( mfn_opts_get( 'blog-author' ) ): ?>
				<div class="column one author-box">
					<div class="author-box-wrapper">
						<div class="avatar-wrapper">
							<?php 
								global $user;
								echo get_avatar( get_the_author_meta('email'), '64', false, get_the_author_meta('display_name', $user['ID']) );
							?>
						</div>
						<div class="desc-wrapper">
							<h5><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>"><?php the_author_meta( 'display_name' ); ?></a></h5>
							<div class="desc"><?php the_author_meta('description'); ?></div>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>	
		</div>
		
	</div>
			
	<div class="section section-post-related">
		<div class="section_wrapper clearfix">

			<?php
				if( mfn_opts_get( 'blog-related' ) && $aCategories = wp_get_post_categories( get_the_ID() ) ){

					$args = array(
						'category__in'			=> $aCategories,
						'ignore_sticky_posts'	=> true,
						'no_found_rows'			=> true,
						'post__not_in'			=> array( get_the_ID() ),
						'posts_per_page'		=> 3,
						'post_status'			=> 'publish',
					);

					$query_related_posts = new WP_Query( $args );
					if( $query_related_posts->have_posts() ){

						echo '<div class="section-related-adjustment">';
							echo '<h4>'. $translate['related'] .'</h4>';
							
							while ( $query_related_posts->have_posts() ){
								$query_related_posts->the_post();
								
								$classes_rel = '';
								if( ! mfn_post_thumbnail( get_the_ID() ) ) $classes_rel .= 'no-img';
								
								echo '<div class="column one-third post-related '. implode( ' ', get_post_class( $classes_rel ) ).'">';	
									
									if( get_post_format() == 'quote'){

										echo '<blockquote>';
											echo '<a href="'. get_permalink() .'">';
												the_title();
											echo '</a>';
										echo '</blockquote>';
										
									} else {

										echo '<div class="image_frame scale-with-grid">';
											echo '<div class="image_wrapper">';
												echo mfn_post_thumbnail( get_the_ID() );
											echo '</div>';
										echo '</div>';
										
									}
									
									echo '<div class="date_label">'. get_the_date() .'</div>';
								
									echo '<div class="desc">';
										if( get_post_format() != 'quote') echo '<h4><a href="'. get_permalink() .'">'. get_the_title() .'</a></h4>';
										echo '<hr class="hr_color" />';
										echo '<a href="'. get_permalink() .'" class="button button_left button_js"><span class="button_icon"><i class="icon-layout"></i></span><span class="button_label">'. $translate['readmore'] .'</span></a>';
									echo '</div>';
									
								echo '</div>';
							}
							
						echo '</div>';
					}
					wp_reset_postdata();
				}	
			?>
			
		</div>
	</div>
	<aside id="a2a_share_save_widget-2" class="widget widget_a2a_share_save_widget"><div class="a2a_kit a2a_kit_size_32 addtoany_list" style="line-height: 32px;"><a class="a2a_button_facebook" href="/#facebook" title="Facebook" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_facebook" style="background-color: rgb(59, 89, 152);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z"></path></svg></span><span class="a2a_label">Facebook</span></a><a class="a2a_button_twitter" href="http://www.addtoany.com/add_to/twitter?linkurl=http%3A%2F%2Fwww.konsult.com%2Fdr-ankur-jindal%2F&amp;linkname=Harmful%20Effects%20of%20Drinking%20-%20Dr.%20Ankur%20Jindal%20%7C%20Konsult%20App&amp;linknote=The%20liver%20is%20a%20complex%20chemical%20factory%20that%20works%2024%20hours%20a%20day!" title="Twitter" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_twitter" style="background-color: rgb(85, 172, 238);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M28 8.557a9.913 9.913 0 0 1-2.828.775 4.93 4.93 0 0 0 2.166-2.725 9.738 9.738 0 0 1-3.13 1.194 4.92 4.92 0 0 0-3.593-1.55 4.924 4.924 0 0 0-4.794 6.049c-4.09-.21-7.72-2.17-10.15-5.15a4.942 4.942 0 0 0-.665 2.477c0 1.71.87 3.214 2.19 4.1a4.968 4.968 0 0 1-2.23-.616v.06c0 2.39 1.7 4.38 3.952 4.83-.414.115-.85.174-1.297.174-.318 0-.626-.03-.928-.086a4.935 4.935 0 0 0 4.6 3.42 9.893 9.893 0 0 1-6.114 2.107c-.398 0-.79-.023-1.175-.068a13.953 13.953 0 0 0 7.55 2.213c9.056 0 14.01-7.507 14.01-14.013 0-.213-.005-.426-.015-.637.96-.695 1.795-1.56 2.455-2.55z"></path></svg></span><span class="a2a_label">Twitter</span></a><a class="a2a_button_google_plus" href="/#google_plus" title="Google+" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_google_plus" style="background-color: rgb(221, 75, 57);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M27 15h-2v-2h-2v2h-2v2h2v2h2v-2h2m-15-2v2.4h3.97c-.16 1.03-1.2 3.02-3.97 3.02-2.39 0-4.34-1.98-4.34-4.42s1.95-4.42 4.34-4.42c1.36 0 2.27.58 2.79 1.08l1.9-1.83C15.47 9.69 13.89 9 12 9c-3.87 0-7 3.13-7 7s3.13 7 7 7c4.04 0 6.72-2.84 6.72-6.84 0-.46-.05-.81-.11-1.16H12z" fill="#FFF"></path></svg></span><span class="a2a_label">Google+</span></a><a class="a2a_button_whatsapp" href="/#whatsapp" title="WhatsApp" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_whatsapp" style="background-color: rgb(18, 175, 10);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF" d="M16.21 4.41C9.973 4.41 4.917 9.465 4.917 15.7c0 2.134.592 4.13 1.62 5.832L4.5 27.59l6.25-2.002a11.241 11.241 0 0 0 5.46 1.404c6.234 0 11.29-5.055 11.29-11.29 0-6.237-5.056-11.292-11.29-11.292zm0 20.69c-1.91 0-3.69-.57-5.173-1.553l-3.61 1.156 1.173-3.49a9.345 9.345 0 0 1-1.79-5.512c0-5.18 4.217-9.4 9.4-9.4 5.183 0 9.397 4.22 9.397 9.4 0 5.188-4.214 9.4-9.398 9.4zm5.293-6.832c-.284-.155-1.673-.906-1.934-1.012-.265-.106-.455-.16-.658.12s-.78.91-.954 1.096c-.176.186-.345.203-.628.048-.282-.154-1.2-.494-2.264-1.517-.83-.795-1.373-1.76-1.53-2.055-.158-.295 0-.445.15-.584.134-.124.3-.326.45-.488.15-.163.203-.28.306-.47.104-.19.06-.36-.005-.506-.066-.147-.59-1.587-.81-2.173-.218-.586-.46-.498-.63-.505-.168-.007-.358-.038-.55-.045-.19-.007-.51.054-.78.332-.277.274-1.05.943-1.1 2.362-.055 1.418.926 2.826 1.064 3.023.137.2 1.874 3.272 4.76 4.537 2.888 1.264 2.9.878 3.43.85.53-.027 1.734-.633 2-1.297.266-.664.287-1.24.22-1.363-.07-.123-.26-.203-.54-.357z"></path></svg></span><span class="a2a_label">WhatsApp</span></a><a class="a2a_button_linkedin" href="/#linkedin" title="LinkedIn" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_linkedin" style="background-color: rgb(0, 123, 181);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path d="M6.227 12.61h4.19v13.48h-4.19V12.61zm2.095-6.7a2.43 2.43 0 0 1 0 4.86c-1.344 0-2.428-1.09-2.428-2.43s1.084-2.43 2.428-2.43m4.72 6.7h4.02v1.84h.058c.56-1.058 1.927-2.176 3.965-2.176 4.238 0 5.02 2.792 5.02 6.42v7.395h-4.183v-6.56c0-1.564-.03-3.574-2.178-3.574-2.18 0-2.514 1.7-2.514 3.46v6.668h-4.187V12.61z" fill="#FFF"></path></svg></span><span class="a2a_label">LinkedIn</span></a><a class="a2a_button_sms" href="/#sms" title="SMS" rel="nofollow" target="_blank"><span class="a2a_svg a2a_s__default a2a_s_sms" style="background-color: rgb(108, 190, 69);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><path fill="#FFF" d="M16 3.543c-7.177 0-13 4.612-13 10.294 0 3.35 2.027 6.33 5.16 8.21 1.71 1.565 1.542 4.08-.827 6.41 2.874 0 7.445-1.698 8.462-4.34H16c7.176 0 13-4.605 13-10.285s-5.824-10.29-13-10.29zM9.045 17.376c-.73 0-1.45-.19-1.81-.388l.294-1.194c.384.2.98.398 1.6.398.66 0 1.01-.275 1.01-.692 0-.398-.302-.625-1.07-.9-1.06-.37-1.753-.957-1.753-1.886 0-1.09.91-1.924 2.415-1.924.72 0 1.25.152 1.63.322l-.322 1.166a3.037 3.037 0 0 0-1.336-.303c-.625 0-.93.284-.93.616 0 .41.36.59 1.186.9 1.127.42 1.658 1.01 1.658 1.91.003 1.07-.822 1.98-2.575 1.98zm9.053-.095l-.095-2.44a72.993 72.993 0 0 1-.057-2.626h-.028a35.41 35.41 0 0 1-.71 2.475l-.778 2.49h-1.128l-.682-2.473a29.602 29.602 0 0 1-.578-2.493h-.02c-.037.863-.065 1.85-.112 2.645l-.114 2.425H12.46l.407-6.386h1.924l.63 2.13c.2.74.397 1.536.54 2.285h.027a52.9 52.9 0 0 1 .607-2.293l.683-2.12h1.886l.35 6.386H18.1zm4.09.1c-.73 0-1.45-.19-1.81-.39l.293-1.194c.39.2.99.398 1.605.398.663 0 1.014-.275 1.014-.692 0-.396-.305-.623-1.07-.9-1.064-.37-1.755-.955-1.755-1.884 0-1.09.91-1.924 2.416-1.924.72 0 1.25.153 1.63.323l-.322 1.166a3.038 3.038 0 0 0-1.337-.303c-.625 0-.93.284-.93.616 0 .408.36.588 1.186.9 1.127.42 1.658 1.006 1.658 1.906.002 1.07-.823 1.98-2.576 1.98z"></path></svg></span><span class="a2a_label">SMS</span></a><a class="a2a_dd addtoany_share_save" href="https://www.addtoany.com/share#url=http%3A%2F%2Fwww.konsult.com%2Fdr-ankur-jindal%2F&amp;title=Harmful%20Effects%20of%20Drinking%20-%20Dr.%20Ankur%20Jindal%20%7C%20Konsult%20App"><span class="a2a_svg a2a_s__default a2a_s_a2a" style="background-color: rgb(1, 102, 255);"><svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32"><g fill="#FFF"><path d="M14 7h4v18h-4z"></path><path d="M7 14h18v4H7z"></path></g></svg></span><span class="a2a_label">Share</span></a></div></aside>
	<?php if( mfn_opts_get( 'blog-comments' ) ): ?>
		<div class="section section-post-comments">
			<div class="section_wrapper clearfix">
			
				<div class="column one comments">
					<?php comments_template( '', true ); ?>
				</div>
				
			</div>
		</div>
	<?php endif; ?>

</div>
