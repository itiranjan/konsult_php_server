<?php

class ErrorLogs extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'error_log';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'errorlog_id';

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }

}
