<?php

class KccCodeInfo extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_code_info';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'code_info_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = false;

    public function validatePromoCode($params = array()) {
        
        if(empty($params['code'])) {
            return array('status' => 2, 'msg' => 'You have not entered the promo code.');
        }
        
        $promoCodeRaw = $this->where('code', '=', $params['code'])->first();
        
        if(COUNT($promoCodeRaw) <= 0 || $promoCodeRaw->is_exhausted) {
            return array('status' => 2, 'msg' => 'Promo code does not exist or already used.');
        }
        
        $currentDateTime = date("Y-m-d H:i:s");
        
        if (!(strtotime($currentDateTime) >= strtotime($promoCodeRaw->valid_from) && strtotime($currentDateTime) <= strtotime($promoCodeRaw->valid_till))) {
            return array('status' => 2, 'msg' => 'Promo code is expired or valid for future dates.');
        }
        
        $rechargeInfo = new KccRechargeInfo();
        $isAvailed = $rechargeInfo->isAvailedByUser(array('code_info_id' => $promoCodeRaw->code_info_id, 'user_id' => $params['user_id'], 'code_type' => $promoCodeRaw->code_type));
        
        if($isAvailed) {
            return array('status' => 3, 'msg' => 'Promo code is already used.');
        }
        
        return array('status' => 1, 'code_info_id' => $promoCodeRaw->code_info_id, 'amount' => $promoCodeRaw->amount, 'code_type' => $promoCodeRaw->code_type, 'msg' => 'Promo code is valid.');
    }
}
