<?php

class University extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'university_master';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'university_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    /**
     * @author : Shipra Agrawal
     * @created : 02 Feb 2015
     * @description : This function get droctor Qualification List by user id
     * @param : int $userid
     * @access public
     * @return  array 
     */
    public static function droctorQualificationList($userid) {
        $favDrList = DB::table('doctor_qualifications AS q')->where('q.user_id', '=', $userid)
                ->join('university_master AS u', 'q.university', '=', 'u.university_id')
                ->join('degree_master AS d', 'd.degree_id', '=', 'q.degree')
                ->select('u.name AS university', 'd.name AS degree', 'start_date', 'end_date', 'is_pursuing')
                ->get();

        return $favDrList;
    }

}
