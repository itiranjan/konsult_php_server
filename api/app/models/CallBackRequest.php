<?php

class CallBackRequest extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'callback_requests';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'callback_request_id';

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'request_from' => 'required',
        'request_to' => 'required',
        'wallet_id' => 'required',
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    public function callBackDoctorRequest($params = array()) {
        
        $userid = $params['request_to'];
        
        $callBackUserList = DB::table('users AS u')->where('cr.request_to', '=', $userid)
                ->join('user_profile AS up', 'u.user_id', '=', 'up.user_id')
                ->join('callback_requests AS cr', 'cr.request_from', '=', 'u.user_id')
                ->select('cr.callback_request_id', 'cr.status', 'cr.request_from', 'u.name', 'photo', 'cr.created_at', 'cr.updated_at')
                ->where('cr.status', '=', 'open')
                ->orderBy('cr.updated_at', 'DESC')
                ->get();

        return $callBackUserList;
    }
    
    public function callBackUserList($params = array()) {
        
        $userid = $params['request_from'];
        
        $callBackDrList = DB::table('users AS u')->where('drs.is_default', '=', 1)->where('df.is_approved', '=', 1)->where('cr.request_from', '=', $userid)
                ->join('user_profile AS up', 'u.user_id', '=', 'up.user_id')
                ->join('doctor_profile AS df', 'df.user_id', '=', 'u.user_id')
                ->join('callback_requests AS cr', 'cr.request_to', '=', 'u.user_id')
                ->join('doctor_specialization_rel AS drs', 'drs.user_id', '=', 'u.user_id')
                ->join('specialization_master AS sm', 'sm.specialization_id', '=', 'drs.specialization_id')
                ->leftJoin('city_master AS cm', 'df.city_id', '=', 'cm.city_id')
                ->select('cr.callback_request_id', 'cr.status', 'cr.request_to', 'u.name', 'df.online_status as is_online', 'sm.name AS specialization_name', 'df.area', 'cm.name AS city_name', 'photo', 'cr.created_at', 'cr.updated_at')
                ->orderBy('cr.updated_at', 'DESC')
                ->get();

        return $callBackDrList;
    }    

}
