<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('main_model');
    }

    public function index() {
        $dataH['js'] = array('jquery-1.6.4.js', 'jquery.jplayer.min.js', 'jplayer.playlist.js', 'jquery.slimscroll.js', 'sdmenu.js',
            'sliding/slides.min.jquery.js', 'colorbox/jquery.colorbox.js');
        $dataH['css'] = array('style.css', 'navigation.css', 'tabber.css', 'jquery.bxslider.css', 'colorbox.css');
        $dataH['script'] = array('jquery-ui.min.js', 'jquery.reveal.js');
        $data['allNews'] = $this->allNews(0, 1);
        $data['newSongs'] = $this->latestSong(0, 1);
        $data['albumList'] = $this->albumList(0, 1);
        $data['playList'] = $this->playList(0, 1);
        $data['photography'] = $this->allPhotography(0, 1);
        $data['ArtistList'] = $this->allArtistList(0, 1);
        $data['didyouKnowList'] = $this->alldidyouKnowList(0, 1);
        $dataH['GetTickerNews'] = $this->GetTickerNews();
        $data['RjVideo'] = $this->RjVideo();
        $this->load->view('main/header.php', $dataH);
        $this->load->view('main/Default.php', $data);
        $this->load->view('main/footer.php');
    }

    public function Details($id) {
        $dataH['js'] = array('jquery-1.6.4.js', 'jquery.jplayer.min.js', 'jplayer.playlist.js', 'jquery.slimscroll.js', 'sdmenu.js', 'sliding/slides.min.jquery.js', 'colorbox/jquery.colorbox.js');
        $dataH['css'] = array('style.css', 'navigation.css', 'tabber.css', 'slideshow.css', 'Slidings.css', 'reveal.css', 'jquery.bxslider.css');
        $dataH['script'] = array('jquery-ui.min.js', 'jquery.reveal.js');
        $dataH['GetTickerNews'] = $this->GetTickerNews();
        $data['DidYouKnowDetail'] = $this->DidYouKnowDetail($id);
        $data['Top5didyouKnow'] = $this->Top5didyouKnow($id);
        $this->load->view('main/header.php', $dataH);
        $this->load->view('main/DidYouKnow_Detail.php', $data);
        $this->load->view('main/footer.php');
    }

    public function Top5didyouKnow($id) {
        $result = $this->main_model->Top5didyouKnow($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'heading' => $row->heading,
                'image' => $row->image
            );
        }
        return $data;
    }

    public function DidYouKnowDetail($id) {
        $result = $this->main_model->didyouKnowDetail($id);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'heading' => $row->heading,
                'image' => $row->image,
                'description' => $row->description
            );
        }
        return $data;
    }

    public function latestSong($limit, $way) {
        $result = $this->main_model->latestSong($limit);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'song' => $row->title,
                'album' => $row->album,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function allNews($limit, $way) {
        $result = $this->main_model->allNews($limit);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'heading' => $row->heading,
                'image' => $row->image,
                'description' => $row->description
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function GetTickerNews() {
        $result = $this->main_model->GetTickerNews();
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'heading' => $row->heading
            );
        }

        return $data;
    }

    public function allPhotography($limit, $way) {
        $data = array();
        $result = $this->main_model->allPhotography(intval($limit));
        //print_r($result);
        foreach ($result->result() as $row) {
            $data[] = array(
                'title' => $row->title,
                'image' => $row->image
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function alldidyouKnowList($limit, $way) {
        $data = array();
        $result = $this->main_model->alldidyouKnowList(intval($limit));
        //print_r($result);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'title' => $row->heading,
                'image' => $row->image,
                'description' => $row->description
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function allArtistList($limit, $way) {
        $data = array();
        $result = $this->main_model->allArtistList(intval($limit));
        //print_r($result);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'title' => $row->name,
                'image' => $row->image
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function playList($limit, $way) {
        $data = array();
        $result = $this->main_model->playList(intval($limit));
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'name' => $row->name,
                'banner' => $row->banner
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function albumList($limit, $way) {
        $data = array();
        $result = $this->main_model->albumList(intval($limit));
        //print_r($result);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'title' => $row->album,
                'image' => $row->banner
            );
        }
        if ($way == 1)
            return $data;
        else
            echo json_encode($data);
    }

    public function listSongs() {
        $data = array();
        $album = $_POST['album'];
        $result = $this->main_model->listSong($album);
        foreach ($result->result() as $row) {
            $data[] = array(
                'title' => $row->title,
                'album' => $row->album,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }
        echo json_encode($data);
    }

    public function listAlbumSongs() {
        $data = array();
        $album = $_POST['album'];
        $result = $this->main_model->listAlbumSongs($album);
        foreach ($result->result() as $row) {
            $data[] = array(
                'title' => $row->title,
                'album' => $row->album,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }
        echo json_encode($data);
    }

    public function Top20Songs($limit) {
        $result = $this->main_model->Top20Songs($limit);
        foreach ($result->result() as $row) {
            $data[] = array(
                'song' => $row->song,
                'album' => $row->album,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }
        echo json_encode($data);
    }

    public function NewReleases($limit) {
        $result = $this->main_model->NewRelease($limit);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->id,
                'song' => $row->song,
                'album' => $row->album,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }

        echo json_encode($data);
    }

    public function GoldenClassics($limit) {
        $result = $this->main_model->GoldenClassics($limit);
        foreach ($result->result() as $row) {
            $data[] = array(
                'id' => $row->song_id,
                'song' => $row->title,
                'album' => $row->name,
                'file' => $row->file,
                'artist' => $row->artist
            );
        }

        echo json_encode($data);
    }

    public function RjVideo() {
        $result = $this->main_model->RjVideo();
        foreach ($result->result() as $row) {
            $data[] = array(
                'file' => $row->file
            );
        }

        return $data;
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */