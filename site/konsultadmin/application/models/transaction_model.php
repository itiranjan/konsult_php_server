<?php

class transaction_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $ci = get_instance();
        $ci->load->helper('miscellaneous_helper');
    }

    function transactionList($params) {
        
        $params['whereCondition'] = "(transactions.pg_response IS null OR transactions.pg_response LIKE 'null' OR transactions.pg_response LIKE '%error%' OR transactions.pg_response LIKE '%FAILURE%' OR transactions.pg_response = '')";

        $mainQueryParams = $params;
        $mainQueryParams['baseQuery'] = "SELECT transactions.transaction_id, transactions.call_id, transactions.created_at, transactions.pg_request, transactions. pg_response, transaction_status_master.name, pgm.name as payment_gateway_name FROM transactions LEFT JOIN transaction_status_master ON transactions.trans_status = transaction_status_master.trans_status_id LEFT JOIN wallet_master pgm ON pgm.wallet_id = transactions.wallet_id ";
        $mainQueryParams['defaultSorting'] = " transactions.created_at DESC ";

        $mainQuery = parseQuery($mainQueryParams);

        $countQueryParams = $params;
        $countQueryParams['baseQuery'] = "SELECT COUNT(*) as total_results FROM transactions LEFT JOIN transaction_status_master ON transactions.trans_status = transaction_status_master.trans_status_id LEFT JOIN wallet_master pgm ON pgm.wallet_id = transactions.wallet_id ";
        $countQueryParams['sort'] = '';
        $countQueryParams['take'] = 0;
        $countQueryParams['skip'] = 0;

        $countQuery = parseQuery($countQueryParams);

        $resultsCount = $this->db->query($countQuery)->row()->total_results;

        return array('resultsCount' => $resultsCount, 'results' => $this->db->query($mainQuery));
    }

}
