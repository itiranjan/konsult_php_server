<?php

class OperationHours extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'operation_hours';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'operation_hour_id';

    /**
     * To store validation errors
     */
    private $errors;

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'user_id' => 'required|integer',
        'day_id' => 'required|integer|exists:days_master,day_id',
        'start_time' => 'required|before:end_time',
        'end_time' => 'required|after:start_time'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    public function getOperationHours($params = array()) {

        $user_id = $params['user_id'];
        
        return $this->where('user_id', '=', $user_id)->get();
    }

}
