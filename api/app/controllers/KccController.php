<?php

class KccController extends ApiBaseController {

    const KonsultUserId = 160;
    const OpertorNumber = '+919560547656,+919599782053';

    public function __construct() {
        $this->helper = new Helper();
    }

    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    public function docConnect() {

        $input = Input::all();

        $extension = trim($input['extension']);
        $mobile = trim($input['mobile']);
        $kccDashboardCall = $input['kccDashboardCall'];

        if (empty($extension) || empty($mobile)) {
            return array('status' => 4, 'doctor_no' => null, 'operator_no' => KccController::OpertorNumber, 'msg' => 'Invalid mobile or extension number.');
        }

        try {

            //CHECK FOR THE USER
            $userModel = new User();
            $userId = $userModel->isUserExist(array('mobile' => $mobile));

            $newUser = 0;
            if (empty($userId)) {
                $userId = $userModel->createKccUser(array('mobile' => $mobile));
                $newUser = 1;
            }

            if ($newUser) {
                return array('status' => 2, 'doctor_no' => null, 'operator_no' => KccController::OpertorNumber, 'msg' => 'Insufficient Balance as user is just registered.');
            }

            //CHECK FOR THE DOCTOR EXTENSION NUMBER
            $doctorProfile = new Doctor();
            $doctorDetails = $doctorProfile->getDoctorDetails(array('extension' => $extension));
            if (COUNT($doctorDetails) <= 0) {
                return array('status' => 3, 'doctor_no' => null, 'operator_no' => KccController::OpertorNumber, 'msg' => 'Wrong extension number.');
            }

            if ($doctorDetails->user_id == $userId) {
                return array('status' => 4, 'doctor_no' => null, 'operator_no' => KccController::OpertorNumber, 'msg' => 'You are calling your own extension number.');
            }

            //CHECK FOR BALANCE
            $kccAccountStatement = new KccAccountStatement();
            $getAccountStatement = $kccAccountStatement->getAccountStatement(array('user_id' => $userId));

            $kccTransactionType = new KccTransactionType();

            $balanceParams = array();
            $balanceParams['transaction_type_id'] = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
            $balanceParams['transferred_from'] = $userId;
            $closing_balance = 0;
            if (count($getAccountStatement) > 0) {
                $balanceParams['created_at'] = $getAccountStatement->statement_date;
                $closing_balance = $getAccountStatement->closing_balance;
            }

            $kccTransaction = new KccTransaction();
            $callBalance = $kccTransaction->getAccountBalance($balanceParams);

            $balanceParams['transferred_to'] = $userId;
            unset($balanceParams['transferred_from']);
            $balanceParams['transaction_type_id'] = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));
            $rechargeBalance = $kccTransaction->getAccountBalance($balanceParams);

            $mainBalance = $closing_balance + $rechargeBalance - $callBalance;

            if ($mainBalance < $doctorDetails->per_min_charges) {
                return array('status' => 2, 'doctor_no' => null, 'operator_no' => KccController::OpertorNumber, 'msg' => 'Insufficient Balance.');
            }

            if ($doctorDetails->per_min_charges > 0) {
                $maxCallDuration = floor(($mainBalance / $doctorDetails->per_min_charges) * 60);
            } else {
                $maxCallDuration = Config::get('constants.MAX_CALL_DURATION_CAPPING');
            }

            $requestDetails = array('status' => 1, 'doctor_no' => $doctorDetails->mobile, 'operator_no' => KccController::OpertorNumber, 'max_call_duration' => $maxCallDuration, 'msg' => 'Please wait while we transferring your call to doctor.');

            //INSERT DATA IN COMMUNICATION TABLE
            $kccCall = new KccCall();
            $kccCall->user_id = $userId;
            $kccCall->doctor_user_id = $doctorDetails->user_id;
            $kccCall->per_min_charges = $doctorDetails->per_min_charges;
            $kccCall->request_details = json_encode($requestDetails);
            $kccCall->save();

            $response = array_merge($requestDetails, array('kcc_call_id' => $kccCall->kcc_call_id, 'call_id' => $kccCall->kcc_call_id, 'communication_id' => $kccCall->kcc_call_id));
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $response['status'] = 4;
            $response['msg'] = $ex->getMessage();
        }

        if ($kccDashboardCall) {
            $url = Config::get('constants.KNOWLARITY_KCC_DASHBOARD') . "customer_number=+$mobile&agent_number=+$doctorDetails->mobile&communication_id=$kccCall->kcc_call_id&max_call_duration=$maxCallDuration&auth_key=" . Config::get('constants.KNOWLARITY_AUTH_KEY');

            $httpHelper = $this->helper->load('http');
            $doc = $httpHelper->httpGet($url);
            $xml = simplexml_load_string($doc);
            $json = json_encode($xml);
            $responseArray = json_decode($json, TRUE);
            if ($responseArray['status'] == 'Success') {
                
            } else {
                $response['status'] = 900;
                $response['msg'] = "Service Provider Issue:" . $responseArray;
            }

            //LOGGING OF ACTION
            $logData = array();
            $logData['type'] = 'kcc_call_connect';
            $logData['user_id'] = $input['user_id'];
            $logData['request'] = json_encode(array('url' => $url));
            $logData['response'] = json_encode($responseArray);
            DB::table('admin_users_access_log')->insert($logData);
        }

        return Response::make($response, 200, array());
    }

    public function callComplete() {

        $input = Input::all();
        $communicationId = $input['communication_id'];
        $callDuration = $input['doctor_call_duration'] ? $input['doctor_call_duration'] : $input['call_duration'];
        $callStartTime = $input['call_starttime'];
        $callerStatus = $input['caller_status'];
        $receiverStatus = $input['receiver_status'];
        $doctorNumber = $input['doctor_no'];
        $patientNumber = $input['patient_no'];

        try {

            if (!empty($communicationId)) {
                $communication = KccCall::where('kcc_call_id', '=', $communicationId)->first();
                if (!empty($communication)) {

                    $minCallDuration = Config::get('constants.MIN_CALL_DURATION');

                    //UPDATE COMMUNICATION TABLE
                    $communication->call_duration = $callDuration;
                    $communication->call_starttime = $callStartTime;
                    $communication->caller_status = $callerStatus;
                    $communication->receiver_status = $receiverStatus;
                    $communication->response_details = json_encode($input);
                    $communication->save();

                    $kccTransactionType = new KccTransactionType();

                    $totalCharges = round($communication->call_duration * $communication->per_min_charges / 60);
                    $konsultCharges = round((floatval(Config::get('constants.KONSULT_PERCENTAGE')) * $totalCharges) / 100);
                    $doctorCharges = $totalCharges - $konsultCharges;

                    //INSERT TRANSACTION DETAILS 
                    $transaction = new KccTransaction();
                    $transaction->transferred_to = $communication->doctor_user_id;
                    $transaction->transferred_from = $communication->user_id;
                    $transaction->amount = ($minCallDuration < $communication->call_duration) ? $totalCharges : 0;
                    $transaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
                    $transaction->transaction_details_id = $communication->kcc_call_id;
                    $transaction->save();

                    //INSERT SPEND TRANSACTION DETAILS 
                    $kccTransactionSpendDetails = new KccTransactionSpendDetails();
                    $kccTransactionSpendDetails->kcc_call_id = $communication->kcc_call_id;
                    $kccTransactionSpendDetails->total_charges = $totalCharges;
                    $kccTransactionSpendDetails->doctor_charges = $doctorCharges;
                    $kccTransactionSpendDetails->virtual_money = $totalCharges;
                    $kccTransactionSpendDetails->real_money = 0;
                    $kccTransactionSpendDetails->save();

                    //PUSH MONEY TO DOCTOR CITRUS ACCOUNT
                    if ($transaction->amount > 0) {

                        $citrusEmailId = UserWallet::where('user_id', '=', $communication->doctor_user_id)->pluck('email');

                        $citrusdata_doc = array();
                        $citrusdata_doc['to'] = $citrusEmailId;
                        $citrusdata_doc['amount'] = $doctorCharges;
                        $citrusdata_doc['message'] = "transaction_id : $transaction->transaction_id";

                        $citrusHelper = $this->helper->load('citrus');
                        $doc_citrus_json = $citrusHelper->pushMoney($citrusdata_doc);

                        $doctor = User::where('user_id', '=', $transaction->transferred_to)->select('name', 'email', 'mobile')->first();
                        $patient = User::where('user_id', '=', $transaction->transferred_from)->select('name', 'email', 'mobile')->first();

                        $communicationHelper = $this->helper->load('communication');

                        //SEND SMS ALERT TO PATIENT
                        $first2Chars = substr($patientNumber, 0, 2);
                        $first3Chars = substr($patientNumber, 0, 3);

                        $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALL_DEBIT_PATIENT');
                        if ($smsText) {

                            $kccTransaction = new KccTransaction();
                            $closing_balance = $kccTransaction->getBalance(array('user_id' => $transaction->transferred_from));
                            $closing_balance = round($closing_balance, 2);

                            //PREPARE DATA FOR SMS AND SEND
                            $smsParams = array();
                            $smsParams['Text'] = str_replace(array('<User>', '<Doctor Name>', '<Value1>', '<Value2>'), array($patient['name'], $doctor['name'], "INR " . $totalCharges, "INR " . $closing_balance), $smsText);
                            $smsParams['To'] = $patient['mobile'];
                            $communicationHelper->smsCommunication($smsParams);
                        }

                        //SEND SMS ALERT TO DOCTOR
                        $first2Chars = substr($doctorNumber, 0, 2);
                        $first3Chars = substr($doctorNumber, 0, 3);

                        $smsText = Config::get('constants.NETCORE_SMS_TEXT_CALL_CREDIT_DOCTOR');
                        if ($citrusdata_doc['amount'] > 0 && $doc_citrus_json['status'] == 'SUCCESSFUL' && $smsText) {
                            $doc_token = UserWallet::where('user_id', '=', $transaction->transferred_to)->pluck('token');

                            $citrusHelper = $this->helper->load('citrus');
                            $mainBalance = $citrusHelper->fetchRealBalance(array('citrusToken' => $doc_token));

                            //PREPARE DATA FOR SMS AND SEND
                            $smsParams = array();
                            $smsParams['Text'] = str_replace(array('<Doctor Name>', '<User>', '<Value1>', '<Value2>'), array($doctor['name'], $patient['name'], $citrusdata_doc['amount'], $mainBalance['value']), $smsText);
                            $smsParams['To'] = $doctor['mobile'];
                            $communicationHelper->smsCommunication($smsParams);
                        }
                    }

                    $result = array('status' => 1, 'msg' => 'Call has been completed successfully.');
                } else {
                    $result = array('status' => 0, 'msg' => 'Undefined communication.');
                }
            } else {
                $result = array('status' => 0, 'msg' => 'Communication id is empty.');
            }
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $result = array('status' => 0, 'msg' => $ex->getMessage());
        }

        return Response::make($result, 200, array());
    }

    public function balance() {
        $input = Input::all();
        $mobile = $input['mobile'];

        try {

            $userModel = new User();
            $userId = $userModel->isUserExist(array('mobile' => $mobile));

            $newUser = 0;
            if (empty($userId)) {
                $userId = $userModel->createKccUser(array('mobile' => $mobile));
                $newUser = 1;
            }

            $locale = isset($input['locale']) ? $input['locale'] : '';
            $callTypeData = User::getCallTypeData(array('mobile' => $mobile, 'locale' => $locale));

            $kccTransaction = new KccTransaction();
            $closing_balance = $kccTransaction->getBalance(array('user_id' => $userId));

            if ($callTypeData['internationalCall']) {
                $closing_balance = $closing_balance / $callTypeData['currencyFactor'];
            }

            $closing_balance = round($closing_balance, 2);
            $response = array('status' => 1, 'closing_balance' => $closing_balance, 'currency' => $callTypeData['currency'], 'currencySymbol' => $callTypeData['currencySymbol']);
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $response = array('status' => 0, 'msg' => $ex->getMessage());
        }

        $response = array_merge($response, array('operator_no' => KccController::OpertorNumber));

        return Response::make($response, 200, array());
    }

    public function recharge() {

        $input = Input::all();
        $mobile = trim($input['mobile']);
        $promoCode = trim($input['promo_code']);

        $userModel = new User();
        $userId = $userModel->isUserExist(array('mobile' => $mobile));

        $newUser = 0;
        if (empty($userId)) {
            $userId = $userModel->createKccUser(array('mobile' => $mobile));
            $newUser = 1;
        }

        try {

            $kccCodeInfo = new KccCodeInfo();
            $validatePromoCode = $kccCodeInfo->validatePromoCode(array('code' => $promoCode, 'user_id' => $userId));

            if (!empty($validatePromoCode) && $validatePromoCode['status'] == 1) {

                DB::beginTransaction();
                try {
                    $rechargeInfo = new KccRechargeInfo();
                    $rechargeInfo->user_id = $userId;
                    $rechargeInfo->code_info_id = $validatePromoCode['code_info_id'];
                    $rechargeInfo->save();

                    $kccTransactionType = new KccTransactionType();

                    //INSERT TRANSACTION DETAILS 
                    $transaction = new KccTransaction();
                    $transaction->transferred_to = $userId;
                    $transaction->transferred_from = KccController::KonsultUserId;
                    $transaction->amount = $validatePromoCode['amount'];
                    $transaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));
                    $transaction->transaction_details_id = $rechargeInfo->recharge_info_id;
                    $transaction->save();

                    if ($validatePromoCode['code_type'] == 1) {
                        KccCodeInfo::where('code_info_id', '=', $validatePromoCode['code_info_id'])->update(array('is_exhausted' => 1));
                    }

                    DB::commit();

                    //ADD THE SMS CODE HERE

                    $response = array('status' => 1, 'msg' => 'Recharge has been done successfully.');
                } catch (Exception $ex) {
                    DB::rollback();
                    $response = array('status' => 2, 'msg' => $ex->getMessage());
                }

                //SEND SMS ALERT
                $smsText = Config::get('constants.NETCORE_SMS_TEXT_RECHARGE_WALLET');
                if ($smsText && $response['status'] == 1) {
                    $kccTransaction = new KccTransaction();
                    $closing_balance = $kccTransaction->getBalance(array('user_id' => $userId));
                    $closing_balance = round($closing_balance, 2);

                    $smsParams = array();
                    $totalAvailableBalance = $transaction->amount;
                    $smsParams['Text'] = str_replace(array('<User>', '<Value1>', '<Value2>'), array('User', "INR " . $transaction->amount, "INR " . $closing_balance), $smsText);
                    $smsParams['To'] = $mobile;
                    $this->helper->load('communication')->smsCommunication($smsParams);
                }
            } else {
                $response = $validatePromoCode;
            }
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $response = array('status' => 2, 'msg' => $ex->getMessage());
        }

        $response = array_merge($response, array('operator_no' => KccController::OpertorNumber));

        return Response::make($response, 200, array());
    }

    public function addRealCash() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $userId = $this->getUserIdbyToken($this->authToken);
            $amount = $data['amount'] * Config::get('constants.INTERNATIONAL_CALL_CHARGE_CURRENCY_FACTOR');

            if (!empty($amount) && !empty($userId)) {

                DB::beginTransaction();
                try {

                    $kccTransactionType = new KccTransactionType();

                    //INSERT TRANSACTION DETAILS 
                    $transaction = new KccTransaction();
                    $transaction->transferred_to = $userId;
                    $transaction->transferred_from = KccController::KonsultUserId;
                    $transaction->amount = $amount;
                    $transaction->transaction_type_id = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));

                    $transaction->save();

                    DB::commit();

                    //ADD THE SMS CODE HERE

                    $response = array('status' => 1, 'msg' => 'Recharge has been done successfully.');
                } catch (Exception $ex) {
                    DB::rollback();
                    $response = array('status' => 2, 'msg' => $ex->getMessage());
                }
            } else {
                $response = array('status' => 0, 'msg' => 'User does not exist or recharge amount is empty!');
            }
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $response = array('status' => 2, 'msg' => $ex->getMessage());
        }

        return Response::make($response, 200, array());
    }

    public function generateStatement() {

        try {
            $kccTransaction = new KccTransaction();
            $userIds = $kccTransaction->getUsersList();

            $kccAccountStatement = new KccAccountStatement();

            $kccTransactionType = new KccTransactionType();
            $callTransactionTypeId = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Call'));
            $rechargeTransactionTypeId = $kccTransactionType->getTransactionTypeId(array('transaction_type_name' => 'Recharge'));

            foreach ($userIds as $userId) {

                //CHECK FOR BALANCE
                $getAccountStatement = $kccAccountStatement->getAccountStatement(array('user_id' => $userId));

                $balanceParams = array();
                $balanceParams['transaction_type_id'] = $callTransactionTypeId;
                $balanceParams['transferred_from'] = $userId;
                if (count($getAccountStatement) > 0) {
                    $balanceParams['created_at'] = $getAccountStatement->statement_date;
                }

                $callBalance = $kccTransaction->getAccountBalance($balanceParams);

                $balanceParams['transferred_to'] = $userId;
                unset($balanceParams['transferred_from']);
                $balanceParams['transaction_type_id'] = $rechargeTransactionTypeId;
                $rechargeBalance = $kccTransaction->getAccountBalance($balanceParams);

                $kccAccountStatement = new KccAccountStatement();
                $kccAccountStatement->user_id = $userId;
                $kccAccountStatement->amount_added = $rechargeBalance;
                $kccAccountStatement->amount_spend = $callBalance;
                $kccAccountStatement->statement_date = date("Y-m-d H:i:s");

                if (count($getAccountStatement) > 0 && !empty($getAccountStatement->closing_balance)) {
                    $kccAccountStatement->closing_balance = $getAccountStatement->closing_balance + $rechargeBalance - $callBalance;
                } else {
                    $kccAccountStatement->closing_balance = $rechargeBalance - $callBalance;
                }
                $kccAccountStatement->save();
            }

            $response = array('status' => 1, 'msg' => 'Statement generation done.');
        } catch (Exception $ex) {

            $this->_logDbError($ex);

            $response = array('status' => 0, 'msg' => $ex->getMessage());
        }

        return Response::make($response, 200, array());
    }

    public function assignMinutes() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $data = Input::all();
            $viewer_id = $this->getUserIdbyToken($this->authToken);

            //WHO CAN CALL THIS API
            $admin_id = DB::table('admin_users')->where('user_id', '=', $viewer_id)->pluck('admin_id');

            if (empty($admin_id)) {
                $result = array('status' => 0, 'msg' => 'Un-authorized access!');
                return Response::make($result, 200, $this->headers);
            }

            $admin_permission_id = DB::table('admin_permissions')->where('admin_id', '=', $admin_id)->where('permission_id', '=', 27)->pluck('admin_id');

            if (empty($admin_permission_id)) {
                $result = array('status' => 0, 'msg' => 'Un-authorized access!');
                return Response::make($result, 200, $this->headers);
            }

            $mobile = trim($data['mobile']);
            $amount = trim($data['amount']);
            $doctor_user_id = trim($data['doctor_user_id']);
            $isValidMobile = preg_match('/^\+[1-9]{1}[0-9]{3,14}$/', $mobile);

            if (empty($mobile) || empty($isValidMobile) || empty($amount) || $amount <= 10 || empty($doctor_user_id)) {
                $result['response']['status'] = 0;
                $result['response']['msg'] = 'Missing or invalid inputs!';
                return $result;
            }

            //CHECK FOR THE USER
            $userModel = new User();
            $mobile = str_replace('+', '', $mobile);
            $userId = $userModel->isUserExist(array('mobile' => $mobile));

            if (empty($userId)) {
                $result = array('status' => 0, 'msg' => 'User does not exists. Please check entered mobile number or create user first!');
                return Response::make($result, 200, $this->headers);
            }

            $perMinCharges = DB::table('doctor_profile')->where('user_id', '=', $doctor_user_id)->pluck('per_min_charges');
            $perMinCharges *= Config::get('constants.INTERNATIONAL_CALL_CHARGE_PER_MIN_RULES'); 
            if (!empty($perMinCharges)) {
                $totalSeconds = ($amount / $perMinCharges) * 60;
                $totalSeconds = (int) $totalSeconds;
            } else {
                $result = array('status' => 0, 'msg' => 'Invalid doctor!');
                return Response::make($result, 200, $this->headers);
            }

            //PROCIDE RECHARGE IF ALL VALID 
            $patDocMapping = PatDocMinInfo::where('patient_user_id', '=', $userId)->where('doctor_user_id', '=', $doctor_user_id)->first();

            if (!empty($patDocMapping) && !empty($patDocMapping->min_info_id)) {
                $patDocMapping->total_bought_seconds += $totalSeconds;
                $patDocMapping->available_seconds += $totalSeconds;
                $patDocMapping->save();
            } else {
                $minInfo = new PatDocMinInfo();
                $minInfo->patient_user_id = $userId;
                $minInfo->doctor_user_id = $doctor_user_id;
                $minInfo->total_bought_seconds = $minInfo->available_seconds = $totalSeconds;
                $minInfo->save();
            }
            
            //ADD DOCTOR IN PATIENT'S FAVOURITE DOCTOR
            $favouriteData = array();
            $favouriteData['patient'] = $userId;
            $favouriteData['doctor'] = $doctor_user_id;
            $favDoctor = new FavoriteDoctor();
            if ($favDoctor->validate($favouriteData)) {
                $doc_exist = Doctor::whereRaw('user_id =' . $favouriteData['doctor'])->exists();
                if ($doc_exist) {
                    $favdoc = FavoriteDoctor::whereRaw('patient = ' . $favouriteData['patient'] . ' && doctor =' . $favouriteData['doctor'])->exists();
                    if (!$favdoc) {
                        $favDoctor->patient = $favouriteData['patient'];
                        $favDoctor->doctor = $favouriteData['doctor'];
                        $favDoctor->save();
                        $result['add_favourite_msg'] = Config::get('config_msg.DR_ADD_FAV_LIST');
                    } else {
                        $favDoctor->save();
                        $result['add_favourite_msg'] = Config::get('config_msg.DR_ALREADY_ADD_FAV_LIST');
                    }
                }
            }            

            $result['status'] = 1;
            $result['msg'] = 'Talk time assigned successfully!';
            $this->setHeader('RESPONSE_CODE', '1');

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }
    
    public function availableMinutes() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $viewer_id = $this->getUserIdbyToken($this->authToken);

            $query = "SELECT pat_doc_min_info.available_seconds, users.user_id, users.mobile, users.salutation, users.name, user_profile.photo, doctor_profile.online_status from pat_doc_min_info JOIN users ON pat_doc_min_info.doctor_user_id = users.user_id LEFT JOIN user_profile ON user_profile.user_id = users.user_id LEFT JOIN doctor_profile ON doctor_profile.user_id = users.user_id WHERE pat_doc_min_info.patient_user_id = $viewer_id";
            $patDocMinInfo = DB::select($query);

            $patDocInfo = Array();
            $count = 0;
            foreach ($patDocMinInfo as $mapping) {
                $response ['user_id'] = $mapping->user_id;
                $response ['name'] = trim($mapping->salutation . " " . $mapping->name);
                $response ['mobile'] = $mapping->mobile;
                $response ['photo'] = User::getPhoto(array('photo' => $mapping->photo, 'is_doctor' => 1));
                $response ['online_status'] = $mapping->online_status;
                $response ['available_seconds'] = $mapping->available_seconds;
                $response ['available_minutes'] = (int) ($mapping->available_seconds/60);
                $json_response = json_encode($response);
                $array_decode = json_decode($json_response);
                array_push($patDocInfo, $array_decode);
                $count++;
            }

            $result['status'] = 1;
            $result['availableMinsInfoCount'] = $count;
            $result ['availableMinsInfo'] = $patDocInfo;
            $result['msg'] = 'List of available mins to doctors!';
            $this->setHeader('RESPONSE_CODE', '1');

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }
    
    public function myPatientsAvailableMinutes() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $viewer_id = $this->getUserIdbyToken($this->authToken);

            $query = "SELECT pat_doc_min_info.available_seconds, users.user_id, users.mobile, users.salutation, users.name, user_profile.photo from pat_doc_min_info JOIN users ON pat_doc_min_info.patient_user_id = users.user_id LEFT JOIN user_profile ON user_profile.user_id = users.user_id LEFT JOIN doctor_profile ON doctor_profile.user_id = users.user_id WHERE pat_doc_min_info.doctor_user_id = $viewer_id AND pat_doc_min_info.available_seconds > 0";
            $patDocMinInfo = DB::select($query);

            $patDocInfo = Array();
            $count = 0;
            foreach ($patDocMinInfo as $mapping) {
                $response ['user_id'] = $mapping->user_id;
                $response ['name'] = trim($mapping->salutation . " " . $mapping->name);
                $response ['mobile'] = $mapping->mobile;
                $response ['photo'] = User::getPhoto(array('photo' => $mapping->photo, 'is_doctor' => 1));
                $response ['available_seconds'] = $mapping->available_seconds;
                $response ['available_minutes'] = (int) ($mapping->available_seconds/60);
                $json_response = json_encode($response);
                $array_decode = json_decode($json_response);
                array_push($patDocInfo, $array_decode);
                $count++;
            }

            $result['status'] = 1;
            $result['availableMinsInfoCount'] = $count;
            $result ['availableMinsInfo'] = $patDocInfo;
            $result['msg'] = 'List of available mins to doctors!';
            $this->setHeader('RESPONSE_CODE', '1');

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }    

    protected function _logDbError($ex) {

        try {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);
        } catch (Exception $e) {
            
        }

        return 1;
    }

}
