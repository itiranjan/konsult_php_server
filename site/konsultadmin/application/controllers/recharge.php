<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class recharge extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->is_logged_in();
        $this->load->model('recharge_model', '', TRUE);
    }

    function is_logged_in() {

        $is_logged_in = $this->session->userdata('userid');

        if (!isset($is_logged_in) || $is_logged_in != true)
            redirect(URL);
    }

    public function mvc() {
        $this->load->view('admin/header');
        $this->load->view("admin/mvc-recharge");
    }

    public function rechargeList($way) {

        $data = array();
        $resultsData = $this->recharge_model->rechargeList($_POST);

        $result = $resultsData['results'];
        $resultsCount = $resultsData['resultsCount'];
        foreach ($result->result() as $row) {
            $data[] = array(
                'rechargelog_id' => $row->rechargelog_id,
                'users$user_id' => $row->user_id,
                'name' => $row->name,
                'mvc_rechargelog$email' => $row->email,
                'mvc_rechargelog$citrus_email' => $row->citrus_email,
                'amount' => $row->amount,
                'created_at' => $row->created_at,
                'recharged_by' => $row->recharged_by,
            );
        }

        header("Content-type: application/json");
        if ($way == 1)
            echo "{\"list\":" . json_encode($data) . ',"total":"' . $resultsCount . '"' . "}";
        else
            echo json_encode($data);
    }

    public function index() {
        $this->load->view('admin/header');
        $this->load->view("admin/recharge-wallet");
    }

    public function wallet() {

        $rowData = array();
        $email = strtolower($_POST['email']);
        $mobile = $_POST['mobile'];
        $amount = $_POST['amount'];
        $password = $_POST['password'];

        //LOGGING OF ACTION
        $logData = array();
        $logData['type'] = 'wallet_recharge';
        $logData['request'] = $_POST;
        $this->load->model('accesslog_model', '', TRUE);

        if (empty($email) || empty($mobile) || empty($amount) || empty($password)) {
            $response = '{"message":"All fields are required","status":"0"}';
            echo $response;
            die;
        }

        $date = date('j');
        $passwordMatch = "Konsult2017$$date";
        if ($password != $passwordMatch) {
            $response = '{"message":"Please enter the valid pass key.","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }

        $userQuery = "SELECT users.user_id, clinic_type FROM users JOIN user_profile ON users.user_id = user_profile.user_id where email = '$email' and mobile = $mobile";
        $userData = $this->db->query($userQuery);
        foreach ($userData->result() as $user) {
            $userId = $user->user_id;
            $isClinic = $user->clinic_type;
        }

        if (empty($userId)) {
            $response = '{"message":"Invalid email or mobile!","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }

        if ($amount > 700) {
            $response = '{"message":"Amount can`t be more than INR 700/-","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }
        
        if (!$isClinic && $amount > 500) {
            $response = '{"message":"Amount can`t be more than INR 500/-","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }        

        $citrusInfoQuery = "SELECT email FROM user_wallet where user_id = '$userId' AND wallet_id = 3";

        $citrusUserData = $this->db->query($citrusInfoQuery);
        foreach ($citrusUserData->result() as $citrusInfo) {
            $citrusEmail = $citrusInfo->email;
        }

        if (empty($citrusEmail)) {
            $response = '{"message":" Mapping does not exist with Konsult and Citrus!","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }

        $currentMVCBalance = $this->fetchMVCBalance(array('email' => $email));
        $currentMVCBalance = $currentMVCBalance['mvcAmount'];
        if ($currentMVCBalance >= 300) {
            $message = "Current balance of user is: INR $currentMVCBalance/- and must be less than INR 300/- to recharge.";
            $response = '{"message":"' . $message . '","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }

        $url = "https://coupons.citruspay.com/cms_api.asmx/IssueCoupons";
        $headerValue = "application/json";
        $CampaignCode = "KONSULTCASH";
        $Type = 'F';
        $PartnerID = "haovrnrhhn";
        $Password = "MQ526OM21YGFL8OW0Z10";
        $data = '{ "CampaignCode": "KONSULTCASH", "UserList": [ { "Email": "' . $email . '", "Mobile": "' . '", "Amount": "' . $amount . '", "Type": "F" } ], "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:' . $headerValue
        ));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        $user_citrus_mvc = json_decode($output, true);
        $mvcissuecoupan = $user_citrus_mvc['d'];
        $mvc_issuecoupan = $mvcissuecoupan['status'];
        $StatusId = $mvc_issuecoupan['StatusID'];
        $currentDateTime = date("Y-m-d H:i:s");

        if (!empty($StatusId) && $StatusId == "1") {

            $rehargedBy = $this->session->userdata('username');
            $usrdb = "INSERT IGNORE INTO `mvc_rechargelog` (`user_id`, `mobile`, `email`,`citrus_email`, `amount`, `created_at`, `updated_at`, `recharged_by`) VALUES ($userId, $mobile, '$email', '$citrusEmail', $amount, '$currentDateTime', '$currentDateTime', '$rehargedBy')";

            $this->db->query($usrdb);

            $message = "You have successfully added " . $amount . " Rupees in Virtual Currency Account!";
            $response = '{"message":"' . $message . '","status":"1"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        } else if (!empty($StatusId) && $StatusId == "0") {
            $response = '{"message":"Input parameters invalid!","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        } else {

            $response = '{"message":"N/A or Partner Account Expired<br/>PartnerID/Password Incorrect or Error occurred <br/>Campaign Code is invalid or Campaign is associated to all users <br/>Maximum count reached for campaign or Following emails are duplicates in this campaign:...<br/> Duplicate emails present in the list or Error in Coupon generation. No users available to generate coupon!","status":"0"}';
            $logData['response'] = json_decode($response);
            $this->accesslog_model->insertLog($logData);
            echo $response;
            die;
        }
    }

    public function fetchMVCBalance($params = array()) {

        $email = $params['email'];
        $mobile = '';

        $MerchantID = 'haovrnrhhn';
        $PartnerID = 'haovrnrhhn';
        $Password = 'MQ526OM21YGFL8OW0Z10';
        $mvcurl = 'https://coupons.citruspay.com/cms_api.asmx' . '/FetchCampaignBalance';
        $headerValue = 'application/json';
        $data = '{"MerchantID": "' . $MerchantID . '", "CampaignCode": "KONSULTCASH", "Email": "' . $email . '", "Mobile": "' . $mobile . '", "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';
        $user_citrus_mvc = $this->httpPostWithHeader($mvcurl, $headerValue, $post = 1, $data);
        $user_citrus_mvc = json_decode($user_citrus_mvc, true);

        if (empty($user_citrus_mvc['d'])) {
            $user_citrus_mvc['errorMsg'] = 'Unable to fetch MVC balance.';
        }

        $user_citrus_mvc['mvcAmount'] = $user_citrus_mvc['d']['Campaigns'][0]['Amount'];

        return $user_citrus_mvc;
    }

    public function httpPostWithHeader($url, $headerValue, $post = Null, $data = Null) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:' . $headerValue));
        if ($post == 1) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $output = curl_exec($ch);
        curl_close($ch);

        return $output;
    }

}
