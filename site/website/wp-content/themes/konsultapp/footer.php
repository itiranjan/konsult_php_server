<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */
?>

<?php //do_action( 'mfn_hook_content_after' ); ?>

<!-- #Footer -->		
<footer id="Footer" class="clearfix">
	
	<?php
    if(is_front_page()){
    
    
    
    //if ( $footer_call_to_action = mfn_opts_get('footer-call-to-action') ): ?>
	<!--	<div class="footer_action">
			<div class="container">
				<div class="column one column_column">
					<?php //echo do_shortcode( $footer_call_to_action ); ?>
				</div>
			</div>
		</div>-->
	<?php //endif; ?>
	
	<?php 
	$sidebars_count = 0;
	for( $i = 1; $i <= 4; $i++ ){
		if ( is_active_sidebar( 'footer-area-'. $i ) ) $sidebars_count++;
	}

	if( $sidebars_count > 0 ){
		echo '<div class="widgets_wrapper">';
		echo '<div class="container">';

		if( $footer_layout = mfn_opts_get( 'footer-layout' ) ){
						// Theme Options

			$footer_layout 	= explode( ';', $footer_layout );
			$footer_cols 	= $footer_layout[0];

			for( $i = 1; $i <= $footer_cols; $i++ ){
				if ( is_active_sidebar( 'footer-area-'. $i ) ){
					echo '<div class="column '. $footer_layout[$i] .'">';
					dynamic_sidebar( 'footer-area-'. $i );
					echo '</div>';
				}
			}						

		} else {
						// Default - Equal Width

			$sidebar_class = '';
			switch( $sidebars_count ){
				case 2: $sidebar_class = 'one-second'; break;
				case 3: $sidebar_class = 'one-third'; break;
				case 4: $sidebar_class = 'one-fourth'; break;
				default: $sidebar_class = 'one';
			}

			for( $i = 1; $i <= 4; $i++ ){
				if ( is_active_sidebar( 'footer-area-'. $i ) ){
					echo '<div class="column '. $sidebar_class .'">';
					dynamic_sidebar( 'footer-area-'. $i );
					echo '</div>';
				}
			}

		}

		echo '</div>';
		echo '</div>';
	}
	?>
	
	<?php
    }
    //if( mfn_opts_get('footer-hide') != 1 ): ?>
		<div class="footer_copy">
			<div class="container">
				<div class="column one">
					
					<?php $back_to_top = mfn_opts_get('back-top-top'); ?>
					<!--<a id="back_to_top" class="button button_left button_js <?php //echo $back_to_top; ?>" href=""><span class="button_icon"><i class="icon-up-open-big"></i></span></a>-->

					<!-- Copyrights -->
					<div class="copyright">
						<?php 
					//	if( mfn_opts_get('footer-copy') ){
						//	echo do_shortcode( mfn_opts_get('footer-copy') );
						//} else {
							//echo '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. ';
							/*echo "<ul class='foter-menu'>";
								echo "<li><a href=". home_url() .">Home</a></li>";
								echo "<li><a href=". home_url() .">About Us</a></li>";
								echo "<li><a href=". home_url() .">FAQ's</a></li>";
								echo "<li><a href=". home_url() .">Contact Us</a></li>";
							echo "</ul>";*/
							echo '<div class="copyright-txt"><div class="footer-sico-copyleft cprght">Copyright &copy; Konsult App 2016 All Rights Reserved.</div><div class="footer-sico-copyright cprght">
<a href="https://twitter.com/konsult_app?ref_src=twsrc%5Etfw" class="icon_bar  icon_bar_twitter icon_bar_large" target="_blank"><span class="t"><i class="fa fa-twitter"></i></span><span class="b"><i class="fa fa-twitter"></i></span></a>
<a href="https://www.facebook.com/konsultapp" class="icon_bar  icon_bar_facebook icon_bar_large" target="_blank"><span class="t"><i class="fa fa-facebook"></i></span><span class="b"><i class="fa fa-facebook"></i></span></a>
<a href="https://www.linkedin.com/company/konsult-app" class="icon_bar  icon_bar_linkedin icon_bar_large" target="_blank"><span class="t"><i class="fa fa-linkedin"></i></span><span class="b"><i class="fa fa-linkedin"></i></span></a>
<a href="https://www.instagram.com/konsultapp/?hl=en" class="icon_bar  icon_bar_instagram icon_bar_large" target="_blank"><span class="t"><i class="fa fa-instagram"></i></span><span class="b"><i class="fa fa-instagram"></i></span></a>
<a href="#" id="go-to-top" class="icon_bar  icon_bar_instagram icon_bar_large "><span class="t"><i class="fa fa-angle-up fa-2x"></i></span><span class="b"><i class="fa fa-angle-up fa-2x"></i></span></a>
</div></div>';
					//	}
						?>
					</div>

					<?php 
					// if( has_nav_menu( 'social-menu-bottom' ) ){

						// // #social-menu
						// //mfn_wp_social_menu_bottom(); /* footer menu */

					// } else {
						
						// $target = mfn_opts_get('social-target') ? 'target="_blank"' : false;

						// echo '<ul class="social">';
						// if( mfn_opts_get('social-skype') ) echo '<li class="skype"><a '.$target.' href="'. mfn_opts_get('social-skype') .'" title="Skype"><i class="icon-skype"></i></a></li>';
						// if( mfn_opts_get('social-facebook') ) echo '<li class="facebook"><a '.$target.' href="'. mfn_opts_get('social-facebook') .'" title="Facebook"><i class="icon-facebook"></i></a></li>';
						// if( mfn_opts_get('social-googleplus') ) echo '<li class="googleplus"><a '.$target.' href="'. mfn_opts_get('social-googleplus') .'" title="Google+"><i class="icon-gplus"></i></a></li>';
						// if( mfn_opts_get('social-twitter') ) echo '<li class="twitter"><a '.$target.' href="'. mfn_opts_get('social-twitter') .'" title="Twitter"><i class="icon-twitter"></i></a></li>';
						// if( mfn_opts_get('social-vimeo') ) echo '<li class="vimeo"><a '.$target.' href="'. mfn_opts_get('social-vimeo') .'" title="Vimeo"><i class="icon-vimeo"></i></a></li>';
						// if( mfn_opts_get('social-youtube') ) echo '<li class="youtube"><a '.$target.' href="'. mfn_opts_get('social-youtube') .'" title="YouTube"><i class="icon-play"></i></a></li>';						
						// if( mfn_opts_get('social-flickr') ) echo '<li class="flickr"><a '.$target.' href="'. mfn_opts_get('social-flickr') .'" title="Flickr"><i class="icon-flickr"></i></a></li>';
						// if( mfn_opts_get('social-linkedin') ) echo '<li class="linkedin"><a '.$target.' href="'. mfn_opts_get('social-linkedin') .'" title="LinkedIn"><i class="icon-linkedin"></i></a></li>';
						// if( mfn_opts_get('social-pinterest') ) echo '<li class="pinterest"><a '.$target.' href="'. mfn_opts_get('social-pinterest') .'" title="Pinterest"><i class="icon-pinterest"></i></a></li>';
						// if( mfn_opts_get('social-dribbble') ) echo '<li class="dribbble"><a '.$target.' href="'. mfn_opts_get('social-dribbble') .'" title="Dribbble"><i class="icon-dribbble"></i></a></li>';
						// if( mfn_opts_get('social-instagram') ) echo '<li class="instagram"><a '.$target.' href="'. mfn_opts_get('social-instagram') .'" title="Instagram"><i class="icon-instagram"></i></a></li>';
						// if( mfn_opts_get('social-behance') ) echo '<li class="behance"><a '.$target.' href="'. mfn_opts_get('social-behance') .'" title="Behance"><i class="icon-behance"></i></a></li>';
						// if( mfn_opts_get('social-tumblr') ) echo '<li class="tumblr"><a '.$target.' href="'. mfn_opts_get('social-tumblr') .'" title="Tumblr"><i class="icon-tumblr"></i></a></li>';
						// if( mfn_opts_get('social-vkontakte') ) echo '<li class="vkontakte"><a '.$target.' href="'. mfn_opts_get('social-vkontakte') .'" title="VKontakte"><i class="icon-vkontakte"></i></a></li>';
						// if( mfn_opts_get('social-viadeo') ) echo '<li class="viadeo"><a '.$target.' href="'. mfn_opts_get('social-viadeo') .'" title="Viadeo"><i class="icon-viadeo"></i></a></li>';
						// if( mfn_opts_get('social-xing') ) echo '<li class="xing"><a '.$target.' href="'. mfn_opts_get('social-xing') .'" title="Xing"><i class="icon-xing"></i></a></li>';
						// if( mfn_opts_get('social-rss') ) echo '<li class="rss"><a '.$target.' href="'. get_bloginfo('rss2_url') .'" title="RSS"><i class="icon-rss"></i></a></li>';
						// echo '</ul>';

					// }
					?>

				</div>
			</div>
		</div>
	<?php// endif; ?>
	
</footer>

</div><!-- #Wrapper -->

<?php //if( mfn_opts_get('popup-contact-form') ): ?>
	<!--<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php //mfn_opts_show( 'popup-contact-form-icon', 'icon-mail-line' ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php// echo do_shortcode( mfn_opts_get('popup-contact-form') ); ?>
			<span class="arrow"></span>
		</div>
	</div>-->
<?php //endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>

<!-- wp_footer() -->
<?php wp_footer(); ?>
<script type="text/javascript">

	jQuery(window).load(function() {
		jQuery("#department  option:first").text("----Select Department-----");
		jQuery("#experience  option:first").text("----Experience-----");
		jQuery("textarea").val("");
	});


	jQuery(document).ready(function () {
		<!--jQuery(".postid-2980 #back_to_top .button_icon .icon-up-open-big").addClass("fa fa-angle-up fa-2x");-->
		<!--jQuery(".postid-2980 #back_to_top .button_icon .fa.fa-angle-up").removeClass("icon-up-open-big");-->
		jQuery('.med-special .image_wrapper a').on("click", function (e) {
			e.preventDefault();
		});
	});
	
	function phonenumber() {
	  var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
	  var mobile = document.getElementsByName('mobile-no')[0].value;
	  if(mobile.match(phoneno)) 
	  {  
		jQuery.ajax({ 
			url: 'http://konsult.com/wp-content/themes/konsultapp/sendsms.php',
			type: "POST",
			data: {
				mobile: mobile,
			},
			success: function (data) {
				jQuery('.smslink').html(data); 
                 jQuery(".sendingmessage").html(data);
                console.log(data);
			}
		});
	  }
	  else {
		//alert("Enter correct Mobile no.");
           jQuery(".sendingmessage").html("<p style='color:red;'>Enter Correct Mobile Number!</p>");
		return false;
	  }
	}
	
	// jQuery(document).ready(function() {		
    
		//jQuery("input[type=button]").on('click', function(e) {			
		//jQuery(".linkonphone").on('click', function(e) {
						
			//e.preventDefault();
		    //var mobile = jQuery(this).parent('form').find('input[name="mobile-no"]').val();
			//var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			
		//});
	//});
</script>
<style>
.searchinputmenu a{
margin-top: 15px;
	}
	#menu-960px-grid{
		margin-bottom:0px;
	}
</style>
<script language="JavaScript" type="text/javascript">
//jQuery(".searchinputmenu").hide();
 jQuery(document).ready(function(){
	 jQuery('.headersearch').focus(function() {
    jQuery(this).attr('placeholder', 'Search Doctors, Specialities or Symptoms')
}).blur(function() {
    jQuery(this).attr('placeholder', 'Search')
})
	
	 jQuery(".searchinputmenu").hover(
  function() {
	 // alert(jQuery(this).find(".headersres a span").length);
	  // if(jQuery(this).find(".headersres a span").length === 1 )
	  // {
	  // jQuery(this).find(".headersres a span").addClass("willfindempty");
	  // }
	 // alert(jQuery('.headersres').children('li').length);
	if (jQuery('.headersres').children('li').length == 0)
{
jQuery('.headersres').parent().hide();

}
  }, function() {
	  jQuery(this).find(".headersres a span").removeClass("willfindempty");
  }
);
	 
	jQuery(".searchinputmenu i").click(function(e){
		var skw = jQuery(".headersearch").val();
		if(jQuery(".headersearch").val() != ""){
			// similar behavior as clicking on a link
window.location.href = "<?php echo site_url(); ?>/what-we-treat/?searchq="+skw;
		}
	
		//jQuery(".searchinputmenu").stop().slideToggle('slow');
		//jQuery(this).find(".fa").toggleClass("fa-times");
		//jQuery(this).find(".fa").toggleClass("fa-search");
	});
  });    
</script> 
<?php
if(is_front_page()){
	?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script language="JavaScript" type="text/javascript">
  jQuery(document).ready(function(){
	
    jQuery('.carousel').carousel({
      interval: 3000
    })
	
  });    
</script> 
	<?php }
elseif(is_page("doctor-registration")){
		?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
}elseif(is_page("careers")){
?>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php
}
else{}	?>
 <script>
 // jQuery(function(){
  // jQuery('.stf').keyup(  );
  // alert("typing!!");
// });.
    

			
			
			
			
			
			
			 // jQuery(function(){
    // jQuery.each(document.images, function(){
               // var this_image = this;
               // var src = jQuery(this_image).attr('src') || '' ;
			   // console.log(src);
               // if(!src.length > 0){
                   // //this_image.src = options.loading; // show loading
                   // var lsrc = jQuery(this_image).attr('src') || '' ;
                   // if(lsrc.length > 0){
                       // var img = new Image();
                       // img.src = lsrc;
                       // jQuery(img).load(function() {
                           // this_image.src = this.src;
						   // console.log("this is happening"+this_image.src);
                       // });
                   // }
               // }
			   // else{
				   
				   // console.log("else in");
			   // }
           // });
  // });
		jQuery(document).ready(function(){
            jQuery(".page-id-3717 .learn-more-btn.vas a").attr('href','http://www.konsult.com/specialities');
            console.log("Speciality Url Changed!");
            
        });	
 </script>
 <style>
 .searchinputmenu .sub-menu li li .right{
	 
	font-size: 12px;
color: rgba(0,0,0,.3) !important;
 }
 .searchinputmenu .sub-menu li li{
	 width: 100%;
 }
 .searchinputmenu .sub-menu li{
	 width: 100% !important;
 }
 .searchinputmenu .sub-menu{
 width: 100%;
 color: white;
 }
     #Footer .menu-secondary-menu-container ul, #Footer, .footer_copy {
background:#24cdd9 !important;
}
     #Top_bar{
         background:#24cdd9
     }
     #Top_bar ul li span {
    color: white;
   
}
     
     .searchinputmenu i {
         color: #e872a6 !important
     }
     .searchinputmenu input:focus {
    
    background: white !important;
}
     #Footer h4{
       color: white !important;  
     }
    
     #menu-footer-menu li a, #Footer .menu-secondary-menu-container ul li a  {
    
    color: white !important;
}
     .cprght {
 
    color: white;
}
     
     #Top_bar .menu > li > a span:not(.description) {
       font-weight: 400;
    font-size: 15px;
     }
     #Top_bar .sub-menu > li > a span {
         font-weight: 400;
     }
     .logo img {
    max-width: 100%;
}
     #Subheader a, #Subheader h1{color:black !important;}
     #Subheader {
    background-color: #f5f9fc;
     }
     #Subheader ul.breadcrumbs li {
         color: black !important;
     }
     #Subheader ul.breadcrumbs li a {
    color: black !important;
}
     @media only screen and (min-width: 768px)
     {
         .page-template-newhome #Top_bar ul li span{
         color:black;
     }
     .page-template-newhome #Top_bar{
         
         background:none ;
         box-shadow: 0px 0px 0px;
     }
.page-template-newhome #Top_bar #logo img {
    max-width: 150% !important;
    height: 150% !important;
    max-height:150%;
    display:none;
}
       .page-template-newhome #Top_bar .menu_wrapper {
    margin-top: 10px ;
}
     }
     @media only screen and (max-width: 768px)
     {
         
         #Footer .menu-secondary-menu-container ul, #Footer, .footer_copy {
background:#24cdd9 !important;
}
     #Top_bar{
         background:#24cdd9 !important;
     }
     }
 </style>
<script>
    jQuery(document).ready(function(){
       // alert("Updating");
        // jQuery(".page-template-newhome #Top_bar .logo .logo-sticky").attr('src','http://www.konsult.com/wp-content/uploads/2017/01/logo-01-1.png');
        
    });
 
</script>
<style>
    .mobile-menu-wrapper{
        -moz-transition: all 1s ease-out;  /* FF4+ */
    -o-transition: all 1s ease-out;  /* Opera 10.5+ */
    -webkit-transition: all 1s ease-out;  /* Saf3.2+, Chrome */
    -ms-transition: all 1s ease-out;  /* IE10 */
    transition: all 1s ease-out;  
            width: 100%;
    height: 100%;
    position: fixed;
    top: 0px;
    left: 0px;
    background: #24cdd9;
    overflow: scroll;
    z-index: 9999;
    }
    .mobile-menu-wrapper .logoinsidemw img{
        margin-top: 10px;
        margin-left: 10px;
    }
    .mobile-menu-wrapper .logoinsidemw{
            width: 50%;
    float: left;
    text-align: center;
   /* padding: 20px;*/
    }
    .mobile-menu-wrapper .crozzinsidemw i{
        cursor:pointer;
        margin-top: 10px;
        margin-left: 10px;
    }
    .mobile-menu-wrapper .crozzinsidemw{
        color: white;
    font-size: 40px;
    font-weight: 100;
    width: 50%;
    float: left;
    text-align: right;
    /*padding: 20px;*/
        
    }
    .mobile-menu-wrapper .menu-mw ul li a span{
            color: white !important;
    /*font-size: 16px !important;*/
    /*font-weight: 600 !important;*/
    }
    .mobile-menu-wrapper .menu-mw ul li{
            display: block;
    clear: both;
    text-align: left;
    /* width: 100%; */
  margin-top:15px;
    }
    .mobile-menu-wrapper .menu-mw ul{
        width: 80%;
    margin: 0 auto;
    }
    .mobile-menu-wrapper .menu-mw .searchinputmenu input{
    width: 360px;
    border-color: #e872a6;
    color: black;
}
   .mobile-menu-wrapper .menu-mw{
            width: 100%;
    display: block;
    margin: 6% auto;
    float: left;
    clear: both;
    text-align: center;
    }
    .mobile-menu-wrapper .sub-menu a span{
        
        font-weight: 200 !important;
        font-size: 14px !important;
        
    }
    .mobile-menu-wrapper .sub-menu{
        background:#24cdd9 !important;
        padding: 10px;
    }
    .mobile-menu-wrapper .searchinputmenu .sub-menu{
        display: none;
    }
    .icon_white{fill:white;}
</style>
<div class="mobile-menu-wrapper" style="margin-left:200%;">
<div class="logoinsidemw"><img class="logo-sticky scale-with-grid" src="http://www.konsult.com/wp-content/uploads/2017/01/logo-02.png" alt=""></div><div class="crozzinsidemw"><i class="" aria-hidden="true"><svg version="1.1" id="icon-close" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="37px" height="37px" viewBox="0 0 37 37" style="enable-background:new 0 0 37 37;" xml:space="preserve">
                        <polygon class="icon_white" points="26.7,11.6 25.4,10.3 18.5,17.2 11.6,10.3 10.3,11.6 17.2,18.5 10.3,25.4 11.6,26.7 18.5,19.8
                            25.4,26.7 26.7,25.4 19.8,18.5 "></polygon>
                        </svg></i></div>
    <div class="menu-mw">
    <ul>
        <?php
        mfn_wp_nav_menu();
        ?>
        </ul>
    </div>
</div>
<script>
jQuery(".mobile-menu-wrapper .fa.fa-search").click(function(){
   // var searchvar = jQuery(".headersearch").val();
    
    //window.location.href = "http://www.konsult.com/what-we-treat?searchq="+searchvar;
});
jQuery(".mobile-menu-wrapper .crozzinsidemw i").click(function(){
   /* jQuery(".mobile-menu-wrapper").animate({width: '1000'}, 0, function(){
    jQuery(this).hide();
});*/
    //jQuery(".mobile-menu-wrapper").animate({'width': 'toggle'});
    jQuery(".mobile-menu-wrapper").css("margin-left",'200%');
});
    jQuery(document).ready(function(){
        jQuery(".responsive-menu-toggle").click(function(e){
            e.preventDefault();
             e.stopPropagation(); 
           jQuery(".mobile-menu-wrapper").css("margin-left",'0px'); 
          // jQuery(".mobile-menu-wrapper").animate({'width': 'toggle'}); 
            jQuery("#Top_bar #menu").hide();
        });
    });
    
     jQuery(document).ready(function(){
        jQuery(".headersearch").on("change keyup paste", function(){
	
	if(jQuery(".headersearch").val() != ""){
		jQuery('.headersres').parent().show();
		load_contents(jQuery(".headersearch").val());
	}
	else{
		jQuery(".headersres").html("");
	}
	
    
});
 function load_contents(search_keyword){
	var data = {
		'action': 'what_we_do_ajax_request',
		'page': search_keyword,
		
	};
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	jQuery.post(ajaxurl, data, function(response) {
		
		
		if(response.trim().length == 0){
			jQuery(".headersres").html("<li><a href='#'><span class='left'>Sorry No Result Found!</span><span class='right'></span></a></li>");
		}
		jQuery(".headersres").html(response);
		//alert(response);
		
//alert(response);		
	});

			} 
         
         
         jQuery(".mobile-menu-wrapper .headersearch").on("change keyup paste", function(){
              //console.log("started typing in moile search bar ");
             if(jQuery(".mobile-menu-wrapper .headersearch").val() != ""){
		jQuery('.mobile-menu-wrapper .headersres').parent().show();
		//load_contents(jQuery(".headersearch").val());
                // console.log(jQuery(".mobile-menu-wrapper .headersearch").val());
                 load_contents_mobile(jQuery(".mobile-menu-wrapper .headersearch").val());
                 
	}
	else{
		jQuery(".headersres").html("");
	}
             
             
         });
         
         function load_contents_mobile(search_keyword){
	var data = {
		'action': 'what_we_do_ajax_request',
		'page': search_keyword,
		
	};
	var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
	//$('.animation_image').show(); //show loading image
	jQuery.post(ajaxurl, data, function(response) {
		
		
		if(response.trim().length == 0){
			jQuery(".headersres").html("<li><a href='#'><span class='left'>Sorry No Result Found!</span><span class='right'></span></a></li>");
		}
		jQuery(".mobile-menu-wrapper .headersres").html(response);
		//alert(response);
		
//alert(response);		
	});

			} 
     });
</script>
<style>
    .mobile-menu-wrapper .sub-menu{
        display:none;
    }
   .mobile-menu-wrapper .searchinputmenu .sub-menu{
        background: white !important;

    }
    .mobile-menu-wrapper .searchinputmenu .sub-menu span.left{
        color:black !important;
    }
</style>
</body>
</html>