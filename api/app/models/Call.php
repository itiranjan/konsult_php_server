<?php

class Call extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'calls';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'call_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;

    public function callDetailsList($params = array()) {

        $receiverid = $params['receiver_id'];

        $query = "select tsd.total_charges, tsd.doctor_charges, call_duration,comm_datetime,case receiver_id when $receiverid then 'I' Else 'O' End as bound,case receiver_id when $receiverid then (select user_id from users where user_id = caller_id) Else (select user_id from users where user_id = receiver_id) End as id, case receiver_id when $receiverid then (select CONCAT(salutation, ' ', name) from users where user_id = caller_id) Else (select CONCAT(salutation, ' ', name) from users where user_id = receiver_id) End as OtherParty, case receiver_id when $receiverid then (select photo from user_profile where user_id = caller_id) Else (select photo from user_profile where user_id = receiver_id) End as photo from calls  LEFT join spends AS tsd ON tsd.call_id = calls.call_id where receiver_id= $receiverid OR caller_id= $receiverid order by comm_datetime desc LIMIT 100";

        return DB::select($query);
    }

}
