<?php

class communicationHelper {

    protected $helper;

    public function __construct() {
        $this->helper = new Helper();
    }

    /**
     * @author : Shipra Agrawal
     * @created : 17 mar 2015
     * @description : This is handle push gcm communication
     * @access public
     * @param  array @param 
     * @return  array 
     */
    public function gcmCommunication($params) {

        //GCM PUSH for Android
        $message = $params['msg'];
        $registrationIDs = array($params['GCMDeviceIds']);
        $url = Config::get('constants.PUSH_GCMAPI_URL');
        $apiKey = Config::get('constants.PUSH_GCMAPI_APIKEY');
        $fields = array(
            'registration_ids' => $registrationIDs,
            'data' => array("message" => $message)
        );
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        $error_no = curl_errno($ch);
        if (curl_errno($ch)) {
            $msg = Config::get('config_msg.GCM_CURL_ERROR');
            $msg = str_replace("XX", curl_error($ch), $msg);
            $responseData = array('status' => 0, 'message' => $msg);
        } else {
            $responseData = array('status' => 1, 'message' => "GCM SENT");
        }
        curl_close($ch);
        return $responseData;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 11 may 2015
     * @description : This is handel push apns communication
     * @access public
     * @param  array @param 
     * @return  array 
     */
    public function pushApnsCommunication($params) {
        return array('status' => 0, 'message' => "APNS Msg not sent");
        //APNS PUSH for IPHONE
        if (empty($params['push_msg'])) {
            $message = '{"ymt":"this is test","pty":[{"x":23,"y":54},{"x":53,"y":14}]}';
        } else {
            $message = $params['push_msg'];
        }


        $device_token = $params['APNSDeviceIds'];
        //$device_token="c82919b6 ef307c8e 8318ae25 d43d9191 af12a8fa 82be191a 15334822 d8e89bc2";

        $badge = 6;
        $sound = 'default';
        $development = Config::get('constants.DEVELOPMENT');

        $payload = array();
        $payload['aps']['alert'] = $params['push_msg']['msg_text'];
        $payload['aps']['sound'] = $sound;

        foreach ($params['push_msg'] as $key => $value) {
            $payload[$key] = $value;
        }

        $apns_url = NULL;
        $apns_cert = NULL;
        $apns_port = Config::get('constants.PUSH_APNSAPI_POST');
        $base_path = public_path();
        if ($development) { //Development
            $apns_url = Config::get('constants.PUSH_APNSAPI_URL_SANDBOX');
            $apns_cert = $base_path . Config::get('constants.PUSH_APNSAPI_CERT_SANDBOX');
        } else {
            $apns_url = Config::get('constants.PUSH_APNSAPI_URL');
            $apns_cert = $base_path . Config::get('constants.PUSH_APNSAPI_CERT');
        }

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'apn.pem');
        // assume the private key passphase was removed.
        // stream_context_set_option($ctx, 'ssl', 'passphrase', $pass);
        $fp = stream_socket_client($apns_url, $err, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
        if (!$fp) {
            $status = "Failed to connect $err.''. $errstr";
            $status1 = 400;
        } else {
            $status = "Your Notification has sent Successfully";
            $status1 = 200;
        }

        $payload = json_encode($payload);
        $msg = chr(0) . pack("n", 32) . pack('H*', str_replace(' ', '', $device_token)) . pack("n", strlen($payload)) . $payload;

        fwrite($fp, $msg);
        fclose($fp);
        if (!$msg) { //APNS not work properly
            $responseData = array('status' => 0, 'message' => "APNS Msg not sent");
        } else {
            $responseData = array('status' => 1, 'message' => "APNS Msg sent");
        }

        return $responseData;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 1 June 2015
     * @description : This is handel  email communication
     * @access public
     * @param  array @param 
     * @return  array 
     */
    public function emailCommunication($data) {

        // SendGrid API - Mail Service
        $from_email = Config::get('constants.FROM_EMAIL');
        $from_name = Config::get('constants.FROM_NAME');
        $no_reply = Config::get('constants.NO_REPLY');

        $mailArray = array(
            'to' => array(
                '0' => array('email' => $data['email'])
            ),
            'from' => $from_email,
            'fromname' => $from_name,
            'subject' => $data['subject'],
            'html' => $data['body'],
            'text' => '',
            'replyto' => $no_reply  //,
        );
        $url = Config::get('constants.SENDGRID_URL');
        $parameter = Config::get('constants.SENDGRID_PARAMETER');
        $user = Config::get('constants.SENDGRID_API_USERNAME');
        $pass = Config::get('constants.SENDGRID_API_PASSWORD');

        $params = array();
        $params['api_user'] = $user;
        $params['api_key'] = $pass;
        $i = 0;
        $json_string = array();
        foreach ($mailArray['to'] as $to) {
            if ($i == 0) {
                $params['to'] = $to['email'];
                $params['toname'] = $to['name'];
                $json_string['to'][] = $to['email'];
            } else {
                $json_string['to'][] = $to['email'];
            }
            $i++;
        }


        $params['from'] = $mailArray['from'];

        if ($mailArray['fromname'] && $mailArray['fromname'] != '') {
            $params['fromname'] = $mailArray['fromname'];
        }

        $params['subject'] = $mailArray['subject'];

        if ($mailArray['html'] && $mailArray['html'] != '') {
            $params['html'] = $mailArray['html'];
        }

        if ($mailArray['text'] && $mailArray['text'] != '') {
            $params['text'] = $mailArray['text'];
        }

        if ($mailArray['replyto'] && $mailArray['replyto'] != '') {
            $params['replyto'] = $mailArray['replyto'];
        }

        if (isset($mailArray['files'])) {
            foreach ($mailArray['files'] as $file) {
                $params['files[' . $file['name'] . ']'] = '@' . $file['path'];
            }
        }

        $params['x-smtpapi'] = json_encode($json_string);
        $request = $url . 'api/mail.send.json';

        // Generate curl request
        $session = curl_init($request);

        // Tell curl to use HTTP POST
        curl_setopt($session, CURLOPT_POST, true);

        // Tell curl that this is the body of the POST
        curl_setopt($session, CURLOPT_POSTFIELDS, $params);

        // Tell curl not to return headers, but do return the response
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        //curl_setopt($session, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2);
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

        // obtain response
        $response = curl_exec($session);
        curl_close($session);

        return $response;
    }

    public function emailCommunication1($data) {

        // SendGrid API - Mail Service
        $from_email = Config::get('constants.FROM_EMAIL');
        $from_name = Config::get('constants.FROM_NAME');
        $no_reply = Config::get('constants.NO_REPLY');

        require 'sendgrid-php/vendor/autoload.php';
        require 'sendgrid-php/lib/SendGrid.php';

        $sendgrid = new SendGrid('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');
        $email1 = new SendGrid\Email();

        $email1
                ->addTo($data['email'])
                ->setFrom($from_email)
                ->setSubject($data['subject'])
                ->setText($data['body'])
                ->setHtml($data['body'])
                ->setReplyTo($no_reply)
        ;

        //$sendgrid->send($email1);
        try {

            $sendgrid->send($email1);
            $response = "Mail Sent";
        } catch (\SendGrid\Exception $e) {

            foreach ($e->getErrors() as $er) {
                $response = "Not Sent";
            }
        }

        return $response;
    }

    public function smsCommunication($params = array()) {

        $isIndianMobile = preg_match('/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/', $params['To']);
        
        if ($isIndianMobile) {

            $netcoreSmsUrl = Config::get('constants.NETCORE_SMS_URL');

            $smsParams = array();
            $smsParams['username'] = Config::get('constants.NETCORE_SMS_USERNAME');
            $smsParams['password'] = Config::get('constants.NETCORE_SMS_PASSWORD');
            $smsParams['feedid'] = Config::get('constants.NETCORE_SMS_FEEDID');
            $smsParams['Text'] = $params['Text'];
            $smsParams['To'] = $params['To'];

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $netcoreSmsUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($smsParams));
            $output = curl_exec($ch);
            curl_close($ch);

            return $output;
        } elseif (!empty($params['To'])) {

            $firstChar = substr($params['To'], 0, 1);
            if($firstChar != '+') {
                $params['To'] = '+'.$params['To'];
            }
            
            $twilioHelper = $this->helper->load('twilio');
            $response = $twilioHelper->sendSMS($params);

            return $response;
        }
    }

}
