<div id="window3">
    <form id="DetailP">

        <table width="100%">

            <tr>
                <td><div style="width:500px" id="patientDetails"/>&nbsp;&nbsp;
                </td>
            </tr>

        </table>
    </form>
</div>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

    <div id="clientsDb">
        <span style="font-size:20px">Review & Ratings<br/><br/></span>
        <div id="grid" ></div>
    </div>    

    <script>

        var window3 = $("#window3");
        $(document).ready(function() {

            var onClose = function()
            {
                mainGrid.data("kendoGrid").refresh();
            }

            window3.kendoWindow({
                width: "700px",
                visible: false,
                title: "Review & Ratings"
            });

            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                edit: function(e) {
                    
//                    var input = e.container.find(".k-input");
//                    input.blur(function(){
//                        column_value = input.val();
//                        updateDoctorProfileColumn(e.model.id, 'extension', column_value);
//                    });                       
                    
                    var inputArea = e.container.find("textarea");
                    inputArea.blur(function(){
                        column_value = inputArea.val();
                        updateRatingFollowup(e.model.rating_id, 'followup_comment', column_value);
                    });
                },                  
                toolbar:["excel"],
                selectable: "multiple cell",
                allowCopy: true,                    
                excel: {
                    allPages: true,
                    fileName: "review_ratings_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                    filterable: true
                },
                dataSource: {
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    transport: {
                        read: {
                            type: "POST",
                            url: base_url + "/index.php/communication/ratingList",
                            dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                        },
                        parameterMap: function (options) {
                            if (options.filter) {
                                KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                            }
                            return options;
                        },
                    },                    
                    schema: {
                        data: "list",
                        total: "total",
                        model: {
                            fields: {
                                rating_id: { type: "number", editable: false},
                                call_id: { type: "number", editable: false},
                                call_ratings$user_id: { type: "number", editable: false},
                                rating: {type:"string", editable: false},
                                comment: {type: "string", editable: false},
                                name: {type: "string", editable: false},
                                age: {type: "number", editable: false},
                                sex: {type: "string", editable: false},
                                call_ratings$created_at: {type: "date", editable: false},
                                followed_by: {type: "string", editable: false},
                                followup_comment: {type: "string" <?php echo (!empty($this->session->userdata('permissions')['communication_updateRatingFollowup_edit']) ? ",editable: true" : ",editable: false"); ?>},
                            }
                        }
                    },
                    pageSize: 100
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            eq: "Is equal to",
                            neq: "Is not equal to"
                        }
                    }
                },                     
                sortable: true,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                pageable: {
                    refresh: true,
                    pageSize: 100,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function(e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                columns: [
                    {field: "rating_id", title: "Id", width: '5px'},
                    {field: "call_ratings$user_id", title: "User Id", width: '5px'},
                    {field: "call_id", title: "Comm Id", width: '5px'},
                    {field: "rating", title: "Rating", width: '5px'},
                    {field: "name", title: "Name", width: '13px'},
                    {field: "comment", title: "Review", width: '20px'},
                    {field: "age", title: "Age (Yrs)", width: '5px'},
                    {field: "sex", title: "Sex", width: '4px'},
                    {field: "call_ratings$created_at", title: "Reviewed On", width: '10px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                    {field: "followed_by", title: "Followed By", width: '8px', filterable: true},    
                    {field: "followup_comment", title: "Follow-up", width: '15px', filterable: true, defaultValue: {}, editor: textEditorInitialize}                
                ],
                editable: true,
            }).data("kendoGrid");              

            mainGrid.thead.kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target;
                    return $(target).text();
                }                    
            });
        });                            

            var textEditorInitialize = function(container, options) {
                $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                .appendTo(container);
            };            
            
            function updateRatingFollowup(rating_id, column_name, column_value) {

                var getConfirmation = confirm("Are you sure that you want to update the column.");
                if(!getConfirmation) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    return;
                }

                $.ajax({

                    'url' : base_url + "/index.php/communication/updateRatingFollowup",
                    'type' : 'POST',
                    'data' : {
                        'rating_id' : rating_id,
                        'column_name' : column_name,
                        'column_value' : column_value
                    },
                    'success' : function(data) {  
                        alert("Changes has been done succesfully!!");
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    },
                    'error' : function(request,error)
                    {
                        alert("Whoops!! Something might be wrong. Please try again.");
                        //alert("Request: "+JSON.stringify(request));
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    }
                });                
            }            

            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function () {
                        alert(error);
                    }
                });
            }       
    </script>
</div>
</div>