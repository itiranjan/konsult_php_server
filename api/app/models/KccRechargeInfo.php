<?php

class KccRechargeInfo extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'kcc_recharge_info';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'recharge_info_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var boolean
     */
    public $timestamps = true;

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }
    
    public function isAvailedByUser($params = array()) {
        
        if($params['code_type'] == 1) {//SINGLE USE
            return $this->where('code_info_id', '=', $params['code_info_id'])->pluck('recharge_info_id');
        }
        elseif($params['code_type'] == 2) {//MULTI USE
            return $this->where('code_info_id', '=', $params['code_info_id'])->where('user_id', '=', $params['user_id'])->pluck('recharge_info_id');
        }
        
    }

}
