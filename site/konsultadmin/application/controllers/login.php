<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index() {
        $data['title'] = "Login";
        $this->load->helper(array('form'));
        $this->load->view('login', $data);
        //$this->load->view('login');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */