<?php
/**
 * Template Name: Sahil Sitemap 
 * Description: A Page Template To Display Sitemaps of doctors.
 * @author Sahil Ahlawat
 */
ini_set('display_errors', 0);
ini_set('display_startup_errors', 0);
//error_reporting(E_ALL);

						global $wp;
						global $wpdb;
						global $my_wpdb ;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						//$my_wpdb = new WPDB('root', '', 'Konsult_Prod', 'localhost');
						global $symptomData ;
						$symptomData = $my_wpdb->get_results("select * FROM symptom_master;", 0, 0);
						global $doctorData ;
						global $specializationData ;
						$specializationData = $my_wpdb->get_results("select * FROM specialization_master;", 0, 0);
						$doctorData = $my_wpdb->get_results("select * FROM doctor_profile WHERE is_approved = 1 ;", 0, 0);
header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>";

//echo '<xml>';
?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php
foreach ($doctorData as $dd) {
   ?>
  <url>
 
                              <loc><?php echo bloginfo('url'); ?>/doctor-profile/<?php echo $dd->page_url; ?></loc>
							  <changefreq>weekly</changefreq>
		                      <priority>0.9</priority>
								 
   </url>
   <?php
                                                      }
foreach ($specializationData as $sd) {
   ?>
  <url>
 
                              <loc><?php echo bloginfo('url'); ?>/speciality/<?php echo urlencode(str_replace(' ', '-', strtolower($sd->name)))?></loc>
							  <changefreq>weekly</changefreq>
		                      <priority>0.9</priority>
								 
   </url>
   <?php
                                                      }
													  foreach ($symptomData as $sdd) {
   ?>
  <url>
 
                              <loc><?php echo bloginfo('url'); ?>/symptom/<?php echo urlencode(str_replace(' ', '-', strtolower($sdd->name))); ?>-treatment</loc>
							  <changefreq>weekly</changefreq>
		                      <priority>0.9</priority>
								 
   </url>
   <?php
                                                      }
													  ?>
													  </urlset>
													  <?php





//echo '</xml>';

?>