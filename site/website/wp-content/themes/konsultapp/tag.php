<?php
get_header();
?>
<!-- #Content -->
<div id="Content">
	<div class="content_wrapper clearfix">
	
	<!-- .sections_group 
		<div class="sections_group">-->
			

<!--<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">-->
<div class="sections_group">
<div class="extra_content">
			
			<div class="blog_wrapper isotope_wrapper clearfix">
			<div class="posts_group lm_wrapper col-3 classic isotope">
			
			
			
			
			<div class="section" style="padding-top:0px; padding-bottom:0px; background-color:">
			<div class="section_wrapper clearfix">
			<div class="items_group clearfix">
			<div class="column one column_blog ">
			<div class="">
			<div id="Filters" class="isotope-filters" data-parent="column_filters">
<ul class="filters_buttons">
<li class="label">Filter by</li>
<li class="categories"><a class="openn" href="#"><i class="icon-docs"></i>Tags<i class="icon-down-dir"></i></a></li>
</ul>
<div class="filters_wrapper">
<ul class="categories">
<!--<li class="reset current-cat"><a class="all" data-rel="*" href="#">All</a></li>-->
<?php
// $categories = get_categories( array(
    // 'orderby' => 'name',
    // 'order'   => 'ASC'
// ) );
 
// foreach( $categories as $category ) {
	// //<a href="%1$s" alt="%2$s">%3$s</a>
    // $category_link = sprintf( 
        // '%3$s',
        // esc_url( get_category_link( $category->term_id ) ),
        // esc_attr( sprintf( __( 'View all posts in %s', 'textdomain' ), $category->name ) ),
        // esc_html( $category->name )
    // );
     
    // echo '<a href="'.get_category_link( $category->term_id).'" alt=""><li class="expert-opinion">' . sprintf( esc_html__( ' %s', 'textdomain' ), $category_link ) . '</li></a> ';
// } 

// if(get_the_tag_list()) {
    // echo get_the_tag_list('<ul><li>','</li><li>','</li></ul>');
// }
$tags = get_tags();
$html = '';
foreach ( $tags as $tag ) {
	$tag_link = get_tag_link( $tag->term_id );
			
	$html .= "<li class='oclicktag'><a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug}'>";
	$html .= "{$tag->name}</a></li>";
}
$html .= '';
echo $html;
?>


<!--<a href="http://www.konsultapp.com/category/expert-opinion/">Expert Opinion</a>
</li>
<li class="health-tips">
<a href="http://www.konsultapp.com/category/health-tips/">Health Tips</a>
</li>-->
<li class="reset close"><a href="#"><i class="icon-cancel"></i></a></li>
</ul>
</div>
</div>
		<?php if ( have_posts() ) : ?>
			<!--<header class="page-header"> -->
				<?php
					//the_archive_title( '<h1 class="page-title">', '</h1>' );
					//the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			<!--</header> .page-header -->

			<?php
			// Start the Loop.
			while ( have_posts() ) : the_post();
?>
<div class="post-item isotope-item clearfix author-admin <?php echo "post-".get_the_ID(); ?> post type-post status-publish format-standard has-post-thumbnail category-health-tips tag-adults tag-and-calcitonin tag-calcium tag-caucasian-women tag-disorder tag-estrogens tag-osteoporosis tag-raloxifene tag-steroids tag-stomach-disorder tag-teriparatide tag-vitamin-d tag-women">
<div class="date_label"><?php echo get_the_date(); ?></div>
<div class="image_frame post-photo-wrapper scale-with-grid">
<div class="image_wrapper">
<?php if ( has_post_thumbnail() ) : ?>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
	<div class="mask">
</div>
	<img src="<?php the_post_thumbnail_url(); ?>" class="scale-with-grid wp-post-image" sizes="(max-width: 560px) 100vw, 560px" data-square="75864" height="397" width="560" />
	</a>
	<div class="image_links double"><a href="<?php the_post_thumbnail_url(); ?>" class="zoom" rel="prettyphoto">
<i class="icon-search">
</i>
</a>
<a href="<?php the_permalink(); ?>" class="link">
<i class="icon-link">
</i>
</a>
</div>
<?php endif; ?>

<!--<img src="http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1.png" class="scale-with-grid wp-post-image" alt="Osteoporosis" itemprop="image" srcset="http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1.png 560w, http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1-300x213.png 300w, http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1-206x146.png 206w, http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1-50x35.png 50w, http://www.konsultapp.com/wp-content/uploads/2017/03/SUI-1-106x75.png 106w" sizes="(max-width: 560px) 100vw, 560px" data-square="75864" height="397" width="560">
-->

</div>
</div>
<div class="post-desc-wrapper"><div class="post-desc">
<div class="post-head">
<div class="post-meta clearfix">
<!--<div class="author-date">
<span class="vcard author post-author"><span class="label">Published by </span>
<i class="icon-user"></i>
 <span class="fn">Konsult App <?php //echo " : ".get_the_author(); ?></span>
 </span>
 <span class="date"><span class="label">at </span>
 <i class="icon-clock"></i> <span class="post-date updated"><?php echo get_the_date(); ?></span>
 </span>
 </div>-->
 <div class="category">
 <span class="cat-btn">
 Categories <i class="icon-down-dir"></i>
 </span>
 <div class="cat-wrapper">
<!-- <ul class="post-categories">
	<li>
	<a href="http://www.konsultapp.com/category/health-tips/" rel="category tag">Health Tips</a>
	</li>
	</ul>-->
	</div>
	</div>
	</div>
	</div>
	<div class="post-title"><h2 class="entry-title" itemprop="headline">
	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</h2>
	<div class="author-date">
<span class="vcard author post-author"><span class="label">Published by </span>
<i class="icon-user"></i>
 <span class="fn">Konsult App <?php //echo " : ".get_the_author(); ?></span>
 </span>
 <span class="date"><span class="label">at </span>
 <i class="fa fa-calendar"></i> <span class="post-date updated"><?php echo get_the_date(); ?></span>
 </span>
 </div>
	</div>
	<div class="post-excerpt"><?php echo get_the_excerpt(); ?><a href="<?php the_permalink(); ?>" class="post-more-for-excrpt">Read more</a></div>
	<!--<div class="post-footer">
	<div class="button-love">
	<span class="love-text">Do you like it?</span>
	<a href="#" class="mfn-love " data-id="4013">
	<span class="icons-wrapper">
	<i class="icon-heart-empty-fa"></i>
	<i class="icon-heart-fa"></i></span>
	<span class="label">0</span>
	</a></div><div class="post-links">
	<i class="icon-comment-empty-fa"></i> <a href="<?php //the_permalink(); ?>#respond" class="post-comments"><?php //echo  get_comments_number(get_the_ID()); ?> </a>
	<i class="icon-doc-text"></i>
	<a href="<?php //the_permalink(); ?>" class="post-more">Read more</a>
	</div>
	</div>-->
	</div>
	</div>
	</div>


<?php
				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				//get_template_part( 'content', get_post_format() );

			// End the loop.
			endwhile;
?>
<div class="column one pager_wrapper"><div class="pager"><div class="pages">

<?php
			// Previous/next page navigation.
			the_posts_pagination( array(
			'mid_size'           => 15,
				'prev_text'          => __( 'Back', 'twentyfifteen' ),
				'next_text'          => __( 'Next', 'twentyfifteen' ),
			) );
?>
</div>
</div>
</div>
<style>
h2.screen-reader-text{
	display:none;
}
.filters_wrapper ul.categories a {
    color: rgba(0,0,0,.7);
}
.author-date{
		width:100%;
		font-size: 11px;
		}
.author-date .vcard, .author-date .date {
    width: 50%;
    float: left; 
}
.author-date .date
{
	text-align: right;
}
</style>
<?php
		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'content', 'none' );

		endif;
		?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
		<!--</main> .site-main -->
	<!--</section> .content-area -->
	<!-- .four-columns - sidebar 
	</div>-->
		<?php get_sidebar( 'blog' ); ?>

	</div>
</div>
<script>
jQuery(document).ready(function() {
    jQuery(".oclicktag a").click(function() {
        //Do stuff when clicked
		//alert(jQuery(this).attr('href'));
		var tored = jQuery(this).attr('href');
		window.location.href = tored;
    });
});
</script>
<?php
get_footer();

?>