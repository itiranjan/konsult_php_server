<?php

class OsVersion extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'os_version';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'ID';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = false;

    public function findosVersion($param) {

        $details = DB::table('os_version')->where('OSVersion', '=', $param['OSVersion'])->get();

        $detailsArray = array();
        if ($details[0]->CurrentVersion <= $param['AppVersion']) {
            $upgradeRequirement = 0;
            $msg = "do Nothing";
        } else if ($details[0]->ForceUpdateMinVersion > $param['AppVersion']) {
            $upgradeRequirement = 2;
            $msg = $details[0]->ForceUpdateMessage;
        } else if ($details[0]->ForceUpdateMinVersion <= $param['AppVersion'] && $details[0]->CurrentVersion >= $param['AppVersion']) {
            $upgradeRequirement = 1;
            $msg = $details[0]->SuggestUpdateMessage;
        } else {
            $upgradeRequirement = 400;
            $msg = 'You are Passing wrong App Version.';
        }
        $detailsArray['upgradeRequirement'] = $upgradeRequirement;
        $detailsArray['message'] = $msg;

        return $detailsArray;
    }

}
