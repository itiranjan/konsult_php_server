<?php

class citrusHelper {

    protected $helper;

    public function __construct() {
        $this->helper = new Helper();
    }

    public function fetchMVCBalance($params = array()) {

        $emailId = $params['emailId'];
        $mobile = '';

        $httpHelper = $this->helper->load('http');

        $MerchantID = Config::get('constants.Merchant_Id');
        $PartnerID = Config::get('constants.Parterner_Id');
        $Password = Config::get('constants.PassWord');
        $mvcurl = Config::get('constants.CITRUS_VIRTUAL_URL') . Config::get('constants.Get_Balance');
        $headerValue = Config::get('constants.Content_Type');
        $data = '{"MerchantID": "' . $MerchantID . '", "CampaignCode": "KONSULTCASH", "Email": "' . $emailId . '", "Mobile": "' . $mobile . '", "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';
        $user_citrus_mvc = $httpHelper->httpPostWithHeader($mvcurl, array('Content-Type:' . $headerValue), $post = 1, $data);
        $user_citrus_mvc = json_decode($user_citrus_mvc, true);

        if (empty($user_citrus_mvc['d'])) {
            $user_citrus_mvc['errorMsg'] = 'Unable to fetch MVC balance.';
        }

        $user_citrus_mvc['mvcAmount'] = $user_citrus_mvc['d']['Campaigns'][0]['Amount'];

        return $user_citrus_mvc;
    }

    public function fetchRealBalance($params = array()) {

        $citrusToken = $params['citrusToken'];
        $httpHelper = $this->helper->load('http');

        $user_token = Config::get('constants.CITRUS_AUTHTOKEN_PREFIX') . $citrusToken;
        $url = Config::get('constants.CITRUS_URL') . Config::get('constants.CITRUS_BALANCE');
        $user_citrus = $httpHelper->httpGetWithHeader($url, array('Authorization:' . $user_token));
        $user_citrus = json_decode($user_citrus, true);

        if (!empty($user_citrus['error'])) {
            $user_citrus['errorMsg'] = 'Unable to fetch main balance.';
        }

        return $user_citrus;
    }

    public function debitRealMoney($params = array()) {

        $citrusdata = array();
        $citrusdata['merchantAccessKey'] = Config::get('constants.CITRUS_ACCESS_KEY');
        $citrusdata['signature'] = Config::get('constants.CITRUS_SECRET_KEY');
        $citrusdata['merchantTransactionId'] = Config::get('constants.CITRUS_PREFIX') . $params['transactionId'];
        $citrusdata['currency'] = !empty($citrusdata['currency']) ? $citrusdata['currency'] : 'INR';
        $citrusdata['amount'] = $params['amount'];
        if (!empty($params['comment'])) {
            $citrusdata['comment'] = $params['comment'];
        }

        $httpHelper = $this->helper->load('http');
        $user_token = UserWallet::where('user_id', '=', $params['user_id'])->pluck('token');
        $user_token = Config::get('constants.CITRUS_AUTHTOKEN_PREFIX') . $user_token;
        $url = Config::get('constants.CITRUS_URL') . Config::get('constants.CITRUS_PAY');
        $user_citrus_json = $httpHelper->httpGetWithHeader($url, array('Authorization:' . $user_token), 1, $citrusdata);
        $user_citrus_json = json_decode($user_citrus_json, true);

        return $user_citrus_json;
    }

    public function redeemVirtualMoney($params = array()) {
        $mvcurl = Config::get('constants.CITRUS_VIRTUAL_URL') . Config::get('constants.Redeem_Virtual_Currency');
        $headerValue = Config::get('constants.Content_Type');

        $MerchantID = Config::get('constants.Merchant_Id');
        $CampaignCode = Config::get('constants.Compaign_Code');
        $PartnerID = Config::get('constants.Parterner_Id');
        $Password = Config::get('constants.PassWord');
        $Amount = $params['amount'];

        $Mode = 'Expiring First';
        $TransReferenceID = $params['transactionId'];
        $emailID = $params['emailId'];
        $mobile = '';

        $data2 = '{"MerchantID": "' . $MerchantID . '", "CampaignCode": "' . $CampaignCode . '", "Email": "' . $emailID . '", "Mobile": "' . $mobile . '","Amount": "' . $Amount . '","Mode": "' . $Mode . '","TransReferenceID": "' . $TransReferenceID . '","PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';

        $requestData = array();
        $requestData['MerchantID'] = $MerchantID;
        $requestData['CampaignCode'] = $CampaignCode;
        $requestData['Email'] = $emailID;
        $requestData['Mobile'] = $mobile;
        $requestData['Amount'] = $Amount;
        $requestData['Mode'] = $Mode;
        $requestData['TransReferenceID'] = $TransReferenceID;
        $requestData['PartnerID'] = $PartnerID;
        $requestData['Password'] = $Password;

        $httpHelper = $this->helper->load('http');
        $user_citrus_mvc = $httpHelper->httpPostWithHeader($mvcurl, array('Content-Type:' . $headerValue), $post = 1, $data2);
        $user_citrus_mvc = json_decode($user_citrus_mvc, true);

        $user_citrus_mvc['requestData'] = $requestData;

        return $user_citrus_mvc;
    }

    public function pushMoney($params = array()) {

        $citrusdata_doc = array();
        $citrusdata_doc['currency'] = !empty($citrusdata_doc['currency']) ? $citrusdata_doc['currency'] : 'INR';
        $citrusdata_doc['to'] = trim($params['to']);
        $citrusdata_doc['amount'] = $params['amount'];
        if (!empty($params['message'])) {
            $citrusdata_doc['message'] = $params['message'];
        }

        $httpHelper = $this->helper->load('http');
        $konsult_token = Config::get('constants.CITRUS_AUTHTOKEN_PREFIX') . Config::get('constants.KONSULT_AUTHTOKEN');
        $url = Config::get('constants.CITRUS_URL') . Config::get('constants.CITRUS_TRANSFER');
        $doc_citrus_json = $httpHelper->httpGetWithHeader($url, array('Authorization:' . $konsult_token), 1, $citrusdata_doc);
        $doc_citrus_json = json_decode($doc_citrus_json, true);

        return $doc_citrus_json;
    }

    public function issueVirtualCurrency($params = array()) {
        
        $httpHelper = $this->helper->load('http');
        $mvcurl = Config::get('constants.CITRUS_VIRTUAL_URL') . Config::get('constants.Issue_Virtual_Currency');
        $headerValue = Config::get('constants.Content_Type');
        $CampaignCode = Config::get('constants.Compaign_Code');
        $PartnerID = Config::get('constants.Parterner_Id');
        $Password = Config::get('constants.PassWord');
        
        $amount = $params['amount'];
        $emailId = $params['emailId'];
        
        $data = '{ "CampaignCode": "' . $CampaignCode . '", "UserList": [ { "Email": "' . $emailId . '", "Mobile": "", "Amount": "' . $amount . '", "Type": "F" } ], "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';

        $user_issue_mvc = $httpHelper->httpPostWithHeader($mvcurl, array('Content-Type:' . $headerValue), $post = 1, $data);
        $user_issue_mvc = json_decode($user_issue_mvc, true);
        
        return $user_issue_mvc;
    }

}
