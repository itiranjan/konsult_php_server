<?php

class helper {

    private $helpers = array();

    /**
     * @author : Parth
     * @created : 27 jan 2015
     * @description : This function is used for load helper class in page 
     * @param string [optional] $helper [name of helper class to be loaded] 
     * @param string [optional] $path [path of helper class]
     * @access public
     * @return  array 
     */
    public function load($helper = '', $path = '/') {
        $folder = app_path() . '/helpers' . $path;
        $filename = $folder . $helper . 'Helper.php';
        if (file_exists($filename)) {
            require_once $filename;
            $classname = $helper . 'helper';
            $hlpobj = new $classname();
            return $hlpobj;
        } else {
            return false;
        }
    }

}
