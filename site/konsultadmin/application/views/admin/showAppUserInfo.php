<div id="userDetails"></div>
<div align="center">

    <?php if ($searchedDocs): ?>
        <br/><br/>
        <h2>Available Specialist Doctors</h2>
        <br/>
        <?php if (!empty($specializationDocs)): ?>
            <table>
                <colgroup>
                    <col span="20" style="background-color:greenyellow">
                </colgroup>
                <tr>
                    <th>Name</th>
                    <th>Per Min Charges</th>
                    <th>Extension</th>
                    <th>Practices At</th>
                    <th>Address</th>
                    <th>Action</th>                    
                </tr>

                <?php foreach ($specializationDocs as $specializationDoc): ?>  
                    <tr>
                        <td><?php echo $specializationDoc->name; ?></td>
                        <td><?php echo $specializationDoc->per_min_charges; ?></td>
                        <td><?php echo $specializationDoc->extension; ?></td>
                        <td><?php echo (!empty($specializationDoc->hospital) ? $specializationDoc->hospital : 'Not Given') ?></td>
                        <td><?php echo (!empty($specializationDoc->address) ? $specializationDoc->address : 'Not Given') ?></td>
                        <td><form name="doc_connect" id="doc_connect" action="doc_connect" method="post" class="doc_connect">
                                <input type="button" name="button_doc_connect" id="button_doc_connect_<?php echo $specializationDoc->user_id; ?>" value="Connect" onclick="connect_doc(<?php echo $userDetails->user_id; ?>, <?php echo $specializationDoc->user_id; ?>, '<?php echo $userDetails->auth_token; ?>')" />
                            </form></td> 
                        <td><form name="callback_request" id="callback_request" action="callback_request" method="post">
                                <input type="button" name="button_callback_request" id="button_callback_request_<?php echo $specializationDoc->user_id; ?>" value="Take CallBack" onclick="callback_request1(<?php echo $userDetails->user_id; ?>, <?php echo $specializationDoc->user_id; ?>, '<?php echo $userDetails->auth_token; ?>')" />
                            </form></td>                         
                    </tr>
                <?php endforeach; ?>

            </table>
        <?php else: ?>
            Sorry, no doctor available right now for this specialization!
        <?php endif; ?>
    <?php endif; ?>

    <br/><br/>    
    <h2>User Details</h2>
    <br/>
    <?php if (!empty($userDetails)): ?>
        <table>
            <colgroup>
                <col span="20" style="background-color:greenyellow">
            </colgroup>
            <tr>
                <?php foreach ($userDetails as $key => $userDetail): ?>  
                    <th><?php if ($key == 'auth_token') continue;
            echo ucfirst($key); ?></th>
    <?php endforeach; ?>
            </tr>
            <tr>
                <?php foreach ($userDetails as $key => $userDetail): ?>  
                    <td><?php if ($key == 'auth_token') continue;
                    echo $userDetail; ?></td>
        <?php endforeach; ?>
            </tr>
        </table>
    <?php else: ?>
        No user details found!
<?php endif; ?>

    <br/><br/>
    <h2>Call Records</h2>
    <br/>
<?php if (!empty($callDetails)): ?>
        <table>
            <colgroup>
                <col span="20" style="background-color:greenyellow">
            </colgroup>

                <?php foreach ($callDetails as $callDetail): ?>
                <tr>
                    <?php foreach ($callDetail as $key => $value): ?>
                        <th><?php echo ucfirst($key); ?></th>
                        <?php
                    endforeach;
                    break;
                    ?>
                </tr>
            <?php endforeach; ?>

                <?php foreach ($callDetails as $key => $callDetail): ?>
                <tr>
                    <?php foreach ($callDetail as $value): ?>
                        <td><?php echo $value; ?></td>
                <?php endforeach; ?>
                </tr>
        <?php endforeach; ?>

        </table>
    <?php else: ?>
        No call records found!
<?php endif; ?>

    <br/><br/><br/><br/><br/><br/><br/><br/>
</div>