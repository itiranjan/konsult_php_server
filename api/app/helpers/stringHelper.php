<?php

class stringHelper {

    /**
     * @author : Shipra Agrawal
     * @created : 28 jan 2015
     * @description : Generate Random number for password
     * @param int length
     * @access public
     * @return  array 
     */
    public function random_string($length) {

        // Move it to the common_helper
        $key = '';
        $keys = range(0, 9);
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 28 jan 2015
     * @description : Generate Random number for Coupon
     * @param int $length
     * @param int $strength
     * @access public
     * @return  array 
     */
    public function generateExtracode($length, $strength) {
        $vowels = 'aeuy';
        $consonants = 'bdghjmnpqrstvz';
        if ($strength & 1) {
            $consonants .= 'BDGHJLMNPQRSTVWXZ';
        }
        if ($strength & 2) {
            $vowels .= "AEUY";
        }
        if ($strength & 4) {
            $consonants .= '23456789';
        }
        if ($strength & 8) {
            $consonants .= '@#$';
        }
        $extraCodes = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $extraCodes .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $extraCodes .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }

        return $extraCodes;
    }

}
