<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="http://konsultapp.com/css/style-konsult.css"
	rel="stylesheet" media="screen">
<script
	src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>
input {
	font-size: 14px;
	min-height: 0px;
	width: 100%;
	border-radius: 0px;
	line-height: 20px;
	padding: 7px 7px 7px;
	border: none;
	margin-bottom: 10px;
	background-color: #fff /**#e9f0f2**/;
	-webkit-transition: background-color 0.2s;
	transition: background-color 0.2s;
	border: 1px #CCC solid;
	font-weight: 200;
}

td span {
	font-size: 14px;
	font-weight: 200;
}

.heading_h {
	color: #666;
	font-size: 24px;
	margin-top: 0;
	padding-bottom: 40px;
	padding-top: 20px;
	color: #0ab2f1;
	float: left;
}

.pic_s {
	border-radius: 100px;
	float: right;
}

.f_div {
	float: left;
	width: 150px;
}

.s_div {
	padding: 5px 10px 5px 10px;
	border: 1px #999 solid;
	float: left;
	margin-right: 50px;
}

.t_div {
	opacity: 0.5;
	padding: 5px 10px 5px 10px;
	border: 1px #999 solid;
	float: left;
}

.f_submit {
	width: 100%;
	background: #0ab2f1;
	color: #FFF;
	font-size: 14px;
}
</style>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script type="text/javascript" language="javascript">
	//var MIN_LENGTH = 3;
	$(document).ready(function() {
		//$("#btn_qulification").keyup(function() {
		$("#btn_qulification").click(function() {
			//var keyword = $("#specialization").val();
			var keyword ="ab";
			//if (keyword.length >= MIN_LENGTH) {
				
				 //$.get( "http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/autospecialization", { keyword: keyword } )
				 $.get( "http://admin.konsultapp.com/index.php/verifylogin/autospecialization", { keyword: keyword } )
				  .done(function( data ) {
				    console.log(data);
				    $('#degree1').html(data);
				    $('#degree2').html(data);
				  });
		});

	});
	
	</script>
<script type="text/javascript" language="javascript">
	$(document).ready(function() {
		//$("#btn_qulification").keyup(function() {
		$("#btn_useredit").click(function() {
			//var keyword = $("#specialization").val();
			var email = document.getElementsByName("email")[0].value;
         	var mob = document.getElementsByName("mobile")[0].value;
         	if(email !="" && mob !=""){
				 //$.get( "http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/getDoctorEditDataforweb", { email: email,mob:mob } )
				 $.get( "http://admin.konsultapp.com/index.php/verifylogin/getDoctorEditDataforweb", { email: email,mob:mob } )
				  .done(function( data ) {
				    console.log(data);

				    var obj = jQuery.parseJSON(data);

				   if(obj.email !="not" && obj.mobile !="diff"){
                        $('#salutation').val(obj.salutation);
                        $('#name').val(obj.name);
				    	$('#email').val(obj.email);
				    	$('#mobile').val(obj.mobile);
				    	$('#city').val(obj.city_name);
				   		$('#address').val(obj.info);
				    	$('#experience').val(obj.total_experience);
				    	$('#specialization').val(obj.specialization_name);
				    	$('#mregistration').val(obj.medical_registration_no);
				    	$('#otherinfo').val(obj.other_info);
				    	$('#pminute').val(obj.per_min_charges);
				    	//$('#allow').val(obj.allow_new_user);
				    	$(".allow").prop("checked", true)
				    	$('#area').val(obj.area);
				    	$('#editid').val("modify");
				    	$("#email").prop("readonly", true);
				    	$("#mobile").prop("readonly", true);
				    for (var i in obj.qualification) {
						if(i==0){
							$('#qualid').val(obj.qualification[0].qualification_id);
					    	$('#college').val(obj.qualification[0].university);
					    	$('#degree').val(obj.qualification[0].degree);
						}
						if(i==1){
							var qualification="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid1' id='qualid1'/><span class='loginLbl'>College</span><input list='college1' name='college1' id='college1'><datalist id='college1'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree1' id='degree1'><datalist id='degree2'><option value='Others'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear1' name='syear1' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear1' name='eyear1' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing1' name='curentpersuing1'><datalist id='curentpersuing1'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
							$('#quali0').html(qualification);
							$('#qualid1').val(obj.qualification[1].qualification_id);
							$('#college1').val(obj.qualification[1].university);
							$('#degree1').val(obj.qualification[1].degree);
							q=1;
						}
						if(i==2){
							var qualification1="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid2' id='qualid2'/><span class='loginLbl'>College</span><input list='college2' name='college2' id='college2'><datalist id='college2'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree2' id='degree2' class='deg2'><datalist id='degree2'><option value='Others'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear2' name='syear2' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear2' name='eyear2' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing2' name='curentpersuing2'><datalist id='curentpersuing2'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
							$('#quali1').html(qualification1);
							$('#qualid2').val(obj.qualification[2].qualification_id);
							$('#college2').val(obj.qualification[2].university);
							$('.deg2').val(obj.qualification[2].degree);
							q=2;
						}
				    }
				    	$('#hospri').val(obj.experience[0].hospital);
				    	$('#designation').val(obj.experience[0].designation);
				    	$('#hospitalid').val(obj.experience[0].workplace_id);
				    	$('#sunstarttime').val(obj.availabilty[0].start_time);
				    	$('#sunendtime').val(obj.availabilty[0].end_time);
				    	$('#monstarttime').val(obj.availabilty[1].start_time);
				    	$('#monendtime').val(obj.availabilty[1].end_time);
				    	$('#tuestarttime').val(obj.availabilty[2].start_time);
				    	$('#tueendtime').val(obj.availabilty[2].end_time);
				    	$('#wedstarttime').val(obj.availabilty[3].start_time);
				    	$('#wedendtime').val(obj.availabilty[3].end_time);
				    	$('#thrustarttime').val(obj.availabilty[4].start_time);
				    	$('#thruendtime').val(obj.availabilty[4].end_time);
				    	$('#fristarttime').val(obj.availabilty[5].start_time);
				    	$('#friendtime').val(obj.availabilty[5].end_time);
				    	$('#satstarttime').val(obj.availabilty[6].start_time);
				    	$('#satendtime').val(obj.availabilty[6].end_time);
				   }else if(obj.mobile=="diff"){
					   alert("This user is not registered as doctor.Please apply for registration");
				   }else{
					   alert("You are not entering valid Email and Mobile Number");
				   }
				  });
         	}else{
             	alert("Please enter Valid Email and Mobile No:-");
         	}
		});

	});
	
	</script>
<script type="text/javascript" language="javascript">
		
		var j=0;
		var q=0;
		
         $(document).ready(function() {
        	//$("#mainqualification").hide();
         	//$("#mainworkex").hide();
       		$("#btn_qulification").click(function(event){
    			
       			var qualification="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid1' id='qualid1'/><span class='loginLbl'>College</span><input list='college1' name='college1' id='college1'><datalist id='college1'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree1' id='degree1'><datalist id='degree2'><option value='Others'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear1' name='syear1' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear1' name='eyear1' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing1' name='curentpersuing1'><datalist id='curentpersuing1'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
       			var qualification1="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Qualification Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><input type='hidden' name='qualid2' id='qualid2'/><span class='loginLbl'>College</span><input list='college2' name='college2' id='college2'><datalist id='college2'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Degree*</span><input list='degree2' name='degree2' id='degree2'><datalist id='degree2'><option value='MBBS'><option value='BDS'><option value='Fellowship in critical care'><option value='MD'><option value='MS'></datalist></td></tr><!--<tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='syear2' name='syear2' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='eyear2' name='eyear2' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently pursuing<small>(Yes/No)</small></span><input list='curentpersuing2' name='curentpersuing2'><datalist id='curentpersuing2'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr>--></table>";
           		//$("#mainqualification").show();
           		
           	if(q==0){	
           	 	$('#quali'+q).html(qualification);
              	 q++;
           	}else if(q==1){
           	 	$('#quali'+q).html(qualification1);
              	 q++;
            }
           		//document.getElementById('quali').innerHTML = qualification;
            });

			$("#btn_experience").click(function(event){
    			
       			var workexperience="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Work Experience Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Hospital or 'Private Practice'</span><input list='hospri1' name='hospri1'><datalist id='hospri1'><option value='A J INSTITUTE OF MEDICAL SCIENCE'><option value='A N MAGADH MEDICAL COLLEGE AND HOSPITAL'><option value='AARVY HOSPITAL'><option value='ADARSH NURSING HOME'><option value='ADITYA BIRLA MEMORIAL HOSPITAL'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Designation</span><input list='designation1' name='designation1'><datalist id='designation1'><option value='ASSOCIATE'><option value='ASSOCIATE CONSULTANT'><option value='CLINICAL ASSOCIATE'><option value='CHIEF MEDICAL OFFICER'><option value='CHIEF SURGEON'></datalist></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='worksyear1' name='worksyear1' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='workeyear1' name='workeyear1' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently working <small>(Yes/No)</small></span><input list='curentworking1' name='curentworking1'><datalist id='curentworking1'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr></table>";
       			var workexperience1="<table width='100%' border='0' cellspacing='0' cellpadding='0'><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;width:50%;'><span class='loginLbl'><strong></>Work Experience Details</strong></span></td><td style='padding-left:5%;width:50%;'></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Hospital or 'Private Practice'</span><input list='hospri2' name='hospri2'><datalist id='hospri2'><option value='A J INSTITUTE OF MEDICAL SCIENCE'><option value='A N MAGADH MEDICAL COLLEGE AND HOSPITAL'><option value='AARVY HOSPITAL'><option value='ADARSH NURSING HOME'><option value='ADITYA BIRLA MEMORIAL HOSPITAL'></datalist></td><td style='padding-left:5%;'><span class='loginLbl'>Designation</span><input list='designation2' name='designation2'><datalist id='designation2'<option value='ASSOCIATE'><option value='ASSOCIATE CONSULTANT'><option value='CLINICAL ASSOCIATE'><option value='CHIEF MEDICAL OFFICER'><option value='CHIEF SURGEON'></datalist></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Start Year</span><input type='date' id='worksyear2' name='worksyear2' class='mediuminput'  value=''/></td><td style='padding-left:5%;'><span class='loginLbl'>End Year</span><input type='date' id='workeyear2' name='workeyear2' class='mediuminput'  value=''/></td></tr><tr><td>&nbsp;</td></tr><tr><td style='padding-right:5%;'><span class='loginLbl'>Currently working <small>(Yes/No)</small></span><input list='curentworking2' name='curentworking2'><datalist id='curentworking2'><option value='Yes'><option value='No'></datalist></td><td style='padding-left:5%;'></td></tr></table>";
           		//$("#mainqualification").show();
           	
         	if(j==0){	
         		 $('#workex'+j).html(workexperience);
         		j++;
           	}else if(j==1){
           	 	 $('#workex'+j).html(workexperience1);
           	 	j++;
            }
           	 
           		//document.getElementById('quali').innerHTML = qualification;
              
            });
            
         	$("#btn_userValidate").click(function(event){
    			var email = document.getElementsByName("email")[0].value;
            	var mob = document.getElementsByName("mobile")[0].value;
               $.get( 
                  "http://admin.konsultapp.com/index.php/verifylogin/validateUser",
                  //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/validateUser",
                  {email: email,mobile: mob},
                  function(data) {
                   //alert(data);
                  $('#editid').val("Changes");
                  $('#txtHint').html(data);
                  }
               );
            });
       
         	 $("#btn_login").click(function(event){
             	            var allowuser="No";
                            var salutation = document.getElementsByName("salutation")[0].value;
                            var name = document.getElementsByName("name")[0].value;
         	    			var email = document.getElementsByName("email")[0].value;
         	            	var mob = document.getElementsByName("mobile")[0].value;
         	    			var city = document.getElementsByName("city")[0].value;
         	    			var address = document.getElementsByName("address")[0].value;
         	    			var experience = document.getElementsByName("experience")[0].value;
         	    			var specialization = document.getElementsByName("specialization")[0].value;

         	    			var mregistration = document.getElementsByName("mregistration")[0].value;
         	    			var sregistration="";
         	    			var pminute = document.getElementsByName("pminute")[0].value;
         	    			if(pminute==""){
         	    				pminute=0.00;
         	    			}

         	    			var allowuser = document.querySelector('#allowuser:checked').value;
         	    			var college = document.getElementsByName("college")[0].value;
         	    			var degree = document.getElementsByName("degree")[0].value;
         	    			var qualid = document.getElementsByName("qualid")[0].value;
         	    			var qsyear="";
         	    			var qeyear="";
         	    			var whospri = document.getElementsByName("hospri")[0].value;
         	    			var designation = document.getElementsByName("designation")[0].value;
         	    			var worksyear="";
         	    			var workeyear="";
         	    			var sunday = document.getElementsByName("sunday")[0].value;
         	            	var sunstarttime = document.getElementsByName("sunstarttime")[0].value;
         	    			var sunendtime = document.getElementsByName("sunendtime")[0].value;
         	    			var monday = document.getElementsByName("monday")[0].value;
         	    			var monstarttime = document.getElementsByName("monstarttime")[0].value;
         	    			var monendtime = document.getElementsByName("monendtime")[0].value;
         	    			var tuesday = document.getElementsByName("tuesday")[0].value;
         	    			var tuestarttime = document.getElementsByName("tuestarttime")[0].value;
         	    			var tueendtime = document.getElementsByName("tueendtime")[0].value;
         	    			var wednesday = document.getElementsByName("wednesday")[0].value;
         	    			var wedstarttime = document.getElementsByName("wedstarttime")[0].value;
         	    			var wedendtime = document.getElementsByName("wedendtime")[0].value;
         	    			var thrusday = document.getElementsByName("thrusday")[0].value;
         	    			var thrustarttime = document.getElementsByName("thrustarttime")[0].value;
         	    			var thruendtime = document.getElementsByName("thruendtime")[0].value;
         	    			var friday = document.getElementsByName("friday")[0].value;
         	    			var fristarttime = document.getElementsByName("fristarttime")[0].value;
         	    			var friendtime = document.getElementsByName("friendtime")[0].value;
         	    			var saturday = document.getElementsByName("saturday")[0].value;
         	    			var satstarttime = document.getElementsByName("satstarttime")[0].value;
         	    			var satendtime = document.getElementsByName("satendtime")[0].value;
         	    			var curentworking="";
         	    			var curentpersuing="";
         	    			var otherinfo = document.getElementsByName("otherinfo")[0].value;
         	    			var area = document.getElementsByName("area")[0].value;
         	    			var editid = document.getElementsByName("editid")[0].value;
         	    			var hopitalid0 = document.getElementsByName("hospitalid")[0].value;
         					if(q==1){
         						var college1 = document.getElementsByName("college1")[0].value;
         						var degree1 = document.getElementsByName("degree1")[0].value;
         						var qualid1 = document.getElementsByName("qualid1")[0].value;
         						var college2 = "";
         						var degree2 = "";
         						var qsyear2 ="";
         						var qeyear2 = "";
         						var curentpersuing2 = "";
         					}else if(q==2){
         						var college1 = document.getElementsByName("college1")[0].value;
         						var degree1 = document.getElementsByName("degree1")[0].value;
         						var qualid1 = document.getElementsByName("qualid1")[0].value;
         						var college2 = document.getElementsByName("college2")[0].value;
         						var degree2 = document.getElementsByName("degree2")[0].value;
         						var qualid2 = document.getElementsByName("qualid2")[0].value;
         					}else{
         						var college1 = "";
         						var degree1 = "";
         						var qsyear1 ="";
         						var qeyear1 = "";
         						var curentpersuing1 = "";
         						var college2 = "";
         						var degree2 = "";
         						var qsyear2 ="";
         						var qeyear2 = "";
         						var curentpersuing2 = "";
         						var qualid1="";
         						var qualid2="";
         					}
         					if(j==1){
         						var whospri1 = document.getElementsByName("hospri1")[0].value;
         						var designation1 = document.getElementsByName("designation1")[0].value;
         						var whospri2 ="";
         						var designation2 ="";
         						var worksyear2 = "";
         						var workeyear2 ="";
         						var curentworking2 ="";
         						
         					}else if(j==2){
         						var whospri1 = document.getElementsByName("hospri1")[0].value;
         						var designation1 = document.getElementsByName("designation1")[0].value;
         						var whospri2 = document.getElementsByName("hospri2")[0].value;
         						var designation2 = document.getElementsByName("designation2")[0].value;

         					}else{
         						var whospri1 ="";
         						var designation1 ="";
         						var worksyear1 = "";
         						var workeyear1 ="";
         						var curentworking1 ="";
         						var whospri2 ="";
         						var designation2 ="";
         						var worksyear2 = "";
         						var workeyear2 ="";
         						var curentworking2 ="";
         					}
         				if(city !="" && specialization !="" && experience !="" && mregistration !="" && degree !="" && whospri!="" && name != ""){		
         	               $.get( 
         	                "http://admin.konsultapp.com/index.php/verifylogin/doctorregistration",
         	                //"http://localhost:8085/konsultdev1/site/admin/index.php/verifylogin/doctorregistration",
         	                  {salutation: salutation, name: name, email: email,mobile: mob, city: city,address: address, experience: experience,specialization: specialization, mregistration: mregistration,sregistration: sregistration,pminute: pminute,allowuser: allowuser,college0: college,degree0: degree,qsyear0: qsyear,qeyear0: qeyear,whospri0: whospri,designation0: designation,worksyear0: worksyear,workeyear0: workeyear,sunday:sunday,sunstarttime:sunstarttime,sunendtime:sunendtime,monday:monday,monstarttime:monstarttime,monendtime:monendtime,tuesday:tuesday,tuestarttime:tuestarttime,tueendtime:tueendtime,wednesday:wednesday,wedstarttime:wedstarttime,wedendtime:wedendtime,thrusday:thrusday,thrustarttime:thrustarttime,thruendtime:thruendtime,friday:friday,fristarttime:fristarttime,friendtime:friendtime,saturday:saturday,satstarttime:satstarttime,satendtime:satendtime,curentworking0:curentworking,curentpersuing0:curentpersuing,college1:college1,degree1:degree1,qsyear1:qsyear1,qeyear1:qeyear1,curentpersuing1:curentpersuing1,college2:college2,degree2:degree2,qsyear2:qsyear2,qeyear2:qeyear2,curentpersuing2:curentpersuing2,whospri1:whospri1,designation1:designation1,worksyear1:worksyear1,workeyear1:workeyear1,curentworking1:curentworking1,whospri2:whospri2,designation2:designation2,worksyear2:worksyear2,workeyear2:workeyear2,curentworking2:curentworking2,q:q,j:j,otherinfo:otherinfo,area:area,qualid0:qualid,qualid1:qualid1,qualid2:qualid2,editid:editid,hopitalid0:hopitalid0},
         	                  function(data) {
         	                   //alert(data);
         	                  $('#txtHint').html(data);
         	                  }
         	               );
         				}
                        else if(name==""){
             				alert("Doctor name can't be blank!");
         				}
                        else if(city ==""){
             				alert("Please Enter City Name");
         				}else if(specialization==""){
             				alert("Please Choose any Specialization");
         				}else if(experience==""){
             				alert("Please Enter Your Experience");
         				}else if(mregistration ==""){
             				alert("Please enter Registration Number");
         				}else if(degree==""){
             				alert("Please Choose andy degree");
         				}else if(whospri==""){
             				alert("Please Enter Hospital Name");
         				}
                        
         	            });
         	         });
      	function workexp(){
       		$("#mainworkex").show();
       	}
      </script>

</head>
<body class="login_bg">
	<div id="menu-bar-fixed" style="opacity: 1; top: 0; display: none;">
		<div class="container">
			<a class="logo" href="index.html"><img
				src="http://konsultapp.com/img/logo-sm.png" alt=""></a>
			<div style="float: right;"></div>
		</div>
	</div>

	<div class="container" style="margin-top: 40px; color: #000;">
		<div id="txtHint">
			<b></b>
		</div>
		<form name="take">
			<input type="hidden" name="identifier" value="<?php session_id(); ?>" />
			<fieldset>

				<table width="100%" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td class="heading_h">Doctor Registration</td>
						<td align="right"><input type="button" class="f_submit"
							name="btn_edit" id="btn_useredit" value="Edit"
							style="width: 80px;" /></td>
					</tr>
                    <tr>
                        <td>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td style="padding-right: 5%;">
                                        <span class="loginLbl">Salutation</span>
                                        <select name="salutation" id="salutation">
                                            <option value="Mr.">Mr.</option>
                                            <option value="Ms.">Ms.</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="Dt.">Dt.</option>
                                        </select>
                                    </td> 
									<td style="padding-right: 8%;">
                                        <span class="loginLbl">Name*</span>
                                        <input type="name" id="name" name="name" class="" value="" />
                                    </td>
								</tr>
                            </table>
                        </td>                        
                        
                        
                    </tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">Email*</span>
							<input type="email" id="email" name="email" class="" value="" /></td>
						<td style="padding-left: 5%;"><table width="100%" border="0"
								cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="2" style="float: left; width: 85%;"><input type="hidden"
										id="editid" name="editid" class="" value="" /><span
										class="loginLbl">Mobile*</span> <input type="text"
										id="mobile" name="mobile" class="" value="" /></td>
									<td width="">&nbsp;&nbsp;<input type="button" class="f_submit"
										name="btn_login" id="btn_userValidate" value="Validate User" /></td>

								</tr>
							</table></td>
					</tr>
					<tr>
						<td>&nbsp;
							</div>
						</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">City*</span>
							<!-- <input type="text" id="p_password" name="city" class=""  value=""/> -->
							<input list="city2" name="city" id="city"> <datalist id="city2">
								<option value="Others"></option>
							</datalist></td>
						<td style="padding-left: 5%;"><span class="loginLbl">Address</span>
							<input type="text" id="address" name="address" class="" value="" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">Experience(Years)*</span>
							<input type="text" id="experience" name="experience"
							class="mediuminput" value="" /></td>
						<td style="padding-left: 5%;"><span class="loginLbl">Specialization*</span>
							<!-- <input type="text" value="" placeholder="" id="specialization"> 
           <div id="country_list_id"></div>--> <input
							list="country_list_id" name="specialization" id="specialization">
							<datalist id="country_list_id">
								<option value="Others"></option>

							</datalist> <!-- <select id="country_list_id">
  			     <option value="Others">Others</option>
  			     <option value="volvo">Volvo</option>
  			<select> --></td>
					</tr>

					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">Medical
								Registration Number*</span> <input type="text"
							id="mregistration" name="mregistration" class="mediuminput"
							value="" /></td>
						<!-- <td style="padding-left:5%;"><span class="loginLbl">State Medical Registration Number</span>
				<input type="text" id="sregistration" name="sregistration" class="mediuminput" value=""/></td> -->
						<td style="padding-left: 5%;"><span class="loginLbl">Other
								Information</span> <input type="text" id="otherinfo"
							name="otherinfo" class="mediuminput" value="" /></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">Per Minute
								Charge</span> <input list="pminute" name="pminute" id="pminute"> <datalist
								id="pminute">
								<option value="5">
								
								
								<option value="10">
								
								
								<option value="15">
								
								
								<option value="20">
								
								
								<option value="25">
								
								
								<option value="30">
								
								
								<option value="35">
								
								
								<option value="40">
								
								
								<option value="45">
								
								
								<option value="50">
								
								
								<option value="55">
								
								
								<option value="60">
								
								
								<option value="65">
								
								
								<option value="70">
								
								
								<option value="75">
								
								
								<option value="80">
								
								
								<option value="85">
								
								
								<option value="90">
								
								
								<option value="95">
								
								
								<option value="100">
							
							</datalist></td>
						<td style="padding-left: 5%;"><span class="loginLbl">New patient
								can call?<small></small>
						</span></br>
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td width="50px"><span class="loginLbl" style="float: left;"><strong>Yes</strong></span></td>
									<td align="left" style="text-align: left;"><input type="radio"
										id="allowuser" name="allowuser" class="allow"
										value="Yes" style="width: 20px;" /></td>
								</tr>
								<tr>
									<td><span class="loginLbl"><strong>No</strong></span></td>
									<td><input type="radio" id="allowuser" name="allowuser" id="allowuser"
										class="mediuminput" value="No" checked style="width: 20px;" /></td>
								</tr>
							</table> <!-- <input list="allowuser" name="allowuser">
			<datalist id="allowuser">
    			<option value="Yes">
    			<option value="No">
  			</datalist> --></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl">Area</span>
							<br /> <input type="text" id="area" name="area"
							class="mediuminput" value="" /></td>
						<td style="padding-left: 5%;"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl"><strong></>Qualification
									Details*</strong></span></td>
						<td style="padding-left: 5%;"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td style="padding-right: 5%;"><input type="hidden" name="qualid" id="qualid" value="0" /><span class="loginLbl">College</span>
							<input list="college" name="college" id="college"> <datalist id="college">
							</datalist></td>
						<td style="padding-left: 5%;"><span class="loginLbl">Degree*</span>
							<input list="degree2" name="degree" id="degree"> <datalist id="degree2">
								<option value="Others"></option>
							</datalist>
					</tr>
					<!--  <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td style="padding-right:5%;"><span class="loginLbl">Start Year</span>
				<input type="date" id="syear" name="syear" class="mediuminput"  value=""/></td>
        <td style="padding-left:5%;"><span class="loginLbl">End Year</span>
        <input type="date" id="eyear" name="eyear" class="mediuminput"  value=""/>
        </td>
      </tr> -->
					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<!-- <td style="padding-right:5%;"><span class="loginLbl">Currently pursuing<small>(Yes/No)</small></span>
			<input list="curentpersuing" name="curentpersuing">
			<datalist id="curentpersuing">
    			<option value="Yes">
    			<option value="No">
  			</datalist>
		</td> -->
						<td style="padding-right: 5%; padding-top: 20px;"><input
							type="button" id="btn_qulification" value="Add More"
							style="background-color: #0ab2f1; color: #fff;" /></td>
					</tr>
					<tr>
						<td colspan="2"><div id="quali0"></div></td>
					</tr>
					<tr>
						<td colspan="2"><div id="quali1"></div></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><span class="loginLbl"><strong></>Currently
									Working*</strong></span></td>
						<td style="padding-left: 5%;"></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td style="padding-right: 5%;"><input type="hidden" name="hospitalid" id="hospitalid" value="0"/><span class="loginLbl">Hospital or
								Clinic*</span> <input list="hospri" name="hospri" id="hospri"> <datalist id="hospri">
								
							</datalist></td>
						<td style="padding-left: 5%;"><span class="loginLbl">Designation</span>
							<input list="designation" name="designation" id="designation"> <datalist
								id="designation">
							</datalist></td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

					<!--  <tr>
        <td style="padding-right:5%;"><span class="loginLbl">Start Year</span>
				<input type="date" id="worksyear" name="worksyear" class="mediuminput"  value=""/></td>
        <td style="padding-left:5%;"><span class="loginLbl">End Year</span>
        <input type="date" id="workeyear" name="workeyear" class="mediuminput"  value=""/>
        </td>
      </tr>
      <tr> 
        <td>&nbsp;</td>
      </tr> 
      <tr>
        <td valign="top" style="padding-right:5%;"><span class="loginLbl">Currently working <small>(Yes/No)</small></span>
			<input list="curentworking" name="curentworking">
			<datalist id="curentworking">
    			<option value="Yes">
    			<option value="No">
  			</datalist>
		</td>
        <td valign="top" style="padding-left:5%;padding-top: 20px;"><input type="button" id="btn_experience" value="Add More" style="background-color: #0ab2f1; color: #fff;"/></td>
      </tr>
      <tr>
     	<td colspan="2"><div id="workex0"> </div></td>
     	</tr>
      <tr>
     	<td colspan="2"><div id="workex1"> </div></td>
      </tr>
    <tr>
        <td>&nbsp;</td>
      </tr>-->
					<!-- <tr>
        <td style="padding-right:5%;"><span class="loginLbl">Per Minute Charges*</span><br/>
				 <select style="margin-top:10px;padding:5px;">
					<option>5</option>
					<option>10</option>
					<option>15</option>
					<option>20</option>
				</select></td>
        <td style="padding-left:5%;">&nbsp;</td>
      </tr>
    <tr>
        <td>&nbsp;</td>
      </tr> -->

					<tr>
						<td style="padding-right: 5%;"><span style="font-weight: bold;">Expected
								Availability Day*</span><br />
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
								<tr>
									<td>
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><input type="hidden" name="sunday" id="sunday" value="1" /><span
													class="loginLbl">Sunday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="sunstarttime" name="sunstarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="sunendtime" name="sunendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="monday" id="monday" value="2" />
													<span class="loginLbl">Monday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="monstarttime" name="monstarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="monendtime" name="monendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="tuesday" id="tuesday"
													value="3" /> <span class="loginLbl">Tuesday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="tuestarttime" name="tuestarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="tueendtime" name="tueendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="wednesday" id="wednesday"
													value="4" /> <span class="loginLbl">Wednesday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="wedstarttime" name="wedstarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="wedendtime" name="wedendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="thrusday" id="thrusday"
													value="5" /> <span class="loginLbl">Thrusday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="thrustarttime" name="thrustarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="thruendtime" name="thruendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="friday" id="friday" value="6" />
													<span class="loginLbl">Friday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="fristarttime" name="fristarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="friendtime" name="friendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td><input type="hidden" name="saturday" id="saturday"
													value="7" /> <span class="loginLbl">Saturday</span></td>
												<td style="padding-left: 40px; padding-right: 40px"><input
													type="time" id="satstarttime" name="satstarttime"
													value="10:00:00" /></td>
												<td><input type="time" id="satendtime" name="satendtime"
													value="22:00:00" /></td>
											</tr>
											<tr>
												<td>&nbsp;</td>
											</tr>
											<tr>
												<td colspan="3"><input type="button" class="f_submit"
													name="btn_login" id="btn_login" value="Submit" /></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="3">&nbsp;</td>
								</tr>
							</table></td>
						<td style="padding-left: 5%;">&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>


				</table>
			</fieldset>

			<!--a href="forgotpassword.php">Forgot Password</a-->
		</form>
	</div>

	<footer id="footer" style="position: static;">
		<div class="container">
			<div id="footer-content">
				<h2>
					<img src="http://konsultapp.com/img/logo.png" alt="">
				</h2>
				<!-- <p id='social-links'>
					<a href="#" class='icon ion-social-facebook'></a>
					<a href="#" class='icon ion-social-twitter'></a>
					<a href="#" class='icon ion-social-googleplus-outline'></a>
					<a href="#" class='icon ion-social-instagram'></a>
				</p> -->
				<ul class="f_links">
					<li><a href="terms.html">Terms &amp; Conditions</a></li>
					<li><a href="policy.html">Privacy Policy</a></li>
				</ul>
				<p class="copyright">Copyright � 2015. KONSULT</p>
			</div>
		</div>
	</footer>
</body>
</html>