 <style>
 #Subheader .column h1.title{
	 margin-top:0px !important;
 }
 #Header{
	min-height:50px !important;
 }
 .handimagedrp img{
	 margin-right:-17%;
     width:100%;
 }
 .handimagedrp{
 text-align: right;
  padding: 0px !important;
  }
  .bhandimagedrp p{
	  font-size:25px !important;
  }
  .bhandimagedrp h2{
	  font-size:40px;
  }
  .bhandimagedrp h1{
	  font-size: 70px;
      font-weight: bold;
  }
  .bhandimagedrp{
	  padding: 7%;
	  
  }
  .bhdrp{
	  height:2px;
	  border:1px solid black;
	  background:black;
	  width:9%;
	  margin-left: 0%;;
  }
  p{
    font-size: 18px !important;
    font-weight: 100 !important;
  }
  .rtdrp h3{
	  font-size: 35px;
  }
  .rtdrp {
    padding: 50px;
box-shadow: 2px 0px 6px 0px;
    margin-bottom: 30px;
	text-align:center;
}
.row {
    margin-top: 25px;
}
.rttdrp img{
	
}
.rttdrp{
	text-align:center;
}
.lttdrp h1, .f h1{
	font-size: 40px;
font-weight: bold;
}
.lttdrp img{
	width: 80%;
margin-top: 20px;

}
.lttdrp{
	padding:3% 7% !important;
}
.oneeee{
	margin-left:-10%;
}
.twoooo{
	margin-left:-30%;
	
}
.s i{
	font-size: 60px;
margin-top: 100% !important;
color: #e872a6;
}
.rwudrps{
	padding: 6% 6%;
	border-top: 1px solid gray;
}
.f{
	margin-top: 18px;
}
.t input, .t textarea {
	border: none !important;
box-shadow: none !important;
border-bottom: 2px solid #e872a6 !important;
}
.t .wpcf7-submit{
border: none !important;
background: #e872a6;
width: 150px;
border-radius:0px;
padding:0px;
}
@media only screen 
    and (max-device-width: 640px), 
    only screen and (max-device-width: 667px), 
    only screen and (max-width: 480px)
{ 
.s i{
	font-size: 60px;
}
.lttdrp h1, .f h1{
font-size: 40px;
}
p{
    font-size: 9px !important;
  }
  .rtdrp h3{
	  font-size: 15px;
  }
    .bhandimagedrp h2{
	  font-size:12px;
  }
  .bhandimagedrp h1{
	 /* font-size: 70px; */
  }
  .bhandimagedrp p {
    font-size: 9px !important;
}
.bhandimagedrp {
    padding: 0% !important;
}
.lttdrp {
    padding: 0% !important;
}
}
     #Top_bar #logo img {
   
    max-height: 120% !important;
}
 </style>
<?php
/**
 * Template Name: doctor Regestration
 * Description: A Page Template that display portfolio items.
 *
 * @package Betheme
 * @author Muffin Group
 */
//require_once('../../../wp-config.php');
 get_header();

 ?>

 <section class="mainbackgrounddrp">
 <div class="container">
 <div class="row">
 <div class="col-sm-6 col-md-6 col-xs-6 bhandimagedrp">
 <h1>KONSULT <br />FOR DOCTORS</h1>
 <h2>Advise over phone 'on the fly'</h2>
 <hr align="left" class="bhdrp" />
 <p>Konsult is a calling solution which opens the door for doctors to reach and help more patients who are in need of medical care.</p>
 </div>
 
 <div class="col-sm-6 col-md-6 col-xs-6 handimagedrp">
 <img src="http://www.konsult.com/wp-content/uploads/2017/05/Konsultfordoctors-1.jpg" class="image-responsive" />
 </div>
 </div>
 <div class="row rowrtdrp">
 <div class="rtdrp"><h3>Get your profile on konsult for free</h3><p>Yours profile will showcase your expertise, years of experience and qualifications. You can also add in any remarkable feat, achievements and awards in medical field.</p></div>
 <div class="rtdrp"><h3>Strict privacy rules</h3><p>1: Protect your privacy where personal number is not divulged</p><p>2: Secure chat feature for easy sharing of clinical records</p></div>
 <div class="rtdrp"><h3>Boost your online presence</h3><p>1: Showcase your knowledge in the 'Expert Opinion' section.</p><p>2: Get promotions on social media through various campaigns</p></div>
 <div class="rtdrp"><h3>Better managemment of time</h3><p>1: Effectively utilize your free time</p><p>2: Set availability on your own and change the mode to offline when busy</p></div>
 <div class="rtdrp"><h3>Earn extra perks through your practice</h3><p>1: Consultation charges are transferred instantly</p><p>2: Set call charges on your own based per minute</p></div>
 </div>
 <div class="row">
 <div class="col-sm-6 col-md-6 col-xs-6 lttdrp">
 <h1>Download <br />The Konsult App</h1>
 <p>If you are a doctor, then konsult is the best way to develop and grow your practice. The app is available on both playstore and app store.</p>
 <div class="col-sm-12 hidden-xs">
  <div class="col-sm-6 col-md-6 col-xs-6 ">
 <img src="http://www.konsult.com/wp-content/uploads/2017/05/playstore-01.jpg" class="image-responsive oneeee" />
 </div>
  <div class="col-sm-6 col-md-6 col-xs-6 ">
 <img src="http://www.konsult.com/wp-content/uploads/2017/05/playstore-02.jpg" class="image-responsive twoooo" />
 </div>
 </div>
 </div>
 
 <div class="col-sm-6 col-md-6 col-xs-6 rttdrp">
  <img src="http://www.konsult.com/wp-content/uploads/2017/05/MOBILE_APP.jpg" class="image-responsive" />

 </div>
 </div>
 <div class="row rwudrps">
 <div class="col-sm-5 f">
 <h1>Register with us</h1>
 <p>Fill up a few general queries.</p>
 <p>Our team will shortly get in touch with you,</p>
 </div>
 <div class="col-sm-1 s hidden-xs">
 <i class="fa fa-angle-double-right" aria-hidden="true"></i>
 </div>
 <div class="col-sm-6 t">
 <?php echo do_shortcode('[contact-form-7 id="4289" title="New Doc Reg"]'); ?>
 </div>
 </div>
 </div>
 </section>
 
 
 
 <?php 
 get_footer();
 ?>