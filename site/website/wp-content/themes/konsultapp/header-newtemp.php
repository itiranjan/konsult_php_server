<?php
/**
 * The Header for our theme.
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */
global $sssssextra;
if($_SERVER['REQUEST_URI'] == '/category/events/' || $_SERVER['REQUEST_URI'] == '/category/'){
header('Location:http://www.konsult.com/health-tips/');
exit;
}

?>
<!DOCTYPE html>
<?php 
	if( $_GET && key_exists('mfn-rtl', $_GET) ):
		echo '<html class="no-js" lang="ar" dir="rtl">';
	else:
?>
<html class="no-js" <?php language_attributes(); ?> <?php mfn_tag_schema(); ?>>
<?php endif; ?>

<!-- head -->
<head>

<!-- meta -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="msvalidate.01" content="AA5344229C960043B92BF47270B21EC8" />
    <?php if ( is_singular() ) echo '<link rel="canonical" href="' . get_permalink() . '" />'; ?>
<?php if( mfn_opts_get('responsive') ) echo '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
 ?>

<title itemprop="name"><?php
if( mfn_title() ){
	echo mfn_title();
} else {
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	if ( $paged >= 2 || $page >= 2 ) echo ' | ' . sprintf( __( 'Page %s', 'betheme' ), max( $paged, $page ) );
}
?></title>
<?php if(is_attachment()){
  echo '<meta name="robots" content="noindex,follow" />';
} ?>
<?php do_action('wp_seo'); ?>

<link rel="shortcut icon" class="shotcut" href="<?php mfn_opts_show( 'favicon-img', THEME_URI .'/images/favicon.ico' ); ?>" />	
<?php if( mfn_opts_get('apple-touch-icon') ): ?>
<link rel="apple-touch-icon" href="<?php mfn_opts_show( 'apple-touch-icon' ); ?>" />
<?php endif; ?>	

<!-- wp_head() -->
<?php wp_head(); ?>
<?php
if(is_front_page()){
	?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
}
elseif(is_page("doctor-registration")){
	?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<?php
}elseif(is_page("careers")){
?>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<?php
}
else{}
    
    if(is_tag()){
        $taggg = single_tag_title( "", false );
        ?>
    <meta name="description" content="Read the health tips associated with <?php echo $taggg; ?> and get everything that you need to know about <?php echo $taggg; ?>.">
    <?php
    }
    
?>
    
</head>

<!-- body -->
<body <?php body_class(); ?>>


<ul class='offersul' style="display:none">
<?php

global $post;
$args = array( 'posts_per_page' => 5,'post_type' => 'offerelement' );

$myposts = get_posts( $args );
foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
	<li>
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</li>
<?php endforeach; 
wp_reset_postdata();?>

</ul>
	<div class="headersearchresults">
	<ul class="resul">
	</ul>
	</div>
	<?php do_action( 'mfn_hook_top' ); ?>
	
	<?php //get_template_part( 'includes/header', 'sliding-area' ); ?>
	
	<?php if( mfn_header_style( true ) == 'header-creative' ) get_template_part( 'includes/header', 'creative' ); ?>
	
	<!-- #Wrapper -->
	<div id="Wrapper">
	
		<?php 
			// Header Featured Image ----------
			$header_style = '';
			
			// Image -----
			// if( mfn_ID() && ! is_search() ){
				// $sssssextra = "Setted";
				// if( ( ( mfn_ID() == get_option( 'page_for_posts' ) ) || ( get_post_type() == 'page' ) ) && has_post_thumbnail( mfn_ID() ) ){
					
					// // Pages & Blog Page ---
					// $subheader_image = wp_get_attachment_image_src( get_post_thumbnail_id( mfn_ID() ), 'full' );
					// $header_style .= ' style="background-image:url('. $subheader_image[0] .');"';

				// } elseif( get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) ){

					// // Single Post ---
					// $header_style .= ' style="background-image:url('. get_post_meta( mfn_ID(), 'mfn-post-header-bg', true ) .');"';

				// }
			// }
			?>
			
			<?php
			// Attachment -----
			// if( mfn_opts_get('img-subheader-attachment') == 'fixed' ){
				// $header_style .= ' class="bg-fixed"';
			// } elseif( mfn_opts_get('img-subheader-attachment') == 'parallax' ){
				// $header_style .= ' class="bg-parallax" data-stellar-background-ratio="0.5"';
			// }
		?>
		
		<?php //if( mfn_header_style( true ) == 'header-below' ) echo mfn_slider(); ?>
	
		<!-- #Header_bg -->
		<div id="Header_wrapper" <?php echo $header_style; ?>>
	
			<!-- #Header -->
			<header id="Header">
				<?php if( mfn_header_style( true ) != 'header-creative' ) get_template_part( 'includes/header', 'top-area' ); ?>	
				<?php //if( mfn_header_style( true ) != 'header-below' ) echo mfn_slider(); ?>
			</header>
				<?php

	
				
							echo '<div id="Subheader">';
								echo '<div class="container">';
									echo '<div class="column one">';
										
										// Title
										echo '<h1 class="title">'. get_the_title() .'</h1>';
										//echo "arsh";
										// Breadcrumbs
									 mfn_breadcrumbs( $breadcrumbs_link );
										
									echo '</div>';
								echo '</div>';
							echo '</div>';
					
				
			?>
		
		</div>
		<style>
            @media screen and (min-width: 768px) {
    #Subheader {
    background-color: #f5f9fc;
    margin-top: 60px;
    margin-bottom: -61px;
}
                .page-id-4226 #Subheader {
    background-color: #f5f9fc;
    margin-top: 0px;
    margin-bottom: -61px;
}
                .topbuddyimage{
                    margin-top: 0% !important;
                }
}
         @media screen and (max-width: 768px) {
    #Subheader {
 display: none;
}
}
        </style>
		<?php //do_action( 'mfn_hook_content_before' ); ?>
