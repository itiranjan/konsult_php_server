<div id="window3">
    <form id="DetailP">

        <table width="100%">

            <tr>
                <td><div style="width:500px" id="callInformations"/>&nbsp;&nbsp;
                </td>
            </tr>

        </table>
    </form>
</div>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

    <div id="clientsDb">
        <span style="font-size:20px">KCC - Call Informations<br/><br/></span>
        <div id="grid" ></div>
    </div>    

    <script>

        var window3 = $("#window3");
        $(document).ready(function() {

            var onClose = function()
            {
                mainGrid.data("kendoGrid").refresh();
            }

            window3.kendoWindow({
                width: "700px",
                visible: false,
                title: "KCC - Call Informations"
            });

            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                
                toolbar:["excel"],
                selectable: "multiple cell",
                allowCopy: true,                    
                excel: {
                    allPages: true,
                    fileName: "kcc_call_list_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                    filterable: true
                },
                dataSource: {
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    transport: {
                        read: {
                            type: "POST",
                            url: base_url + "/index.php/kcc/callInfoList/1",
                            dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                        },
                        parameterMap: function (options) {
                            if (options.filter) {
                                KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                            }
                            return options;
                        },
                    },                    
                    schema: {
                        data: "list",
                        total: "total",
                        model: {
                            fields: {
                                kcc_calls$kcc_call_id: {type: "number", editable: false},
                                dr$name: {type: "string", editable: false},
                                pt$name: {type: "string", editable: false},
                                dr$email: {type: "string", editable: false},
                                pt$email: {type: "string", editable: false}, 
                                caller_status: {type: "string", editable: false}, 
                                receiver_status: {type: "string", editable: false}, 
                                call_duration: {type: "number", editable: false},
                                total_charges: {type: "number", editable: false},
                                doctor_charges: {type: "number", editable: false},
                                konsult_charges: {type: "number", editable: false},
                                kcc_calls$created_at: {type: "date", editable: false},
                            }
                        }
                    },
                    pageSize: 100
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            //eq: "Is equal to",
                            //neq: "Is not equal to"
                        },
                        number: {
                            eq: "Equal To",
                            gt: "Greater Than",
                            //gte: "Greater than or equal",
                            lt: "Less Than",
                            //lte: "Less than or equal",                                
                        }
                    }
                },                     
                sortable: true,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                pageable: {
                    refresh: true,
                    pageSize: 100,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function(e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                columns: [
                        {field: "kcc_calls$kcc_call_id", title: "Comm Id", width: '5px'},
                        {field: "dr$name", title: "Doctor Name", width: '15px'},
                        {field: "dr$email", title: "Doctor Email", width: '15px'},
                        {field: "pt$name", title: "Patient Name", width: '12px'},
                        {field: "pt$email", title: "Patient Email", width: '12px'},
                        {field: "caller_status", title: "Caller Status", width: '12px'},
                        {field: "receiver_status", title: "Receiver Status", width: '12px'},
                        {field: "kcc_calls$created_at", title: "Called On", width: '15px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "call_duration", title: "Duration (Seconds)", width: '7px'},
                        {field: "total_charges", title: "Patient Debit", width: '7px'},
                        {field: "doctor_charges", title: "Doctor Credit", width: '7px'},
                        {field: "konsult_charges", title: "Konsult Credit", width: '7px'},
                ],
                editable: true,
            }).data("kendoGrid");              

            mainGrid.thead.kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target;
                    return $(target).text();
                }                    
            });
        });

        function ajaxCall(url, data, customFunction) {
            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                dataType: 'json',
                beforeSend: function () {
                    $('#loading').show();
                },
                complete: function () {
                    $('#loading').hide();
                },
                success: customFunction,
                error: function () {
                    alert(error);
                }
            });
        }       
    </script>
</div>
</div>