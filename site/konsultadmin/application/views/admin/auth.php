<style>

    .form_section{border: 1px solid #ccc;
                  background-color: #fff;
                  text-align: center;
                  width: 450px;
                  margin: 10px auto 0;
                  padding: 20px;}
    </style>

    <div id="select_user" class="form_section">
    <h1 style="font-size: 16px;font-weight: bold;">Authorization Access Control Panel</h1>
    <form method="POST" style="margin-top: 15px; padding-top:15px; border-top:1px #e5e4e2 solid;">
        <select name="cars" id="cars" onclick="getUserAuth(this.value);">
            <?php foreach ($userEmailArray as $key => $userEmail) { ?>
                <option  value="<?php echo $key; ?>"><?php echo $userEmail; ?></option>
            <?php } ?>
        </select>
    </form>	
</div>

<div id="saveAuthPermissions"></div>    

<script>

    window.onload = function () {
        getUserAuth(document.getElementById('cars').value);
    }

    function getUserAuth(admin_id) {

        if (!admin_id) {
            alert("Missing email id!");
            return;
        }

        var base_url = "<?php echo URL; ?>";
        $.ajax({
            'url': base_url + "/index.php/user/saveAuthPermissions",
            'type': 'POST',
            'data': {
                format: 'json',
                'admin_id': admin_id
            },
            success: function (data) {

                if (data == 0) {
                    document.getElementById("saveAuthPermissions").innerHTML = '';
                    return;
                }

                document.getElementById("saveAuthPermissions").innerHTML = data;
            },
            'error': function (request, error)
            {
                alert("Whoops!! Something might be wrong. Please try again.");
            }
        });
    }
</script>

<script>
    function changeAuthorization() {

        var getConfirmation = confirm("Are you sure you want update the changes?");
        if (!getConfirmation) {
            return;
        }

        var queryString = $('#change_authorization').serialize();

        var base_url = "<?php echo URL; ?>";
        $.ajax({
            'url': base_url + "/index.php/user/changeAuthorization",
            'type': 'POST',
            'data': queryString,
            'success': function (data) {
                var returnedData = jQuery.parseJSON(data);
                alert(returnedData.message);
            },
            'error': function (request, error)
            {
                alert("Whoops!! Something might be wrong. Please try again.");
            }
        });
    }
</script>