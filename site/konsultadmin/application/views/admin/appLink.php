<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<style>
.k-grid td {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
}    
</style>    

<div style="width:100%">
    <div style="width:100%;display:block;">
        <div id="clientsDb">
            <span style="font-size:20px">Get App Link Log<br/><br/></span>
            <div id="grid" ></div>
        </div>    

        <script>
            
            $(document).ready(function () {

                var onClose = function ()
                {
                    mainGrid.data("kendoGrid").refresh();
                }

                var selectedRows = [];
                mainGrid = $("#grid").kendoGrid({
             
                    toolbar: ["excel"],
                    selectable: "multiple cell",
                    allowCopy: true,                    
                    excel: {
                        allPages: true,
                        fileName: "recharge_list_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                        filterable: true
                    },
                    dataSource: {
                        type: "json",
                        serverPaging: true,
                        serverSorting: true,
                        serverFiltering: true,
                        transport: {
                            read: {
                                type: "POST",
                                url: base_url + "/index.php/appLink/appLinkList/1",
                                dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                            },
                            parameterMap: function (options) {
                                if (options.filter) {
                                    KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                                }
                                return options;
                            },
                        },                        
                        
                        schema: {
                            data: "list",
                            total: "total",
                            model: {
                                fields: {
                                    applinklog_id: {type: "number", editable: false},
                                    mobile: {type: "string", editable: false},
                                    sent_count: {type: "number", editable: false},
                                    created_at: {type: "date", editable: false},
                                    updated_at: {type: "date", editable: false}
                                }
                            }
                        },
                        pageSize: 100
                    },
                    filterable: {
                        extra: false,
                        operators: {
                            string: {
                                contains: "Contains",
                                startswith: "Starts with",
                            },
                            number: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                lt: "Less Than",                            
                            }
                        }
                    },
                    sortable: true,
                    scrollable: true,
                    dataBound: function () {
                        this.expandRow(this.tbody.find("tr.k-master-row").first());
                    },
                    pageable: {
                        refresh: true,
                        pageSize: 100,
                        numeric: true,
                        buttonCount: 20,
                        info: true
                    },
                    change: function (e) {
                        var selected = this.select();
                        for (var i = 0; i < selected.length; i++) {
                            var dataItem = this.dataItem(selected[i]);
                            selectedRows.push(dataItem);
                        }
                    },
                    columns: [
                        {field: "applinklog_id", title: "Log Id", width: '7px'},
                        {field: "mobile", title: "Mobile No", width: '12px'},
                        {field: "sent_count", title: "Request Count", width: '10px'},
                        {field: "created_at", title: "First Requested On", width: '15px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "updated_at", title: "Last Requested On", width: '15px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}}                
                    ]
                }).data("kendoGrid");

                mainGrid.thead.kendoTooltip({
                    filter: "th",
                    content: function (e) {
                        var target = e.target; // element for which the tooltip is shown
                        return $(target).text();
                    }
                });
                
                $("#grid").kendoTooltip({
                   filter: "td:nth-child(10)", //this filter selects the first column cells
                   position: "left",
                   content: function(e){
                       var dataItem = $("#grid").data("kendoGrid").dataItem(e.target.closest("tr"));
                       var content = dataItem.comment;
                       return content;
                   }
                   }).data("kendoTooltip");
               });                

            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function () {
                        alert(error);
                    }
                });
            }
        </script>
    </div>
</div>