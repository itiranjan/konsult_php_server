<?php

class UserController extends ApiBaseController {

    /**
     * @author : Shipra Agrawal
     * @created : 27 jan 2015
     * @description : This method used to call appStatusVerify Method for verify Api key, App Version, config version and Auth Token
     * @access public
     * @param  bool $checkAuthToken (To Check Auth token)
     * @return  array 
     */
    protected function checkAuth($checkAuthToken = TRUE) {
        $requestHeaders = apache_request_headers();
        if ($requestHeaders['API_KEY'] && $requestHeaders['APP_VERSION'] && $requestHeaders['CONFIG_VERSION']) {
            $result = $this->appStatusVerify($requestHeaders, $checkAuthToken);
            $this->setHeader('API_KEY', $requestHeaders['API_KEY']);
            $this->setHeader('APP_VERSION', $requestHeaders['APP_VERSION']);
            $this->setHeader('CONFIG_VERSION', $requestHeaders['CONFIG_VERSION']);
            if ($checkAuthToken == TRUE) {
                if (!$requestHeaders['API_KEY']) {
                    $result['config_status'] = -1;
                    $result['config_msg'] = Config::get('config_msg.AUTH_TOKEN_MISSING');
                    return $result;
                }
            }
            if ($result['config_status'] == 1 && $checkAuthToken == TRUE) {
                $this->verfiedAuth = true;
                $this->authToken = $requestHeaders['AUTH_TOKEN'];
            }
        } else {
            $result['config_status'] = -1;
            $result['config_msg'] = Config::get('config_msg.HEADER_FIELD_MISSING');
        }
        return $result;
    }

    /**
     * @author : Shipra Agrawal
     * @created : 28 jan 2015
     * @description : This method used to register user [ store basic user information in database ]
     * @access public
     * @return  array 
     */
    public function userRegistration() {
        try {
            $checkAuthToken = False;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $emailPassword = 0;
            $emailData = array();
            if (!empty($data['password'])) {
                $password = trim($data['password']);
            } else {
                $stringHelper = $this->helper->load('string');
                $password = $stringHelper->random_string(4);
                $data['password'] = $password;
                $emailPassword = 1;
                $emailData['messagedata']['password'] = $password;
                $emailData['receiver'] = $data['email'];
                $emailData['subject'] = "Konsult : Thanks for registration";
                $emailData['viewname'] = "sendPassword";
            }

            $user = new User;
            $data['name'] = trim($data['name']);
            $data['email'] = trim($data['email']);
            $data['mobile'] = trim($data['mobile']);
            $data['password'] = trim($data['password']);
            if (!$user->validate($data)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['status'] = 0;
                $mobarray = $user->errors();
                if ($mobarray[0][messages][0] == "The mobile has already been taken.") {
                    $result['msg'] = $mobarray[0][messages][0] = 'This mobile no has already been used.';
                } elseif ($mobarray[0][messages][0] == "The email has already been taken.") {
                    $result['msg'] = 'This email address has already been used.';
                } else {
                    $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                }
                $result['errors'] = $mobarray;
                $this->setHeader('RESPONSE_CODE', 0);
            } else {

                $userId = User::where('email', '=', $data['email'])->pluck('user_id');

                if (!empty($userId)) {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $result['status'] = 0;
                    $result['errors'] = "This email is already registered!";
                    return Response::make($result, 200, $this->headers);
                }

                if ($data['social_auth_token'] || $data['social_network'] || $data['social_username']) {
                    $socialdata['social_auth_token'] = $data['social_auth_token'];
                    $socialdata['social_network'] = $data['social_network'];
                    $socialdata['social_login_id'] = $data['social_username'];
                    $socialdata['photo_url'] = $data['photo_url'];
                    $socialAccount = new SocialAccounts;
                    if (!$socialAccount->validate($socialdata)) {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                        $result['status'] = 0;
                        $result['errors'] = $socialAccount->errors();
                        return Response::make($result, 200, $this->headers);
                    }
                }
                if (!empty($data['salutation'])) {
                    $user->salutation = trim($data['salutation']);
                }

                $encryptHelper = $this->helper->load('encrypt');

                $user->name = ucfirst($data['name']);
                $user->email = $data['email'];
                $user->mobile = $data['mobile'];
                $user->password = $encryptHelper->encryptPWD(array('PWD' => $password));
                // $user->status = 1;
                $user->save();

                $userprofile = new UserProfile;

                if (Input::hasFile('photo') && Input::file('photo')->isValid()) {
                    $avatar = Input::file('photo')->getClientOriginalName();
                    $extension = Input::file('photo')->getClientOriginalExtension();
                    $temp = explode(".", $avatar);
                    $img_name = $temp[0] . '_' . $user->user_id . '.' . $extension;

                    Input::file('photo')->move(base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR, $img_name);
                    $userprofile->photo = $img_name;
                }

                if ($data['photo_url']) {
                    $userprofile->photo = $data['photo_url'];
                }

                $userprofile->user_id = $user->user_id;
                $userprofile->save();

                if ($data['social_auth_token'] && $data['social_network'] && $data['social_username']) {
                    $socialAccount->user_id = $user->user_id;
                    $socialAccount->social_network = $data['social_network'];
                    $socialAccount->social_login_id = $data['social_username'];
                    $socialAccount->social_auth_token = $data['social_auth_token'];
                    $socialAccount->save();
                }

                $auth_token = $this->saveAuthToken($user->user_id);
                $this->setHeader('RESPONSE_CODE', '1');
                $this->setHeader('AUTH_TOKEN', $auth_token);

                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.USER_REGISTERED');
                $result['response']['user_id'] = $user->user_id;
                $result['response']['name'] = $user->name;
                $result['response']['email'] = $user->email;
                $result['response']['dob'] = '';
                $result['response']['mobile'] = $user->mobile;
                $result['response']['is_doctor'] = 0;
                $result['response']['is_approved'] = 0;
                $result['response']['address'] = Doctor::where('user_id', '=', $user->user_id)->pluck('address');
                $result['response']['photo'] = User::getPhoto(array('photo' => $data['photo'], 'is_doctor' => 0));

                $emaildata['name'] = $user->name;
                $emaildata['email'] = $user->email;
                $emaildata['mobile'] = $user->mobile;
                $emaildata['password'] = $user->password;
                $emaildata['logo'] = Config::get('constants.LOGO');

                $emailbody = View::make('user_registration')->with('data', $emaildata)->render();
                $mailArray = array();
                $mailArray['email'] = $user->email;
                $mailArray['subject'] = Config::get('config_msg.USER_REG_EMAIL_SUBJECT');
                $mailArray['body'] = $emailbody;

                $communicationHelper = $this->helper->load('communication');
                $result['email_response'] = $communicationHelper->emailCommunication($mailArray);

                //SEND SMS ALERT
                $smsText = Config::get('constants.NETCORE_SMS_TEXT_APP_REGISTRATION');
                if ($smsText) {
                    $smsParams = array();
                    $smsParams['Text'] = str_replace('<User>', $user->name, $smsText);
                    $smsParams['To'] = $user->mobile;
                    $this->helper->load('communication')->smsCommunication($smsParams);
                }
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 28 jan 2015
     * @description : User App Registration function is used for Register user app detail.
     * @access public
     * @return  array 
     */
    public function userDeviceInfo() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);

            if (!empty($data['app_registration_id'])) {
                $requestHeaders = apache_request_headers();
                $user_id = $this->getUserIdbyToken($this->authToken);
                $platform_id = ApiPlatform::where('api_key', '=', $requestHeaders['API_KEY'])->pluck('id');
                $app_exist = DeviceInfo::where('user_id', '=', $user_id)->exists();
                if ($app_exist) {
                    DeviceInfo::where('user_id', '=', $user_id)->update(array('platform_id' => $platform_id, 'registartion_id' => $data['app_registration_id']));
                } else {
                    $appRegDetail = new DeviceInfo;
                    $appRegDetail->user_id = $user_id;
                    $appRegDetail->platform_id = $platform_id;
                    $appRegDetail->registartion_id = $data['app_registration_id'];
                    $appRegDetail->save();
                }

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.APP_REGISTERED');
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1041;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.APP_REG_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 30 jan 2015
     * @description : User Update basic or personal information.
     * @access public
     * @return  array 
     */
    public function profileUpdate() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $userData = array();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            $user_deatil = new User();
            $userArr = array();

            if (!empty($data['mobile'])) {
                $userArr['mobile'] = $data['mobile'];

                $getUserId = User::where('mobile', '=', $userArr['mobile'])->pluck('user_id');
                if ($user_id == $getUserId) {
                    unset($data['mobile']);
                }
            }

            if (!empty($data['name'])) {
                $userArr['name'] = trim(ucfirst(strtolower($data['name'])));
            }

            $email_same = User::where('user_id', '=', $user_id)->pluck('email');

            if (!empty($email_same)) {
                unset($data['email']);
            } else {
                $userArr['email'] = $data['email'];
            }

            if ($user_deatil->updateprofilerules($data)) {
                if (!empty($data['password']) && !empty($data['newpassword'])) {

                    $encryptHelper = $this->helper->load('encrypt');
                    $dbPWD = User::where('user_id', '=', $user_id)->pluck('password');
                    $isPWDMatched = $encryptHelper->validatePWD(array('dbPWD' => $dbPWD, 'postedPWD' => $data['password']));

                    if (!$isPWDMatched) {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1045;
                        $error = $this->getError($error);
                        $result['password_msg'] = Config::get('config_msg.CHANGE_PASS_FAIL');
                        $result['status'] = 0;
                        $result['errors'] = $error;
                        return Response::make($result, 200, $this->headers);
                    }

                    $newPWD = $encryptHelper->encryptPWD(array('PWD' => $data['newpassword']));

                    User::where('user_id', '=', $user_id)->update(array('password' => $newPWD));

                    $result['password_msg'] = Config::get('config_msg.CHANGE_PASS_SUCCESS');
                }

                if (!empty($userArr)) {
                    User::where('user_id', '=', $user_id)->update($userArr);
                }

//                if (!empty($data['email'])) {
//                    $stringHelper = $this->helper->load('string');
//                    $varification_key = $stringHelper->random_string(20);
//                    $oldemail = User::where('user_id', '=', $user_id)->pluck('email');
//                    User::where('user_id', '=', $user_id)->update(array('varification_key' => $varification_key));
//
//                    $encryptHelper = $this->helper->load('encrypt');
//                    $activationData = array('newemail' => $data['email'],
//                        'oldemail' => $oldemail,
//                        'varification_key' => $varification_key);
//
//                    $json_data = json_encode($activationData);
//                    $encrypted_data = base64_encode($encryptHelper->mcEncrypt($json_data, Config::get('constants.PRIVATE_KEY')));
//                    $response = urlencode($encrypted_data);
//                    $activationLink = URL::to('/') . DIRECTORY_SEPARATOR . 'account' . DIRECTORY_SEPARATOR . 'email' . DIRECTORY_SEPARATOR . 'verify' . DIRECTORY_SEPARATOR . $response;
//
//                    $result['email_msg'] = Config::get('config_msg.REMINDER_ACTIVATION_EMAIL');
//                    $result['response']['link'] = $activationLink;
//
//                    $emaildata['name'] = User::where('user_id', '=', $user_id)->pluck('name');
//                    $emaildata['link'] = $activationLink;
//                    $emailbody = View::make('user_reset_email')->with('data', $emaildata)->render();
//
//                    $mailArray = array();
//                    $mailArray['email'] = $data['email'];
//                    $mailArray['subject'] = Config::get('config_msg.USER_RESET_EMAIL_SUBJECT');
//                    $mailArray['body'] = $emailbody;
//
//                    $communicationHelper = $this->helper->load('communication');
//                    $result['email_response'] = $communicationHelper->emailCommunication($mailArray);
//                }
            } else {

                $errorMsgArray = $user_deatil->errors();
                if (is_array($errorMsgArray)) {
                    $msg = !empty($errorMsgArray[0]['messages'][0]) ? ($errorMsgArray[0]['messages'][0]) : Config::get('config_msg.FAILED_TO_SAVE_DATA');
                }

                if ($msg == 'The mobile has already been taken.') {
                    $msg = 'This mobile number is already taken.';
                }

                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $user_deatil->errors();
                return Response::make($result, 200, $this->headers);
            }
            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.PROFILE_UPDATE_SUCCESS');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 30 jan 2015
     * @description : This function is used to update user profile image.
     * @access public
     * @return  array 
     */
    public function profilePictureUpdate() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            if (Input::hasFile('photo') && Input::file('photo')->isValid()) {
                $avatar = Input::file('photo')->getClientOriginalName();
                $extension = Input::file('photo')->getClientOriginalExtension();
                $temp = explode(".", $avatar);
                $img_name = $temp[0] . '_' . $user_id . '.' . $extension;

                Input::file('photo')->move(base_path() . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR, $img_name);

                UserProfile::where('user_id', '=', $user_id)->update(array('photo' => $img_name));
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.IMAGE_UPDATE_SUCCESS');
                $result['photo'] = URL::to('/') . DIRECTORY_SEPARATOR . 'photo' . DIRECTORY_SEPARATOR . $img_name;
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1049;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.IMAGE_UPDATE_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 30 jan 2015
     * @description : This function is used to update doctor profile.
     * @access public
     * @return  array 
     */
    public function doctorProfileUpdate() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);

            UserProfile::where('user_id', '=', $user_id)->update(array('is_doctor' => 1));

            $doctor = array();
            $doctor['user_id'] = $user_id;
            $doc = Doctor::where('user_id', '=', $doctor['user_id'])->get();
            if (!empty($data['medical_registration_no'])) {
                $doctor['medical_registration_no'] = trim($data['medical_registration_no']);
            } else {
                $doctor['medical_registration_no'] = $doc[0]->medical_registration_no;
            }

            if (!empty($data['total_experience'])) {
                $doctor['total_experience'] = trim($data['total_experience']);
            } else {
                $doctor['total_experience'] = $doc[0]->total_experience;
            }

            if (isset($data['per_min_charges'])) {
                $doctor['per_min_charges'] = $data['per_min_charges'];
            } else {
                $doctor['per_min_charges'] = $doc[0]->per_min_charges;
            }

            if (!empty($data['other_info'])) {
                $doctor['other_info'] = trim($data['other_info']);
            } else {
                $doctor['other_info'] = $doc[0]->other_info;
            }
            if (!empty($data['area'])) {
                $doctor['area'] = trim($data['area']);
            } else {
                $doctor['area'] = $doc[0]->area;
            }
            if (!empty($data['info'])) {
                $doctor['info'] = trim($data['info']);
            }

            if (!empty($data['city'])) {
                $doctor['city_id'] = $data['city'];
            }

            $docPro = new Doctor();
            if (!$docPro->validate($doctor)) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $docPro->errors();
                return Response::make($result, 200, $this->headers);
            }

            $docExist = Doctor::where('user_id', '=', $user_id)->exists();

            if ($docExist) {
                Doctor::where('user_id', '=', $user_id)->update($doctor);
            } else {
                $doctorModel = new Doctor();
                $doctorModel->user_id = $doctor['user_id'];
                $doctorModel->medical_registration_no = $doctor['medical_registration_no'];
                $doctorModel->total_experience = $doctor['total_experience'];
                $doctorModel->per_min_charges = $doctor['per_min_charges'];
                $doctorModel->other_info = $doctor['other_info'];
                $doctorModel->area = $doctor['area'];
                $doctorModel->other_specializations = $doctor['other_specializations'];

                if (!empty($doctor['info'])) {
                    $doctorModel->info = $doctor['info'];
                }

                $doctorModel->save();
            }
            if (!empty($data['qualification'])) {
                if (json_decode($data['qualification'])) {
                    $requestData = json_decode($data['qualification'], TRUE);
                    DoctorQualification::where('user_id', '=', $user_id)->delete();

                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {
                        $qual = array();
                        $qual['user_id'] = $user_id;

                        if ($requestData[$i]['degreeid'] == null || $requestData[$i]['degreeid'] == 0) {
                            $degree = new Degree();
                            $degree->name = $requestData[$i]['degree'];
                            $degree->save();
                            $qual['degree'] = $degree->degree_id;
                        } else {
                            $qual['degree'] = $requestData[$i]['degreeid'];
                        }

                        $qual['university'] = 1;
                        $qual['college'] = $requestData[$i]['university'];
                        $qual['start_date'] = date("Y-m-d");
                        $qual['end_date'] = date("Y-m-d");
                        $qual['is_pursuing'] = $requestData[$i]['is_pursuing'];

                        $qualobj = new DoctorQualification();
                        if ($qualobj->validate($qual)) {
                            $doctorQualification = new DoctorQualification();
                            $doctorQualification->user_id = $qual['user_id'];
                            $doctorQualification->degree = $qual['degree'];
                            $doctorQualification->university = $qual['university'];
                            $doctorQualification->college = $qual['college'];
                            $doctorQualification->start_date = $qual['start_date'];
                            $doctorQualification->end_date = $qual['end_date'];
                            $doctorQualification->is_pursuing = $qual['is_pursuing'];
                            $doctorQualification->save();
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                            $result['status'] = 0;
                            $result['errors'] = $qualobj->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.QUALIFICATION_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }
            if (!empty($data['specialization'])) {
                if (json_decode($data['specialization'])) {
                    $requestData = json_decode($data['specialization'], TRUE);
                    DoctorSpecialization::where('user_id', '=', $user_id)->delete();
                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {
                        $docsp = array();
                        $docSpec = new DoctorSpecialization();
                        $docsp['user_id'] = $user_id;
                        if ($requestData[$i]['specializationid'] == null || $requestData[$i]['specializationid'] === 0) {
                            $spec = new Specialization();
                            $spec->name = $requestData[$i]['specialization'];
                            $spec->save();
                            $docsp['specialization_id'] = $spec->specialization_id;
                        } else {
                            $docsp['specialization_id'] = $requestData[$i]['specializationid'];
                        }
                        if ($count_reqdata == 1) {
                            $requestData[$i]['is_default'] = 1;
                        }
                        $docsp['is_default'] = $requestData[$i]['is_default'];
                        if ($docSpec->validate($docsp)) {
                            $doctorSpecialization = new DoctorSpecialization();
                            $doctorSpecialization->user_id = $docsp['user_id'];
                            $doctorSpecialization->specialization_id = $docsp['specialization_id'];
                            $doctorSpecialization->is_default = $docsp['is_default'];
                            $doctorSpecialization->save();
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                            $result['status'] = 0;
                            $result['errors'] = $docSpec->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.SPECIALIZATION_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }
            if (!empty($data['experience'])) {
                if (json_decode($data['experience'])) {
                    $requestData = json_decode($data['experience'], TRUE);
                    DoctorWorkplace::where('user_id', '=', $user_id)->delete();
                    $count_reqdata = count($requestData);
                    for ($i = 0; $count_reqdata > $i; $i++) {
                        $docExArr = array();
                        $docExp = new DoctorWorkplace();
                        $docExArr['user_id'] = $user_id;
                        $docExArr['hospital_id'] = 1;
                        $docExArr['designation_id'] = 1;
                        $docExArr['hospital_name'] = $requestData[$i]['hospital'];
                        $docExArr['designation_name'] = $requestData[$i]['designation'];
                        $docExArr['join_date'] = date("Y-m-d");
                        $docExArr['end_date'] = date("Y-m-d");
                        $docExArr['is_current'] = $requestData[$i]['is_current'];
                        if ($docExp->validate($docExArr)) {
                            $doctorExperience = new DoctorWorkplace();
                            $doctorExperience->user_id = $docExArr['user_id'];
                            $doctorExperience->hospital_id = $docExArr['hospital_id'];
                            $doctorExperience->designation_id = $docExArr['designation_id'];
                            $doctorExperience->hospital_name = $docExArr['hospital_name'];
                            $doctorExperience->designation_name = $docExArr['designation_name'];
                            $doctorExperience->address = $docExArr['address'];
                            $doctorExperience->join_date = $docExArr['join_date'];
                            $doctorExperience->end_date = $docExArr['end_date'];
                            $doctorExperience->is_current = $docExArr['is_current'];
                            $doctorExperience->save();
                        } else {
                            $this->setHeader('RESPONSE_CODE', '0');
                            $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                            $result['status'] = 0;
                            $result['errors'] = $docExp->errors();
                            return Response::make($result, 200, $this->headers);
                        }
                    }
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1052;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.EXPERIENCE_INFO_FAIL');
                    $result['errors'] = $error;
                    return Response::make($result, 200, $this->headers);
                }
            }

            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.DR_PROFILE_UPDATE_SUCCESS');
            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

//    /**
//     * @author : Shipra Agrawal
//     * @created : 02 Feb 2015
//     * @description : This fuction used for save or update user setting in database.
//     * @access public
//     * @return  array 
//     */
//    public function userSetting() {
//        try {
//            $checkAuthToken = TRUE;
//            $result = $this->checkAuth($checkAuthToken);
//            if ($result['config_status'] == -1) {
//                return ($result);
//            } elseif ($result['config_status'] == 0) {
//                $this->setHeader('RESPONSE_CODE', 0);
//                return Response::make($result, 200, $this->headers);
//            }
//
//            $data = Input::all();
//            $this->setHeader('AUTH_TOKEN', $this->authToken);
//            if (json_decode($data['setting'])) {
//                $requestData = json_decode($data['setting']);
//                $user_id = $this->getUserIdbyToken($this->authToken);
//                UserSettingRelation::where('user_id', '=', $user_id)->delete();
//
//                if ($requestData->setting) {
//                    $count_setting = count($requestData->setting);
//                    for ($i = 0; $count_setting > $i; $i++) {
//                        $settingData = new UserSettingRelation();
//                        $data = array();
//                        $data['id'] = $requestData->setting[$i]->id;
//                        if ($settingData->validate($data)) {
//                            $settingData->user_id = $user_id;
//                            $settingData->setting = $requestData->setting[$i]->id;
//                            $settingData->save();
//                        } else {
//                            $this->setHeader('RESPONSE_CODE', '0');
//                            $result['msg'] = Config::get('config_msg.USER_SETTING_UPDATE_FAIL');
//                            $result['status'] = 0;
//                            $result['errors'] = $settingData->errors();
//                            return Response::make($result, 200, $this->headers);
//                        }
//                    }
//                }
//                $result['status'] = 1;
//                $result['msg'] = Config::get('config_msg.USER_SETTING_UPDATE_SUCCESS');
//                $this->setHeader('RESPONSE_CODE', '1');
//            } else {
//                $this->setHeader('RESPONSE_CODE', '0');
//                $error[] = 1052;
//                $error = $this->getError($error);
//                $result['status'] = 0;
//                $result['msg'] = Config::get('config_msg.USER_SETTING_UPDATE_FAIL');
//                $result['errors'] = $error;
//            }
//            return Response::make($result, 200, $this->headers);
//        } catch (Exception $ex) {
//            $error = array();
//            $error['error_code'] = $ex->getCode();
//            $error['error_type'] = $ex->getFile();
//            $error['error_message'] = $ex->getMessage();
//            $error['error_detail'] = $ex->getTraceAsString();
//            DBError::Insert($error);
//
//            $this->setHeader('RESPONSE_CODE', '0');
//            $error[] = 1011;
//            $error = $this->getError($error);
//            $result['status'] = 0;
//            $result['msg'] = "Error";
//            $result['errors'] = $error;
//            return Response::make($result, 200, $this->headers);
//        }
//    }
//
//    /**
//     * @author : Shipra Agrawal
//     * @created : 02 Feb 2015
//     * @description : This fuction used for update user setting in database.
//     * @access public
//     * @return  array 
//     */
//    public function updateUserSetting($status = 0) {
//        try {
//            $checkAuthToken = TRUE;
//            $result = $this->checkAuth($checkAuthToken);
//            if ($result['config_status'] == -1) {
//                return ($result);
//            } elseif ($result['config_status'] == 0) {
//                $this->setHeader('RESPONSE_CODE', 0);
//                return Response::make($result, 200, $this->headers);
//            }
//
//            $this->setHeader('AUTH_TOKEN', $this->authToken);
//            $user_id = $this->getUserIdbyToken($this->authToken);
//            if ($status == 0) {
//                UserSettingRelation::where('user_id', '=', $user_id)->delete();
//            } else {
//                $exist = UserSettingRelation::where('user_id', '=', $user_id)->exists();
//                if (!$exist) {
//                    $settingData = new UserSettingRelation();
//                    $settingData->user_id = $user_id;
//                    $settingData->setting = 1;
//                    $settingData->save();
//                }
//            }
//            $result['status'] = 1;
//            $result['msg'] = Config::get('config_msg.USER_SETTING_UPDATE_SUCCESS');
//            $this->setHeader('RESPONSE_CODE', '1');
//            return Response::make($result, 200, $this->headers);
//        } catch (Exception $ex) {
//            $error = array();
//            $error['error_code'] = $ex->getCode();
//            $error['error_type'] = $ex->getFile();
//            $error['error_message'] = $ex->getMessage();
//            $error['error_detail'] = $ex->getTraceAsString();
//            DBError::Insert($error);
//
//            $this->setHeader('RESPONSE_CODE', '0');
//            $error[] = 1011;
//            $error = $this->getError($error);
//            $result['status'] = 0;
//            $result['msg'] = "Error";
//            $result['errors'] = $error;
//            return Response::make($result, 200, $this->headers);
//        }
//    }
//
//    /**
//     * @author : Shipra Agrawal
//     * @created : 02 Feb 2015
//     * @description : Get list of user setting.
//     * @access public
//     * @return  array 
//     */
//    public function userSettingList() { 
//        try {
//            $checkAuthToken = TRUE;
//            $result = $this->checkAuth($checkAuthToken);
//            if ($result['config_status'] == -1) {
//                return ($result);
//            } elseif ($result['config_status'] == 0) {
//                $this->setHeader('RESPONSE_CODE', 0);
//                return Response::make($result, 200, $this->headers);
//            }
//
//            $user_id = $this->getUserIdbyToken($this->authToken);
//            $user = new User();
//            $settinglist = $user->userSettingList($user_id);
//            $this->setHeader('RESPONSE_CODE', '1');
//            $this->setHeader('AUTH_TOKEN', $this->authToken);
//            $result['status'] = 1;
//            $result['msg'] = Config::get('config_msg.USER_SETTING_LIST');
//            $result['response']['setting_list'] = $settinglist;
//            return Response::make($result, 200, $this->headers);
//        } catch (Exception $ex) {
//            $error = array();
//            $error['error_code'] = $ex->getCode();
//            $error['error_type'] = $ex->getFile();
//            $error['error_message'] = $ex->getMessage();
//            $error['error_detail'] = $ex->getTraceAsString();
//            DBError::Insert($error);
//
//            $this->setHeader('RESPONSE_CODE', '0');
//            $error[] = 1011;
//            $error = $this->getError($error);
//            $result['status'] = 0;
//            $result['msg'] = "Error";
//            $result['errors'] = $error;
//            return Response::make($result, 200, $this->headers);
//        }
//    }


    public function userfullprofile($userid = 0) {

        try {

            $requestHeaders = apache_request_headers();

            $checkAuthToken = isset($requestHeaders['AUTH_TOKEN']) ? TRUE : FALSE;

            $result = $this->checkAuth($checkAuthToken);

            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = ($userid == 0) ? $this->getUserIdbyToken($this->authToken) : $userid;

            //CHECK FOR INTERNATIONAL CALL
            $loggedInUserId = $this->getUserIdbyToken($this->authToken);
            $data = Input::all();
            $locale = isset($data['locale']) ? $data['locale'] : NULL;
            $callTypeData = User::getCallTypeData(array('user_id' => $loggedInUserId, 'locale' => $locale));

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_exist = User::where('user_id', '=', $user_id)->exists();
            if ($user_exist) {
                $increase = 0;

                $query_condition = "select u.user_id,CONCAT(u.salutation, ' ', u.name) AS name,dp.address,up.is_doctor,dp.city_id,up.photo,dp.medical_registration_no,dp.info,dp.per_min_charges,dp.total_experience,dp.is_approved,dp.allow_new_user,ds.specialization_id,u.email,u.mobile,dp.info,dp.online_status ,dp.area,dp.other_specializations from users u LEFT JOIN user_profile up ON (u.user_id=up.user_id) LEFT JOIN doctor_profile dp ON (up.user_id=dp.user_id) LEFT JOIN doctor_specialization_rel ds ON(dp.user_id=ds.user_id) where u.user_id='$user_id'";

                $doclist = Array();
                $doctorquery = DB::select($query_condition);
                $doctorPatientMapping = new DoctorPatientMapping();
                foreach ($doctorquery as $doct) {
                    $increase++;
                    if ($increase == 1) {
                        $dbuserid = $doct->user_id;
                        $response1['user_id'] = $doct->user_id;
                        if ($userid == 0) {
                            $response1['email'] = $doct->email;
                            $response1['mobile'] = $doct->mobile;
                            $response1['info'] = $doct->info;
                        }

                        $requestHeaders = apache_request_headers();
                        $authToken = $requestHeaders ['AUTH_TOKEN'];
                        $response1 ['added_as_favourite'] = 2;
                        $response1['enableChat'] = 0;
                        if (!empty($authToken)) {
                            $patient = $this->getUserIdbyToken($authToken);
                            if (!empty($patient) && !empty($userid) && $userid != $patient) {
                                $addedAsFavourite = FavoriteDoctor::where('doctor', '=', $userid)->where('patient', '=', $patient)->exists();
                                $response1 ['added_as_favourite'] = $addedAsFavourite ? 1 : 0;
                            }

                            $response1['enableChat'] = $doctorPatientMapping->checkMapping(array('doctor_id' => $doct->user_id, 'patient_id' => $patient, 'type' => 'calling'));
                        }

                        $response1['name'] = $doct->name;
                        $response1['dob'] = '';
                        $response1['user_id'] = $doct->user_id;
                        $response1['name'] = $doct->name;
                        $response1['address'] = $doct->address ? $doct->address : '';
                        $response1['is_doctor'] = $doct->is_doctor;
                        $response1['city'] = $doct->city_id;
                        $response1['area'] = $doct->area ? $doct->area : '';
                        $response1['other_specializations'] = $doct->other_specializations;

                        $cityMaster = new City();
                        $response1['city_name'] = $cityMaster->getCityName($doct->city_id);

                        $response1['medical_registration_no'] = $doct->medical_registration_no;
                        $response1['info'] = $doct->info;
                        $response1['currency'] = $callTypeData['currency'];
                        $response1['currencySymbol'] = $callTypeData['currencySymbol'];
                        $response1['per_min_charges'] = $callTypeData['surcharge'] * $doct->per_min_charges;
                        $response1['per_min_charges'] = round($response1['per_min_charges'], 2);
                        $response1['fix_charges_per_call'] = $response1['per_min_charges'] * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                        $response1['total_experience'] = $doct->total_experience;
                        $response1['virtual_no'] = "";

                        $response1['photo_url'] = $response1 ['photo'] = User::getPhoto(array('photo' => $doct->photo, 'is_doctor' => $doct->is_doctor));

                        $response1['is_online'] = $doct->online_status;
                        $qulilist = array();
                        $univerlinequery = DB::select("select college,degree from doctor_qualifications where user_id='$dbuserid'");
                        foreach ($univerlinequery as $doctuni) {
                            $university = $doctuni->college;
                            $degreeid = $doctuni->degree;
                            if (!empty($degreeid)) {
                                $deglinequery = DB::select("select name from degree_master where degree_id='$degreeid'");
                                foreach ($deglinequery as $deg) {
                                    $degree = $deg->name;
                                }
                            }

                            $response['university'] = !empty($university) ? $university : "";
                            $response['degree'] = $degree;
                            $json_response = json_encode($response);
                            $quali_decode = json_decode($json_response);
                            array_push($qulilist, $quali_decode);
                        }

                        $expelist = array();
                        $expelinequery = DB::select("select hm.hospital_id, hm.name, hm.address, dw.designation_name from doctor_workplace AS dw LEFT JOIN hospital_master AS hm ON dw.hospital_id = hm.hospital_id where user_id='$dbuserid' AND dw.hospital_id IS NOT NULL");
                        foreach ($expelinequery as $doctex) {
                            $response2['hospital_id'] = $doctex->hospital_id;
                            $response2['hospital'] = $doctex->name;
                            $response2['designation'] = empty($doctex->designation_name) ? "" : $doctex->designation_name;
                            $response2['address'] = $doctex->address ? $doctex->address : '';
                            $json_response2 = json_encode($response2);
                            $quali_decode2 = json_decode($json_response2);
                            array_push($expelist, $quali_decode2);
                        }

                        $specidlist = array();
                        $specdetails = array();
                        $specilinequery = DB::select("select specialization_id from doctor_specialization_rel where user_id='$dbuserid'");
                        foreach ($specilinequery as $doctspec) {
                            $specializationid = $doctspec->specialization_id;
                            array_push($specidlist, $specializationid);
                        }

                        $speceid = implode(",", $specidlist);
                        if (!empty($speceid)) {
                            $specilinequery = DB::select("select specialization_id,name from specialization_master where specialization_id IN($speceid)");
                            foreach ($specilinequery as $doctspeci) {
                                $response_spec['specialization_id'] = $doctspeci->specialization_id;
                                $response_spec['name'] = $doctspeci->name;
                                $json_response_spec = json_encode($response_spec);
                                $specialization_decode = json_decode($json_response_spec);
                                array_push($specdetails, $specialization_decode);
                            }
                        }

                        $rel_exist = DoctorPatientRelation::whereRaw('(doctor = ' . $userid . ' and patient = ' . $user_id . ') or (patient = ' . $userid . ' and doctor = ' . $user_id . ')')->first();

                        if ($user_id != 0) {

                            $response1['is_allow_new_user'] = Doctor::where('user_id', '=', $user_id)->pluck('allow_new_user');

                            $user_id = isset($requestHeaders['AUTH_TOKEN']) ? $this->getUserIdbyToken($this->authToken) : $userid;

                            $rel_exist = DoctorPatientRelation::whereRaw('(doctor = ' . $userid . ' and patient = ' . $user_id . ') or (patient = ' . $userid . ' and doctor = ' . $user_id . ')')->first();

                            if ($rel_exist) {
                                $response1['is_subscribed'] = $rel_exist['is_subscribed'];
                                $response1['is_block'] = 0;
                                $response1['can_unblock'] = 0;

                                if (!empty($rel_exist['blocked_by'])) {
                                    $response1['is_block'] = 1;
                                    if ($user_id == $rel_exist['blocked_by']) {
                                        $response1['can_unblock'] = 1;
                                    }
                                }

                                $response1['is_friend'] = $rel_exist['is_approved'];
                            } else {
                                $response1['is_subscribed'] = 0;
                                $response1['is_block'] = 0;
                                $response1['can_unblock'] = 0;
                                $response1['is_friend'] = 0;
                            }
                        }

                        $response1['is_approved'] = $doct->is_approved;
                        $response1['specialization'] = $specdetails;
                        $response1['qualification'] = $qulilist;
                        $response1['experience'] = $expelist;

                        //SEND AVAILABLE MINUTES IF INTERNATIONAL PATIENT
                        if (!empty($callTypeData['internationalCall'])) {
                            $patDocMapping = PatDocMinInfo::where('patient_user_id', '=', $loggedInUserId)->where('doctor_user_id', '=', $doct->user_id)->first();
                            if (!empty($patDocMapping) && !empty($patDocMapping->min_info_id)) {
                                $response1['available_seconds'] = $patDocMapping->available_seconds;
                                $response1['available_minutes'] = (int) ($patDocMapping->available_seconds /60);
                            }
                        }

                        $json_response1 = json_encode($response1);
                        $array_decode = json_decode($json_response1);
                        array_push($doclist, $array_decode);
                    }
                }

                $result['login_paytm'] = $result['login_citrus'] = -1;
                $walletLogins = UserWallet::select('login', 'wallet_id')->where('user_id', '=', $user_id)->get();
                foreach ($walletLogins as $walletLogin) {
                    if ($walletLogin->wallet_id == 1) {
                        $result['login_paytm'] = $walletLogin->login;
                    } elseif ($walletLogin->wallet_id == 3) {
                        $result['login_citrus'] = $walletLogin->login;
                    }
                }

                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.USER_DETAIL');
                $result['response'] = $doclist;
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1046;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.USER_DETAIL_NOT_FOUND');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

//    /**
//     * @author : Shipra Agrawal
//     * @created : 02 Feb 2015
//     * @description : This function get detail by user id 
//     * @param : int userid
//     * @access public
//     * @return  array 
//     */
//    public function userDetail($userid = 0) {
//        try {
//            $checkAuthToken = TRUE;
//            $result = $this->checkAuth($checkAuthToken);
//            if ($result['config_status'] == -1) {
//                return ($result);
//            } elseif ($result['config_status'] == 0) {
//                $this->setHeader('RESPONSE_CODE', 0);
//                return Response::make($result, 200, $this->headers);
//            }
//
//            $user_id = ($userid === 0) ? $this->getUserIdbyToken($this->authToken) : $userid;
//
//            $this->setHeader('AUTH_TOKEN', $this->authToken);
//            $user_exist = User::where('user_id', '=', $user_id)->exists();
//            if ($user_exist) {
//                $url = Config::get('constants.SOLR_URL') . "select?q=user_id:" . $user_id . "&wt=json&indent=true";
//                $httpHelper = $this->helper->load('http');
//                $doc = $httpHelper->httpGet($url);
//                $doc = json_decode($doc, true);
//                if (!empty($doc['response']['docs'])) {
//                    $count = count($doc['response']['docs']);
//                    for ($i = 0; $i < $count; $i++) {
//
//                        $doc['response']['docs'][$i]['photo'] = User::getPhoto(array('photo' => $doc['response']['docs'][$i]['photo'], 'is_doctor' => $doc['response']['docs'][$i]['is_doctor']));
//                        if (($doc['response']['docs'][$i]['is_doctor'] == 1 && $doc['response']['docs'][$i]['is_approved'] == 1) || $userid == 0) {
//                            $doc['response']['docs'][$i]['qualification'] = json_decode($doc['response']['docs'][$i]['qualification']);
//                            $doc['response']['docs'][$i]['experience'] = json_decode($doc['response']['docs'][$i]['experience']);
//                            $doc['response']['docs'][$i]['specialization'] = json_decode($doc['response']['docs'][$i]['specialization']);
//                            $doc['response']['docs'][$i]['availabilty'] = json_decode($doc['response']['docs'][$i]['availabilty']);
//                        } else {
//                            unset($doc['response']['docs'][$i]['qualification']);
//                            unset($doc['response']['docs'][$i]['experience']);
//                            unset($doc['response']['docs'][$i]['specialization_id']);
//                            unset($doc['response']['docs'][$i]['specialization_name']);
//                            unset($doc['response']['docs'][$i]['specialization']);
//                            unset($doc['response']['docs'][$i]['availabilty']);
//                            unset($doc['response']['docs'][$i]['per_min_charges']);
//                            unset($doc['response']['docs'][$i]['total_experience']);
//                        }
//                        $doc['response']['docs'][$i]['is_online'] = Doctor::where('user_id', '=', $user_id)->pluck('online_status');
//                        unset($doc['response']['docs'][$i]['hospital_id']);
//                        unset($doc['response']['docs'][$i]['specialization']);
//                        unset($doc['response']['docs'][$i]['_version_']);
//                        unset($doc['response']['docs'][$i]['timestamp']);
//                        unset($doc['response']['docs'][$i]['personal']);
//
//                        if ($userid != 0) {
//                            unset($doc['response']['docs'][$i]['mobile']);
//                            unset($doc['response']['docs'][$i]['email']);
//                            $user_id = $this->getUserIdbyToken($this->authToken);
//                            $rel_exist = DoctorPatientRelation::whereRaw('(doctor = ' . $userid . ' and patient = ' . $user_id . ') or (patient = ' . $userid . ' and doctor = ' . $user_id . ')')->first();
//                            if ($rel_exist) {
//                                $doc['response']['docs'][$i]['is_subscribed'] = $rel_exist['is_subscribed'];
//                                $doc['response']['docs'][$i]['is_block'] = 0;
//                                $doc['response']['docs'][$i]['can_unblock'] = 0;
//
//                                if (!empty($rel_exist['blocked_by'])) {
//                                    $doc['response']['docs'][$i]['is_block'] = 1;
//                                    $doc['response']['docs'][$i]['is_friend'] = 0;
//                                    if ($user_id == $rel_exist['blocked_by']) {
//                                        $doc['response']['docs'][$i]['can_unblock'] = 1;
//                                    }
//                                } else {
//                                    if ($rel_exist['is_approved'] == 1) {
//                                        $doc['response']['docs'][$i]['is_friend'] = 1;
//                                    } else {
//                                        $doc['response']['docs'][$i]['is_friend'] = 2;
//                                    }
//                                }
//                            } else {
//                                $doc['response']['docs'][$i]['is_subscribed'] = 0;
//                                $doc['response']['docs'][$i]['is_block'] = 0;
//                                $doc['response']['docs'][$i]['can_unblock'] = 0;
//                                $doc['response']['docs'][$i]['is_friend'] = 0;
//                            }
//                            $doc['response']['docs'][$i]['is_allow_new_user'] = Doctor::where('user_id', '=', $user_id)->pluck('allow_new_user');
//                        }
//                    }
//                }
//                $this->setHeader('RESPONSE_CODE', '1');
//                $result['status'] = 1;
//                $result['msg'] = Config::get('config_msg.USER_DETAIL');
//                $result['response'] = $doc['response']['docs'][0];
//            } else {
//                $this->setHeader('RESPONSE_CODE', '0');
//                $error[] = 1046;
//                $error = $this->getError($error);
//                $result['status'] = 0;
//                $result['msg'] = Config::get('config_msg.USER_DETAIL_NOT_FOUND');
//                $result['errors'] = $error;
//            }
//            return Response::make($result, 200, $this->headers);
//        } catch (Exception $ex) {
//            $error = array();
//            $error['error_code'] = $ex->getCode();
//            $error['error_type'] = $ex->getFile();
//            $error['error_message'] = $ex->getMessage();
//            $error['error_detail'] = $ex->getTraceAsString();
//            DBError::Insert($error);
//
//            $this->setHeader('RESPONSE_CODE', '0');
//            $error[] = 1011;
//            $error = $this->getError($error);
//            $result['status'] = 0;
//            $result['msg'] = "Error";
//            $result['errors'] = $error;
//            return Response::make($result, 200, $this->headers);
//        }
//    }

    /**
     * @author : Shipra Agrawal 
     * @created : 21 jan 2015
     * @description : Get list of user favourit doctors.
     *
     * @param array;
     * @return array 
     */
    public function myDoctorList() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $doctorlist = array();
            $user = new User();

            $data = Input::all();
            $locale = isset($data['locale']) ? $data['locale'] : NULL;
            $callTypeData = User::getCallTypeData(array('user_id' => $user_id, 'locale' => $locale));

            $doctorArr = $user->favDroctorList($user_id);
            $msg = Config::get('config_msg.FAV_DR_LIST');
            if ($doctorArr) {
                $doctorPatientMapping = new DoctorPatientMapping();
                $arr_count = count($doctorArr);
                $doc = array();
                for ($i = 0; $i < $arr_count; $i++) {
                    $doc[$i] = $doctorArr[$i]->specialization_name;
                }
                $doc = array_values(array_unique($doc));
                for ($j = 0; $j < count($doc); $j++) {
                    $doctorlist[$j]['specilization'] = $doc[$j];
                    for ($i = 0; $i < $arr_count; $i++) {
                        if ($doc[$j] == $doctorArr[$i]->specialization_name) {
                            $doctorArr[$i]->qualification = University::droctorQualificationList($doctorArr[$i]->user_id);
                            $doctorArr[$i]->enableChat = $doctorPatientMapping->checkMapping(array('doctor_id' => $doctorArr[$i]->user_id, 'patient_id' => $user_id, 'type' => 'calling'));

                            $doctorArr[$i]->per_min_charges = $callTypeData['surcharge'] * $doctorArr[$i]->per_min_charges;
                            $doctorArr[$i]->per_min_charges = round($doctorArr[$i]->per_min_charges, 2);
                            $doctorArr[$i]->fix_charges_per_call = $doctorArr[$i]->per_min_charges * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                            $doctorArr[$i]->currency = $callTypeData['currency'];
                            $doctorArr[$i]->currencySymbol = $callTypeData['currencySymbol'];
                            $doctorArr[$i]->photo = User::getPhoto(array('photo' => $doctorArr[$i]->photo, 'is_doctor' => 1));

                            $doctorlist[$j]['doctors'][] = $doctorArr[$i];
                        }
                    }
                }
            } else {
                $msg = Config::get('config_msg.FAV_DR_LIST_NOT_FOUND');
            }
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = $msg;
            $result['response']['list'] = $doctorlist;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function favouriteDoctors() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);

            $docsList = array();
            $user = new User();
            $doctorArr = $user->favDroctorList($user_id);

            $data = Input::all();
            $locale = isset($data['locale']) ? $data['locale'] : NULL;
            $callTypeData = User::getCallTypeData(array('user_id' => $user_id, 'locale' => $locale));

            if (count($doctorArr)) {
                $msg = Config::get('config_msg.FAV_DR_LIST');
                $doctorPatientMapping = new DoctorPatientMapping();
                foreach ($doctorArr as $key => $value) {
                    $docsList[$key] = $value;

                    $doctor_id = $docsList[$key]->user_id;
                    $docsList[$key]->enableChat = $doctorPatientMapping->checkMapping(array('doctor_id' => $doctorArr[$key]->user_id, 'patient_id' => $user_id, 'type' => 'calling'));
                    $experience = DB::select("select address, hospital_name from doctor_workplace where user_id='$doctor_id'");

                    $docsList[$key]->photo = User::getPhoto(array('photo' => $doctorArr[$key]->photo, 'is_doctor' => 1));

                    if (!empty($doctor_id)) {

                        $docsList[$key]->is_allow_new_user = Doctor::where('user_id', '=', $doctor_id)->pluck('allow_new_user');

                        $relationExist = DoctorPatientRelation::whereRaw('(doctor = ' . $doctor_id . ' and patient = ' . $user_id . ') or (patient = ' . $doctor_id . ' and doctor = ' . $user_id . ')')->first();

                        if ($relationExist) {
                            $docsList[$key]->is_friend = $relationExist['is_approved'];
                        } else {
                            $docsList[$key]->is_friend = 0;
                        }
                    }

                    foreach ($experience as $doctexp) {
                        $docsList[$key]->address = empty($doctexp->address) ? '' : $doctexp->address;
                        $docsList[$key]->hospital_name = empty($doctexp->hospital_name) ? '' : $doctexp->hospital_name;
                        $docsList[$key]->per_min_charges = round($callTypeData['surcharge'] * $docsList[$key]->per_min_charges, 2);
                        $docsList[$key]->per_min_charges = strval($docsList[$key]->per_min_charges);
                        $docsList[$key]->fix_charges_per_call = $docsList[$key]->per_min_charges * Config::get('constants.FIX_CHARGES_PER_CALL_TOTAL_MINS');
                        $docsList[$key]->currency = $callTypeData['currency'];
                    }
                }
            } else {
                $msg = Config::get('config_msg.FAV_DR_LIST_NOT_FOUND');
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;
            $result['msg'] = $msg;
            $result['response'] = $docsList;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : By this fuction Patient or User subscribe the doctor.
     *
     * @access public
     * @return array 
     */
    public function subscribeToDoc() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $params = Input::all();
            $data['doctor'] = $params['doctor_id'];
            $data['patient'] = $user_id;
            $data['is_subscribed'] = 1;

            $docPatRel = new DoctorPatientRelation;
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            if (!$docPatRel->validate($data)) {
                $result['msg'] = Config::get('config_msg.SUBSCRIBTION_FAIL');
                $result['status'] = 0;
                $result['response'] = $docPatRel->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            } else {
                $user_exist = User::where('user_id', '=', $params['doctor_id'])->exists();
                if ($user_exist) {
                    $this->setHeader('RESPONSE_CODE', '1');
                    $result['status'] = 1;
                    $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $data['doctor'] . ' and patient = ' . $data['patient'] . '')->exists();
                    if ($doc_exist) {
                        DoctorPatientRelation::where('doctor', '=', $data['doctor'])->where('patient', '=', $data['patient'])->update(array('is_subscribed' => 1));
                    } else {
                        $docPatRel->doctor = $data['doctor'];
                        $docPatRel->patient = $data['patient'];
                        $docPatRel->is_subscribed = $data['is_subscribed'];
                        $docPatRel->save();
                    }
                    $result['msg'] = Config::get('config_msg.INFORM_DR_ONLINE');
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1047;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.SUBSCRIBTION_FAIL');
                    $result['errors'] = $error;
                }
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 02 Feb 2015
     * @description : This fuction used for user unsubscribe the doctor.
     * @access public
     * @return  array 
     */
    public function unsubscribeToDoc() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $params = Input::all();
            $data['doctor'] = $params['doctor_id'];
            $data['patient'] = $user_id;
            $data['is_subscribed'] = 0;

            $docPatRel = new DoctorPatientRelation;
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            if (!$docPatRel->validate($data)) {
                $result['msg'] = Config::get('config_msg.UNSUBSCRIBTION_FAIL');
                $result['status'] = 0;
                $result['response'] = $docPatRel->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            } else {
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $data['doctor'] . ' and patient = ' . $data['patient'] . '')->exists();
                if ($doc_exist) {
                    DoctorPatientRelation::where('doctor', '=', $data['doctor'])->where('patient', '=', $data['patient'])->update(array('is_subscribed' => 0));
                    $result['msg'] = Config::get('config_msg.UNSUBSCRIBTION_SUCCESS');
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1047;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.UNSUBSCRIBTION_FAIL');
                    $result['errors'] = $error;
                }
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : By this fuction Patient or Doctor block to user.
     *
     * @access public
     * @return array 
     */
    public function blockToUser() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);
            $params = Input::all();
            if (!empty($params['user_id']) && ctype_digit($params['user_id'])) {
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $params['user_id'] . ' and patient = ' . $user_id . '')->exists();
                if ($doc_exist) {
                    DoctorPatientRelation::where('doctor', '=', $params['user_id'])->where('patient', '=', $user_id)->update(array('blocked_by' => $user_id));
                    $result['msg'] = Config::get('config_msg.BLOCK_USER_SUCCESS');
                } else {
                    $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $user_id . ' and patient = ' . $params['user_id'] . '')->exists();
                    if ($doc_exist) {
                        DoctorPatientRelation::where('doctor', '=', $user_id)->where('patient', '=', $params['user_id'])->update(array('blocked_by' => $user_id));
                        $result['msg'] = Config::get('config_msg.BLOCK_USER_SUCCESS');
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1046;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.BLOCK_USER_FAIL');
                        $result['errors'] = $error;
                    }
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1046;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.BLOCK_USER_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : By this fuction Patient or Doctor unblock to user.
     *
     * @access public
     * @return array 
     */
    public function unblockToUser() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);
            $params = Input::all();
            if (ctype_digit($params['user_id'])) {
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $params['user_id'] . ' and patient = ' . $user_id . '')->exists();
                if ($doc_exist) {
                    DoctorPatientRelation::where('doctor', '=', $params['user_id'])->where('patient', '=', $user_id)->update(array('blocked_by' => null));
                    $result['msg'] = Config::get('config_msg.UNBLOCK_USER_SUCCESS');
                } else {
                    $doc_exist = DoctorPatientRelation::whereRaw('doctor = ' . $user_id . ' and patient = ' . $params['user_id'] . '')->exists();
                    if ($doc_exist) {
                        DoctorPatientRelation::where('doctor', '=', $user_id)->where('patient', '=', $params['user_id'])->update(array('blocked_by' => null));
                        $result['msg'] = Config::get('config_msg.UNBLOCK_USER_SUCCESS');
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $error[] = 1046;
                        $error = $this->getError($error);
                        $result['status'] = 0;
                        $result['msg'] = Config::get('config_msg.UNBLOCK_USER_FAIL');
                        $result['errors'] = $error;
                    }
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $error[] = 1046;
                $error = $this->getError($error);
                $result['status'] = 0;
                $result['msg'] = Config::get('config_msg.UNBLOCK_USER_FAIL');
                $result['errors'] = $error;
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : Get list of block users.
     *
     * @access public
     * @return array 
     */
    public function listBlockUser() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $user_id = $this->getUserIdbyToken($this->authToken);
            $user = new User();
            $blocklist = $user->userBlockList($user_id);

            foreach ($blocklist as $blockUser) {
                $blockUser->photo = User::getPhoto(array('photo' => $blockUser->photo, 'is_doctor' => 0));
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $result['status'] = 1;
            $result['msg'] = Config::get('config_msg.BLOCK_USER_LIST');
            $result['response']['block_list'] = $blocklist;
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 29 jan 2015
     * @description : doctor add in user favorite list.
     * @access public
     * @return  array 
     */
    public function addFavDoctor($userid) {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data['patient'] = $this->getUserIdbyToken($this->authToken);
            $data['doctor'] = $userid;
            $favDoctor = new FavoriteDoctor();
            if ($favDoctor->validate($data)) {
                $doc_exist = Doctor::whereRaw('user_id =' . $data['doctor'])->exists();
                if ($doc_exist && $data['patient'] != $data['doctor']) {
                    $favdoc = FavoriteDoctor::whereRaw('patient = ' . $data['patient'] . ' && doctor =' . $data['doctor'])->exists();
                    if (!$favdoc) {
                        $favDoctor->patient = $data['patient'];
                        $favDoctor->doctor = $data['doctor'];
                        $favDoctor->save();
                        $result['msg'] = Config::get('config_msg.DR_ADD_FAV_LIST');
                    } else {
                        $result['msg'] = Config::get('config_msg.DR_ALREADY_ADD_FAV_LIST');
                    }
                    $this->setHeader('RESPONSE_CODE', '1');
                    $result['status'] = 1;
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1047;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $favDoctor->errors();
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 29 jan 2015
     * @description : doctor record delete in user favorite list.
     * @access public
     * @return  array 
     */
    public function deleteFavDoctor($userid) {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data['patient'] = $this->getUserIdbyToken($this->authToken);
            $data['doctor'] = $userid;
            $favDoctor = new FavoriteDoctor();
            if ($favDoctor->validate($data)) {
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $favdoc = FavoriteDoctor::whereRaw('patient = ' . $data['patient'] . ' && doctor =' . $data['doctor'])->exists();
                if ($favdoc) {
                    FavoriteDoctor::whereRaw('patient = ' . $data['patient'] . ' && doctor =' . $data['doctor'])->delete();
                    $result['msg'] = Config::get('config_msg.DR_DELETE_FAV_LIST');
                } else {
                    $this->setHeader('RESPONSE_CODE', '0');
                    $error[] = 1046;
                    $error = $this->getError($error);
                    $result['status'] = 0;
                    $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                    $result['errors'] = $error;
                }
            } else {
                $this->setHeader('RESPONSE_CODE', '0');
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $favDoctor->errors();
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 24 mar 2015
     * @description : This method is used for approve doctor detail only for testing
     * @access public
     * @return  array  
     */
    public function userApprove($doc_id) {
        try {
            Doctor::where('user_id', '=', $doc_id)->update(array('is_approved' => 1));
            $user_app = DeviceInfo::where('user_id', '=', $doc_id)->first();
            $communicationHelper = $this->helper->load('communication');

            $result['status'] = 1;
            $result['msg'] = "User profile has been approved successfully";

            $GCMData = array();
            $GCMData['msg_text'] = "Doctor profile has been approved successfully";
            $GCMData['type'] = 'Doctor_Approved';

            $params['msg'] = json_encode($GCMData);
            if ($user_app['platform_id'] == 1) {
                $params['GCMDeviceIds'] = $user_app['registartion_id'];
                $params['msg'] = json_encode($GCMData);
                $result['response'] = $communicationHelper->gcmCommunication($params);
            } else if ($user_app['platform_id'] == 2) {
                $params['APNSDeviceIds'] = $user_app['registartion_id'];
                $params['push_msg'] = $GCMData;
                $result['response'] = $communicationHelper->pushApnsCommunication($params);
            }

            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 09 apr 2015
     * @description : This method used to update user status
     * @access public
     * @return  array 
     */
    public function editUserStatus() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);
            $user = new User();
            if ($user->validateStatus($data)) {

                Doctor::where('user_id', '=', $user_id)->update(array('online_status' => $data['availability_status']));
                $this->setHeader('RESPONSE_CODE', '1');
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.STATUS_CHANGED_SUCCESS');
            } else {
                $result['msg'] = Config::get('config_msg.STATUS_UPDATE_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    /**
     * @author : Shipra Agrawal
     * @created : 09 apr 2015
     * @description : This method used to update user status
     * @access public
     * @return  array 
     */
    public function CheckUserStatus($user_id = 0) {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            if ($user_id == 0) {
                $user_id = $this->getUserIdbyToken($this->authToken);
            }
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user = new User();
            if ($user->validatecheckStatus(array('user_id' => $user_id))) {
                $result['status'] = 1;
                $result['msg'] = Config::get('config_msg.USER_STATUS');
                $result['response']['is_online'] = Doctor::where('user_id', '=', $user_id)->pluck('online_status');
                $this->setHeader('RESPONSE_CODE', '1');
            } else {
                $result['msg'] = Config::get('config_msg.USER_STATUS_FAIL');
                $result['status'] = 0;
                $result['errors'] = $user->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function updateAppDetails() {

        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $user_id = $this->getUserIdbyToken($this->authToken);

            $userProfileData = array();
            $userProfileData['last_accessed'] = date("Y-m-d H:i:s");

            if (!empty($data['location'])) {
                $userProfileData['location'] = $data['location'];
            }

            UserProfile::where('user_id', '=', $user_id)->update($userProfileData);

            $updateData = array();

            if (!empty($data['current_os_version'])) {
                $updateData['current_os_version'] = $data['current_os_version'];
            }

            if (!empty($data['current_app_version'])) {
                $updateData['current_app_version'] = $data['current_app_version'];
            }

            if (!empty($updateData)) {
                DeviceInfo::where('user_id', '=', $user_id)->update($updateData);
            }

            $this->setHeader('RESPONSE_CODE', '1');
            $result['status'] = 1;

            $result['msg'] = 'User app details has been updated successfully!';
            $result['response']['status'] = 0;

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function getBalance() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $cond = Input::all();

            $this->setHeader('RESPONSE_CODE', '1');

            if (empty($cond['user_id'])) {
                $this->setHeader('RESPONSE_CODE', '0');
                $result ['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result ['status'] = 0;
                $result ['errors'] = 'User does not exist.';
            } else {
                $httpHelper = $this->helper->load('http');

                if ($cond['type'] == 'main') {
                    $user_token = UserWallet::where('user_id', '=', $cond['user_id'])->pluck('token');

                    if ($user_token) {
                        $user_token = Config::get('constants.CITRUS_AUTHTOKEN_PREFIX') . $user_token;
                        $url = Config::get('constants.CITRUS_URL') . Config::get('constants.CITRUS_BALANCE');
                        $citrusdata = array();
                        $citrusdata['access_key'] = Config::get('constants.CITRUS_ACCESS_KEY');
                        $citrusdata['Secret_key'] = Config::get('constants.CITRUS_SECRET_KEY');
                        $user_citrus = $httpHelper->httpGetWithHeader($url, array('Authorization:' . $user_token));
                        $user_citrus = json_decode($user_citrus, true);

                        $this->setHeader('RESPONSE_CODE', '1');
                        $result ['status'] = 1;
                        $result ['errors'] = "Main balance is: " . $user_citrus['value'];
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $result ['status'] = 0;
                        $result ['errors'] = 'Missing citrus auth token.';
                    }
                } else {
                    $mvcurl = Config::get('constants.CITRUS_VIRTUAL_URL') . Config::get('constants.Get_Balance');

                    $headerValue = Config::get('constants.Content_Type');
                    $citrusModel = new UserWallet();
                    $emailID = $citrusModel->getWalletInfoColumn(array('user_id' => $cond['user_id'], 'columnName' => 'email'));

                    if ($emailID) {
                        $MerchantID = Config::get('constants.Merchant_Id');
                        $PartnerID = Config::get('constants.Parterner_Id');
                        $Password = Config::get('constants.PassWord');
                        $data1 = '{"MerchantID": "' . $MerchantID . '", "CampaignCode": "KONSULTCASH", "Email": "' . $emailID . '", "Mobile": "' . $mobile . '", "PartnerID": "' . $PartnerID . '", "Password": "' . $Password . '" }';
                        $user_citrus_mvc = $httpHelper->httpPostWithHeader($mvcurl, array('Content-Type:' . $headerValue), $post = 1, $data1);
                        $user_citrus_mvc = json_decode($user_citrus_mvc, true);

                        $mvcbal = $user_citrus_mvc['d'];
                        $mvc_bal = $mvcbal['Campaigns'];

                        $this->setHeader('RESPONSE_CODE', '1');
                        $result ['status'] = 1;
                        $result ['errors'] = "Promo balance is: " . $mvc_bal[0]['Amount'];
                    } else {
                        $this->setHeader('RESPONSE_CODE', '0');
                        $result ['status'] = 0;
                        $result ['errors'] = 'Missing user email id.';
                    }
                }
            }
            return Response::make($result);
        } catch (Exception $ex) {
            $error = array();
            $error ['error_code'] = $ex->getCode();
            $error ['error_type'] = $ex->getFile();
            $error ['error_message'] = $ex->getMessage();
            $error ['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error [] = 1011;
            $error = $this->getError($error);
            $result ['status'] = 0;
            $result ['msg'] = "Error";
            $result ['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function blockUser() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $data = Input::all();
            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $data['blocker_id'] = $blocker_id = $this->getUserIdbyToken($this->authToken);
            $data['blocked_id'] = $blocked_id = $data['user_id'];
            $userBlock = new UserBlock();
            if ($userBlock->validate($data)) {
                $this->setHeader('RESPONSE_CODE', '1');
                $block = UserBlock::whereRaw('blocker_id = ' . $blocker_id . ' AND ' . 'blocked_id = ' . $blocked_id)->first();

                if (!empty($block)) {
                    $result['msg'] = "Already blocked by this user.";
                    $result['status'] = 0;
                    $this->setHeader('RESPONSE_CODE', 0);
                } else {
                    $userBlock->blocker_id = $blocker_id;
                    $userBlock->blocked_id = $blocked_id;
                    $userBlock->save();
                    $result['status'] = 1;
                    $result['msg'] = "User has been blocked successfully.";
                }
            } else {
                $result['msg'] = Config::get('config_msg.FAILED_TO_SAVE_DATA');
                $result['status'] = 0;
                $result['errors'] = $userBlock->errors();
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function blockUserList() {
        try {
            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);
            $blocker_id = $this->getUserIdbyToken($this->authToken);

            if ($blocker_id) {
                $this->setHeader('RESPONSE_CODE', '1');
                $blockUserList = DB::table('user_blocks')->where('blocker_id', '=', $blocker_id)->lists('blocked_id');

                if (!empty($blockUserList)) {
                    $result['msg'] = "Blocked user list.";
                    $result['status'] = 1;
                    $result['blockUserList'] = $blockUserList;
                } else {
                    $result['status'] = 1;
                    $result['msg'] = "No user in block list.";
                }
            } else {
                $result['status'] = 0;
                $result['errors'] = "Invalid User";
                $this->setHeader('RESPONSE_CODE', 0);
            }
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function userType() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $data = Input::all();
            $user_id = $data['user_id'];

            $result['userIsDoctor'] = UserProfile::whereRaw('user_id = ' . $user_id)->pluck('is_doctor');
            $result['status'] = 1;
            $this->setHeader('RESPONSE_CODE', '1');
            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function setPwd() {
        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);
            $newPassword = trim($data['password']);

            if (empty($user_id) || empty($newPassword) || strlen($newPassword) < 4) {
                $result['status'] = 0;
                $result['msg'] = 'Either user is invalid or password length is less than 4!';
                return $result;
            }

            $password = User::whereRaw('user_id = ' . $user_id)->pluck('password');
            if (!empty($password)) {
                $result['status'] = 0;
                $result['msg'] = 'Password is already set!';
            } else {

                $newEncryptedPwd = $this->helper->load('encrypt')->encryptPWD(array('PWD' => $newPassword));

                User::where('user_id', '=', $user_id)->update(array('password' => $newEncryptedPwd));

                $result['status'] = 1;
                $result['msg'] = 'Password have been saved!';
                $this->setHeader('RESPONSE_CODE', '1');
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

    public function setEmail() {

        try {

            $checkAuthToken = TRUE;
            $result = $this->checkAuth($checkAuthToken);
            if ($result['config_status'] == -1) {
                return ($result);
            } elseif ($result['config_status'] == 0) {
                $this->setHeader('RESPONSE_CODE', 0);
                return Response::make($result, 200, $this->headers);
            }

            $this->setHeader('AUTH_TOKEN', $this->authToken);

            $data = Input::all();
            $user_id = $this->getUserIdbyToken($this->authToken);

            $newEmail = trim($data["email"]);
            if (empty($user_id) || !filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
                $result['status'] = 0;
                $result['msg'] = 'Either user or email is invalid!';
                return $result;
            }

            $email = User::whereRaw('user_id = ' . $user_id)->pluck('email');
            if (!empty($email)) {
                $result['status'] = 0;
                $result['msg'] = 'Email is already mapped with this user!';
            } else {

                User::where('user_id', '=', $user_id)->update(array('email' => $newEmail));

                $result['status'] = 1;
                $result['msg'] = 'Email have been saved!';
                $this->setHeader('RESPONSE_CODE', '1');
            }

            return Response::make($result, 200, $this->headers);
        } catch (Exception $ex) {
            $error = array();
            $error['error_code'] = $ex->getCode();
            $error['error_type'] = $ex->getFile();
            $error['error_message'] = $ex->getMessage();
            $error['error_detail'] = $ex->getTraceAsString();
            DBError::Insert($error);

            $this->setHeader('RESPONSE_CODE', '0');
            $error[] = 1011;
            $error = $this->getError($error);
            $result['status'] = 0;
            $result['msg'] = "Error";
            $result['errors'] = $error;
            return Response::make($result, 200, $this->headers);
        }
    }

}
