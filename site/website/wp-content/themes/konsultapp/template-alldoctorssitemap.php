<?php
/**
 * Template Name: Doctors Alphabetical Sitemap 
 * Description: A Page Template To Display Sitemaps of doctors.
 * @author Sahil Ahlawat
 */
get_header(sitemap);
?>
<div id="Content">
	<div class="content_wrapper clearfix">

		<!-- .sections_group -->
		<div class="sections_group">
		
			<div class="section">
				<div class="section_wrapper clearfix">

<h2>List Of Doctors</h2>
                    <hr />
<?php
//error_reporting(E_ALL);

						global $wp;
						global $wpdb;
						global $my_wpdb ;
						$my_wpdb = new WPDB('root', '$#KonsultApp@2015#$', 'Konsult_Prod', 'localhost');
						//$my_wpdb = new WPDB('root', '', 'Konsult_Prod', 'localhost');
					//	global $symptomData ;
					//	$symptomData = $my_wpdb->get_results("select * FROM symptom_master;", 0, 0);
						global $doctorData ;
					//	global $specializationData ;
					//	$specializationData = $my_wpdb->get_results("select * FROM specialization_master;", 0, 0);
                    $alph = get_query_var( 'alphabet', 'a' ); 

    $doctorData = $my_wpdb->get_results("select users.name as username, specialization_master.name sname, doctor_profile.page_url as page_url FROM doctor_profile JOIN users on users.user_id = doctor_profile.user_id JOIN doctor_specialization_rel on doctor_specialization_rel.user_id = doctor_profile.user_id JOIN specialization_master on specialization_master.specialization_id = doctor_specialization_rel.specialization_id WHERE doctor_profile.is_approved = 1 AND doctor_profile.page_url LIKE 'dr-".$alph."%';", 0, 0);
    

						


//echo '<xml>';
?>
                    <ul class="alphmenu">
                    <?php
foreach (range('A', 'Z') as $char) {
    echo "<li><a href='".get_permalink()."/".$char."' >".$char. "</a></li>";
}
?>
                        </ul>
                    <hr />
<ul class="doclist">
<?php
foreach ($doctorData as $dd) {
   //var_dump($dd);
    ?>
  <li>
 
                              <a href="<?php echo bloginfo('url'); ?>/doctor-profile/<?php echo $dd->page_url; ?>" >
      <?php //echo str_replace('-',' ', $dd->page_url); ?>
      <?php echo "Dr. ".$dd->username; ?>
                                  
                                  
      </a>
      <span class="spclz"><?php echo $dd->sname; ?></span>
								 
   </li>
   <?php
                                                      }

													
													  
?>
    
</ul>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    h2{
        text-align: center;
    }
    .alphmenu li a{
            width: 20px;
    padding: 8px;
    /* border: 1px solid; */
    text-transform: capitalize;
    }
    .alphmenu li{
        display:inline-block;
    }
    .alphmenu{
        text-align: center;
        margin-bottom: 10px;
    }
    .doclist li a{
        text-transform: capitalize;
        font-size: 12px;
    line-height: 1.6;
    white-space: nowrap;
    text-decoration: none;
    height: 24px;
    text-transform: capitalize;
            margin-left: 40%;
    }
    .doclist li{
        width:33%;
        float:left;
        text-align: left;
    }
    .doclist{
            width: 80%;
    text-align: center;
    margin: 0 auto;
    }
    .spclz{
        font-size:12px;
        display:block;
        margin-top: -10px;
            margin-left: 40%;
    }
</style>
    <?php


get_footer();
?>