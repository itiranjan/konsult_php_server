<?php

class DoctorPatientRelation extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'doctor_patient_rel';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'doctor_patient_id';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = true;
    
    private $errors;

    public function setUpdatedAtAttribute() {
        // to Disable updated_at
    }

    /**
     * To return validation errors
     */
    public function errors() {
        $response = NULL;
        if (is_array($this->errors)) {
            $response = array();
            foreach ($this->errors as $k => $e) {
                $response[] = array(
                    'field' => $k,
                    'messages' => $e
                );
            }
        }
        return $response;
    }

    /**
     * validation rules defined
     * @var array
     */
    private $rules = array(
        'doctor' => 'required|integer',
        'patient' => 'required|numeric',
        'is_subscribed' => 'required'
    );

    /**
     * To validate data for model
     * @param array: data
     * @return boolean
     */
    public function validate($data) {

        $v = Validator :: make($data, $this->rules);

        if ($v->fails()) {
            $this->errors = $v->messages()->getMessages();
            return false;
        }

        return true;
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 3 Feb 2015
     * @description : By this fuction Patient or User subscribe the doctor.
     *
     * @access public
     * @return array 
     */
    public function subscribeToDoc() {
        return $this->belongsTo('Doctor', 'doctor', 'doctor_id');
    }

    /**
     * @author : Shipra Agrawal 
     * @created : 4 Feb 2015
     * @description : Get subscriber list for send Push msg .
     * @param : int [user id]
     * @param : string [$api_key]
     * @access public
     * @return array 
     */
    public function subscribeUserList($userid, $api_key) {
        $userList = DB::table('users AS u')->where('ufd.doctor', '=', $userid)->where('ufd.is_subscribed', '=', 1)->whereNull('ufd.blocked_by')
                ->where('pl.api_key', '=', $api_key)
                ->join('user_profile AS up', 'u.user_id', '=', 'up.user_id')
                ->join('doctor_patient_rel AS ufd', 'ufd.patient', '=', 'u.user_id')
                ->join('device_info AS app', 'app.user_id', '=', 'u.user_id')
                ->join('api_platforms AS pl', 'pl.id', '=', 'app.platform_id')
                ->select('u.user_id', 'u.name', 'u.email', 'app.registartion_id', 'app.device_id', 'app.platform_id')
                ->get();

        return $userList;
    }

    public function pendingApproval($userid) {

        $pendingApprovalList = DB::table('users AS u')->where('ufd.doctor', '=', $userid)->where('ufd.is_approved', '=', 2)
                ->join('user_profile AS up', 'u.user_id', '=', 'up.user_id')
                ->join('doctor_patient_rel AS ufd', 'ufd.patient', '=', 'u.user_id')
                ->select('ufd.patient AS patient_user_id', 'u.name', 'photo', 'ufd.created_at AS requested_on')
                ->get();

        return $pendingApprovalList;
    }

}
