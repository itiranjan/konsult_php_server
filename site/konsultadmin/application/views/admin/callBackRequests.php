<div id="window3">
    <form id="DetailP">

        <table width="100%">

            <tr>
                <td><div style="width:500px" id="callInformations"/>&nbsp;&nbsp;
                </td>
            </tr>

        </table>
    </form>
</div>

<script>
    var base_url = "<?php echo URL; ?>";
    var mainGrid;
</script>

<div style="width:100%">
    <div style="width:100%;display:block;">

    <div id="clientsDb">
        <span style="font-size:20px">Call Back Requests<br/><br/></span>
        <div id="grid" ></div>
    </div>    

    <script>

        var window3 = $("#window3");
        $(document).ready(function() {

            var onClose = function()
            {
                mainGrid.data("kendoGrid").refresh();
            }

            window3.kendoWindow({
                width: "700px",
                visible: false,
                title: "Call Back Requests"
            });

            var selectedRows = [];
            mainGrid = $("#grid").kendoGrid({
                edit: function(e) {
//                    var input = e.container.find(".k-input");
//                    input.blur(function(){
//                        column_value = input.val();
//                        alert(e.model.id);
//                        updateCallBackDetails(e.model.id, 'followup_comment', column_value);
//                    });  

                    var inputArea = e.container.find("textarea");
                    inputArea.blur(function(){
                        column_value = inputArea.val();
                        updateCallBackDetails(e.model.callback_request_id, 'followup_comment', column_value);
                    });
                },                  
                toolbar:["excel"],
                selectable: "multiple cell",
                allowCopy: true,                    
                excel: {
                    allPages: true,
                    fileName: "callBackRequests_<?php echo date('d-m-Y_h:ia'); ?>.xlsx",
                    filterable: true
                },
                dataSource: {                    
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    transport: {
                        read: {
                            type: "POST",
                            url: base_url + "/index.php/communication/callBackRequestList",
                            dataType: "json" // "jsonp" is required for cross-domain requests; use "json" for same-domain requests
                        },
                        parameterMap: function (options) {
                            if (options.filter) {
                                KendoGrid_FixFilter(mainGrid.dataSource.options, options.filter);
                            }
                            return options;
                        },
                    },                     
                    schema: {
                        data: "list",
                        total: "total",
                        model: {
                            fields: {
                                callback_request_id: {type: "number", editable: false},
                                dr$name: {type: "string", editable: false},
                                pt$name: {type: "string", editable: false},
                                dr$email: {type: "string", editable: false},
                                pt$email: {type: "string", editable: false}, 
                                request_call_id: {type: "number", editable: false},
                                callback_call_id: {type: "number", editable: false},
                                callback_requests$created_at: {type: "date", editable: false},
                                pgm$name: {type: "string", editable: false},
                                callback_requests$status: {type: "string", editable: false},
                                followed_by: {type: "string", editable: false},
                                followup_comment: {type: "string" <?php echo (!empty($this->session->userdata('permissions')['communication_updateCallBackDetails_edit']) ? ",editable: true" : ",editable: false"); ?>},
                                //followup_comment: {type: "string"},
                            }
                        }
                    },
                    pageSize: 40
                },
                filterable: {
                    extra: false,
                    operators: {
                        string: {
                            contains: "Contains",
                            startswith: "Starts with",
                            //eq: "Is equal to",
                            //neq: "Is not equal to"
                        },
                        number: {
                            eq: "Equal To",
                            gt: "Greater Than",
                            //gte: "Greater than or equal",
                            lt: "Less Than",
                            //lte: "Less than or equal",                                
                        }
                    }
                },                     
                sortable: true,
                dataBound: function() {
                    this.expandRow(this.tbody.find("tr.k-master-row").first());
                },
                pageable: {
                    refresh: true,
                    pageSize: 40,
                    numeric: true,
                    buttonCount: 20,
                    info: true
                },
                change: function(e) {
                    var selected = this.select();
                    for (var i = 0; i < selected.length; i++) {
                        var dataItem = this.dataItem(selected[i]);
                        selectedRows.push(dataItem);
                    }
                },
                columns: [
                        {field: "callback_request_id", title: "CallBack Id", width: '5px'},
                        {field: "dr$name", title: "Doctor Name", width: '12px'},
                        {field: "dr$email", title: "Doctor Email", width: '12px'},
                        {field: "pt$name", title: "Patient Name", width: '12px'},
                        {field: "pt$email", title: "Patient Email", width: '12px'},
                        {field: "request_call_id", title: "Comm Id", width: '8px'},
                        {field: "callback_call_id", title: "CallBack Comm Id", width: '8px'},
                        
                        {field: "callback_requests$created_at", title: "Requested On", width: '10px',format: "{0:dd/MM/yyyy HH.mm.ss}", filterable: {ui: "datepicker", extra: true, "messages": { "info": "Select Date Range:" } , operators: {
                            date: {
                                eq: "Equal To",
                                gt: "Greater Than",
                                gte: "Greater than or equal",
                                lt: "Less Than",
                                lte: "Less than or equal",
                            }
                        }}},
                        {field: "pgm$name", title: "Wallet", width: '8px'},
                        
                        {field: "callback_requests$status", title: "Status", width: '7px', template: "<a id='toggle' onclick=<?php echo (!empty(1) ? 'toggleStatus(#= callback_request_id#);' : "javascript:void(0);"); ?> style='cursor:pointer'><img align='absmiddle' src='<?php echo IMG; ?>icons/#= callback_requests$status == 'open' ? 'open.png' : 'closed.png' #' title='Click to change status'></a>"},

                        {field: "followed_by", title: "Followed By", width: '8px', filterable: true},
                        {field: "followup_comment", title: "Follow-up", width: '15px', filterable: true, defaultValue: {}, editor: textEditorInitialize}
                ],
                editable: true,
            }).data("kendoGrid");              

            mainGrid.thead.kendoTooltip({
                filter: "th",
                content: function (e) {
                    var target = e.target;
                    return $(target).text();
                }                    
            });
        });
        
            var textEditorInitialize = function(container, options) {
                $('<textarea name="' + options.field + '" style="width: ' + container.width() + 'px;height:' + container.height() + 'px" />')
                .appendTo(container);
            };            
            
            function updateCallBackDetails(callback_request_id, column_name, column_value) {

                var getConfirmation = confirm("Are you sure that you want to update the column.");
                if(!getConfirmation) {
                    $('#grid').data('kendoGrid').dataSource.read();
                    $('#grid').data('kendoGrid').refresh();
                    return;
                }

                $.ajax({

                    'url' : base_url + "/index.php/communication/updateCallBackDetails",
                    'type' : 'POST',
                    'data' : {
                        'callback_request_id' : callback_request_id,
                        'column_name' : column_name,
                        'column_value' : column_value
                    },
                    'success' : function(data) {  
                        alert("Changes has been done succesfully!!");
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    },
                    'error' : function(request,error)
                    {
                        alert("Whoops!! Something might be wrong. Please try again.");
                        //alert("Request: "+JSON.stringify(request));
                        $('#grid').data('kendoGrid').dataSource.read();
                        $('#grid').data('kendoGrid').refresh();

                    }
                });                
            }                  

            function ajaxCall(url, data, customFunction) {
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        $('#loading').show();
                    },
                    complete: function () {
                        $('#loading').hide();
                    },
                    success: customFunction,
                    error: function () {
                        alert(error);
                    }
                });
            }       
            
//            function statusFilter(element) {
//
//                element.kendoDropDownList({
//                    //filter: "contains",
//                    dataTextField: "name",
//                    dataValueField: "id",
//                    dataSource: [
//                        { id: 1, name: "Open" },
//                        { id: 0, name: "Closed" }
//                    ],
//                    optionLabel: "--Select Status--"  
//                });
//            }  
            
            function toggleStatus(id) {
                
                if(!confirm("Are you sure that you want to change the request status?")) {
                    return;
                }                
                
                callFunction = function(d) {
                    $("#grid").data("kendoGrid").dataSource.read();
                };
                ajaxCall('toggleCallbackrequestStatus', 'id=' + id , callFunction);
            }            
    </script>
</div>
</div>