<?php

class Offer extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'offers';

    /**
     * To disbaled/enable need of updated_at and created_at columns on your table by default
     * @var string
     */
    public $timestamps = false;

    public function getOffers() {

        return $this->where('enabled', '=', 1)->get();
    }

}
