<?php

class AppLinkLog extends Eloquent {

    /**
     * The database table used by the model.
     * @var string
     */
    protected $table = 'applink_log';

    /**
     * The primary of table used by the model.
     * @var string
     */
    protected $primaryKey = 'applinklog_id';
}
