<style>

    #saveAuthPermissions{}
    #saveAuthPermissions .container{}
    #saveAuthPermissions .container .main{
        text-align:center;
    }
    #saveAuthPermissions .container .main h2{}
    #saveAuthPermissions .container .main form{}
    #saveAuthPermissions .container .main form label{
        vertical-align: top;
    }
    #saveAuthPermissions .container .main form input{
        margin-right:30px;
        cursor: pointer;
    }
    .formgroup{
        border: 1px solid grey;
        float: left;
        margin-bottom: 5px;
        margin-left: 80px;
        padding: 4px;
        text-align: left;
        width: 41%;
    }
    .submit:hover{
        box-shadow: 0px 0px 0px 0px;
    }
    .submit{
        background: rgba(0, 0, 0, 0) -moz-linear-gradient(center top , #25a6e1 0%, #188bc0 100%) repeat scroll 0 0;
    border: 1px solid #1a87b9;
    color: #fff;
        display: table-caption;
        height: 40px;
        margin: 25px auto !important;
        padding: 3px;
        width: 350px;
       /* box-shadow: 2px 2px 2px 2px;*/
        cursor: pointer;
     
    }
</style>
<?php if (!empty($userPermissions)) { ?>    
    <div class="container">
        <div class="main">
            <h2>Assign Permissions</h2>
            <form id="change_authorization" action="javascript:void(0);" method="post" onsubmit="changeAuthorization();">
                <?php foreach ($userPermissions as $admin_id => $userPermission) { ?>
                    <input name="admin_id" type="hidden" value="<?php echo $admin_id ?>">
                    <?php foreach ($userPermission['permissions'] as $permission_id => $value) { ?>
                        <div class="formgroup">  <input <?php if ($value['value']) {
                echo "checked=checked";
            } ?> type="checkbox" name="check_list[]" value="<?php echo $permission_id ?>"><label><?php echo $value['description']; ?></label></div>
        <?php } ?>
    <?php } ?>
                <input class="submit" type="submit" name="submit" Value="Save Changes"/>
            </form>
        </div>
    </div>

<?php } ?>
