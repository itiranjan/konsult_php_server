<!DOCTYPE html>
<html lang="en">
<head>
<title>Registration</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="http://konsultapp.com/css/style-konsult.css" rel="stylesheet" media="screen">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>
input {
	font-size: 18px;
	min-height: 0px;
	width: 100%;
	border-radius: 0px;
	line-height: 0px;
	padding: 7px 7px 7px;
	border: none;
	margin-bottom: 10px;
	background-color: #fff /**#e9f0f2**/;
	-webkit-transition: background-color 0.2s;
	transition: background-color 0.2s;
	border: 1px #CCC solid;
}
.heading_h{color:#666;font-size:24px;margin-top:0;padding-bottom:40px;padding-top:20px;color:#0ab2f1;float:left;}
.pic_s{border-radius:100px;float:right;}
.f_div{float:left;width:150px;}
.s_div{padding:5px 10px 5px 10px;border:1px #999 solid;float:left;margin-right:50px;}
.t_div{opacity:0.5;padding:5px 10px 5px 10px;border:1px #999 solid;float:left;}
.f_submit{width:150px;padding-top:20px;padding-bottom:20px;background:#0ab2f1;color:#FFF;}
</style>
</head>
<body>
<div id="menu-bar-fixed" style="opacity: 1;top: 0;">
<div class="container"> <a class="logo" href="index.html"><img src="http://konsultapp.com/img/logo-sm.png" alt=""></a>
<div style="float:right;">Sudhesh Parasar</div>
</div>
</div>
{{Form::open (array ('url' => 'doctorregistraion'))}}
<div class="container" style="margin-top: 120px;color:#000;">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="padding-right:5%;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td class="heading_h">Doctor Registration</h1></td>
<td style="float:right;"><span>Email</span><br/>
 {{Form::text ('emailid', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
</tr>
</table></td>
<td style="padding-left:5%;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td style="float:left;"><span>Mobile</span><br/>
{{Form::text ('mob', ��, array ('placeholder'=>'','maxlength'=>10))}}</td>
<td><img src="img/profile.jpg" height="100"  alt="" class="pic_s"/></td>
</tr>
</table></td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>City</span><br/>
{{Form::text ('city', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
<td style="padding-left:5%;"><span>Address</span><br/>
{{Form::text ('address', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Qualification</span><br/>
{{Form::text ('qualification', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
<td style="padding-left:5%;"><span>Experience(Years)*</span><br/>
{{Form::text ('experience', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Specialization*</span><br/>
{{Form::text ('specialization', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
<td style="padding-left:5%;"><span>Work Experience</span><br/>
{{Form::text ('wexperience', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;"><span>Medical Registration Number*</span><br/>
{{Form::text ('mregdNumber', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
<td style="padding-left:5%;"><span>State Medical Registration Number</span><br/>
{{Form::text ('stmregNumber', ��, array ('placeholder'=>'','maxlength'=>30))}}</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;padding-top:10px;"><span style="font-weight:bold;">Expected Availability Day*</span><br/>
<select style="margin-top:10px;padding:5px;">
<option>5</option>
<option>10</option>
<option>15</option>
<option>20</option>
<option>25</option>
<option>30</option>
<option>35</option>
<option>40</option>
<option>45</option>
<option>50</option>
</select></td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="padding-right:5%;padding-top:50px;"><span style="font-weight:bold;">Expected Availability Day*</span><br/>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Sunday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Monday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Tuesday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Wednesday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Thursday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Friday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td><div class="f_div">Saturday</div>
<div class="s_div">10:00 - 22:00</div>
<div class="t_div">N/A</div></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td colspan="3" style="padding-top:30px;padding-bottom:20px;">
<!-- <input type="submit" class="f_submit"> -->
{{Form::submit ('Submit')}}
</td>
</tr>
</table></td>
<td>&nbsp;</td>
</tr>
</table>
</div>
{{Form::close ()}}
<footer id="footer" style="position:static;">
<div class="container">
<div id="footer-content">
<h2><img src="http://konsultapp.com/img/logo.png" alt=""></h2>
<!-- <p id='social-links'>
<a href="#" class='icon ion-social-facebook'></a>
<a href="#" class='icon ion-social-twitter'></a>
<a href="#" class='icon ion-social-googleplus-outline'></a>
<a href="#" class='icon ion-social-instagram'></a>
</p> -->
<ul class="f_links">
		<li><a href="terms.html">Terms &amp; Conditions</a></li>
		<li><a href="policy.html">Privacy Policy</a></li>
		</ul>
		<p class="copyright">Copyright � 2015. KONSULT</p>
		</div>
		</div>
		</footer>
		</body>
		</html>