<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() {
    return View::make('hello');
});

Route::get('calldetails', 'CallDetailsController@totalcalldetails');

Route::post('/promo', 'CallDetailsController@promodetails');

Route::group(array('prefix' => 'account'), function() {
    Route::post('/login', 'AccountController@userLogin');
    Route::post('/logout', 'AccountController@userLogout');
    Route::get('/email/verify/{resetkey}', 'AccountController@emailConfirmed');
    Route::put('/email', 'AccountController@editEmail');
    Route::put('/mobile', 'AccountController@editMobile');
    Route::post('/changepass', 'AccountController@changePassword');
    Route::post('/sociallogin', 'AccountController@userSocialLogin');
    Route::post('forgot', 'AccountController@forgotPassword');
    Route::get('reset/{resetkey}', 'AccountController@resetPassword');
});

Route::group(array('prefix' => 'autocomplete'), function() {
    Route::get('/university', 'AutoCompleteController@universityList');
    Route::get('/designation', 'AutoCompleteController@designationList');
    Route::get('/degree', 'AutoCompleteController@degreeList');
    Route::get('/hospital', 'AutoCompleteController@hospitalList');
    Route::get('/specialization', 'AutoCompleteController@specializationList');
    Route::get('/city', 'AutoCompleteController@cityList');
});

//Route::group(array('prefix' => 'users'), function() {
//    Route::get('/', 'UserController@userDetail'); 
//});

Route::group(array('prefix' => 'users'), function() {
    Route::put('doctreg', 'DoctorController@doctorRegistration');
    Route::post('/', 'UserController@userRegistration');
    Route::post('profileimg', 'UserController@profilePictureUpdate');
    Route::get('favs', 'UserController@myDoctorList');
    Route::get('favs/{id}', 'UserController@addFavDoctor');
    Route::delete('favs/{id}', 'UserController@deleteFavDoctor');
});

Route::group(array('prefix' => 'users'), function() {
    Route::post('appregistration', 'UserController@userDeviceInfo');
    Route::get('userdetails/{id}', 'UserController@userfullprofile');
    Route::put('profile', 'UserController@profileUpdate');
});

Route::group(array('prefix' => 'doctors'), function() {
    Route::get('/doc', 'DoctorController@docListing');
    Route::get('/docsearch', 'DoctorController@docSearching');
});

Route::group(array('prefix' => 'doctors'), function() {
    Route::post('/settings', 'DoctorController@updateSettings');
    Route::get('/getSettings', 'DoctorController@getSettings');
});

//FINAL CHANGES ================================================================
Route::group(array('prefix' => 'list'), function() {
    Route::get('/university', 'AutoCompleteController@universityList');
    Route::get('/designation', 'AutoCompleteController@designationList');
    Route::get('/degree', 'AutoCompleteController@degreeList');
    Route::get('/hospital', 'AutoCompleteController@hospitalList');
    Route::get('/specialization', 'AutoCompleteController@specializationList');
    Route::get('/city', 'AutoCompleteController@cityList');
    Route::get('/doctor', 'DoctorController@docListing');
    Route::get('/offer', 'OfferController@offerListing');
});

Route::group(array('prefix' => 'user'), function() {
    Route::get('calldetails', 'CallDetailsController@totalcalldetails');
    Route::post('applypromo', 'CallDetailsController@promodetails');
    Route::post('/login', 'AccountController@userLogin');
    Route::post('/logout', 'AccountController@userLogout');
    Route::put('/email', 'AccountController@editEmail');
    Route::put('/mobile', 'AccountController@editMobile');
    Route::post('/sociallogin', 'AccountController@userSocialLogin');
    Route::post('/chpwd', 'AccountController@changePassword');
    Route::post('forgotpwd', 'AccountController@forgotPassword');
    Route::post('/register', 'UserController@userRegistration');
//    Route::get('/profile', 'UserController@userDetail'); 
    Route::post('profilepic', 'UserController@profilePictureUpdate');
    Route::put('docreg', 'DoctorController@doctorRegistration');
    //Route::get('favs', 'UserController@myDoctorList');
    Route::get('favs', 'UserController@favouriteDoctors');
    Route::put('favs/{id}', 'UserController@addFavDoctor');
    Route::delete('favs/{id}', 'UserController@deleteFavDoctor');
    Route::post('appreg', 'UserController@userDeviceInfo');
    Route::put('profile', 'UserController@profileUpdate');
    Route::get('call', 'CallController@callmvc');
    Route::post('citrusToken', 'CallController@saveCitrusAuthToken');
    Route::post('blockUser', 'UserController@blockUser');
    Route::get('blockList', 'UserController@blockUserList');
    Route::get('callingFlags', 'CallDetailsController@callingFlags');
    Route::post('setPwd', 'UserController@setPwd');
    Route::post('setEmail', 'UserController@setEmail');
});

Route::group(array('prefix' => 'doctor'), function() {
    Route::get('approve/{id}/{value}', 'DoctorController@doctorApprove');
    Route::post('/settings', 'DoctorController@updateSettings');
    Route::get('/settings', 'DoctorController@getSettings');
    Route::post('availibilityStatus', 'UserController@editUserStatus');
    Route::get('approvalStatus', 'DoctorController@doctorStatus');
    Route::get('pendingRequest', 'ChatController@pendingRequest');
    Route::post('operationHours', 'DoctorController@setOperationHours');
    Route::get('operationHours', 'DoctorController@getOperationHours');
});

Route::group(array('prefix' => 'call'), function() {
    Route::post('rating', 'CallController@rating');
    Route::get('status', 'CallController@callStatus');
});

Route::get('infoPopup', 'MiscellaneousController@infoPopup');

Route::post('citrusWithdrawalDetails', 'MiscellaneousController@citrusWithdrawalDetails');

Route::post('generateOTP', 'MiscellaneousController@generateOTP');
Route::get('verifyOTP', 'MiscellaneousController@verifyOTP');
Route::post('user/signup', 'MiscellaneousController@signup'); //NEW SIGNUP FLOW
Route::post('user/signin', 'MiscellaneousController@signin'); //NEW SIGNIN FLOW

Route::get('userprofile/{id}', 'UserController@userfullprofile');

Route::get('/docsearch', 'DoctorController@docSearching');

Route::get('/getAppLink', 'WebController@getAppLink');

Route::get('blogInfo', 'WebController@blogInfo');

Route::get('blogListing', 'WebController@blogListing');

Route::get('/verifyMobile', 'MiscellaneousController@verifyMobile');

Route::post('upgradeStatus', 'CallDetailsController@latestVersion');

Route::get('topSpecialization', 'MiscellaneousController@priorityspecialization');

Route::get('kcc/docConnect', 'KccController@docConnect');

Route::get('kcc/callComplete', 'KccController@callComplete');

Route::get('kcc/recharge', 'KccController@recharge');

Route::post('kcc/addRealCash', 'KccController@addRealCash');

Route::get('kcc/balance', 'KccController@balance');

Route::get('kcc/generateStatement', 'KccController@generateStatement');

Route::post('kcc/assignMinutes', 'KccController@assignMinutes');

Route::get('kcc/my-patients-available-minutes', 'KccController@myPatientsAvailableMinutes');

Route::get('kcc/availableMinutes', 'KccController@availableMinutes');

Route::get('doctor/callBack', 'CallDetailsController@callBack');

Route::post('callBackRequest', 'CallDetailsController@callBackRequest');

Route::get('user/callBackRequests', 'CallDetailsController@userCallBackRequests');

Route::get('doctor/callBackRequests', 'CallDetailsController@doctorCallBackRequests');

Route::group(array('prefix' => 'admin'), function() {
    Route::get('getBalance', 'AdminController@getBalance');
    Route::get('debitAmount', 'AdminController@debitAmount');
    Route::get('debitVirtualAmount', 'AdminController@debitVirtualAmount');
    Route::get('creditAmount', 'AdminController@creditAmount');
    Route::post('setDefaultPassword', 'AdminController@setDefaultPassword');
    Route::post('createKccUser', 'AdminController@createKccUser');
});

Route::group(array('prefix' => 'paytm'), function() {
    Route::post('/signinOtp', 'WalletController@signinOtp');
    Route::post('/signinValidateOtp', 'WalletController@signinValidateOtp');
    Route::get('/userDetails', 'WalletController@userDetails');
    Route::get('/walletWebCheckBalance', 'WalletController@walletWebCheckBalance');
    Route::post('/initiateAddMoney', 'WalletController@initiateAddMoney');
    Route::get('/oltpHandlerffWithdrawScw', 'WalletController@oltpHandlerffWithdrawScw');
    Route::get('/salesToUserCredit', 'WalletController@salesToUserCredit');
});

Route::post('/wallet/logout', 'WalletController@walletLogout');
Route::get('/wallet/loginStatus', 'WalletController@walletLoginStatus');

Route::get('userType', 'UserController@userType');

Route::get('userExist', 'AccountController@userExist');

Route::get('hospital/{id}', 'DoctorController@hospitalDetails');

//HIT BY KNOWLARITY AND TWILIO AFTER CALL COMPLETION
Route::get('callinfo', 'CallController@callinfoMvc');

Route::get('operatorCallInfo/{callId}', 'TwilioController@operatorCallInfo');

Route::post('call-connect/{callId}', 'TwilioController@callConnect');

Route::post('operator-option/{callId}', 'TwilioController@operatorOption');

Route::post('operator-connect/{callId}', 'TwilioController@operatorConnect');

Route::get('generate-access-token', 'TwilioController@generateAccessToken');

Route::get('capability-token', 'TwilioController@getCapabilityToken');

Route::get('updateDoctorsStatus', 'CronController@updateDoctorsStatus');

Route::get('medicalTourism', 'TwilioController@medicalTourism');

Route::post('connectISDCall', 'TwilioController@connectISDCall');
Route::post('call-isd-connect/{isdCallId}', 'TwilioController@callISDConnect');
Route::get('isdCallInfo', 'TwilioController@isdCallInfo');

Route::post('medicalTourismInitiateCall', 'TwilioController@medicalTourismInitiateCall');
Route::post('medicalTourismcallConnect/{isdCallId}', 'TwilioController@medicalTourismcallConnect');
Route::get('medicalTourismCallInfo', 'TwilioController@medicalTourismCallInfo');
//==============================================================================
//REMOVE IF ONLY FOR TESTING
Route::get('appcheck', 'AppController@getAppCheck');

//HAVE TO REMOVE AFTER CONFIRMING BY IOS AND ANDROID DEVELOPER IF USELESS
Route::get('user/favourites', 'UserController@favouriteDoctors');
Route::post('citrus/authtoken', 'CallController@saveCitrusAuthToken');
Route::post('OsVersion', 'CallDetailsController@latestVersion');
Route::get('specialization', 'MiscellaneousController@priorityspecialization');
Route::group(array('prefix' => 'doctors'), function() {
    Route::get('/status', 'DoctorController@doctorStatus');
});
Route::group(array('prefix' => 'query'), function() {
    Route::get('/', 'AutoCompleteController@getQuery');
    Route::get('version/{version}', 'AutoCompleteController@getQueryVersion');
});
Route::group(array('prefix' => 'users'), function() {
    Route::get('referrals', 'UserController@referralList');
    Route::get('referrals/coupons', 'UserController@couponList');
    Route::put('subscriptions', 'UserController@unsubscribeToDoc');
    Route::post('subscriptions', 'UserController@subscribeToDoc');
    Route::get('bank', 'UserController@bankDetail');
    Route::put('block', 'UserController@unblockToUser');
    Route::get('block', 'UserController@listBlockUser');
    Route::post('block', 'UserController@blockToUser');
    Route::get('/deluser', 'UserController@deleteUserfromDoctorList');
    Route::get('approve/{id}', 'UserController@userApprove');
    Route::put('doctor', 'UserController@doctorProfileUpdate');
    Route::post('status', 'UserController@editUserStatus');
    Route::get('status', 'UserController@CheckUserStatus');
    Route::get('status/{id}', 'UserController@CheckUserStatus');
//    Route::get('/{id}', 'UserController@userDetail');
    //Route::get('list/{id}', 'UserController@userDetail1');
//    Route::put('settings', 'UserController@userSetting');
//    Route::get('settings', 'UserController@userSettingList');
//    Route::get('setting/{status}', 'UserController@updateUserSetting');     
});

//NEED NOT TO CHANGE
Route::group(array('prefix' => 'friend'), function() {
    Route::post('/request', 'ChatController@friendRequest');
    Route::post('/request/response', 'ChatController@friendRequestResponse');
});

Route::get('chat', 'ChatController@chatlist');

Route::group(array('prefix' => 'user'), function() {
    Route::put('/updateAppDetails', 'UserController@updateAppDetails');
});

Route::group(array('prefix' => 'call'), function() {
    Route::get('/', 'CallController@callmvc');
    Route::get('/accept', 'CallController@callRequestList');
});

Route::group(array('prefix' => 'wallet'), function() {
    Route::get('/transactions', 'WalletController@transactionList');
    Route::get('/transactions/{page}', 'WalletController@transactionList');
    Route::get('/redirectUrlLoadCash', 'WalletController@redirectUrlLoadCash');
});

Route::group(array('prefix' => 'paypal'), function() {
    Route::get('/generateAccessToken', 'PaypalController@generateAccessToken');
    Route::post('/transaction', 'PaypalController@transaction');
});
